

define(['cookie', 'validatePlu', 'fblogin', 'payment', 'recurly'], function(){
//facebook ID
//for www.bigpicapp.co : 942305319152258
//for test.bigpicapp    : 947797725307604
//for internal-testing : 1552667065032372
//for localhost        : 147242882316703


var response_info = {
    'emailSucces'               : 'An email has been sent to:',
    'emailError'                : 'Invalid email or password. Please try again.',
    'emailExisting'             : 'Email is linked to a Premium account. Please log in.',
    'emailNoexisting'           : 'Invalid email or password. Please try again.',
    'forgotPwdProAndDaypass'    : 'This email is not associated with an active Premium subscription. ',
    'billingInfo'               : 'There was an error completing payment.',
    //'errorBasic'       : 'Email is linked to a Premium account. Please log in.',
    'systemError'               : 'An unexpected error occurred. Please try again later.', // for errorOther
    'lessOneContact'            : 'Please enter at least one valid email.',
    'sentSuccessfully'          : 'Thank you! Your invitation has been sent.',
    'resetPassword'             : 'Your password has been reset.' + '\n' + 'You can know use your new password to login.'
},
    ST = Date.now(),
    internetRequestInfo = 'An internet connection is required.',
    facebookID;

function switchFacebookID(){

    if(document.domain === 'localhost'){
        facebookID = '147242882316703';
    }
    if(document.domain === 'www.test.bigpicapp.co'){
        facebookID = '519462188252785';
    }
    if(document.domain === 'www.bigpicapp.co'){
        facebookID = '172952166477185';
    }


    //load facebook SDK first for facebook login:
    $('<div id="fb-root"></div>').appendTo('body').ready(function(e) {
            window.fbAsyncInit = function() {
                FB.init({appId: facebookID, status: true, cookie: true,
                xfbml: true});
                };
                (function() {
                var e = document.createElement('script'); e.async = true;
                e.src = document.location.protocol +
                '//connect.facebook.net/en_US/all.js';
                document.getElementById('fb-root').appendChild(e);
            }());
    });
}
switchFacebookID();

function switchRecurlyID(){
    if(document.domain == 'bigpicapp.co' || document.domain == 'www.bigpicapp.co'){
            // Configure recurly.js
            recurly.configure('ewr1-FK1UvBU8VkBr0bHJWt1UZm');
            console.log('live');
   }
   else{
    // Configure recurly.js
    recurly.configure('ewr1-XPXOgsDL8wqlqChzlJ3zPj');
    console.log('localhost');
  }
}

switchRecurlyID();



//validation mobile devices and hide contas sales form if it return ture.
if (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent)) {
       if($("body").attr('data-status') === 'ContactForm'){
            $("body").attr('data-status','');
       }
//       $('a,.subtitle_smScreen, .btn').css({'font-weight':'400'});
}


 //clear input values
 $.fn.clearFormvalues = function(obj){
      $('input',obj).not(':button, :reset, :hidden, :submit')
      .val('')
      .removeAttr('checked')
      .removeAttr('selected');
  };
 // validate email
 function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

//validat for expriation date

$.validator.addMethod('CCExp', function(value, element, params) {
      var inputDate	=	$('.expdate').val().split('/'),
            minMonth = null,
            maxMonth = 12,
          minYear = new Date().getFullYear(),
          maxYear = minYear + 20,
          month = parseInt(inputDate[0], 10),
          year = parseInt(inputDate[1], 10);

          if(year.toString().length === 2){
            year = '20'+ year;
           }

          if(year > minYear){
            minMonth = 1;
           }

          if(year <= minYear){
            minMonth = new Date().getMonth() + 1;
          }

          $('input[data-recurly="month"]').val(month);
          $('input[data-recurly="year"]').val(year);

      return (year >= minYear && year <= maxYear && month >= minMonth && month <= maxMonth );
}, 'Invalid exp. date');

 //strict validate email regex
 jQuery.validator.addMethod("email", function(value, element) {
  return this.optional( element ) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test( value );
}, 'Please enter a valid email address');

//validate zip
jQuery.validator.addMethod("zipcode", function(value, element) {
      if($.cookie('country') !== 'CA'){
        return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
      }
      else{
        //CA: X1X 1X1
        return this.optional(element) || value.match(/[a-zA-Z][0-9][a-zA-Z](-| |)[0-9][a-zA-Z][0-9]/);
      }
}, "Please provide a valid zipcode");


 //login error
 function $errorLogin(obj,text){
    var $email          = $('input[type="email"]',obj),
        $emailNextEle   = $email.siblings('.existing');
        if(!$('input[type="email"]',obj).length){
            $email      = $('input[type="password"]',obj);
         }
    console.log(!$emailNextEle.length);
    if(!$emailNextEle.length){ //just show one time
        $('<label class="error existing"></label>')
         .insertBefore($email)
         .text(text);

         //.delay(5000)
//         .fadeOut(300, function(){
//            $(this).remove('.existing');
//         });
    }
     //remove error info when typing again:
    $email.on('click',function(){
        if($email.length){
        $email.siblings('.existing').remove();
        }
    });
 }



 //switch step on sign for premium

    function switchStep(){
        var stepFirst  = $('#firstStep,.firstStepTitle'),
            stepSecond = $('#secondStep,.secondStepTitle');

                $('.signupSteps li:first').toggleClass('currentStep stepCompleted')
                    .next()
                    .addClass('currentStep');
                stepFirst.fadeOut(500)
                 .delay(500,function(){
                    stepSecond.fadeIn(500);
                 });

    }

    //error warning
    function errorWarning(obj,text){
        if (text){
            window.scrollTo(0,0);

            $(obj).html(text)
            .stop(true, true)
            .slideDown(500)
            .delay(15000)
            .slideUp(500);
        }
    }


(function loginForreferrals(){
    if($.cookie('fromReferrals') === '1'){
        /*
        $('.loginForReferralling')
            .stop()
            .slideDown(500);
        */
        $('.login, .referral-openBP, .referral-logout').on('click',function(){
            console.log(1);
            $.cookie("fromReferrals", null, { path: '/' });
        });

    }
    if($.cookie('plancode') !== 'big_picture_premium'){
        $('.referralForm').hide();
        $('.sigupPremiumNote').show();
        //$('.referral-login').show();
    }
    if($.cookie('plancode') === null || $.cookie('plancode') === undefined){
        $('.referral-login').show();
    }
    else{
        $('.referral-login').hide();
        $('.referral-logout').show();
        $('.referral-openBP').show();
    }


})();

function validateErrorLength(obj,error){
    if(Object.keys(error).length > 0){
        var wTop     = $(window).scrollTop(),
            browserW = $(window).width();
            console.log('window scroll:' + $(window).scrollTop());
            console.log(wTop);
            //home page login 1:
            if(obj.attr('data-status') === 'loginForFree_1' &&
                wTop > 490){
                $('body,html').animate({'scrollTop':'480'},500);
            }
            //home page login 2:
            if(obj.attr('data-status') === 'loginForFree_2'){
                if(browserW > 992 && wTop > 3900) {
                      $('body,html').animate({'scrollTop':'3880'},500);
                 }
                 //less than 992px devices:
                 if(browserW < 992 && wTop > 4060){
                      $('body,html').animate({'scrollTop':'4020'},500);
                 }
            }
            //login for premium and free page:
            if((obj.hasClass('sign-up-form') || obj.hasClass('log-in')) && wTop > 230){
                $('body,html').animate({'scrollTop':'200'},500);
            }
            // sig up for premium on first step:
            if(obj.attr('id') === 'firstStep' && wTop > 240){
                $('body,html').animate({'scrollTop':'200'},500);
            }
            // get error from recurly on premium page on second step:
            if(obj.attr('id') === 'secondStep' && wTop > 324){
                $('body,html').animate({'scrollTop':'280'},500);
            }
            //get error on referral:
            if(obj.hasClass('referralForm') && wTop > 1277){
                $('body,html').animate({'scrollTop':'1220'},500);
            }
            //forgot password:
            if(obj.hasClass('forgot-password') && wTop > 247){
                $('body,html').animate({'scrollTop':'200'},500);
            }
            //reset password
            if(obj.hasClass('reset-password') && wTop > 190){
                $('body,html').animate({'scrollTop':'160'},500);
            }
            //download for free
            if(obj.hasClass('downloadForFreeForm') && wTop > 1030){
                $('body,html').animate({'scrollTop':'990'},500);
            }

    }

}

function doNotShowAgain_FreeVersion(){
    $('.do-not-show-again-thank-you-free-version input').on({
        click: function(e){
            e.stopPropagation();
            if(!$(this).is(':checked')){
                $.cookie('doNotShowAgain-ThankYouFreeVersion', null);
            }
            else{
                $.cookie('doNotShowAgain-ThankYouFreeVersion', 'true');
            }
            console.log($.cookie('doNotShowAgain-ThankYouFreeVersion'));
    }
    })
}
doNotShowAgain_FreeVersion();
// Get the size of an object

//call ajax for free login:
    function callAjax(obj, data){
        var $this       = $(obj),
            sendData    = data,
            submitBtn   = $('button',obj);
            console.log(sendData);
            console.log($this);
            //submitBtn.prop('disabled',true);
            $.ajax({
                type:"POST",
                data:sendData,
                dataType:"json",
                url:"/bigpicture_recurly/create_trial.php",
                success: function(errorcode){
                    console.log(errorcode);
                    submitBtn.prop('disabled', false);
                    validateErrorLength($this,errorcode);
                    if(errorcode.errorOther){
                        $errorLogin($this,response_info.systemError);
                    }

                    else if(errorcode.expired){
                         
                         if(errorcode.plancode === 'big_picture_premium'){
                             window.open('/expiration.html', '_self', false);

                         }
                    }
                    else if(errorcode.errorBasic){
                        $errorLogin($this,response_info.emailExisting);
                    }
                    else if(errorcode.errorMessage){
                        console.log(errorcode.errorMessage);
                    }
                    else{
                         //$form[0].reset();
                        if (errorcode.trialExpired){
                            window.open('/trial-expired.html', '_self', false);
                        }else{
                            window.open('/bigpicapp/home', '_self', false);
                        }
                        
                     }

                },
                error:function(data){
                        console.log(data);
                        submitBtn.prop('disabled', false);
                }

        });
    }

//cal ajax for download event
    function callAjaxFordownLoad(obj, data){
        var $this   =   $(obj),
            submitBtn   = $('button', obj),
            sendData = data;
            console.log(submitBtn.length);
            $.ajax({
//					url: '/TOP-5-RETIREMENT-PLANNING-MYTHS.pdf',
//					success: function(data) {
//						var blob=new Blob([data]);
//						var link=document.createElement('a');
//						link.href=window.URL.createObjectURL(blob);
//						link.download="Dossier_"+new Date()+".pdf";
//						link.click();
//					}
				
				
                    type:'POST',
                    data:sendData,
                    dataType:"json",
                    url:"/bigpicture_recurly/e_book/collectEmail.php",
                    success: function(errorcode){
                        console.log(errorcode);
                        submitBtn.prop('disabled', false);
                        validateErrorLength($this,errorcode);
                        if(errorcode.errorOther){
                            $errorLogin($this,response_info.systemError);
                        }
                        else if(errorcode.errorMessage){
                            console.log(errorcode.errorMessage);
                        }
                        else{
                            console.log('download');
                            //alert('did it');
//                            window.open('/ebook/TOP-5-RETIREMENT-PLANNING-MYTHS.html','_blank');
							window.open('/TOP-5-RETIREMENT-PLANNING-MYTHS.pdf','_blank');
                        }

                    },
                    errror:function(data){
                        submitBtn.prop('disabled', false);
                        
                    },
                    async: false
                });


    }
// recurly error
function error (obj,err) {
    validateErrorLength(obj,err);

    var newErrorText=[];
    $.each(err.fields, function (i, field) {
      newErrorText.push($('[data-recurly=' + field + ']').attr('placeholder'));
    });
    errorWarning('#errors','The following fields appear to be invalid: ' + newErrorText.join(', '));
    // $('#errors').slideDown().text('The following fields appear to be invalid: ' + err.fields.join(', '));
    $('button',obj).prop('disabled', false);
    $.each(err.fields, function (i, field) {
      $('[data-recurly=' + field + ']').addClass('error');
    });
 }



 $('body').on({
     //Free Trial
        submit:function(event){
            event.preventDefault();
            var obj = this,
                $form    		=   $(this),
                ET              = 	new Date(),
                limitT = ET - ST,
                honeypotNumber  =   $('.honeypotNumber', this).val(),
                honeypotVal     =   $('.honeypotVal', this).val();
                $form.validate({
                      errorClass: 'errorBorder',
                   // errorElement:'lable',
                    rules: {
                        checkbox: "required",
                        email: {
                            required: true,
                            email: true
                        }

                    },
                    errorPlacement: function() {
                         return true;
                    },

                    messages: {
                        checkbox: "Please agree to the Terms of Use.",
                        email: "Please enter a valid email address"
                    }

              });

                if($form.valid()){
                    if(!navigator.onLine){
                        var internetObj = $('.internetRequest', this);
                        errorWarning(internetObj, internetRequestInfo);
                    }
                    else{
                        var data        = $(this).serialize(),
                        submitBtn   = $('button',this),
                        countryInput = $('.currentCountry',this);

                        submitBtn.prop('disabled', true);
                        // validate if it is a robot
                        if(honeypotNumber.length !== 0 || honeypotVal.length !== 0 || limitT <=400 || ST === undefined || ST === null){
                            console.log(limitT);
                            return false;
                        }
                        // send data
                        else{
                           if(!countryInput.val().length){
                               countryInput.val('US');
                               data   = $form.serialize();
                               callAjax(obj, data);
                               console.log(data);
                               window.location = "/pro.html";
                           }
                           //if get country val from cookie, send ajax:
                           else{
                                window.location = "/pro.html";
                           }
                        }
                    }



                }
            }



    },'.sign-up-form,.simple_sign_up')

    .on({
        click:function(e){
            e.preventDefault();//stop scroll top when clicking;
            if(!navigator.onLine){
                var internetObj = $(this).parent().siblings('.internetRequest');
                errorWarning(internetObj, internetRequestInfo);
            }
            else{
                var obj			= $(this).parent().parent().parent(),
                    $btn    	= obj.find('button'),
                countryInput 	= $(this).parent().parent().find('.currentCountry');
                $btn.prop('disabled', true);
                $.fblogin({

                    fbId: facebookID,
                    permissions: 'email',
                    fields: 'email,first_name,last_name',
                    success: function (data) {
                        $btn.prop('disabled', false);
                        console.log('Basic public user data returned by Facebook', data);
                        var dataFilter = {
                            'email'             : data.email,
                            'country'           : countryInput.val(), //it's val from ip address;
                            'referrer_email_id' : $('input[name="referrer_email_id"]',obj).val(),
                            isSocialLogin: true
                        };
                        if(!countryInput.val().length){
                            dataFilter.country =  countryInput.val('US');
                            callAjax(obj, dataFilter);

                       }
                       //if get country val from cookie send ajax:
                       else{
                            console.log(dataFilter);
                            callAjax(obj, dataFilter);
                       }

                    },
                    error: function (error) {
                        $btn.prop('disabled', false);
                        console.log('An error occurred about facebook login plugin.', error);
                    }
                });
            }

        }
    },'.loginBasicWithFacebook')


    .on({
        click:function(){
            var obj     = $(this).parent().parent().parent().parent(),
                $btn    = $(this).parent().parent().find('button'); //select two bottons;
            if(!navigator.onLine){
                var internetObj = obj.siblings('.internetRequest');
                errorWarning(internetObj, internetRequestInfo);
            }
            else{
                $btn.prop('disabled', true);
                $.fblogin({
                    fbId: facebookID,
                    permissions: 'email',
                    fields: 'email,first_name,last_name',
                    success: function (data) {
                        $btn.prop('disabled', false);
                        console.log('Basic public user data returned by Facebook', data);
                        $('.facebook_logined_alert').slideDown(200,function(){
                            $(this).on('click',function(){
                                $(this).slideUp(200);
                            });
                        });
                        $('.facebook_area').hide();
                        obj.find('input[data-roles="first_name"]').val(data.first_name);
                        obj.find('input[data-roles="last_name"]').val(data.last_name);
                        obj.find('input[data-recurly="email"],input.confirm_email').val(data.email);

                    },
                    error: function (error) {
                        $btn.prop('disabled', false);
                        console.log('An error occurred about facebook login plugin.', error);
                    }
                });
            }

        }
    },'.SignupWithFacebook')

    .on({
        click:function(){

            if($(this).prop('checked') === true){
                $.cookie('openFree', 'true');
            }
            else{
                $.cookie('openFree', 'false');
            }
        }
    },'#rememberMe')

    .on({
    click:function(){
        console.log('yes.........');
        var data = {
            email:$.cookie('email'),
            country:(function(){
                if($.cookie('country').length){
                    return $.cookie('country');
                }
                else{
                    return 'US';
                }

                })(),

            openFree:true
        };
        $.ajax({
            type:'POST',
            dataType:"json",
            data:data,
            url:"/bigpicture_recurly/create_trial.php",
            success: function(errorcode){
                console.log(errorcode);
                console.log(data);
                window.open('/bigpicapp/home', '_self', false);
            },
            error: function(errorcode){
                console.log('fail info about ajax', errorcode);
                console.log('data', data);
            }
        });
    }
},'.openBigPictureBasic')

    //validate login form
    .on({

        submit:function(event){
           event.preventDefault();
            var $form	    =	$(this),
                $submitBtn  =   $('button', this);

            $form.validate({
                errorClass:'errorBorder',
                rules:{
                    email:{
                        required:true,
                        email	:true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },
                errorPlacement: function() {

                        return true;

                    },
                messages: {
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 6 characters long"
                    },

                    email: "Please enter a valid email address",
                }
            });

            if($form.valid()){
 var internetObj = $('.internetRequest', this);
//                console.log(navigator.userAgent.toLowerCase());
//                
//                if(!navigator.onLine){
//                        var internetObj = $('.internetRequest', this);
//                        errorWarning(internetObj, internetRequestInfo);
//                }
//                else{
                    $submitBtn.prop('disabled', true);
                    $.ajax({
                    type: "POST",
                    dataType: "json",
                    data:$(this).serialize(),
                    url: "account_login.php",
                    success 	: function(errorcode) {
                            $submitBtn.prop('disabled', false);
                            validateErrorLength($form,errorcode);
                            console.log(errorcode);

                            if(errorcode.errorEmail){
                                $errorLogin('.log-in',response_info.emailNoexisting);
                            }
                            else if (errorcode.errorLogin) {

                                $errorLogin('.log-in',response_info.emailError);
                                console.log('errorLogin');

                            }

                            else if (errorcode.errorOther) {
                               
                                console.log(internetObj);
                                errorWarning(internetObj, internetRequestInfo);

                            }
                            else if(errorcode.errorMessage){
                                console.log('Recurly error:' + errorcode.errorMessage);
                           }

                           else if(errorcode.expired){
                                console.log('pro:'+ errorcode.plancode + $.cookie('openFree'));
                                if(errorcode.plancode === 'big_picture_premium'){
                                    if($.cookie('openFree') === 'true'){
                                        window.open('/bigpicapp/home', '_self', false);
                                     }
                                     else{
                                        window.open('/expiration.html', '_self', false);
                                     }

                                }

                                if(errorcode.plancode === 'big_picture_daypass'){
                                    console.log('daypass:'+ errorcode.plancode + $.cookie('openFree'));
                                    if($.cookie('openFree') === 'true'){
                                        window.open('/bigpicapp/home', '_self', false);
                                     }
                                     else{
                                        window.open('/expiration-daypass.html', '_self', false);
                                     }
                                }
                           }

                            else{

                                if($.cookie('fromReferrals') === '1'){
                                   window.open('/referral-rewards.php', '_self', false);
                                   $.cookie("fromReferrals", null, { path: '/' });
                                   console.log('cookie test:' + $.cookie("fromReferrals"));
                                }
                                else{
                                  console.log('home page');
                                  window.open('/bigpicapp/home', '_self', false);
                                }

                                //$form[0].reset();
                                console.log('seccuss log in');

                             }

                    },
                    error:function(data){
                        $submitBtn.prop('disabled', false);
                        console.log('fail info');
                        console.log(data);
                    }
                });
//                }


            }
        }

    },'.log-in')
    //login with facebook
    .on({
        click:function(){
            if(!navigator.onLine){
                var internetObj = $(this).siblings('.internetRequest');

                errorWarning(internetObj, internetRequestInfo);
            }
            else{
                var $btn       = $(this).parent().parent().find('button');
                $btn.prop('disabled', true);
                $.fblogin({
                fbId: facebookID,
                permissions: 'email',
                fields: 'email,first_name,last_name',
                success: function (data) {
                    console.log('Basic public user data returned by Facebook', data);
                    var dataFilter = {
                        email : data.email,
                        isSocialLogin: true
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        data:dataFilter,
                        url: "account_login.php",
                        success 	: function(errorcode) {
                            console.log(dataFilter);
                            $btn.prop('disabled', false);
                            if(errorcode.errorEmail){
                                $errorLogin('.log-in',response_info.emailNoexisting);
                            }
                            else if (errorcode.errorLogin) {
                                $errorLogin('.log-in',response_info.emailError);

                            }

                            else if (errorcode.errorOther) {
                                    $errorLogin('.log-in', response_info.systemError);
                                    console.log(errorcode.errorOther);

                            }
                            else if(errorcode.errorMessage){
                                console.log('Recurly error:' + errorcode.errorMessage);
                           }

                           else if(errorcode.expired){
                                if(errorcode.plancode === 'big_picture_premium'){
                                    if($.cookie('openFree') === 'true'){
                                        window.open('/bigpicapp/home', '_self', false);
                                     }
                                     else{
                                        window.open('/expiration.html', '_self', false);
                                     }

                                }

                                if(errorcode.plancode === 'big_picture_daypass'){
                                    if($.cookie('openFree') === 'true'){
                                        window.open('/bigpicapp/home', '_self', false);
                                     }
                                     else{
                                        window.open('/expiration-daypass.html', '_self', false);
                                     }
                                }
                           }

                            else{

                                if($.cookie('fromReferrals') === '1'){
                                   window.open('/referral-rewards.php', '_self', false);
                                   $.cookie("fromReferrals", null, { path: '/' });
                                }
                                else{
                                  window.open('/bigpicapp/home', '_self', false);
                                }

                             }
                        },
                        error:function(errorcode){
                            $btn.prop('disabled', false);
                            console.log('fail info about ajax:', errorcode);
                        }

                    });
                },
                error: function (error) {
                    $btn.prop('disabled', false);
                    console.log('An error occurred about facebook login plugin.', error);
                }
            });
            }
        }
     },'.loginWithFacebook')


    //forgot password

    .on({
        submit:function(event){
           event.preventDefault();
           console.log(1);
            var $form   =   $(this);
            $form.validate({
                errorClass:'errorBorder',
                rules:{
                    email:{
                        required:true,
                        email:true
                    }
                },
                messages: {
                    email: "Please enter a valid email address",
                },
                errorPlacement: function() {
                    return true;
                }

            });

            if($form.valid()){
               if(!navigator.onLine){
                    var internetObj = $('.internetRequest', this);
                    errorWarning(internetObj, internetRequestInfo);
                }
                else{
                    $('button',$form).prop('disabled', true);
                    var data = {
                        'email'         : $('input[type="email"]',this).val()
                    };

                    $.ajax({
                        type:'POST',
                        dataType:'json',
                        data:data,
                        url:'/bigpicture_recurly/settingsPageBackend/forgot_password.php',
                        success: function(errorcode){
                            $('button',$form).prop('disabled', false);
                            validateErrorLength($form,errorcode);
                            console.log(errorcode);
                            if(errorcode.errorEmail){
                                $('.forgot_password_error').text(response_info.forgotPwdProAndDaypass).slideDown(500);
                                //errorWarning('.forgot_password_error', response_info.emailNoexisting);

                            }
                            if(errorcode.errorOther){
                                errorWarning('.forgot_password_error', response_info.systemError);

                            }
                            if(errorcode.errorEmail === false && errorcode.errorOther === false){

                                $('.forgot_password_succes')
                                    .fadeIn(500)
                                    .find('p')
                                    .html(response_info.emailSucces + '<br>' + '<span class="thin">' + data.email + '</span>');

                               $('.forgot-password').hide();
                            }
                        },
                        error:function(){
                            $('button',$form).prop('disabled', false);
                            console.log('Send data fail');
                        }


                    });
                }


            }
        }
    },'.forgot-password')

    .on({
        submit:function(event){

            event.preventDefault();
            var $form = $(this);

            $form.validate({
                errorClass:'errorBorder',
                rules:{
                    new_password : {
                        required: true,
                        minlength:6
                    },
                    confirm_password:{
                        required: true,
                        equalTo: '#reset_password',
                        minlength:6
                    }
                },
                messages: {
                        password    :   {
                            required:   "Please provide a password.",
                            minlength:  "Your password must be at least 6 characters long."
                        },
                        confirm_email: "Sorry, it looks like those emails didn't match. Please try again."
                    },
                 errorPlacement: function(error, element) {
                        // var textinfo = error.text();
                         //console.log(textinfo);
                         //error.appendTo($('#errors'));
                         //$('#errors').html($(error).text());
                         return true;

                }

            });
            if($form.valid()){
                if(!navigator.onLine){
                    var internetObj = $('.internetRequest', $form);
                    errorWarning(internetObj, internetRequestInfo);
                }
                else{

                    $('button',$form).prop('disabled', true);
                    var data = {
                        'email_id' : $('#email_id').val(),
                        'new_password' : $('#reset_password').val()
                    };
                    console.log(data);
                    $.ajax({
                        type:'POST',
                        data:data,
                        dataType:"json",
                        url:"/bigpicture_recurly/settingsPageBackend/reset_password.php",
                        success: function(errorcode){
                            validateErrorLength($form,errorcode);
                            $('button',$form).prop('disabled', false);
                            console.log(errorcode);
                            if(errorcode.errorEmail){
                                errorWarning('.reset_password_error',response_info.emailNoexisting);

                            }
                            if(errorcode.errorOther || errorcode.errorMessage){
                                errorWarning('.reset_password_error',response_info.systemError);
                            }
                            else{
//                                errorWarning('.reset_password_succes',response_info.resetPassword);
                                console.log('did it');

                                $('.submit_contactForm_succes').fadeIn(500);
                                $('.reset-password').hide();
                            }
                        }
                    });
                }

            }
            else{
                var error = {};
                error.length = 1;
                validateErrorLength($form,error);
            }
        }
    },'.reset-password')




    .on({
        submit:function(event){
 
           
//            $('input[data-roles="first_name"]',$form).val(firstName);
//                        $('input[data-roles="first_name"]',$form).html(firstName);
//            
            //switchStep();
            event.preventDefault();
            var $form            =   $(this),
                $submitBtn  =   $(this).parent().parent().find('button'),
                data = {'email' : $('input[data-recurly="email"]',$form).val()},
				price = $('#teamPrice').text();
            
          

            
			//$('.price-summary .price, .planPrice').text(price * 12);
            $('.price-summary .price, .planPrice').text(price);
            $form.validate({

                    errorClass:'errorBorder',
                    rules:{
                        firstname   : {
                            required: true
                        },

                        lastname    :  {
                            required: true
                        },

                        email       :   {
                            required:   true,
                            email   :   true
                        },

                        confirm_email:   {
                            email   : true ,
                            equalTo: '#email'
                        },

                        password    :   {
                            required:   true,
                            minlength:  6
                        },
                        shipping_address : {
                            required :  function(){
                                if($('#shipMePoster').prop('checked') === true){
                                    return true;
                                }
                                else{
                                    return false;
                                }
                            }
                        },

                        shipping_city : {
                            required : function(){
                                if($('#shipMePoster').prop('checked') === true){
                                    return true;
                                }
                                else{
                                    return false;
                                }
                            }
                        },

                        shipping_state : {
                            required : function(){
                                if($('#shipMePoster').prop('checked') === true){
                                    return true;
                                }
                                else{
                                    return false;
                                }
                            }
                        },

                        shipping_zipCode : {
                            required : function(){
                                if($('#shipMePoster').prop('checked') === true){
                                    return true;
                                }
                                else{
                                    return false;
                                }
                            }
                        },
                        
                        country     :   {
                            required:true
                        },
                        creditcard  :   {
                            required:   true,
                            minlength:17
                            //creditcard: true
                        },
                        ccode   :   {
                            required:true,
                            minlength:3
                        },


                        expdate   :   {
                                CCExp:true,
                                minlength:5
                                    },

                        zip     :   {
                            //zipcode:true,
                            required:true
                        },


                        checkbox:   'required'


                    },

                    messages: {
                        firstname   :   "Please enter your firstname.",
                        lastname: "Please enter your lastname",
                        email       :   "Please enter a valid email address.",
                        password    :   {
                            required:   "Please provide a password.",
                            minlength:  "Your password must be at least 6 characters long."
                        },
                        confirm_email: "Sorry, it looks like those emails didn't match. Please try again.",
                        country     :   "Please select one version",
                        creditcard  :   "Please enter a valid credit card number",
                        ccode       :   "Invalid security code",
                        //zip         :   "Please enter a valid postal code",
                        checkbox    :   "Please agree to the terms of service"
                    },

                    errorPlacement: function(error, element) {
                        console.log(element);
                        window.scrollTo(0,500);
                        var emptyValLength = $("input[placeholder]", $form).filter(function() {
                            return !this.value;
                        }).length;
                      
                        if(emptyValLength <=1){ //this validation is for safari, 'required' attrubite is not working on safari;
                            if($(element).hasClass('confirm_email')){
                                error.appendTo($('.validateError'));
                            }
                        }
                        else{
                            return true;
                        }
                    }
                });
           

            if($form.valid() === true){

                if(!navigator.onLine){
                    var internetObj = $('.internetRequest', this);
                    errorWarning(internetObj, internetRequestInfo);
                }
                else{
                    // Reset the errors display
                    $('#errors').text('');
                    $('input').removeClass('error');

                    // Disable the submit button
                    $submitBtn.prop('disabled', true);

                    var form = this;

                    // Now we call recurly.token with the form. It goes to Recurly servers
                    // to tokenize the credit card information, then injects the token into the
                    // data-recurly="token" field above

                    recurly.token(form, function (err, token) {

                        var data   =   {
                                'email'             	:   $('input[data-recurly="email"]',$form).val().trim(),
                                'firstname'         	:   $('input[data-roles="first_name"]',$form).val().trim(),
                                'lastname'          	:   $('input[data-roles="last_name"]',$form).val().trim(),
                                'password'          	:   $('input[data-recurly="password"]',$form).val(),
                                'country'           	:   $('.version_country input:checked',$form).attr('id'),
                                'recurly-token'     	:   $('input[data-recurly="token"]',$form).val(),
                                'referrer_email_id' 	:   $('input[name="referrer_email_id"]').val(),
                                'shipping_address1'		:   $('.shipping_address').val(),
                                'shipping_city'			:	$('.shipping_city').val(),
                                'shipping_state'		:	$('.shipping_states option:selected').val(),
                                'shipping_postalcode'	:	$('.shipping_zipCode').val(),
                                'numberOfUsers'         :   $('input[name="numberOfUsers"]',$form).val(),
								'coupon'				:   $('input[name="coupon"]',$form).val()
                        };
                        if($('#shipMePoster').prop('checked') !== true){
							data.shipping_address1 = '';
							data.shipping_city = '';
							data.shipping_state = '';
							data.shipping_postalcode = '';	
                        }
                        // send any errors to the error function below
                        if (err) {
                       
                            if (err.fields.length>0){
                                err.message = "Your transaction was declined. Please check that your credit card information is correct." 
                            }
                            errorWarning('.billing_info', err.message);
                            $submitBtn.prop('disabled', false);
                           
                        } else { // Otherwise we continue with the form submission
 
//                                   url:'/bigpicture_recurly/settingsPageBackend/subscription_detail.php',
//                                       
//                                       if(subscription_data.errorEmail){
                             $.ajax({
                                   type: "POST",
                                   dataType: "json",
                                   cache: false,
                                   data:data,
                                   url: '/bigpicture_recurly/create_premium.php', //Relative or absolute path to response.php file
                                   success 	: function(errorcode) {
                                        $submitBtn.prop('disabled', false);
                                                                   
                                        if(errorcode.errorMessage && errorcode.errorMessage.length>0){
                                            errorWarning('.billing_info',errorcode.errorMessage);
                                        }else if( errorcode.errorBilling === false && errorcode.errorOther === false){
                                            window.location.href='/bigpicapp/home';
                                        }
                                    },
                                    error:function(data){
                                        $submitBtn.prop('disabled', false);
                                        console.log('fail info:', data );
                                    }
                             });
                        }
                    });
                 }
             }
        }
    },'#secondStep')


    //submite contact us form
    .on({
        submit:function(event){
            event.preventDefault();
            var $form       =   $(this),
                $submitBtn  =   $('input[type ="submit"]',this),
                data        =   $(this).serialize(),
                ET          = Date.now(),
                limitT = ET - ST,
                honeypotNumber  =   $('.honeypotNumber', this).val(),
                honeypotVal     =   $('.honeypotVal', this).val();

            $form.validate({
                errorClass: 'errorBorder',
                debug: true,
                rules:{
                    name:'required',
                    email:{
                        required:true,
                        email   :true
                    },
                    comment:'required'

                },
                messages: {
                        name        :   "Please enter your name.",
                        email       :   "Please enter a valid email address.",
                        comment     :   "Please enter your Question / comment."
                },
                errorPlacement: function() {
                        return true;
                }
            });
            if($form.valid()){
                if(!navigator.onLine){
                    var internetObj = $('.internetRequest',this);
                    errorWarning(internetObj, internetRequestInfo);
                }
                else{
                    // validate user if it is a robot
                    if(honeypotNumber.length !== 0 || honeypotVal.length !== 0 || limitT <=400 || ST === undefined || ST === null){
                        return false;
                    }
                    //if not send data:
                    else{
                    $submitBtn.prop('disabled', true);
                    $.ajax({
                        type:'POST',
                        data:data,
                        dataType:"json",
                        url:"/contact_form_email.php",
                        success: function(errorcode){
                            console.log(data);
                            console.log(errorcode);
                            $submitBtn.prop('disabled', false);
                            if(errorcode.errorOther){
                               // console.log(data);
                                console.log(errorcode);
                                $errorLogin('.contact-us-form-error',response_info.systemError);
                             }
                            if(errorcode.errorMessage){
                                console.log('This is system errors');
                            }
                            else{

                                $('.submit_contactUs_succes').fadeIn(500);
                                $('.row[data-role="beforeSubmit"]').hide();
                                $form.hide();
                                $form[0].reset();
                                console.log('did it');
                            }
                        },
                        error: function(xhr){
                            $submitBtn.prop('disabled', false);
                            console.log(xhr);
                            console.log('fail');
                        }

                    });
                    }
                }

            }

        }
   },'.contact-us-form')

  .on({
        submit:function(event){
            event.preventDefault();
            var $form       =   $(this),
                $submitBtn  =   $('input[type = "submit"]',this),
                data        =   $(this).serialize(),
                ET          = Date.now(),
                limitT = ET - ST,
                honeypotNumber  =   $('.honeypotNumber', this).val(),
                honeypotVal     =   $('.honeypotVal', this).val();

            $form.validate({
                errorClass:'errorBorder',
                debug: true,
                rules:{
                    name:'required',
                    email:{
                        required:true,
                        email   :true
                    },
                    comment:'required'

                },
                errorPlacement: function() {
                       return true;
                    }

            });
            if($form.valid()){
                if(!navigator.onLine){
                    var H = $('.internetRequest_contactForm').outerHeight(true);
                    console.log(H);
                    var internetObj = $('.internetRequest', this);
                    errorWarning(internetObj, internetRequestInfo);

                }
                else{
                   // validate user if it is a robot
                    if(honeypotNumber.length !== 0 || honeypotVal.length !== 0 || limitT <=400 || ST === undefined || ST === null){
                        return false;
                    }
                    //if not send data:
                    else{
                        console.log(data);
                        $submitBtn.prop('disabled', true);
                        $.ajax({
                            type:'POST',
                            data:$(this).serialize(),
                            dataType:"json",
                            url:"/contact_form_email.php",
                            success: function(errorcode){
                                $submitBtn.prop('disabled', false);
                                if(errorcode.errorOther){
                                   // console.log(data);
                                    console.log(errorcode);
                                    $errorLogin('.sign-up-form,.simple_sign_up',response_info.systemError);
                                 }
                                if(errorcode.errorMessage){
                                    console.log('This is system errors');
                                }
                                else{

                                   // $('.submit_contactForm_succes').show();
                                   // $form.hide();
                                   $('.contact-sales-table').hide();
                                   $('.submit_contactForm_succes').fadeIn(500,function(){
                                        $('.contact-sales').delay(4000).animate({
                                            'bottom':'-450px'
                                        },500);
                                    });
                                    $form[0].reset();
                                    console.log('did it');
                                }
                            },
                            error: function(xhr){
                                $submitBtn.prop('disabled', false);
                                console.log(xhr);
                                console.log('fail');
                            }
                        });
                    }
                }

            }

        }
    },'.contact-sales');


    var regularPriceX;
		function getPrice(obj,data,regularPrice){

		$.ajax({
			type:'POST',
			data:data,
			dataType:"json",
			url:"/bigpicture_integrations/teams/get_team_price.php",
			success: function(response){
				if(response){
					console.log(response);
					regularPriceX = response.regularPrice;
					if(response.couponPrice.length === 0){
						$('#teamPrice').text(response.regularPrice);
                        $('.price').text(response.regularPrice);
                        $('.planPrice').text(response.regularPrice);
                        
						if(obj.attr('id') === 'coupon'){
							obj.removeClass('coupon_valid');
							if(obj.val().length !== 0){
								obj.addClass('errorBorder');
							}
							else{
								obj.removeClass('errorBorder');
							}
						}

					}
					else{
						$('#teamPrice').text(response.couponPrice);
                        $('.price').text(response.couponPrice);
                        $('.planPrice').text(response.couponPrice);
						if(obj.attr('id') === 'coupon'){
							obj.removeClass('errorBorder').addClass('coupon_valid');
							
						}
						
					}

				}

			},
			error: function(response){
				console.log('response error:' , response);
			}
		});
	}
	function couponValidation(){
		var defaultPrice = $('#teamPrice').text();
		var status = true;
		$('.firstStep').on({
			'paste keyup' : function(e){
				if(e.type === 'paste'){
					$(this).off('keyup');
				}
				var $this	= $(this),
					l		= $('#coupon').val().length,
					v 		= $.trim($('#coupon').val()),
					n		= $('#numberOfUsers').val();
					

				var alNumAndCharacter = /^[a-zA-Z0-9]*$/;
				$(this).val(v);
	
				
				if(alNumAndCharacter.test(v) && l == 8){
					
					$this.removeClass('errorBorder');
					var data = {
						numberOfUsers 	: n,
						coupon			: v,
						requestFromBrowser: "yes"
					};
					if(e.type === 'keyup' && status){
						getPrice($this, data, regularPriceX);
						status = false;
					}
					
					
				}

				if(l!==8){
					status = true;
					$('#teamPrice').text(regularPriceX);
                    $('.price').text(regularPriceX);
                    $('.planPrice').text(regularPriceX);
                    
					$this.addClass('errorBorder').removeClass('coupon_valid');
				}
				if(l==0){
					$this.removeClass('errorBorder');
				}
				
			}
		}, '#coupon')
		
		.on({
			keyup:function(e){
				var $this	= $(this),
					l		= $('#coupon').val().length,
					v 		= $.trim($('#coupon').val()),
					n		= $(this).val();
				var data = {
						numberOfUsers 	: n,
						coupon			: v,
						requestFromBrowser: "yes"
					};
				
				if(e.which != 8 && isNaN(String.fromCharCode(e.which))){
				   e.preventDefault(); //stop character from entering input
			    }
				else{
					if (n.length==0) return;
					if (n==0){
						n = 1;
						$(this).val(n);
						
					}
					else{
						getPrice($this, data, regularPriceX);
					}
					
				}
				
			},
			blur:function(e){
				
				var $this	= $(this),
					l		= $('#coupon').val().length,
					v 		= $.trim($('#coupon').val()),
					n		= $(this).val();
				var data = {
						numberOfUsers 	: n,
						coupon			: v,
						requestFromBrowser: "yes"
					};
				if (n.length==0 ){
					n = 1;
					$(this).val(n);
					data.numberOfUsers = n;
					getPrice($this, data, regularPriceX);
				}
				if(n.length>0 && n !== 1){
					getPrice($this, data, regularPriceX);
				}
			

			}
		},'#numberOfUsers');
		
	}
	couponValidation();
 

    //validate credit card type
        $('.payment').each(function() {
            $('#ccn').payment('formatCardNumber');
            $('.expdate').payment('formatCardExpiry');
            $('#ccode').payment('formatCardCVC');
            //validate credit card types
            $('#ccn').keyup(function(){
                var cardType = $.payment.cardType($('#ccn').val());
                if(cardType === null){
                    $('.credit_card_type').attr('class', 'credit_card_type');
                }
                else{
                    $('.credit_card_type').addClass(cardType);
                }
            });
        });

// donwload for free

function emptySpaceLimit(obj){
	//upgrade_password
	$(obj).on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });
	
}	
	emptySpaceLimit('.upgrade_password, #reset_password, #confirm_password');
	
$('body').on({
        submit:function(event){
            event.preventDefault();
            var $form = $(this);
            $form.validate({
                errorClass : 'errorBorder',
                rules:{
                    email:{
                        required: true,
                        email	: true
                    }
                },
                errorPlacement: function() {
                     return true;

                }

            });

            if($form.valid()){
                if(!navigator.onLine){
                var internetObj = $('.internetRequest', this);
                errorWarning(internetObj, internetRequestInfo);
            }
            else{
                var obj             =   this,
                ET              =   new Date(),
                limitT = ET - ST,
                honeypotNumber  =   $('.honeypotNumber', this).val(),
                honeypotVal     =   $('.honeypotVal', this).val(),
                data            =   $(this).serialize();
                $(this)[0].reset();
                if(honeypotNumber.length !== 0 || honeypotVal.length !== 0 || limitT <=400 || ST === undefined || ST === null){
                    console.log(limitT);
                    return false;
                }
                else{
                    //run ajax for here:
                    $('button',obj).prop('disabled', true);
                    callAjaxFordownLoad(obj, data);
                }
            }
        }


     }
  },'.downloadForFreeForm')

    .on({
        click:function(e){
            e.preventDefault();
            var obj         =   $(this).parent().parent();
            if(!navigator.onLine){
                var internetObj = $('.internetRequest', obj);
                errorWarning(internetObj, internetRequestInfo);
            }
            else{
                $('button',obj).prop('disabled', true);
                $.fblogin({
                    fbId: facebookID,
                    permissions: 'email',
                    fields: 'email,first_name,last_name',
                    success: function (data) {
                        console.log('Basic public user data returned by Facebook', data);
                        var dataFilter = {
                            'email'     : data.email
                            //'country'   : countryInput; //it's val from ip address;
                        };

                       //call ajax for here:
                       callAjaxFordownLoad(obj, dataFilter);

                    },
                    error: function (error) {
                        $('button',obj).prop('disabled', false);
                        console.log('An error occurred about facebook login plugin.', error);
                    }
                });
            }


        }
    },'.downloadWithFacebook');


//send email on referral page
    //call ajax :
    function referralRewardAjax(obj,data){
        var submitBtn   = $('input[type ="button"]', obj);
        submitBtn.prop('disabled',true);
        $.ajax({
                       type:'POST',
                        data:data,
                        dataType:"json",
                        url:"/bigpicture_recurly/referral_rewards/referral_rewards.php",
                        success: function(errorcode){
                            validateErrorLength(obj,errorcode);
                            console.log(data);
                            console.log(errorcode);
                            submitBtn.prop('disabled',false);
                            if(errorcode.errorOther){
                                errorWarning('.respone_info',response_info.systemError);
                                $('.respone_successful').fadeOut();
                             }
                            if(errorcode.errorMessage){
                                errorWarning('.respone_info',response_info.systemError);
                                $('.respone_successful').fadeOut();
                            }
                            else{
                                if($('.respone_info').css('display')==='block'){
                                    console.log($('.respone_info'));
                                    $('.respone_info').fadeOut(100, function(){
                                        errorWarning('.respone_successful', response_info.lessOneContact);
                                    });
                                }
                                else{
                                        errorWarning('.respone_successful', response_info.sentSuccessfully);
                                    }
                                $('.referralRewardsTextArea').clearFormvalues();
                                $('.token').remove();

                            }
                        },
                        error: function(xhr){
                            submitBtn.prop('disabled',false);
                            console.log(xhr);
                            console.log('fail');
                        }

                 });
    }

$('input[type ="button"]', '.referralForm').on('click',function(event){
        event.preventDefault();
        console.log('test');

        if(!navigator.onLine){
            var internetObj = $(this).siblings('.internetRequest');
            errorWarning(internetObj, internetRequestInfo);

        }
        else{
            var collectionArea = $(this).siblings('.referralRewardsTextArea'),
                emailLength = $('.token', collectionArea).length,
                emailFormatStatus = $('.invalid', collectionArea).length,
                sucessfulSent = emailLength - emailFormatStatus,
                vals       = $('input', collectionArea).val(),
                emailTrue  = [],
                validateEmail = [],
                obj = $('.referralForm'),
                data = {
                     subscriber_email    : $.cookie('email'),
                     referred_email_list : null
                 },
                 $form = $('.referralForm'),
                 emailVal =  $('.token-input').val();

            emailTrue = vals.split(', ');
            //emailFormatStatus = $('.token',this).hasClass('invalid'),
            $.each(emailTrue, function(index, email) {
                if(isEmail(email)===true){
                    validateEmail.push(email);
                }

             });

             $form.validate({
                    rules:{
                        email:{
                            required: true,
                            email	: true
                        }
                    },
                    errorPlacement: function() {
                         return true;
                    }

            });
            if($form.valid()){
                 validateEmail.push(emailVal);
            }

            if(sucessfulSent <=0 && !$form.valid()){
                if($('.respone_successful').css('display')==='block'){
                    $('.respone_successful').fadeOut(100, function(){
                        errorWarning('.respone_info',response_info.lessOneContact);
                    });
                }
                else{
                    errorWarning('.respone_info',response_info.lessOneContact);
                    var error = {};
                        error.length = 1;
                        validateErrorLength(obj,error);
                }
            }
            else{
                 data.referred_email_list = validateEmail;
                 console.log(emailLength);
                 referralRewardAjax(obj, data);

           }
        }

    });



});
