// JavaScript Document
requirejs.config( { 
    baseUrl: '/lib',
    paths: {
        jquery              :   'jquery-1.11.3.min',
        jqueryUI            :   'custom-ui.min',
        cookie              :   'jquery.cookie.min',
        bootstrap           :   'bootstrap.min',
		recurly             :   'recurly',
        payment             :   'payment.min',
        validatePlu         :   'jquery.validate',
		fblogin             :   'jquery.fblogin',
        tokenfield          :   'bootstrap-tokenfield',
		xepOnline			:   'xepOnline.jqPlugin',
        referral_rewards    :   '../js/referral_rewards',
		client		        :	'../js/client',
		validate	        :	'../js/validate'
        
        
    },
	shim : {
        'bootstrap'		: { 'deps' :['jquery'] },
		'jqueryUI'		: { 'deps' :['jquery'] },
		'cookie'		: { 'deps' :['jquery'] },
		'fblogin'		: { 'deps' :['jquery'] },
        'payment'		: { 'deps' :['jquery'] },
        'tokenfield'    : { 'deps' :['jquery'] },
		'referral_rewards': { 'deps' : ['tokenfield']},
		'xepOnline'		: { 'deps' :['jquery'] }
		
	}
});


require([
			'jquery',			 
			'bootstrap', 
			'cookie',
			'client',
			'validate',
			'referral_rewards',
			'xepOnline'
			
		], 
	function (
			$,
			bootstrap,	
			cookie,
			client,
			validate,
			referral_rewards,
			xepOnline
			
		) {
   			 //require(['validate'], function(){});
			 
        }
);