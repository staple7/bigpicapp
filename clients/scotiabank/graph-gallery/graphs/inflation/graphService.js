define(['postal', 'data/rawData/CA/inflation', 'graph-gallery/graphs/inflation/messages'], 
    function(postal, inflation, messages){

        var context;
    
        function graph(initDate){
            removeElements();
      
            var inflationList = inflation.DATA;

            var result = {};
            

//             var userInitDate = new Date(initDate.initDate);
            var userInitDate = initDate;
            result = calculateDataList(inflationList, userInitDate);
            var inflationDataList = result.inflationDataList;
            
            var minXValue = 0;

//            graphWidth = d3.select(".x.axis").node().getBBox().width;
            minYValue = result.minYValue;
            maxYValue = result.maxYValue;
         
            minXValue = 0;
            maxXValue = inflationDataList.length;
            
           
            yAxis = d3.svg.axis(); 

             x = d3.scale
                .linear()  
                .domain([minXValue, maxXValue])
                .range([0, graphWidth]);
 
//            var parseDate = d3.time.format("%m-%d-%Y").parse;
            
            var xPos = 0;
            
            y = d3.scale
                .linear()                   
                .domain([0, maxYValue])
                .range([graphHeight, 0]);
          
            var formatPercent = d3.format(".0%");

            
            yAxis.scale(y)
                .orient("left")
                .ticks(3)
                .tickFormat(formatPercent);;

            svg.selectAll(".y_axis").call(yAxis);
       
             var valueline = d3.svg.line()
                .x(function(d) { return x(d.x); })
                .y(function(d) { return y(d.y); });
           

            svg.selectAll(".y_axis,.tick").each(function(i, e){
//                console.log(e);
                if (e<4){
                    svg.append("line")
                        .attr("class", "line1")
                        .attr("x1", 0)
                        .attr("x2", graphWidth)                   
                        .attr("stroke", "#d6d6d6")
                        .attr('stroke-width', '0.4px')
                        .attr('stroke-dasharray','5,5')
                        .attr("transform", "translate(" + 0 + "," + y(i) + ")");  
                }
          
            });
            
            
            svg.append("path")
              .attr("id","graphPath")
              .data([inflationDataList])
              .attr("class", "line")
              .style("fill", "none")
              .attr("stroke-width", 2)
              .attr("stroke", "#455A64")
              .attr("d", valueline);
            
           
            postal.channel().publish("graphService.graph.done");
        }
  
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
            return months <= 0 ? 1 : months;
        }

      
        function calculateDataList(inflationList, userInitDate){
            var regionInitDate = new Date("1935-01-01");
//            var inflationDataList = [{x:0, y:0}];
//             userInitDate = new Date("1985-01-30");
            var inflationDataList = [];
            var inflationValue;
            var minYValue;
            var maxYValue;
            var index = 1;
            var monthIndex=monthDiff(regionInitDate, userInitDate)-1;
   
            for(; monthIndex<inflationList.length; monthIndex += 1){
                inflationValue = inflationList[monthIndex];
                minYValue = (minYValue === undefined || inflationValue < minYValue) ? inflationValue : minYValue;                
                maxYValue = (maxYValue === undefined || inflationValue > maxYValue) ? inflationValue : maxYValue;
                inflationDataList.push({x:index, y:inflationValue});
                index++;
            }          
            
            
//           inflationDataList.push({x:index, y:0});
//           console.log(inflationDataList);
            return {inflationDataList: inflationDataList,
                    minYValue: minYValue,
                    maxYValue: maxYValue
                   };            
        }
    
        
        function setGraphTitles(investmentHorizon){
//            xAxisTitle.html(Handlebars.compile(context.xAxisTitle)({investmentHorizon:investmentHorizon}));       
            
//            var graphWidthLimit = 1359;
//            var documentWidth = $(document).width();
//            var graphSubtitle = context.graphSubtitle.normal;
//
//            if  (documentWidth<graphWidthLimit){
//                graphSubtitle = context.graphSubtitle.abbreviate
//            }
//            
//            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
//                                ({investmentHorizon:investmentHorizon,
//                                regionStartYear:localStorage.getItem("regionStartYear")}));
            
        }     
    

        function removeElements(){
            $("#graphPath").remove();
            $("#y2Axis").remove();
            $("#graphBars").remove();
            $(".line1").remove();
//            $("#activeAreas").remove();
            
        }
    
        
        function finalize(){
            removeElements();
            localGraphData = null;
            context = null;

        }
    
        function branchChanged(){
         
            finalize();
        }
    
        function graphChanged(){            
            finalize();
        }
        
        function getGraphDefinition(){
//            return Handlebars.compile(context.graphDefinition)(localGraphData);
        }


        function removeAxis(){
//            svg.selectAll(".x.axis").remove();
//            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            svg.append("g").attr("class", "x_axis");
            svg.append("g").attr("class", "y_axis");
        }
    
        function init(lang, branchCode){
            addAxis();
//            calculateGraphDimensions();
//            periodicity = 1; 
          
            context = messages[branchCode]; 
//            y2AxisLabel = context.y2AxisTitle;
//            $(".graph_title").html(context.graphTitle);
            

            yAxisTitle.html("");
            postal.channel().publish("graph-gallery.graph.initiated");
           
        }

        
        function getGraphElements(){
            
        }

    
        return {
            init:               init,
            graph:              graph,
            getGraphDefinition: getGraphDefinition,
            graphChanged:       graphChanged,
            getGraphElements:   getGraphElements,
            branchChanged:      branchChanged
        };
    
});     
                