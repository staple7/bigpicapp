define (['graph-gallery/graph-galleryView', 
         'postal'], 

    function(graph-galleryView, postal){

        $('#graph_1').on("click", function () {
            if ($(this).hasClass("thick")) {return false};
            $(this).toggleClass("thick");
            $('#graph_2').toggleClass("thick");
            channel.publish("graph-gallery.graph.changed", 0);
        });
    
        $('#graph_2').on("click", function () {
            if ($(this).hasClass("thick")) {return false};
            $(this).toggleClass("thick");
            $('#graph_1').toggleClass("thick");
            channel.publish("graph-gallery.graph.changed", 1);
          
        });
    
    
        $('#graph_3').on("click", function () {
            channel.publish("graph-gallery.graph.changed", 2);
          
        });
    
        var channel = postal.channel();    
        var graphData = {};
        var currentGraphCode;
        var graphTimer;
        var currentGraphService;
    
        var graphServiceCodeList = ["primeRate","exchangeRate"];
    
        channel.subscribe("graph-gallery.graph.changed", function(graphId){     
            currentGraphService.graphChanged();
            initGraph(graphId)
        });
    
        channel.subscribe("graph-gallery.graph.initiated", function(){
            currentGraphService.graph(graphData);
            channel.publish("inputController.calculate.output");
        });  
     
    
        function initGraph(graphId){
            clearTimeout(graphTimer);
            graphTimer = setTimeout(function(){
                currentGraphCode = graphServiceCodeList[graphId];
                //console.log('graph-gallery/graphs/' + currentGraphCode + '/graphService');
                //console.log("currentGraphCode", currentGraphCode);
                require(['graph-gallery/graphs/' + currentGraphCode + '/graphService'], 
                        function(graphService){
                            currentGraphService = graphService;   
                            channel.publish("graph-galleryController.current.graph", graphId);
                            currentGraphService.init(localStorage.getItem("lang"), graphData);
                });
            }, 300);
        }    

       
    
        function loadModule(initDate){
            graphData["initDate"] = initDate;
            graph-galleryView.start(initDate);
        };
    

        channel.subscribe("graph-galleryController.regraph", function(year){
           
            graph-galleryView.start();
            currentGraphService.graph({initDate:"1/01/"+year});
        });
    
      
        var rendered = channel.subscribe("graph-gallery.template.rendered", function(){
            initGraph(0);            
            rendered.unsubscribe();
            rendered = null;
        });

        
        function showGraph(show){
            graph-galleryView.showAllGraphElements(show);
        }
    
        return {
            loadModule:          loadModule,
            showGraph:           showGraph
            
        };

    }
);
