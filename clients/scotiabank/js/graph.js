//define([''], function(){
      
    var lifeline;
    var squiggly_data = JSON.parse(window.localStorage.getItem('data'));
    var assets_to_show = window.localStorage.getItem("default_assets").split(',');
    
    var bd_year = parseInt(window.localStorage.getItem('bd_year'));
    var bd_month = parseInt(window.localStorage.getItem('bd_month'));
    // Normalization value
    var mmult = 1000;
    // Date parsing
    var date_format = d3.time.format("%Y-%m-%d");
    // Containment
    var margin = {
        top: $('#parent .header').outerHeight() + 90,
        right: 130,
        bottom: 80,
        left: 110
    };
    
    var cont_w = $(window).width();
    var wh = ($(window).height() > cont_w) ? cont_w : $(window).height();
    var cont_h = wh;
    var svg;
    cont_h = cont_h + 70;
    // Graph size
    var graph_w = cont_w - margin.left - margin.right;
    var graph_h = cont_h - margin.top - margin.bottom;

    // Initial events
    var all_events = [{
        'event_type': 'me',
        'month': bd_month,
        'year': bd_year
    }];
    var asset_labels_legend;
    var yPosForLegends = graph_h*.50;
   yPosForLegends =  166;
    // ON DEVICE READY - PHONEGAP
    
    var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-8, 0]);
    
    onDeviceReady();
    function onDeviceReady() {

        // LOCALIZATION
        var temp_lang = window.localStorage.getItem("temp_lang");
        if (temp_lang !== undefined && temp_lang !== null) {
            var lang_to_use = temp_lang;
        } else {
            var lang_to_use = window.localStorage.getItem("lang");
        }
       
        Globalize.culture(lang_to_use);




        // If preferences aren't set, go back to the relcome screen to setup the app
        if (window.localStorage.getItem("default_assets") === null) {
            window.location = "welcome.html";
        }


        var latest_date = JSON.parse(window.localStorage.getItem('data'));
        latest_date = latest_date[latest_date.length - 1].date;
        var latest_year = parseInt(latest_date.substr(0, 4));
        var latest_month = parseInt(latest_date.substr(5, 2));

        // FILL YEAR FIELD
        // Every year from 1935 to latest date in data
        var temp_str = '';
        for (var i = bd_year; i <= latest_year-11; i++) {
            temp_str += "<option value='" + i + "'>" + i + "</option>";
        }
        $('#year').html(temp_str);

        var temp_str = '';
        for (var i = bd_month; i <= 12; i++) {
            temp_str += "<option value='" + i + "' data-localize-month='" + i + "'></option>";
        }
//        console.log($('#event_menu .content select#month').length);
        $('#month').html(temp_str);
        $("[data-localize-month]").each(function () {
            $(this).html(Globalize.culture().calendars.standard.months.names[(parseInt($(this).attr('data-localize-month')) - 1)].capitalize(true));
        });

        $('#year,#month').change(function () {
            var month=$('#month').val();
            var year =$('#year').val();
            make_lifeline(month, year);
//            postal.channel().publish("graph-galleryController.regraph",year);

        });

        $(window).resize(function(){
            var month=$('#month').val();
            var year =$('#year').val();
            setTimeout(function(){
                location.reload();
            },1000);
            
        });
        
        var mm_options = {
            position: "left",
            zposition: "front",
            isMenu: false,
            moveBackground: true,
            dragOpen: false
        };
        $("nav#event_menu").mmenu(mm_options);

        // HANDLE GESTURES
        // Asset class toggles

        var assets_for_legends = [];
        $(".btn-asset").click(function () {
            assets_for_legends = [];
            var assetsCount = 0;
            var limit = 6;
            var thisVal = $(this).val();
            if ($(this).hasClass("btn-default") == true) {
                $(".btn-asset").each(function () {
                    if ($(this).hasClass("btn-primary")) {
                        assetsCount++;
                    }
                });
                if (assetsCount == limit) {
//                    $(".assets-modal-footer").css("visibility", "visible");

                } else {
//                    $(".assets-modal-footer").css("visibility", "hidden");
                    $(this).removeClass("btn-default").addClass("btn-primary");
//                    console.log("on", thisVal);
                    d3.selectAll('svg path.asset_line.line-' + thisVal).classed('hidden', false);
                    d3.selectAll('svg text.asset_label.label-' + thisVal).classed('hidden', false);
                    $('#bnw_modal .' + thisVal).removeClass('hidden');

                }
            } else {
//                $(".assets-modal-footer").css("visibility", "hidden");
                $(this).removeClass("btn-primary").addClass("btn-default");
                $('#bnw_modal .' + thisVal).addClass('hidden');
                //            assets_for_legends.splice($.inArray(thisVal, assets_for_legends), 1);


                d3.selectAll('svg path.asset_line.line-' + thisVal).classed('hidden', true);
                d3.selectAll('svg text.asset_label.label-' + thisVal).classed('hidden', true);
            }

            $(".btn-asset").each(function () {
//                console.log($(this).val(),$(this).hasClass("btn-primary") );
                if ($(this).hasClass("btn-primary")) {
                    assets_for_legends.push($(this).val());
                }
            });
            window.localStorage.setItem("default_assets",assets_for_legends);
            display_legends(assets_for_legends);
            create_bubbles(svg);

        });


        $('#main').hammer().on("tap", function () {
            //$('#parent .header').toggleClass( 'hide' );
        });

        // BEST & WORST YEARS
        var bnw_data = JSON.parse(window.localStorage.getItem("bnw_data"));
        // Bottom buttons
        var temp_str = '';
        for (var bnw_key in bnw_data) {
            if (bnw_key == "alltime") {
                temp_str = '<button type="button" class="btn btn-primary" value="' + bnw_key + '">' + Globalize.localize(bnw_key, Globalize.culture()) + '</button>' + temp_str;
            } else if (bnw_key != "extent") {
                temp_str = '<button type="button" class="btn btn-default" value="' + bnw_key + '">' + sprintf(Globalize.localize('last_years', Globalize.culture()), parseInt(bnw_key)) + '</button>' + temp_str;
            }
        }
        temp_str = '<div class="btn-group bnw-footer">' + temp_str + '</div>';
        $('#bnw_modal div.modal-footer').html(temp_str);

        // Handle bottom buttons
        $('#bnw_modal div.modal-footer button').hammer().on("tap", function () {
            $('#bnw_modal div.modal-footer button').addClass('btn-default').removeClass('btn-primary');
            $(this).addClass('btn-primary');
            make_bnw();
        });

        // B&W Graph
        var bnw_h = $(window).height() - 155;
        var bnw_mmult = 90;
        $('#bnw_modal div.modal-body').css({
            height: bnw_h
        });
        $('.modal-body .div-top, .modal-body .div-bottom').css({
            height: ($('#bnw_modal div.modal-body').innerHeight() - $('.modal-body .div-bar').outerHeight() - 40) / 2
        });
        $('.modal-body .div-top, .modal-body .div-bar, .modal-body .div-bottom').html($('.modal-body .div-temp').html());
        $("[data-localize-modal]").each(function () {
            $(this).html(Globalize.localize($(this).attr('data-localize-modal'), Globalize.culture()));
        });
        $('.modal-body .div-top .bar-cont, .modal-body .div-bottom .bar-cont').html('<div class="bar-bar bar-1yr"></div><div class="bar-bar bar-5yr"></div>');

        function make_bnw() {
           
            var bnw_period = bnw_data[$('#bnw_modal div.modal-footer button.btn-primary').val()];
           
            for (var bnw_asset_key in bnw_period) {
                var bnw_asset = bnw_period[bnw_asset_key];
 
                for (var bnw_rolling_period_key in bnw_asset) {
                    var bnw_rolling_period = bnw_asset[bnw_rolling_period_key];

                    var temp_val = bnw_rolling_period.best * 100;
//$(this).html(Globalize.localize($(this).attr('data-localize'), Globalize.culture()));
                    $('#bnw_modal div.modal-body .div-top .' + bnw_asset_key + ' .bar-' + bnw_rolling_period_key).css({
                        height: (bnw_rolling_period.best / bnw_data['extent']) * bnw_mmult + '%'
                    }).html('<p class="' + ((temp_val >= 0) ? 'text-muted' : 'text-danger') + '">' + format_percentage(temp_val.toFixed(1)) + '</p>');

                    var temp_val = bnw_rolling_period.worst * 100;
                    $('#bnw_modal div.modal-body .div-bottom .' + bnw_asset_key + ' .bar-' + bnw_rolling_period_key).css({
                        height: (-bnw_rolling_period.worst / bnw_data['extent']) * bnw_mmult + '%'
                    }).html('<p class="' + ((temp_val >= 0) ? 'text-muted' : 'text-danger') + '">' + format_percentage(temp_val.toFixed(1)) + '</p>');
                }
            }
        }
        
        make_bnw();


        // Handle buttons
        $('.header button#asset_classes').hammer().on("tap", function () {
            $('#asset_menu').trigger('open');
        });
        $('.header button#events').hammer().on("tap", function () {
            $('#event_menu').trigger('open');
        });
        $('.mm-menu .header button').hammer().on("tap", function () {
            $('#asset_menu').trigger('close');
            $('#event_menu').trigger('close');
        });
        $('.header button#client').hammer().on("tap", function () {
            location.reload();
        });

        $('button.btn-asset').each(function () {
            $(this).html('<div class="icon icon_' + $(this).val() + ' icon-30"></div><span data-localize="' + $(this).val() + '">' + Globalize.localize($(this).val(), Globalize.culture()) + '</span>').addClass('btn-' + $(this).val());
        });

        make_lifeline();

        // General strings
        $("[data-localize]").each(function () {
            $(this).html(Globalize.localize($(this).attr('data-localize'), Globalize.culture()));
        });
        // Months
        $("[data-localize-month]").each(function () {
            $(this).html(Globalize.culture().calendars.standard.months.names[(parseInt($(this).attr('data-localize-month')) - 1)].capitalize(true));
        });
        // Title
        var f_name = window.localStorage.getItem("f_name");
        if (f_name !== '' && f_name !== null && f_name !== undefined) {
            $("[data-localize-title]").each(function () {
                $(this).html(sprintf(Globalize.localize('personal_lifeline', Globalize.culture()), f_name));
            });
        } else {
            $("[data-localize-title]").each(function () {
                $(this).html(Globalize.localize('lifeline', Globalize.culture()))
            });
        }

    }

    function create_bubbles(svg){
           
        $("[id^=circles]").remove();
        $("[id^=dotsRect]").remove();

        var bubbleSpacingFactor = 15;
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
            bubbleSpacingFactor = 18;
        }

        //The smaller bubbleSpacingFactor is the less bubbles there will be.
        var yPos = 0;
        var bubble = [];
        var bubbleGroups = [];

        var assets = d3.selectAll('svg #asset_labels text.asset_label');
        var lastYPos = 0;            
        var assets_to_show = window.localStorage.getItem("default_assets").split(',');
        for (var x = 0; x < assets_to_show.length; x++) {

            var asset = d3.selectAll('svg text.asset_label.label-' + assets_to_show[x]);
            asset.classed('hidden',false);
        }
        
        var count = 0;
        assets.each(function () {
            count++;
            var thisAsset = d3.select(this);               
            if (thisAsset.classed("hidden")) return true;
            var thisValue = thisAsset.text();                
            var thisYPos  = thisAsset.attr('y');
            if (bubble.length == 0 || lastYPos == 0){ //This is the first asset or starts a new possible bubble.
                bubble.push(thisAsset); //Add element to a possible Bubble.
            }else{
                if ((thisYPos - lastYPos) > bubbleSpacingFactor){ //Checks spacing vs last assets.                     
                    if (bubble.length>1){ //There is a Bubble!!!
                        bubbleGroups.push(bubble); //Adds the bubble to the array of bubbles.                            
                    }
                    bubble = []; //Cleans the Bubble to start a new one.
                }else{                    
                    if (count == assets.size()) { //Special case when it comes to be the last asset.
                        bubbleGroups.push(bubble);
                    }
                }
                bubble.push(thisAsset); //Add element to a possible Bubble. This line will add the last asset into the bubble groups.
            }
            lastYPos = thisYPos; //Tracks the previous position.
        });



        for (var x = 0; x < bubbleGroups.length; x++) {

            var assetArr = bubbleGroups[x];
            var asset;
            var dotsPosition = 0;
            var text="";
            var assetCodes = "";
            var numOfValues = 0
            var dotsYPos = 0;
            for (numOfValues = 0; numOfValues < assetArr.length; numOfValues++) {
                asset = assetArr[numOfValues];


                text += "<div style='margin:7px'><p style='font-weight: 900;font-size:14px;font-family:Scotia;font-weight:bolder;color:"+asset.style('fill')+"'>"+format_y_ticks(getNumber(asset.text()))+"</p></div>";
                assetCodes += asset.attr('class').substring(18, 150) + ",";
                dotsPosition += parseFloat(asset.attr("y"));
                asset.classed("hidden",true);
            }

            dotsYPos = (dotsPosition / numOfValues);

            svg.append("g")
                .attr("id","circles"+x)

            svg.append("rect")
                .attr("id", "dotsRect"+x)
                .attr("height", "30px")
                .attr('width', "60px")
                .attr("x",asset.attr('x')-17)
                .attr("y",(dotsYPos)-15)
                .attr("fill", "transparent")
                .attr("text", text)
                .attr("assetCodes", assetCodes);

            d3.select("#dotsRect"+x)
                .on("mouseover", function (d) {
                    d3.select(this).style("cursor", "pointer"); 
                    var $this = d3.select(this);                    
                    tip.html(d3.select(this).attr("text"));
                    tip.show();                    
                    var assetCodeList = $this.attr('assetCodes').split(',');
                    for (var x = 0; x < assetCodeList.length; x++) {
                        var assetCode = assetCodeList[x];
                        if (assetCode.length>0){
                            highLightAsset(undefined,true,assetCode);
                        }
                    }
                })
                .on("mouseleave", function (d) {
                  d3.select(this).style("cursor", "default"); 
                    tip.hide(); 
                    var $this = d3.select(this);
                    var assetCodeList = $this.attr('assetCodes').split(',');
                    for (var x = 0; x < assetCodeList.length; x++) {
                        var assetCode = assetCodeList[x];
                        if (assetCode.length>0){
                            highLightAsset(undefined,false,assetCode);
                        }
                    }
                })

            d3.select("#circles"+x)
               .append("circle")
               .attr("cx", parseFloat(asset.attr('x'))+5)
               .attr("cy", dotsYPos)
               .attr("fill","#9E9E9E")
               .attr("r", 4);

            d3.select("#circles"+x)
               .append("circle")
               .attr("cx", parseFloat(asset.attr('x')) + 17)
               .attr("cy", dotsYPos)
                .attr("fill","#9E9E9E")
               .attr("r", 4);

            d3.select("#circles"+x)
               .append("circle")
               .attr("cx", parseFloat(asset.attr('x')) + 29)
               .attr("cy", dotsYPos)
              .attr("fill","#9E9E9E")
               .attr("r", 4);

        }
      
    }

    function make_lifeline(start_month, start_year) {
    //start_month ="1";
    //    start_year="1940";
        // Default is chosen birth date, but you can override to graph data from any date
        var start_month = (start_month === undefined) ? bd_month : start_month;
        var start_year = (start_year === undefined) ? bd_year : start_year;
//        console.log('Drawing graph from ' + start_year + '-' + start_month);

        // SETUP CONTAINER
        $('#parent').height(wh);
        $('#parent #main').height(cont_h).empty();
        var main_container = d3.select('#parent #main');

        // NORMALIZE ASSET DATA
        var norm_date = null;
        if (start_month - 1 < 1) {
            norm_date = (start_year - 1) + '-12-' + '31';
        } else {
            norm_date = start_year + '-' + String('0' + (start_month)).slice(-2) + '-' + days_in_month(start_month, start_year);
        }
        //    console.log(squiggly_data);
        //    console.log(norm_date);
        var data = normalize_data(squiggly_data, norm_date);
        //	console.log(data);
        // DATES
        var f_date = date_format.parse(norm_date);
        var l_date = date_format.parse(squiggly_data[squiggly_data.length - 1].date);
        //    console.log(l_date, f_date);
        // GRAPH SCALE
        var xScale = d3.time.scale().domain([f_date, l_date]).range([margin.left, cont_w - margin.right]);

        var data_min = d3.min(data, function (d) {
            return d.min;
        });
        
        var data_max = d3.max(data, function (d) {
            return d.max;
        });
        var yScale = null;
        if (parseFloat(data_max) > 50000) {
            yScale = d3.scale.log();
            var is_log = true;
        } else {
            yScale = d3.scale.linear();
        }
        yScale.domain([data_min, data_max]).range([cont_h - margin.bottom, margin.top + 50]);

        var zoomScale = d3.scale.linear().domain([14.5, 78.5]).range([2.9, 15.75]); // MAX ZOOM LEVEL TO NOT GET MONTHS DISPLAYED
        var minTxScale = d3.scale.linear().domain([1, 15.7489]).range([0, -1623.3769]); // LEFT OF GRAPH
        var maxOffsetScaleX = d3.scale.linear().domain([800, 1200]).range([-9879.3056, -15780.4825]); // RIGHT OF GRAPH
        var maxTxScale = d3.scale.linear().domain([1, 15.7489]).range([0, maxOffsetScaleX(cont_w)]);
        var minTyScale = d3.scale.linear().domain([1, 15.7489]).range([0, -1447.9731]); // TOP OF GRAPH
        var maxOffsetScaleY = d3.scale.linear().domain([600, 900]).range([-7668.8417, -12096.1056]); // BOTTOM OF GRAPH
        var maxTyScale = d3.scale.linear().domain([1, 15.7489]).range([0, maxOffsetScaleY(cont_h)]);

        // DEFINE LINES
        var asset_line_def = d3.svg.line()
            .x(function (d) {
                return limit_decimals(xScale(date_format.parse(d.date)));
            })
            .y(function (d) {
                return limit_decimals(yScale(d.value));
            });

        // DEFINE AXES
        var xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickSize(-cont_h + margin.bottom + margin.top - 5).tickPadding(6);
        var yAxis = d3.svg.axis().scale(yScale).orient("left").tickFormat(format_y_ticks).tickPadding(10).innerTickSize(-cont_w + margin.left + margin.right - 10);
        if (is_log === undefined) {
            yAxis.ticks(5);
        } else {
            yAxis.tickValues(y_tick_values(yScale, 5));
        }

        // DEFINE ZOOM
        var zoom_diff = (l_date - f_date) / (1000 * 60 * 60 * 24 * 365.26);
        var zoom_def = d3.behavior.zoom().x(xScale).y(yScale).scaleExtent([1, zoomScale(zoom_diff)]).on("zoomstart", zoom_start).on("zoomend", zoom_stop);


        svg = main_container.append('svg').attr('id', 'lifeline_svg').attr('width', cont_w).attr('height', cont_h).attr('viewBox', '0 150 ' + cont_w + ' ' + cont_h);


        // CLIPPING PATHS
        var graph_clip = svg.append("svg:clipPath").attr("id", "graph_clip").append("svg:rect")
            .attr("width", cont_w - margin.right - margin.left)
            .attr("height", cont_h - margin.top - margin.bottom - 30)
            .attr('x', margin.left).attr('y', margin.top + 40);
        var y_axes_clip = svg.append("svg:clipPath").attr("id", "y_axes_clip").append("svg:rect")
            .attr("width", cont_w).attr("height", cont_h - margin.bottom - margin.top - 30).attr('x', 0).attr('y', margin.top + 40);
        var h_events_clip = svg.append("svg:clipPath").attr("id", "h_events_clip").append("svg:rect")
            .attr("width", cont_w - margin.right - margin.left)
            .attr("height", cont_h - margin.top - 5)
            .attr('x', margin.left).attr('y', margin.top + 5);

        // Basically, just a background
        svg.append("rect").attr("width", cont_w).attr("height", cont_h).attr('x', 0).attr('y', 0).attr('fill', $('body').css('background-color'));



        if (window.localStorage.getItem("default_h_events") == 'true') {
            // RECESSIONS
            var recession_cont = svg.append('g').attr('id', 'recession_cont').attr("clip-path", "url(#h_events_clip)");
            var recession_grp = recession_cont.append('g').attr('id', 'recession_grp');
            recession_grp.selectAll('rect').data(recession_data).enter().append('rect')
                .attr('x', function (d) {
                    return xScale(date_format.parse(d.start));
                }).attr('y', margin.top + 5)
                .attr('width', function (d) {
                    return xScale(date_format.parse(d.end)) - xScale(date_format.parse(d.start));
                })
                .attr('height', cont_h - margin.top - margin.bottom + 5)
                .attr('fill', '#f4f4f4').call(zoom_def);

            // FINANCIAL EVENTS
            var f_events_cont = svg.append('g').attr('id', 'f_events_cont').attr("clip-path", "url(#h_events_clip)");
            for (var f_event_key in financial_event_data) {
                var f_event = financial_event_data[f_event_key];
                if (date_format.parse(f_event.date) > f_date) {
                    f_events_cont.append('text').attr('fill', '#666').attr('text-anchor', f_event.halign)
                        .attr('x', xScale(date_format.parse(f_event.date))).attr('y', cont_h - margin.bottom + 58)
                        .attr('event-code',f_event.code)
                        .text(Globalize.localize(f_event.code, Globalize.culture()));
                }
            }
        }

        // CREATE AXES 
        var x_axis = svg.append("g").attr("class", "x axis").attr("transform", "translate(0," + (cont_h - margin.bottom + 10) + ")").call(xAxis);
        x_axis.select('path.domain').attr('fill', 'none');
        var y_axis_cont = svg.append('g').attr("clip-path", "url(#y_axes_clip)");
        var y_axis = y_axis_cont.append("g").attr("class", "y axis").attr("transform", "translate(" + (margin.left - 10) + ", 0)").call(yAxis);

       
           
        
        svg.call(tip);

        svg.append("line")
                        .attr("class", "line13")
                        .attr("x1", 0)
                        .attr("x2", graph_w)                   
                        .attr("stroke", "#eee")

                        .attr("transform", "translate(" + 110 + "," + 151 + ")");  

        // EVENTS CONTAINER
        var event_cont = svg.append('g').attr('id', 'event_cont');

        // DRAW ASSET LINES
        var asset_lines_cont = svg.append("g").attr("clip-path", "url(#graph_clip)");
        var asset_lines = asset_lines_cont.append("g").attr('id', 'asset_lines');
        var asset_line = [];
        var asset_line_data = [];
        //    console.log(asset_line_data);
        for (var node in data[0]) {
            if (node != 'date' && node != 'max' && node != 'min') {
                asset_line_data[node] = data.map(function (d) {
                    return {
                        date: d.date,
                        value: d[node]
                    };
                });
                asset_line[node] = asset_lines.append("path")
                    .datum(asset_line_data[node])
                    .attr("class", "asset_line line-" + node)
                    .attr("vector-effect", "non-scaling-stroke")
                    .attr("d", asset_line_def);
            }
        }

        // ASSET LABELS
        var asset_labels = svg.append('g').attr('id', 'asset_labels');
        // Sort values from mx to min
        var asset_label_y = [];
        for (var node in data[data.length - 1]) {
            if (node != 'date' && node != 'max' && node != 'min') {
                asset_label_y.push([node, data[data.length - 1][node]]);
            }
        }
        asset_label_y.sort(function (a, b) {
            return b[1] - a[1]
        });
        
//        console.log(asset_label_y);
        
        // Print labels
        print_labels();

         
         $('#lang button').hammer().on("tap", function() {
             
            $(this).parent().children('button').toggleClass('btn-default btn-primary');
            $(this).parent().children('button').removeClass("hidden");
             $(this).addClass("hidden");
            var lang_code=$('#lang button.btn-primary').val();
             
            Globalize.culture(lang_code);
            $("[data-localize]").each(function() {
                $(this).html(Globalize.localize($(this).attr('data-localize'),Globalize.culture())); 
            });
             
            display_legends();
            
            create_bubbles(svg);
             
            setImageUrl(lang_code);
             
            d3.selectAll('svg .y.axis text').each(function () {
                var thisAsset = d3.select(this);               
//                if (thisAsset.classed("hidden")) return true;
                var thisValue = thisAsset.text();   
                thisAsset.text(format_y_ticks(getNumber(thisValue)));                
            });
            
            //final values
            d3.selectAll('svg #asset_labels text.asset_label').each(function () {
                var thisAsset = d3.select(this);               
//                if (thisAsset.classed("hidden")) return true;
                var thisValue = thisAsset.text();   
                thisAsset.text(format_y_ticks(getNumber(thisValue)));                
            });
             
            $("[data-localize-placeholder]").each(function() {
                $(this).attr('placeholder', Globalize.localize( $(this).attr('data-localize-placeholder'), Globalize.culture()) );
            });
             
            $("[data-localize-month]").each(function() { 
                $(this).html( Globalize.culture().calendars.standard.months.names[(parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true)); 
            });
             
            d3.selectAll('svg #f_events_cont text').each(function (d) {
//                if (thisEvent.classed("hidden")) return true;
                d3.select(this).text(Globalize.localize($(this).attr("event-code"), Globalize.culture()));
            });
            
            d3.selectAll('svg #customize').text(Globalize.localize("customize", Globalize.culture()));
            
             
           
             
            var bnw_data = JSON.parse(window.localStorage.getItem("bnw_data"));
            var temp_str = '';
             
            for (var bnw_key in bnw_data) {
                var element = $('.bnw-footer button[value="' + bnw_key + '"]');
                if (bnw_key == "alltime") {
                    element.text(Globalize.localize(bnw_key, Globalize.culture()));
                } else if (bnw_key != "extent") {                
                    element.text(sprintf(Globalize.localize('last_years', Globalize.culture()), parseInt(bnw_key)));
                }
            }
 
        });
 
      

        function print_labels(){
            for (var i = 0; i < asset_label_y.length; i++) {
//                if (asset_label_y[i][0]!=="inflation" && $.inArray(asset_label_y[i][0], assets_to_show) == -1) continue;
                var yPos = yScale(asset_label_y[i][1]); 
//                var yPosPrev = undefined;
//                if (yScale(asset_label_y[i - 1])){
//                    yPosPrev = yScale(asset_label_y[i - 1][1]);
//                }                    
//                if (i > 0 && label_offset !== undefined && yPos < yPosPrev + label_offset + 20) {
//                    console.log( yPosPrev);
//                    var label_offset = 20 - (yPos - yPosPrev) + label_offset;
//                } else if (i > 0 && yPos < yPosPrev + 20) {
//                    var label_offset = 20 - (yPos - yPosPrev);
//                } else {
//                    var label_offset = 5;
//                }
                label_offset=4;//tries to center the number in relation to its line.

                asset_labels.append("text")
                    .attr("class", "asset_label label-" + asset_label_y[i][0])
                    .attr("x", cont_w - margin.right + 10)
                    .attr("y", yPos + label_offset)
                    .on("mouseover", function (d) {
                        highLightAsset($(this), true);
                    })
                    .on("mouseleave", function (d) {
                        highLightAsset($(this), false);
                    })
                    .text(format_y_ticks(asset_label_y[i][1]));

            }
           
           
        }
        
        $(".asset_line") .on("mouseover", function (d) {
            highLightAsset($(this), true);
        })
        .on("mouseleave", function (d) {
            highLightAsset($(this), false);
        });



        // ASSET LABEL LEGEND
        asset_labels_legend = svg.append('g').attr('id', 'asset_labels_legend');

        asset_labels_legend.append('rect').attr('x', 120).attr('y', yPosForLegends - 5).attr('width', 225).attr('height', 175).attr('rx', 10).attr('ry', 10).attr('fill', '#f4f4f4').attr('stroke', '#ccc').attr('stroke-width', '0.5');

        display_legends(assets_to_show);


        // ZOOM HANDLING
        var anim_handler = null;
        // Activate frame rate detection for optimal rendering
        function zoom_start() {}

        function zoom_stop() {
            cancelAnimationFrame(anim_handler);
            anim_handler = null;
        }

        // Turn on asset class buttons accordingly
        display_assets();

        


        function render_events(all_events) {
         
            $('#event_cont').empty();

            var color_class = 'lblue';
            var temp_str = '';

            for (var key in all_events) {
                var this_event = all_events[key];
                if (this_event.event_type != 'me') {
                    temp_str += '<div class="event_list_item event_' + key + '">';
                    temp_str += '<span class="icon event_' + this_event.event_type + ' icon-30 fill-' + color_class + '"></span>';
                    temp_str += '<div class="event_list_item_text">';
                    temp_str += '<p>' + Globalize.localize('event_' + this_event.event_type, Globalize.culture()) + '</p>';
                    temp_str += '<p>' + Globalize.culture().calendars.standard.months.names[parseInt(this_event.month - 1)].capitalize(true) + ' ' + this_event.year + '</p>';
                    temp_str += '</div>';
                    temp_str += '<button type="button" class="btn delete_event" data-delete="' + key + '">&times;</button>';
                    temp_str += '</div>';
                }
            }
            if (all_events.length === 1) {
                $('#event_list').html('<p data-localize="no_events">' + Globalize.localize('no_events', Globalize.culture()) + '</p>');
            } else {
                $('#event_menu #event_list').html(temp_str);
            }

            // Add to graph
            for (var key in all_events) {
                var this_event = all_events[key];
                if (this_event.event_type == 'me') {
                    var event_line_x = 0.5;
                    var event_icon_x = xScale(date_format.parse(this_event.year + '-' + this_event.month + '-15'));
                } else {
                    var event_line_x = event_icon_w / 2;
                    var event_icon_x = xScale(date_format.parse(this_event.year + '-' + this_event.month + '-15')) - (event_icon_w / 2);
                }

            }


            $('#event_list button.btn.delete_event').hammer().on("tap", function () {
                all_events.splice($(this).attr('data-delete'), 1);
                d3.select('#event_cont .graphed_event.event_' + $(this).attr('data-delete'))
                render_events(all_events);
            });
        }
        render_events(all_events);

        function save_chart(buttonIndex) {
            if (buttonIndex == 1) { // device
                window.canvas2ImagePlugin.saveImageDataToLibrary(
                    function (msg) {
                        navigator.notification.alert(Globalize.localize('image_good', Globalize.culture()), null, Globalize.localize('lifeline', Globalize.culture()), 'OK');
                    },
                    function (err) {
                        navigator.notification.alert(Globalize.localize('image_bad', Globalize.culture()), null, Globalize.localize('lifeline', Globalize.culture()), 'OK');
                    },
                    document.getElementById('lifeline_canvas')
                );
 
            } else { // email
                window.plugins.socialsharing.share(null, null, document.getElementById('lifeline_canvas').toDataURL(), null);
            }
        }

        // ANIMATIONS
 
        var graph_clip_witdh = cont_w - margin.right - margin.left;
        graph_clip.attr("width", 0).transition().duration(1500).attr("width", graph_clip_witdh);
        
        create_bubbles(svg);
    }
     function getNumber(value){
        return parseFloat(value.replace(/[^0-9.]/g, ""));
    } 

     function highLightAsset($this, highLight, assetCode) {
            var assetCode = !assetCode?$this.attr("class").split("-")[1]:assetCode;
            var assetText = d3.selectAll('svg text.asset_label.label-' + assetCode);
            var assetLine = d3.selectAll('svg path.asset_line.line-' + assetCode);
            if (highLight) {
                assetText.style('font-weight', 'bolder');
                assetText.style('font-size', '0.95em');
                assetLine.style('stroke-width', '3px');
            } else {
                assetText.style('font-weight', 'normal');
                assetText.style('font-size', '0.8em');
                assetLine.style('stroke-width', '1.5px');
            }



        }
    
       
    
    function display_legends(assets_to_show) {
        
        var assets_to_show = window.localStorage.getItem("default_assets").split(',');
        $("#asset_legends").remove();
        $("#id_inflation").remove();
        $("#customize").remove();
        
        var i = yPosForLegends;
        var assetList = assets_to_show;
//        if (assetList[assetList.length-1]!=="inflation"){
//            assetList[assetList.length-1]="inflation";
//        }
        asset_labels_legend.append('g').attr('id', 'asset_legends')
            .selectAll('text').data(assetList).enter().append('text')
            .attr('class', function (d) {
                return "asset_label label-" + d
            })            
            .attr("x", 135).attr("y", function (d) {
                i += 22;
                return i;
            })
            .on("mouseover", function (d) {
                highLightAsset($(this), true);
            })
            .on("mouseleave", function (d) {
                highLightAsset($(this), false);
            })
            .text(function (d) {
           
                return Globalize.localize(d, Globalize.culture())
            });
        
        if (assetList[assetList.length-1]!=="inflation"){
        
            asset_labels_legend.append('g').attr('id', 'id_inflation')
                .selectAll('text').data(["inflation"]).enter().append('text')
                .attr('class', function (d) {
                    return "asset_label label-" + d
                })
                .attr("x", 135).attr("y", function (d) {
                    i += 22;
                    return i;
                })
                .on("mouseover", function (d) {
                    highLightAsset($(this), true);
                })
                .on("mouseleave", function (d) {
                    highLightAsset($(this), false);
                })
                .text(function (d) {
                    return Globalize.localize(d, Globalize.culture())
                });
            
        }
         
        asset_labels_legend.append('text')
            .attr('id', 'customize')
            .attr('fill', '#0000EE')
            .attr('class', function (d) {
                return "asset_label label-" + d
            })
            .style('font-size', '0.75em')
            .attr("x", 260)
            .attr("y", function (d) {
            
                if (assets_to_show.length>5){
                    i += 6;
                }else{
                    i += 22;
                }
                return i;
            })
            .on("mouseover", function (d) {
                d3.select(this).style("cursor", "pointer"); 
            })
            .on("mouseleave", function (d) {
                d3.select(this).style("cursor", "default"); 
            })
            .text(function (d) {
                return Globalize.localize("customize", Globalize.culture());
            }) 
            .attr("data-toggle","modal")
            .attr("data-target","#asset_menu")

    }

    function display_assets() {
        var assets_to_show = window.localStorage.getItem("default_assets").split(',');
        $('#asset_menu .modal-body button.btn-asset').each(function () {
            var thisVal = $(this).val();
            if ($.inArray(thisVal, assets_to_show) !== -1 || thisVal == "inflation") { //Activates the asset
                $('.btn-asset').filter(function () {
                    if (this.value == thisVal) {
                        $(this).removeClass("btn-default").addClass("btn-primary");
                    }
                });
                d3.selectAll('svg path.asset_line.line-' + thisVal).classed('hidden', false);
                d3.selectAll('svg text.asset_label.label-' + thisVal).classed('hidden', false);
                $('#bnw_modal .' + thisVal).removeClass('hidden');
            } else {
                $(this).removeClass("btn-primary").addClass("btn-default");
                d3.selectAll('svg path.asset_line.line-' + thisVal).classed('hidden', true);
                d3.selectAll('svg text.asset_label.label-' + thisVal).classed('hidden', true);
                $('#bnw_modal .' + thisVal).addClass('hidden');
            }
        });
    }

    function normalize_data(input_data, norm_date) {
        //    console.log(norm_date); 
        //    console.log(input_data);
        var output_data = [];
        var j = 0;
        for (var i = get_node_with_date(norm_date, squiggly_data[0].date); i < input_data.length; i++) {
            output_data[j] = {};
            for (var node in input_data[i]) {
                if (node != 'date') {
                    output_data[j][node] = Math.round((input_data[i][node] / input_data[get_node_with_date(norm_date, squiggly_data[0].date)][node]) * mmult * 100) / 100;
                    // Save min value
                    if (output_data[j].max === undefined || output_data[j][node] > output_data[j].max) {
                        output_data[j].max = output_data[j][node];
                    }

                    // Save max value
                    if (output_data[j].min === undefined || output_data[j][node] < output_data[j].min) {
                        output_data[j].min = output_data[j][node];
                    }
                } else {
                    output_data[j][node] = input_data[i][node];
                }
            }
            j += 1;
        }
        //console.log(output_data);
        return output_data;
    }

    function get_node_with_date(date, first_date) {
        // Looks for a node with a matching year and month in the 'date' field
        var data_freq = window.localStorage.getItem("data_freq");
        var date_arr = date.split("-");
        var f_date_arr = first_date.split("-");
        var year_diff = parseInt(date_arr[0]) - parseInt(f_date_arr[0]);

        if (data_freq == "q") {
            var month_diff = Math.ceil((parseInt(date_arr[1]) - parseInt(f_date_arr[1])) / 3);
            return (year_diff * 4) + month_diff;
        } else {
            var month_diff = parseInt(date_arr[1]) - parseInt(f_date_arr[1]);
            return (year_diff * 12) + month_diff;
        }
    }

    function days_in_month(month, year) {
        var isLeapYear = (year % 4) || ((year % 100 === 0) && (year % 400)) ? 0 : 1;
        return (month === 2) ? (28 + isLeapYear) : 31 - (month - 1) % 7 % 2;
    }

    function format_y_ticks(d) {
        return Globalize.format(d, "c0");
    }

    function format_percentage(d){
        d=parseFloat(d/100);
        return Globalize.format(d, "p1");  
    }

    function limit_decimals(num) {
        return (Math.round(num * 10000) / 10000);
    }

    function round_prec(num, type, precision) {
        precision = typeof precision !== 'undefined' ? precision : 0;
        num = parseFloat(num);

        if (precision !== 0) {
            num = num * Math.pow(10, precision);
        }

        if (type == 'ceil') {
            num = Math.ceil(num);
        } else if (type == 'floor') {
            num = Math.floor(num);
        } else {
            num = Math.round(num);
        }

        if (precision !== 0) {
            num = num / Math.pow(10, precision);
        }
        if (num == -0) {
            num = 0;
        }
        return num;
    }

    function y_tick_values(scale, num_ticks) {
        var min_exp = 3;
        var max = round_prec(scale.domain()[1], 'floor', -4);
        var max_exp = log10(max);
        var exp_div = (max_exp - min_exp) / (num_ticks - 1);

        var ticks = [];
        for (var i = min_exp; i < max_exp; i += exp_div) {
            var exp = -Math.round(Math.pow(10, i)).toString().length + 2;
            ticks.push(round_prec(Math.pow(10, i), 'ceil', exp));
        }
        if (ticks.length != num_ticks) {
            ticks.push(max);
        }

        return ticks;
    }

    function log10(val) {
        return Math.log(val) / Math.LN10;
    }


    /* PHP JS */
    // sprintf
    function sprintf() {
        var e = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
        var t = arguments,
            n = 0,
            r = t[n++];
        var i = function (e, t, n, r) {
            if (!n) {
                n = " "
            }
            var i = e.length >= t ? "" : Array(1 + t - e.length >>> 0).join(n);
            return r ? e + i : i + e
        };
        var s = function (e, t, n, r, s, o) {
            var u = r - e.length;
            if (u > 0) {
                if (n || !s) {
                    e = i(e, r, o, n)
                } else {
                    e = e.slice(0, t.length) + i("", u, "0", true) + e.slice(t.length)
                }
            }
            return e
        };
        var o = function (e, t, n, r, o, u, a) {
            var f = e >>> 0;
            n = n && f && {
                2: "0b",
                8: "0",
                16: "0x"
            }[t] || "";
            e = n + i(f.toString(t), u || 0, "0", false);
            return s(e, n, r, o, a)
        };
        var u = function (e, t, n, r, i, o) {
            if (r != null) {
                e = e.slice(0, r)
            }
            return s(e, "", t, n, i, o)
        };
        var a = function (e, r, a, f, l, c, h) {
            var p;
            var d;
            var v;
            var m;
            var g;
            if (e === "%%") {
                return "%"
            }
            var y = false,
                b = "",
                w = false,
                E = false,
                S = " ";
            var x = a.length;
            for (var T = 0; a && T < x; T++) {
                switch (a.charAt(T)) {
                    case " ":
                        b = " ";
                        break;
                    case "+":
                        b = "+";
                        break;
                    case "-":
                        y = true;
                        break;
                    case "'":
                        S = a.charAt(T + 1);
                        break;
                    case "0":
                        w = true;
                        break;
                    case "#":
                        E = true;
                        break
                }
            }
            if (!f) {
                f = 0
            } else if (f === "*") {
                f = +t[n++]
            } else if (f.charAt(0) == "*") {
                f = +t[f.slice(1, -1)]
            } else {
                f = +f
            }
            if (f < 0) {
                f = -f;
                y = true
            }
            if (!isFinite(f)) {
                throw new Error("sprintf: (minimum-)width must be finite")
            }
            if (!c) {
                c = "fFeE".indexOf(h) > -1 ? 6 : h === "d" ? 0 : undefined
            } else if (c === "*") {
                c = +t[n++]
            } else if (c.charAt(0) == "*") {
                c = +t[c.slice(1, -1)]
            } else {
                c = +c
            }
            g = r ? t[r.slice(0, -1)] : t[n++];
            switch (h) {
                case "s":
                    return u(String(g), y, f, c, w, S);
                case "c":
                    return u(String.fromCharCode(+g), y, f, c, w);
                case "b":
                    return o(g, 2, E, y, f, c, w);
                case "o":
                    return o(g, 8, E, y, f, c, w);
                case "x":
                    return o(g, 16, E, y, f, c, w);
                case "X":
                    return o(g, 16, E, y, f, c, w).toUpperCase();
                case "u":
                    return o(g, 10, E, y, f, c, w);
                case "i":
                case "d":
                    p = +g || 0;
                    p = Math.round(p - p % 1);
                    d = p < 0 ? "-" : b;
                    g = d + i(String(Math.abs(p)), c, "0", false);
                    return s(g, d, y, f, w);
                case "e":
                case "E":
                case "f":
                case "F":
                case "g":
                case "G":
                    p = +g;
                    d = p < 0 ? "-" : b;
                    v = ["toExponential", "toFixed", "toPrecision"]["efg".indexOf(h.toLowerCase())];
                    m = ["toString", "toUpperCase"]["eEfFgG".indexOf(h) % 2];
                    g = d + Math.abs(p)[v](c);
                    return s(g, d, y, f, w)[m]();
                default:
                    return e
            }
        };
        return r.replace(e, a)
    }

    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
    (function () {
        var e = 0;
        var t = ["ms", "moz", "webkit", "o"];
        for (var n = 0; n < t.length && !window.requestAnimationFrame; ++n) {
            window.requestAnimationFrame = window[t[n] + "RequestAnimationFrame"];
            window.cancelAnimationFrame = window[t[n] + "CancelAnimationFrame"] || window[t[n] + "CancelRequestAnimationFrame"]
        }
        if (!window.requestAnimationFrame) window.requestAnimationFrame = function (t, n) {
            var r = (new Date).getTime();
            var i = Math.max(0, 16 - (r - e));
            var s = window.setTimeout(function () {
                t(r + i)
            }, i);
            e = r + i;
            return s
        };
        if (!window.cancelAnimationFrame) window.cancelAnimationFrame = function (e) {
            clearTimeout(e)
        }
    })();
    
//});