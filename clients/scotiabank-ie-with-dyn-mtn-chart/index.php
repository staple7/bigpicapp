<!DOCTYPE html>
<!--
    CLIENT.HTML
    This is the page where the client inputs his/her information.
-->

<?php
$app_version = $_COOKIE["app_version"];

?>

<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" /> 
        <link rel="stylesheet" href="css/bootstrap.min.css?v<?php echo $app_version;?>"/>
        <link rel="stylesheet" href="css/general.css?v<?php echo $app_version;?>"/>
        <link rel="stylesheet" href="css/icons.css?v<?php echo $app_version;?>"/>
        
        <link rel="stylesheet" href="css/graph.css?v<?php echo $app_version;?>" />
        <script src="js/vendor/d3.v3.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery-2.0.3.min.js"></script>
        <script src="js/vendor/jquery.cookie.js"></script>        
        <script type="text/javascript" src="js/vendor/bootstrap.min.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/jquery.hammer.min.js?v<?php echo $app_version;?>"></script>    
        <script type="text/javascript" src="js/vendor/jquery.mmenu_4_0_3.min.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/globalize.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/cultures/globalize.culture.en.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/cultures/globalize.culture.fr.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/data.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/data_update.js?v<?php echo $app_version;?>"></script>
        <script>
                // Handles back key to exit the app every time
        function backKeyHandler(e){
            e.preventDefault();
            navigator.app.exitApp();
        }

        // Capitalize
        if (!String.prototype.capitalize) {
            String.prototype.capitalize = function(lower) {
                return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
            };
        }

        </script>

        
        <title>The Big Picture</title>
        <style>
        .onepop {
            width: 80%;
            height: 46%;
            margin: 1% auto;
            background-color: #fff;
/*            padding: 10px 10px;*/
            text-align: left;
            position: relative;
            border-radius: 4px;
            
        }
            
        #popupOverlay {
            position: fixed;
            display: none;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba( 0,0,0, 0.7 );
            text-align: center;
            z-index: 500;
        }
            
        </style>
        
        <script>
            $.cookie("app_version", "<?php echo $app_version;?>" , { path: '/' });
        </script>
    </head>
    
    <body>
        
         <div class="loaderPageGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>
        

        <script>
            setTimeout(function(){
                $(".loaderPageGraphic").css("display", "none");
            },0);
        </script>
               

        <script type="text/javascript">
            var isMobile = false;
            ///Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)?isMobile=true:isMobile=false;
            if ($(document).width() < 800){
                isMobile=true;
            }
            if (isMobile){
                $("#popupOverlay").css("display", "block");
            }
        </script>

        
        
    <div id="parent">

        <div class="header text-center show">

            <div class="row" style="background:#E0E0E0;border-top: 1px solid #BDBDBD;border-bottom: 1px solid #BDBDBD;">
                <div class="col-12">

                    <button type="button" id="asset_classes" class="btn btn-icon pull-left" data-toggle="modal" data-target="#asset_menu" style="margin-left:17px"><div class="icon icon_assets icon-30" style="fill:#01579B"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 100"><path d="M45.877 42.361l-17.636 28.657-1.936-1.189 18.727-30.434 13.734 6.866 15.336-19.716 1.795 1.393-16.482 21.191-13.539-6.768zm-10.566 8.527l-7.764-1.991-.55 2.205 7.091 1.773 1.223-1.986zm23.768 13.534l-12.898-10.75-3.955-1.014-1.2 1.95 3.9.975 14.175 11.811 16.636-14.259-1.477-1.727-15.182 13.014z"></path></svg></div>
                    </button>
                    <button type="button" id='bnw' class="btn btn-icon pull-left" data-toggle="modal" data-target="#bnw_modal"><div class="icon icon_bnw icon-30 fill-lgray"></div></button>

                    <button type="button" id='bnw' class="btn btn-icont" data-toggle="modal" data-target="#chart_1" data-localize="time_and_risk"></button>


                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_2" data-localize="bulls_and_bears"></button>

                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_3" data-localize="matrix"></button>

                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_4" data-localize="diversification"></button>

                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_5" data-localize="downturns"></button>

                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_6" data-localize="rebalancing"></button>

                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_7" data-localize="stocks_vs_port"></button>

                    <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#chart_8" data-localize="gics"></button>


                </div>
            </div>

<!--            <div class="row" style="background:#eee">-->
            <div class="row">
                <div class="col-12">

                    <button type="button" id='graph_1' class="btn btn-icon thick" data-localize="cad_usd_inflation_chart"></button>

                    <button type="button" id='graph_2' class="btn btn-icon" data-localize="exchange_rate_chart"></button>


                </div>
            </div>



        </div>


        <!-- /#header -->




        <div id="main">





        </div>

        <button type="button" id='info' class="btn btn-icon" data-toggle="modal" data-target="#disclaimer_modal"><div class="icon icon_info icon-30 fill-mgray"></div></button>

        <div class="modal fade" id="bnw_modal" tabindex="-1" role="dialog" aria-labelledby="bnw_modal" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                            <h4 class="modal-title" data-localize-modal="best_n_worst_years"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="div-top"></div>
                            <div class="div-bar"></div>
                            <div class="div-bottom"></div>
                            <div class="div-temp">
                                <div class="bar-cont us_stocks" data-localize-modal="us_stocks_short"></div>
                                <div class="bar-cont cdn_stocks" data-localize-modal="cdn_stocks_short"></div>
                                <div class="bar-cont intl_stocks" data-localize-modal="intl_stocks_short"></div>
                                <div class="bar-cont cdn_bonds" data-localize-modal="cdn_bonds_short"></div>
                                <div class="bar-cont moderate_portfolio" data-localize-modal="moderate_portfolio"></div>
                                <div class="bar-cont cdn_tbills" data-localize-modal="cdn_tbills"></div>
                            </div>
                            <div class='footnote'>
                                <p data-localize-modal="bnw_footnote1"></p>
                                <p data-localize-modal="bnw_footnote2"></p>
                            </div>
                            <div class='example'>
                                <div class="ex-top">
                                    <p class='tag' data-localize-modal="best"></p>
                                    <div class="ex-bar-bar ex-1yr"><p class="text-muted" data-localize-modal="1yr"></p></div>
                                    <div class="ex-bar-bar ex-5yr"><p class="text-muted" data-localize-modal="5yr"></p></div>
                                </div>
                                <div class="ex-bar" data-localize-modal="asset_class"></div>
                                <div class="ex-bottom">
                                    <p class='tag' data-localize-modal="worst"></p>
                                    <div class="ex-bar-bar ex-1yr"><p class="text-muted" data-localize-modal="1yr"></p></div>
                                    <div class="ex-bar-bar ex-5yr"><p class="text-muted" data-localize-modal="5yr"></p></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        <!-- /.modal -->

        <!-- /.modal -->

    </div>


    <div class="modal fade" id="disclaimer_modal" tabindex="-1" role="dialog" aria-labelledby="disclaimer_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <p>
                        <label class="" data-localize="disclaimer_t1"></label>
                    </p>
                    <p>
                        <label class="" data-localize="disclaimer_t2"></label>
                    </p>
                    <p>
                        <label class="" data-localize="disclaimer_t3"></label>
                    </p>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="asset_menu" tabindex="-1" role="dialog" aria-labelledby="asset_menu" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize="assets_to_be_shown"></label>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-xs-4">
                            <label class="title" style="margin-bottom:15px" data-localize="equities"></label>



                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="cdn_stocks"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_total_equity_mkt"></button>-->

                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_stocks"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_mid_cap"></button>-->
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_small_cap"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_micro_cap"></button>-->
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="intl_stocks"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="emerging_mkt_stock"></button>-->


                        </div>

                        <div class="col-xs-4">
                            <label class="title" style="margin-bottom:15px" data-localize="bonds_and_gold"></label>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="cdn_bonds"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_bonds"></button>-->

                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="cdn_tbills"></button>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="gold"></button>
                        </div>

                        <div class="col-xs-4">
                            <label class="title" style="margin-bottom:15px" data-localize="neutral_model_portfolios"></label>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="mg_portfolio"></button>-->
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="aggresive_portfolio"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="bg_portfolio"></button>-->
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="moderate_portfolio"></button>
                            <!--                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="bi_portfolio"></button>-->
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="conservative_portfolio"></button>




                        </div>


                    </div>

                    <div class="row text-center" style="margin-top:20px">
                        <label class="title" data-localize="historical_monthly_data" style="color:#78909C"></label>
                    </div>

                    <div class="assets-modal-footer text-center" style="visibility:hidden;margin-top:20px">
                        <span data-localize="max_assets_selected" style="color:#d50000"></span>
                    </div>
                    
                    <div id="birth_date" class="row">
                        <div class="col-xs-6">
                            <select id="month" class="form-control input-lg"></select>
                        </div>
                        <div class="col-xs-6">
                            <select id='year' class="form-control input-lg"></select>
                        </div>
                    </div>


                    <div id="lang" class="text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default" value='en'>English</button>
                            <button type="button" class="btn btn-default" value='fr'>Français</button>
                        </div>
                    </div>
                    
                </div>
                <!-- /.modal-content -->


            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>


    <div class="modal fade" id="chart_1" tabindex="-1" role="dialog" aria-labelledby="chart_1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/time-and-risk.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chart_2" tabindex="-1" role="dialog" aria-labelledby="chart_2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/bulls-and-bears.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chart_3" tabindex="-1" role="dialog" aria-labelledby="chart_3" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/chasing-performance.jpg">
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="chart_4" tabindex="-1" role="dialog" aria-labelledby="chart_4" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/diversification.jpg">
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="chart_5" tabindex="-1" role="dialog" aria-labelledby="chart_5" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/downturns-and-recoveries.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chart_6" tabindex="-1" role="dialog" aria-labelledby="chart_6" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/rebalancing-act.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chart_7" tabindex="-1" role="dialog" aria-labelledby="chart_7" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/stocks-vs-balanced-portfolio.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chart_8" tabindex="-1" role="dialog" aria-labelledby="chart_8" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-lgray"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:800px" src="/graphGallery/images/en/gic-returns.jpg">
                </div>
            </div>
        </div>
    </div>


        
    </body>
</html>

<script>
    
$(function() {
//	check_network();

	// SESSION DATA PERSISTENCE
	var bd_year = parseInt(window.localStorage.getItem('bd_year'));
	var bd_month = parseInt(window.localStorage.getItem('bd_month'));
	var f_name = window.localStorage.getItem("f_name");
	var temp_lang = window.localStorage.getItem("temp_lang");

	if (f_name !== undefined) { $('#about_you #f_name input#first_name').val(f_name) };

	// FILL YEAR FIELD
	// Every year from 1935 to 15 years ago
	var year_offset = 15;
	var latest_date = JSON.parse(window.localStorage.getItem('data'));
	latest_date = latest_date[latest_date.length-1].date;
	var latest_year = parseInt(latest_date.substr(0,4));
	var latest_month = parseInt(latest_date.substr(5,2));
	
	var temp_str = '';
	for (var i = 1935; i <= latest_year - year_offset; i++) {
		if (bd_year !== undefined && i == bd_year) {
			temp_str += "<option value='"+i+"' selected='selected'>"+i+"</option>";
		} else {
			temp_str += "<option value='"+i+"'>"+i+"</option>";
		}
	}
	$('#birth_date #year').html( temp_str );

	var temp_str = '';
	for (var i = 1; i <= 12; i++) {
		if (bd_month !== undefined && i == bd_month) {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
		} else {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
		}
	}
	$('#birth_date #month').html( temp_str );
	$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });

	$('#birth_date #year').change(function() {
		var curr_sel_month = $('#birth_date #month').val();
		if (parseInt($(this).val()) == latest_year - year_offset && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= latest_month; i++) {
				if ((curr_sel_month > latest_month && i == latest_month) || (curr_sel_month <= latest_month && i == curr_sel_month)) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		} else if ($('#event_menu .content select#month option').length < 12 && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= 12; i++) {
				if (i == curr_sel_month) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		}
		$('#birth_date #month').html( temp_str );
		$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });
	});
	
	// Hide keyboard fix
	var last_selected = null; 
	$('select').focus(function(){
		if (last_selected.toLowerCase() == 'input') {
			$('input').focus();
			if (last_selected.toLowerCase() !== 'select') {
				last_selected = this.tagName;
				$(this).focus();
			}
		}
	});
	$('input').focus(function () { last_selected = this.tagName; });

	//Lang
	$('#lang button').each(function() {
		if (temp_lang !== undefined && temp_lang !== null) {
			if (temp_lang == $(this).val()) {
				$(this).toggleClass('btn-default btn-primary');
			}
		} else if (window.localStorage.getItem("lang") == $(this).val()) {
			$(this).toggleClass('btn-default btn-primary');
		}
	});

		window.localStorage.setItem("f_name", $('#about_you #f_name input#first_name').val());
		window.localStorage.setItem("bd_month", $('#birth_date select#month').val());
		window.localStorage.setItem("bd_year", $('#birth_date select#year').val());
		window.localStorage.setItem("temp_lang", $('#lang button.btn-primary').val());



});

 </script>

    
    <!-- D3 -->

    <!-- PHONEGAP -->
    <!--<script data-main="main" src="js/vendor/require.js"></script>-->
    <!-- II SCRIPTS -->
<!--    <script type="text/javascript" src="js/graph.js?v<?php echo $app_version;?>"></script>-->
   
 <script type="text/javascript">
            function get_localized(lang) {
                window.localStorage.setItem("lang", "en");
                Globalize.culture(lang);

                // Localization
                $("[data-localize]").each(function() { $(this).html(Globalize.localize($(this).attr('data-localize'),Globalize.culture())); });
            }

        
      
                window.localStorage.clear();
                
                // INITIALIZE PREFERENCES
                if (navigator.globalization !== undefined && navigator.globalization !== null) {
                    navigator.globalization.getPreferredLanguage(
                        // Checks for french. Else, uses English
                        function (language) { // Found a prefered language
                            if (language.value.toLowerCase().substr(0,2) == 'fr') {
                                get_localized('fr');
                            } else {
                                get_localized('en');
                            }
                        },
                        function () { get_localized('en'); }
                    );
                } else { get_localized('en'); }
               
                  window.localStorage.setItem("default_assets",["cdn_stocks","us_stocks","us_small_cap","intl_stocks","cdn_bonds","cdn_tbills"]);
                
                window.localStorage.setItem("default_h_events", "true");
                verify_asset_data(true);
                //console.log(window.localStorage.getItem("data"));

                // Handle back button
                document.addEventListener("backbutton", backKeyHandler, false);

          
        </script>

 <script type="text/javascript">
        

        var lang_code = window.localStorage.getItem("lang");

        var baseUrl = "/clients/scotiabank-ie/graphGallery/images/";

//        $(".logo").attr("src", "/clients/scotiabank-ie/img/" + lang_code + "-logo.png");

        $("#chart_1").children().find("img").attr("src", baseUrl + lang_code + "/time_and_risk.jpg");
        $("#chart_2").children().find("img").attr("src", baseUrl + lang_code + "/bulls_and_bears.jpg");
        $("#chart_3").children().find("img").attr("src", baseUrl + lang_code + "/matrix.jpg");
        $("#chart_4").children().find("img").attr("src", baseUrl + lang_code + "/diversification.jpg");
        $("#chart_5").children().find("img").attr("src", baseUrl + lang_code + "/downturns.jpg");
        $("#chart_6").children().find("img").attr("src", baseUrl + lang_code + "/rebalancing.jpg");
        $("#chart_7").children().find("img").attr("src", baseUrl + lang_code + "/stocks_vs_port.jpg");
        $("#chart_8").children().find("img").attr("src", baseUrl + lang_code + "/gics.jpg");
    </script>
<footer><script data-main="main" src="js/vendor/require.js"></script></footer>