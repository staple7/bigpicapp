var app_version;

requirejs.config({
    baseUrl: '/clients/scotiabank-ie',
    paths: {
        lodash: "/clients/scotiabank-ie/js/vendor/lodash",
        postal: "/clients/scotiabank-ie/js/vendor/postal"

    }
});

requirejs(['postal',
           'js/graph',
           'graphGallery/graphGalleryController'],

    function (
        postal,
         graph,
        graphGalleryController
    ) {

 

        var initData = {
            languageCode: "en",
            regionCode: "CA"
        };
    
        var initDate = window.localStorage.getItem('bd_month') + "/01/" + window.localStorage.getItem('bd_year');
    
        graphGalleryController.loadModule(initDate);

    }
);