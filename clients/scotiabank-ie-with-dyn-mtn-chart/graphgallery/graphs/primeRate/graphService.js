define(['postal', 'data/rawData/CA/primeRate', 'graphGallery/graphs/primeRate/messages'], 
    function(postal, primeRate, messages){

        var context;
    
        function graph(initDate){
            addAxis();
            
            console.log(initDate)
           
            calculateGraphDimensions();
            
            removeElements();

            var primeRateList = primeRate.DATA;

            var result = {};
            

             var userInitDate = new Date(initDate.initDate);
            
            result = calculateDataList(primeRateList, userInitDate);
            var primeRateDataList = result.primeRateDataList;

            var minXValue = 0;

//            graphWidth = d3.select(".x.axis").node().getBBox().width;
            minYValue = result.minYValue;
            maxYValue = result.maxYValue;
         
            minXValue = 0;
            maxXValue = primeRateDataList.length;
            
         
            
            yAxis = d3.svg.axis(); 

             x = d3.scale
                .linear()  
                .domain([minXValue, maxXValue])
                .range([0, graphWidth]);
 
//            var parseDate = d3.time.format("%m-%d-%Y").parse;
            
            var xPos = 0;
            
            y = d3.scale
                .linear()                   
                .domain([0, maxYValue])
                .range([graphHeight, 0]);
         
            var formatPercent = d3.format(".0%");

            
            yAxis.scale(y)
                .orient("left")
                .ticks(3)
                .tickFormat(formatPercent);

            svg.selectAll(".y_axis").call(yAxis);
       
            var valueline = d3.svg.line()
                .x(function(d) { return x(d.x); })
                .y(function(d) { return y(d.y); });
           
            
            var area = d3.svg.area()
                .x(function(d) { return x(d.x); })
                .y0(graphHeight)
                .y1(function(d) { return y(d.y); });
            
            
            svg.append("path")
               .data([primeRateDataList])
               .attr("class", "area")
                .style("fill", function (d,i) { return "#B0BEC5"; })
               .attr("d", area);
           
            svg.append("path")
              .attr("id","graphPath")
              .data([primeRateDataList])
                .attr("class", "line")
              .style("fill", "none")
              .attr("stroke-width", 2)
//              .attr("stroke", "#72635c")
              .attr("d", valueline);
            
//            .style("fill", function (d,i) { return "#007c88"; })
           
              require(['graphGallery/graphs/inflation/graphService'], 
                        function(graphService1){
                            graphService1.graph(userInitDate);
                });
            
            postal.channel().publish("graphService.graph.done");
        }
  
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
            return months <= 0 ? 1 : months;
        }
    
        function calculateGraphDimensions(){
      
            var yPos = -30;
            var w = d3.select(".x.axis").node().getBBox();
            graphWidth = w.width;
            xPos = w.x;
            graphHeight = ($(svgArea).height());   
            graphHeight=65;

         
           
            svg.attr("transform", "translate(108,170)");
        }
    
      
        function calculateDataList(primeRateList, userInitDate){
            
//            userInitDate = new Date("1985-01-30");
            var regionInitDate = new Date("1935-01-01");
            //            var primeRateDataList = [{x:0, y:0}];
            
            var primeRateDataList = [];
            var primeRateValue;
            var minYValue;
            var maxYValue;
            var index = 1;
            var monthIndex=monthDiff(regionInitDate, userInitDate)-1;
   
            for(; monthIndex<primeRateList.length; monthIndex += 1){
                primeRateValue = primeRateList[monthIndex];
                minYValue = (minYValue === undefined || primeRateValue < minYValue) ? primeRateValue : minYValue;                
                maxYValue = (maxYValue === undefined || primeRateValue > maxYValue) ? primeRateValue : maxYValue;
                primeRateDataList.push({x:index, y:primeRateValue});
                index++;
            }          
//           primeRateDataList.push({x:index, y:0});
//           console.log(primeRateDataList);
            return {primeRateDataList: primeRateDataList,
                    minYValue: minYValue,
                    maxYValue: maxYValue
                   };            
        }
    
        
    
        function setGraphTitles(investmentHorizon){
//            xAxisTitle.html(Handlebars.compile(context.xAxisTitle)({investmentHorizon:investmentHorizon}));       
            
//            var graphWidthLimit = 1359;
//            var documentWidth = $(document).width();
//            var graphSubtitle = context.graphSubtitle.normal;
//
//            if  (documentWidth<graphWidthLimit){
//                graphSubtitle = context.graphSubtitle.abbreviate
//            }
//            
//            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
//                                ({investmentHorizon:investmentHorizon,
//                                regionStartYear:localStorage.getItem("regionStartYear")}));
            
        }     
    

        function removeElements(){
//          alert(2)
            $("#graphPath").remove();
            $(".area").remove();
            $("#y2Axis").remove();
            $(".line1").remove();
            $("#graphBars").remove();
            
        }
    
        
        function finalize(){
            removeElements();
            localGraphData = null;
            context = null;

        }
    
        function branchChanged(){
         
            finalize();
        }
    
        function graphChanged(){            
            finalize();
        }
        
        function getGraphDefinition(){
//            return Handlebars.compile(context.graphDefinition)(localGraphData);
        }


        function removeAxis(){
//            svg.selectAll(".x.axis").remove();
//            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
             $(".x_axis").remove();
            $(".y_axis").remove();
            svg.append("g").attr("class", "x_axis");
            svg.append("g").attr("class", "y_axis");
        }
    
        function init(lang, branchCode){
           
            
//            calculateGraphDimensions();
//            periodicity = 1; 
          
            context = messages[branchCode]; 
//            y2AxisLabel = context.y2AxisTitle;
//            $(".graph_title").html(context.graphTitle);
            

            yAxisTitle.html("");
            postal.channel().publish("graphGallery.graph.initiated");
           
        }

        
        function getGraphElements(){
            
        }

    
        return {
            init:               init,
            graph:              graph,
            getGraphDefinition: getGraphDefinition,
            graphChanged:       graphChanged,
            getGraphElements:   getGraphElements,
            branchChanged:      branchChanged
        };
    
});     
                