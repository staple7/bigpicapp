define(['postal', 'data/rawData/CA/exchangeRate', 'graphGallery/graphs/exchangeRate/messages'], 
    function(postal, exchangeRate, messages){

        var context;
    calculateGraphDimensions();
   
        function graph(initDate){
          
             addAxis();
            
            removeElements();
      
            var exchangeRateList = exchangeRate.DATA;

            var result = {};
 
            var userInitDate = new Date(initDate.initDate);
          
            result = calculateDataList(exchangeRateList, userInitDate);
            var exchangeRateDataList = result.exchangeRateDataList;
            
            
            var minXValue = 0;

//            graphWidth = d3.select(".x.axis").node().getBBox().width;
            minYValue = result.minYValue;
            maxYValue = result.maxYValue;
         
            minXValue = 0;
            maxXValue = exchangeRateDataList.length;
            
           
            yAxis = d3.svg.axis(); 
 
             x = d3.scale
                .linear()  
                .domain([minXValue, maxXValue])
                .range([0, graphWidth]);

            var parseDate = d3.time.format("%m-%d-%Y").parse;
            
            var xPos = 0;
            
            var diff1 = Math.abs(maxYValue);
            var diff2 = Math.abs(minYValue);
            
            var maxDiff = diff1>diff2?diff1:diff2;
                        
            y = d3.scale
                .linear()                   
                .domain([maxDiff*-1, maxDiff])
                .range([graphHeight, 0]);
      
            yAxis.scale(y)
                .orient("left")
                .tickValues([maxDiff*-1, 0, maxDiff])
                .tickFormat(function(d, i){
                   if (i==1){
                       return (lang_code=="en")?"$1":"1 $";
                   }else{
                       return "";
                   }
                });

            svg.selectAll(".y_axis").call(yAxis);
            
       
             var valueline = d3.svg.line()
                .x(function(d) { return x(d.x); })
                .y(function(d) { return y(d.y); });
          
            svg.append("path")
              .attr("id","graphPath")
              .data([exchangeRateDataList])
              .attr("class", "line")
              .style("fill", function (d,i) { return "#4DB6AC"; })
              .attr("d", valueline);
            
            
           
            postal.channel().publish("graphService.graph.done");
        }
  
        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth();
            months += d2.getMonth();
            return months <= 0 ? 1 : months;
        }

      
        function calculateGraphDimensions(){
        var yPos = 50;
            var w = d3.select(".x.axis").node().getBBox();
            graphWidth = w.width;
            xPos = w.x;
//            graphWidth +=3;
           
            graphHeight = ($(svgArea).height());   
            graphHeight=65;
            if ($(document).width() <= 1799){
//                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
//                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            yPos = -30; 
           
            svg.attr("transform", "translate(108,170)");
        }
        
       
      
        function calculateDataList(exchangeRateList, userInitDate){
           
//            exchangeRateList.push(0);
            
//            userInitDate = new Date("1985-01-30");
            var regionInitDate = new Date("1935-01-01");
            var exchangeRateDataList = [{x:0, y:0}];
            var exchangeRateValue;
            var minYValue;
            var maxYValue;
            var index = 1;
           
            var monthIndex=monthDiff(regionInitDate, userInitDate)-1;
           
//            monthIndex=0;
            for(; monthIndex<exchangeRateList.length; monthIndex += 1){
                exchangeRateValue = exchangeRateList[monthIndex];
                minYValue = (minYValue === undefined || exchangeRateValue < minYValue) ? exchangeRateValue : minYValue;                
                maxYValue = (maxYValue === undefined || exchangeRateValue > maxYValue) ? exchangeRateValue : maxYValue;
                exchangeRateDataList.push({x:index, y:exchangeRateValue});
                index++;
            }
           exchangeRateDataList.push({x:index, y:0});
        
            return {exchangeRateDataList: exchangeRateDataList,
                    minYValue: minYValue,
                    maxYValue: maxYValue
                   };            
        }
    
        
        function setGraphTitles(investmentHorizon){
//            xAxisTitle.html(Handlebars.compile(context.xAxisTitle)({investmentHorizon:investmentHorizon}));       
            
//            var graphWidthLimit = 1359;
//            var documentWidth = $(document).width();
//            var graphSubtitle = context.graphSubtitle.normal;
//
//            if  (documentWidth<graphWidthLimit){
//                graphSubtitle = context.graphSubtitle.abbreviate
//            }
//            
//            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
//                                ({investmentHorizon:investmentHorizon,
//                                regionStartYear:localStorage.getItem("regionStartYear")}));
            
        }     
    

        function removeElements(){
            $("#graphPath").remove();
            $("#y2Axis").remove();
           
            $("#graphBars").remove();
//            $("#activeAreas").remove();
            
        }
    
        
        function finalize(){
            
            removeAxis();
            removeElements();
            localGraphData = null;
//            xAxisTitle.html("");
            context = null;
//            periodicity = null;
//            y2AxisLabel = null;
//            clearTimeout(timer);
//            timer = null;
        }
    
        function branchChanged(){
         
            finalize();
        }
    
        function graphChanged(){            
            finalize();
        }
        
        function getGraphDefinition(){
//            return Handlebars.compile(context.graphDefinition)(localGraphData);
        }


    
        function removeAxis(){
//            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            $(".x_axis").remove();
            $(".y_axis").remove();
            svg.append("g").attr("class", "x_axis");
            svg.append("g").attr("class", "y_axis");

        }
    
        function init(lang, branchCode){
            
           
            calculateGraphDimensions();
//            periodicity = 1; 
          
            context = messages[branchCode]; 
//            y2AxisLabel = context.y2AxisTitle;
//            $(".graph_title").html(context.graphTitle);
            

//            yAxisTitle.html("");
            postal.channel().publish("graphGallery.graph.initiated");
           
        }

        
        function getGraphElements(){
            
        }

    
        return {
            init:               init,
            graph:              graph,
            getGraphDefinition: getGraphDefinition,
            graphChanged:       graphChanged,
            getGraphElements:   getGraphElements,
            branchChanged:      branchChanged
        };
    
});     
                