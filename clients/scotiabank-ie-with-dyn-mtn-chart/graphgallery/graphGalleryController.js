define (['graphGallery/graphGalleryView', 
         'postal'], 

    function(graphGalleryView, postal){

        $('#graph_1').on("click", function () {
            if ($(this).hasClass("thick")) {return false};
            $(this).toggleClass("thick");
            $('#graph_2').toggleClass("thick");
            channel.publish("graphGallery.graph.changed", 0);
        });
    
        $('#graph_2').on("click", function () {
            if ($(this).hasClass("thick")) {return false};
            $(this).toggleClass("thick");
            $('#graph_1').toggleClass("thick");
            channel.publish("graphGallery.graph.changed", 1);
          
        });
    
    
        $('#graph_3').on("click", function () {
            channel.publish("graphGallery.graph.changed", 2);
          
        });
    
        var channel = postal.channel();    
        var graphData = {};
        var currentGraphCode;
        var graphTimer;
        var currentGraphService;
    
        var graphServiceCodeList = ["primeRate","exchangeRate"];
    
        channel.subscribe("graphGallery.graph.changed", function(graphId){     
            currentGraphService.graphChanged();
            initGraph(graphId)
        });
    
        channel.subscribe("graphGallery.graph.initiated", function(){
            currentGraphService.graph(graphData);
            channel.publish("inputController.calculate.output");
        });  
     
    
        function initGraph(graphId){
            clearTimeout(graphTimer);
            graphTimer = setTimeout(function(){
                currentGraphCode = graphServiceCodeList[graphId];
                //console.log('graphGallery/graphs/' + currentGraphCode + '/graphService');
                //console.log("currentGraphCode", currentGraphCode);
                require(['graphGallery/graphs/' + currentGraphCode + '/graphService'], 
                        function(graphService){
                            currentGraphService = graphService;   
                            channel.publish("graphGalleryController.current.graph", graphId);
                            currentGraphService.init(localStorage.getItem("lang"), graphData);
                });
            }, 300);
        }    

       
    
        function loadModule(initDate){
            graphData["initDate"] = initDate;
            graphGalleryView.start(initDate);
        };
    

        channel.subscribe("graphGalleryController.regraph", function(year){
           
            graphGalleryView.start();
            currentGraphService.graph({initDate:"1/01/"+year});
        });
    
      
        var rendered = channel.subscribe("graphGallery.template.rendered", function(){
            initGraph(0);            
            rendered.unsubscribe();
            rendered = null;
        });

        
        function showGraph(show){
            graphGalleryView.showAllGraphElements(show);
        }
    
        return {
            loadModule:          loadModule,
            showGraph:           showGraph
            
        };

    }
);
