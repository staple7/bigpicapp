/*
 * Globalize Culture fr
 *
 * http://github.com/jquery/globalize
 *
 * Copyright Software Freedom Conservancy, Inc.
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * This file was generated by the Globalize Culture Generator
 * Translation: bugs found in this file need to be fixed in the generator
 */

(function( window, undefined ) {

var Globalize;

if ( typeof require !== "undefined" &&
	typeof exports !== "undefined" &&
	typeof module !== "undefined" ) {
	// Assume CommonJS
	Globalize = require( "globalize" );
} else {
	// Global variable
	Globalize = window.Globalize;
}

Globalize.addCultureInfo( "fr", "default", {
	name: "fr",
	englishName: "French",
	nativeName: "français",
	language: "fr",
	numberFormat: {
		",": " ",
		".": ",",
		"NaN": "Non Numérique",
		negativeInfinity: "-Infini",
		positiveInfinity: "+Infini",
		percent: {
			",": " ",
			".": ","
		},
		currency: {
			pattern: ["(n $)","n $"],
			",": " ",
			".": ","
		}
	},
	calendars: {
		standard: {
			firstDay: 1,
			days: {
				names: ["dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"],
				namesAbbr: ["dim.","lun.","mar.","mer.","jeu.","ven.","sam."],
				namesShort: ["di","lu","ma","me","je","ve","sa"]
			},
			months: {
				names: ["janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre",""],
				namesAbbr: ["janv.","févr.","mars","avr.","mai","juin","juil.","août","sept.","oct.","nov.","déc.",""]
			},
			AM: null,
			PM: null,
			eras: [{"name":"ap. J.-C.","start":null,"offset":0}],
			patterns: {
				d: "dd/MM/yyyy",
				D: "dddd d MMMM yyyy",
				t: "HH:mm",
				T: "HH:mm:ss",
				f: "dddd d MMMM yyyy HH:mm",
				F: "dddd d MMMM yyyy HH:mm:ss",
				M: "d MMMM",
				Y: "MMMM yyyy"
			}
		}
	},
	messages: {
		"lifeline" : "Le Portrait Global",
		"welcome_message" : "Le Portrait Global personnalise le monde des placements en faisant le lien entre les tendances historiques des marchés et la vie de l’investisseur.  Cet outil vous aidera à faire en sorte que les clients se sentent plus concernés, à gérer les attentes, à expliquer les principes clés et à discuter de stratégies. Vous aurez ainsi plus de facilité à attirer de nouveaux clients et à les fidéliser.",
		"simple_title" : '<?xml version="1.0" encoding="UTF-8"?><svg width="267.97" height="37.133" version="1.1" viewBox="0 0 267.97333 37.133331" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><metadata><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/></cc:Work></rdf:RDF></metadata><defs><clipPath id="a"><path d="m0 278.51v-278.51h2009.8v278.51z"/></clipPath></defs><g transform="matrix(1.3333 0 0 -1.3333 0 37.133)"><g transform="scale(.1)"><g clip-path="url(#a)" fill="#ed192d"><path d="m222.73 139.15c0.972-47.621-42.082-83.554-78.039-83.554h-88.907l-55.781-55.598h143.72c45.867 0 89.882 41.316 89.882 90.574 0 14.485-1.742 33.324-10.878 47.617zm-15.254-35.082c5.535 9.922 8.257 20.813 8.933 31.399h-40.32c0-9.817-1.652-20.508-5.356-31.399zm-39.36-6.2106c-4.566-11.66-15.449-26.812-26.043-35.078 20.496 0 43.914 8.9492 61.035 35.078zm-4.566 6.2106c3.601 9.922 5.242 20.813 6.219 31.399h-61.028c0.969-10.586 2.629-21.477 6.223-31.399zm-55.586 0c-3.598 10.891-5.539 21.582-5.539 31.399h-40.324c0.9687-10.586 3.5976-21.477 8.9375-31.399zm31.387-34.976c7.965 7.1953 16.914 18.855 21.472 28.766h-43.144c4.582-9.9102 13.515-21.57 21.672-28.766zm-64.707 28.766c17.871-26.129 41.297-35.078 62.089-35.078-10.89 8.2656-22.453 23.418-26.144 35.078zm96.089 75.305c3.704-9.609 5.356-20.5 5.356-31.379h40.32c-0.676 10.879-3.398 21.77-8.933 31.379zm32.375 7.297c-17.121 25.066-40.539 34.98-61.035 34.98 10.594-7.968 21.477-23.125 26.043-34.98zm-88.144-7.297c-3.594-9.609-5.254-20.5-6.223-31.379h61.028c-0.977 10.879-2.618 21.77-6.219 31.379zm-43.926 0c-5.3399-9.609-7.9688-20.5-8.9375-31.379h40.324c0 10.879 1.941 21.77 5.539 31.379zm68.313 35.274c-8.157-7.188-17.09-18.067-21.672-27.977h43.144c-4.558 9.91-13.507 20.789-21.472 27.977zm-28.762-27.977c3.691 11.855 15.254 27.012 26.144 34.98-20.792 0-44.218-9.914-62.089-34.98zm-54.805-40.332c-8.9454 14.195-10.887 33.035-10.887 47.519 0 49.266 44.02 90.86 89.89 90.86h143.72l-55.781-56.262h-88.918c-35.754 0-78.993-36.25-78.028-82.117"/><path d="m1544.1 90.547c15.59 0 24.76 13.652 24.76 29.941 0 16.496-8.73 30.157-24.76 30.157-16.71 0-24.72-13.661-24.72-30.157 0-16.289 9.19-29.941 24.72-29.941zm0-27.59c-37.32 0-64.08 22.445-64.08 57.531 0 34.668 27.72 57.735 64.08 57.735 36.4 0 64.08-22.93 64.08-57.735 0-35.086-26.94-57.531-64.08-57.531"/><path d="m1733.3 231.25c-11.81 0-21.54-9.707-21.54-21.543 0-11.816 9.73-21.543 21.54-21.543 11.82 0 21.55 9.727 21.55 21.543 0 11.836-9.73 21.543-21.55 21.543zm-19.2-55.84h38.43v-110.33h-38.43v110.33"/><path d="m1470.2 173.79c-2.87 0.801-13.38 4.414-27.35 4.414-34.56 0-60.44-21.633-60.44-57.637 0-32.746 23.25-57.644 58.91-57.644 15.16 0 26.36 4.0508 28.88 4.8125l-1.86 32.148c-2.94-1.3281-10.58-5.7031-19.4-5.7031-15.47 0-26.79 10.949-26.79 26.386 0 16.18 12.08 26.622 27.22 26.622 8.53 0 16.15-3.965 18.97-5.379l1.86 31.98"/><path d="m1810.3 121.48c0-18.235 10.2-29.622 23.36-29.622 11.95 0 23.59 11.172 23.59 28.124 0 17.083-10.33 28.75-23.59 28.75-13.71 0-23.36-12.226-23.36-27.253zm84.77 54.011v-110.25h-37.26l-0.56 11.113h-0.81c-4.13-3.7305-12.73-13.566-36.75-13.566-26.67 0-49.98 23.164-49.98 57.688 0 32.011 20.71 57.578 52.43 57.578 21.59 0 30.43-9.5 33.71-12.539h0.99l0.39 9.98h37.84"/><path d="m1259.3 69.316c10.73-3.25 27.04-6.2187 44.29-6.2187 32.75 0 65.41 11.582 65.41 47.527 0 49.309-68.01 42.48-68.01 62.141 0 10.058 10.46 12.234 20.72 12.234 14.2 0 32.18-6.824 36.18-8.445l3.09 33.965c-9.93 3.378-24.99 5.3-39.14 5.3-32.31 0-64.19-12.617-64.19-44.621 0-48.457 67.8-38.347 67.8-63.777 0-9.7736-9.78-13.496-22.21-13.496-17.15 0-33.21 8.5742-40.18 11.66l-3.76-36.27"/><path d="m1695.4 95.281c-1.88-0.5235-7.04-1.8516-10.61-1.8516-7.79 0-16.28 3.6328-16.28 17.117v37.266h24.14v27.714h-24.14v35.508h-38.32v-35.508h-16.77v-27.714h16.77v-43.614c0-23.996 11.12-41.18 39.49-41.18 8.25 0 15.79 1.1211 20.99 2.0977l4.73 30.164"/><path d="m416.65 127.42h-18.035v-34.07h18.035c12.707 0 20.586 6.3554 20.586 17.019 0 10.442-6.851 17.051-20.586 17.051zm-1.777 56.672h-16.258v-29.746h16.258c12.199 0 17.797 7.117 17.797 15 0 8.132-5.09 14.746-17.797 14.746zm11.438-119.2h-67.356v147.66h69.145c30.996 0 45.488-18.301 45.488-38.117 0-17.539-12.199-27.2-18.051-31.524v-0.769c13.469-5.578 22.883-17.524 22.883-34.036 0-27.187-23.906-43.21-52.109-43.21"/><path d="m557.67 148.51c-13.723 0-23.383-12.207-23.383-27.453 0-18.047 10.168-29.485 23.383-29.485 11.941 0 23.641 11.196 23.641 28.215 0 17.024-10.418 28.723-23.641 28.723zm24.148-83.614-0.507 11.188h-0.762c-4.324-3.8242-12.969-13.726-36.856-13.726-26.699 0-50.07 23.387-50.07 57.684 0 32.277 20.59 57.695 52.356 57.695 21.855 0 30.488-9.406 33.796-12.453h1.02l0.516 9.914h37.863v-110.3h-37.356"/><path d="m726.45 64.894v56.922c0 14.239-7.371 22.618-20.34 22.618-11.179 0-20.066-9.141-20.066-22.618v-56.922h-37.363v110.05h33.543l0.507-12.207h0.762c4.578 4.317 15 15 34.563 15 29.746 0 45.75-21.347 45.75-46.511v-66.328h-37.356"/><path d="m846 148c-13.215 0-23.137-11.187-23.137-27.957 0-14.734 8.391-27.961 23.137-27.961 12.957 0 23.379 10.93 23.379 27.961 0 16.262-8.645 27.957-23.379 27.957zm22.617-128.61v53.379h-0.254c-2.793-1.5156-15.246-10.41-33.293-10.41-29.988 0-51.851 23.883-51.851 55.652 0 39.648 24.402 59.472 52.105 59.472 20.84 0 31.516-9.66 34.055-12.46h0.762l0.508 10.175h36.347v-155.81h-38.379"/><path d="m1016 64.894-0.5 12.195h-0.76c-4.57-4.0703-15.764-15-35.339-15-29.734 0-44.988 21.602-44.988 46.504v66.347h37.363v-56.933c0-13.977 7.372-22.364 20.332-22.364 11.182 0 20.082 9.1525 20.082 22.364v56.933h37.35v-110.05h-33.54"/><path d="m1129.3 153.07c-12.45 0-19.3-9.91-20.08-21.851h40.17c0 11.941-6.36 21.851-20.09 21.851zm-19.3-43.965c3.81-13.98 14.74-20.078 31.75-20.078 13.97 0 26.7 6.371 31.27 8.6445l2.8-27.703c-9.66-4.3204-23.64-7.6172-42.69-7.6172-36.61 0-62.79 23.387-62.79 57.938 0 35.32 24.91 57.441 59.22 57.441 40.66 0 55.92-30.246 55.92-61.757v-6.868h-75.48"/><path d="m1922.8 219.41h8.52l12.58-37.039 12.53 37.039h8.42v-43.809h-5.66v25.86c0 0.898 0.02 2.375 0.04 4.445 0.04 2.07 0.07 4.285 0.07 6.649l-12.49-36.954h-5.91l-12.6 36.954v-1.336c0-1.086 0.02-2.715 0.06-4.914 0.09-2.196 0.12-3.809 0.12-4.844v-25.86h-5.68v43.809"/><path d="m1990.4 180.68c1.99 0 3.59 0.207 4.86 0.625 2.25 0.781 4.12 2.257 5.55 4.472 1.14 1.77 1.95 4.024 2.48 6.797 0.28 1.652 0.43 3.184 0.43 4.59 0 5.43-1.06 9.648-3.16 12.648-2.1 2.997-5.52 4.5-10.18 4.5h-10.32v-33.632zm-16.32 38.73h17.5c5.93 0 10.55-2.137 13.83-6.414 2.93-3.848 4.39-8.789 4.39-14.816 0-4.649-0.86-8.856-2.59-12.618-3.03-6.64-8.25-9.961-15.7-9.961h-17.43v43.809"/></g></g></g></svg>',
        
        "header_logo" : '',
        
		"get_started": "Commencer",
		"cdn_stocks" : "Actions canadiennes",
		"cdn_stocks_short" : "Actions can.",
		"us_stocks" : "Actions américaines",
		"us_stocks_short" : "Actions am.",
		"intl_stocks" : "Actions internationales",
		"intl_stocks_short" : "Actions int.",
		"b_portfolio" : "Portefeuille équilibré",
		"b_portfolio_short" : "Port. équilibré",        
		"bonds" : "Obligations",
		"bonds_short" : "Obligations",
		"tbills" : "Bons du Trésor",
		"tbills_short" : "Bons du Trésor",
		"inflation" : "Inflation",
		"preferences" : "Préférences",
		"asset_classes" : "Catégories d'actif",
  
        "mg_portfolio":"Port. de Croissance Max.",
        "growth_portfolio":"Port. de Croissance",
        "bg_portfolio":"Port. de Croissance Équilibré",
        "balanced_portfolio":"Port. Équilibré",
        "bi_portfolio":"Port. de Revenu Équilibré",
        "income_portfolio":"Port. de Revenu",
        "aggresive_portfolio":"Aggresive Portfolio",
        "moderate_portfolio":"Moderate Portfolio",
        "conservative_portfolio":"Conservative Portfolio",
        
        "time_and_risk": "Temps & Risque",
        "bulls_and_bears": "Marchés H & B ",
        "matrix": "Matrice",
        "diversification": "Diversification",
        "downturns": "Effondrements",
        "rebalancing": "Rééquilibrage",
        "stocks_vs_port": "Actions vs. Port.",
        "gics": "CPGs",
        
        "max_assets_selected":"Max. nombre d'actifs. Veuillez désélectionner un ou plusieurs avant de faire une nouvelle sélection.",
        
        "cad_usd_inflation_chart":"<span style='color:#78909C'>Taux préférentiel</span>&nbsp;/&nbsp;<span style='color:#455A64'>Inflation</span>",
        "exchange_rate_chart":"<span style='color:#26A69A'>USD par CAD</span>",
       
        
		"historial_events" : "Événements historiques",
		"show" : "Montrer",
		"hide" : "Cacher",
		"language" : "Langue",
		"data_update" : "Mise à jour des données",
		"check_now" : "Vérifier maintenant",
		"about_you" : "À propos de vous",
        "select_start_date": "Sélectionnez la date de début",
		"name" : "Nom",
		"last_name" : "Nom de famille",
		"birth_date" : "Date de naissance",
		"see_lifeline" : "Prochain",
		"start_life_in_markets" : "Consulter Le Portrait Global",
		"personal_events" : "Événements personnels",
		"personal_lifeline" : "Le Portrait Global de %s",
		"assets_to_be_shown" : "Catégories d'actif à montrer",
		"add_event" : "Ajouter un événement",
		"type_of_event" : "Type d'événement",
		"event_home" : "Acheté une maison",
        "event_marriage" : "Mariage",
        "event_divorce" : "Divorce",
        "event_child" : "Eu un enfant",
        "event_school" : "École a commencé",
        "event_graduation" : "Graduation",
        "event_purchase" : "Achat important",
        "event_business" : "Ouvert une entreprise",
        "event_carrer" : "Première/nouvelle carrière",
        "event_canada" : "Arrivée au Canada",
        "event_death" : "Décès d'un conjoint",
        "date" : "Date",
        "add" : "Ajouter",
        "events_added" : "Événements ajoutés",
        "no_events" : "Aucun événement n'a été ajouté à votre Portrait Global.",
        "best_n_worst_years" : "Meilleures et pires années",
        "alltime" : "Toutes les années",
        "last_years" : "%s dernières années",
        "best" : "Meilleures",
        "worst" : "Pires",
        "asset_class" : "Catégorie d'actif",
        "1yr" : "1 an",
        "5yr" : "5 ans",
        "bnw_footnote1" : "Périodes consécutives de 1 an et 5 ans.",
        "bnw_footnote2" : "Tous les rendements sont des rendements annuels composés.",
        "share" : "Partager Le Portrait Global",
        "share_prompt" : "Choisir une méthode de partage, s'il vous plaît.",
        "via_device" : "Sauver à la galerie",
        "via_other_app" : "Menu Partager",
        "image_good" : "Votre Portrait Global a été enregistré à la galerie d'images.",
        "image_bad" : "Désolé, un problème est survenu lors de la sauvegarde de votre Portrait Global.",
        "legend" : "Légende",
        "no_network" : "Désolé, aucune connexion réseau n’est disponible.",
        "no_connection" : "Erreur de connexion. Impossible de mettre à jour les données en ce moment.",
        "no_update" : "Il n'y a pas de nouvelles données disponibles.",
        "yes_update" : "Nouvelles données récupérées, mise à jour terminée.",
        "last_check" : "Dernière vérification",
        "ww2" : "2de GM",
		"vietnam_war" : "Guerre du Vietnam",
		"cold_war" : "Fin de la guerre froide",
		"nine_eleven" : "11 sept.",
		"korean_war" : "Guerre de Corée",
		"iraq" : "Guerre en Irak",
		"disclaimer" : "Désistement",
		"disclaimer_t1" : "Ce graphique illustre la croissance projetée de mille dollars investis à la date de début sélectionnée. Il est conçu à des fins d’illustration seulement ; il ne peut servir de recommandation à l’investissement et on ne doit aucunement se baser sur lui à cette fin. Il présuppose le réinvestissement de tout bénéfice net et ne comprend aucun frais de transaction et aucune taxe. Les portefeuilles servant d’illustration ne sont pas réels, pas plus qu’ils ne sauraient faire l’objet d’une recommandation. Ils ont été rééquilibrés à chaque mois de janvier , et se composent comme suit : Portefeuille de Croissance—37.5 % Actions Canadiennes, 37.5 % Actions Internationales, 25 % Obligations; Portefeuille Équilibré—25 % Actions Canadiennes, 25 % Actions Internationales, 50 % Obligations; Portefeuille de Revenu—12,5 % Actions Canadiennes, 12,5 % Actions Internationales, 75 % Obligations. Tous les rendements sont des rendements annuels composés.",
		"disclaimer_t2" : "La reproduction en tout ou en partie de cette publication sans le consentement écrit préalable de Investments Illustrated, Inc. est interdite. La performance passée n’est pas un indicateur d’une future performance. Cette application est utilisée sous licence. © Investments Illustrated, Inc. Tous droits réservés.",
		"disclaimer_t3" : "Sources: Actions américaines de grande capitalisation : indice de rendement global S&P 500; Actions américaines à faible capitalisation—Center for Research in Security Prices (CRSP). Actions internationales : indice de rendement global à l’extérieur des Etats-Unis ; Actions canadiennes : indice de rendement global composé S&P/TSX ; Bons du trésor : indice de rendement global des bons du Trésor sur une période de trois mois ; Obligations : indice de rendement global des obligations du gouvernement canadien sur une période de dix ans ; Taux de change ; l’Or—Global Financial Data Inc. Inflation : indice des prix à la consommation ; Récessions—Statistique Canada."
	}
});

}( this ));
