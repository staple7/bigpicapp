/**
 * PREFERENCES
 */



$(function() {
//	check_network();

	/* TURN ON PREFERENCES ACCORDINGLY */
	// Asset Classes
	var assets_to_show = window.localStorage.getItem("default_assets").split(',');
	$('button.btn-asset').each(function() {
		$(this).html('<div class="icon icon_'+$(this).val()+' icon-30"></div><span data-localize="'+$(this).val()+'">'+Globalize.localize( $(this).val(), Globalize.culture())+'</span>').addClass('btn-'+$(this).val());
		if (assets_to_show.indexOf($(this).val()) > -1) { $(this).addClass('selected'); } else { $(this).addClass('notselected'); }
	});

	// Historical Events
	$('#h_events button').each(function() {
		if (window.localStorage.getItem("default_h_events") == $(this).val()) {
			$(this).toggleClass('btn-default btn-primary');
		};
	});

	// Language
	$('#lang button').each(function() {
		if (window.localStorage.getItem("lang") == $(this).val()) {
			$(this).toggleClass('btn-default btn-primary');
		};
	});
	// Last checked data
	data_check_text();

	// HANDLE TOUCH GESTURES
	// Turn on and off buttons appropriately
	$('#main button').hammer().on("tap", function() {
		if ($(this).parent().attr('id') == 'h_events' || $(this).parent().attr('id') == 'lang') {
			$(this).parent().children('button').toggleClass('btn-default btn-primary');

			if ($(this).parent().attr('id') == 'lang') {
				// Change language
				Globalize.culture($('#lang button.btn-primary').val());
				$("[data-localize]").each(function() { $(this).html(Globalize.localize($(this).attr('data-localize'),Globalize.culture())); });
				data_check_text();
			};
		} else {
			$(this).toggleClass('selected notselected');
		};
	});
	// Check for data updates
	$('#data_update button').hammer().on("tap", function() {
//		check_network(true);
		data_check_text();
	});
	// Save preferences
	$('.header button#next').hammer().on("tap", function() {
		// Asset classes
		var default_assets = new Array();
		$('button.btn-asset.selected').each(function() { default_assets.push($(this).val()); });
		window.localStorage.setItem("default_assets", default_assets.toString());

		// Historical Events
		var default_h_events = $('#h_events button.btn-primary').val();
		window.localStorage.setItem("default_h_events", default_h_events);
		
		//Language
		var lang = $('#lang button.btn-primary').val();
		window.localStorage.setItem("lang", lang);

		window.location = 'http://investmentsillustrated.com/scotia/';
	});

	// FOR TESTING PURPOSES, HOLDING ON THE 'PREFERENCES' TITLE
	// WILL ESSENTIALLY RESET THE APP
	$('.header .title').hammer().on("hold", function() {
		window.localStorage.clear();
		window.location = 'index.html';
	});
});

function data_check_text() {
	var data_checked = (window.localStorage.getItem("data_checked") == 0 || window.localStorage.getItem("data_checked") == null) ? 'Never' : window.localStorage.getItem("data_checked");
	$('#data_update p').text( Globalize.localize("last_check", Globalize.culture()) + ': ' + data_checked );
}