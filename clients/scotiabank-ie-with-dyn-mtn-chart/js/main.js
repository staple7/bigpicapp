/**
 * CLIENT
 */

$(function() {
//	check_network();

	// SESSION DATA PERSISTENCE
	var bd_year = parseInt(window.localStorage.getItem('bd_year'));
	var bd_month = parseInt(window.localStorage.getItem('bd_month'));
	var f_name = window.localStorage.getItem("f_name");
	var temp_lang = window.localStorage.getItem("temp_lang");

	if (f_name !== undefined) { $('#about_you #f_name input#first_name').val(f_name) };

	// FILL YEAR FIELD
	// Every year from 1935 to 15 years ago
	var year_offset = 15;
	var latest_date = JSON.parse(window.localStorage.getItem('data'));
	latest_date = latest_date[latest_date.length-1].date;
	var latest_year = parseInt(latest_date.substr(0,4));
	var latest_month = parseInt(latest_date.substr(5,2));
	
	var temp_str = '';
	for (var i = 1935; i <= latest_year - year_offset; i++) {
		if (bd_year !== undefined && i == bd_year) {
			temp_str += "<option value='"+i+"' selected='selected'>"+i+"</option>";
		} else {
			temp_str += "<option value='"+i+"'>"+i+"</option>";
		}
	}
	$('#birth_date #year').html( temp_str );

	var temp_str = '';
	for (var i = 1; i <= 12; i++) {
		if (bd_month !== undefined && i == bd_month) {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
		} else {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
		}
	}
	$('#birth_date #month').html( temp_str );
	$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });

	$('#birth_date #year').change(function() {
		var curr_sel_month = $('#birth_date #month').val();
		if (parseInt($(this).val()) == latest_year - year_offset && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= latest_month; i++) {
				if ((curr_sel_month > latest_month && i == latest_month) || (curr_sel_month <= latest_month && i == curr_sel_month)) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		} else if ($('#event_menu .content select#month option').length < 12 && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= 12; i++) {
				if (i == curr_sel_month) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		}
		$('#birth_date #month').html( temp_str );
		$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });
	});
	
	// Hide keyboard fix
	var last_selected = null; 
	$('select').focus(function(){
		if (last_selected.toLowerCase() == 'input') {
			$('input').focus();
			if (last_selected.toLowerCase() !== 'select') {
				last_selected = this.tagName;
				$(this).focus();
			}
		}
	});
	$('input').focus(function () { last_selected = this.tagName; });

	//Lang
	$('#lang button').each(function() {
		if (temp_lang !== undefined && temp_lang !== null) {
			if (temp_lang == $(this).val()) {
				$(this).toggleClass('btn-default btn-primary');
			}
		} else if (window.localStorage.getItem("lang") == $(this).val()) {
			$(this).toggleClass('btn-default btn-primary');
		}
	});


  

});



 