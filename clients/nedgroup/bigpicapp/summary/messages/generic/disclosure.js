define({
    

    DISCLOSURE:{

        TITLE: "Disclaimer:",
        
        PARAGRAPH1: "The charts and figures contained herein are based on historical total return data beginning January 1, 1926, and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended.",

        PARAGRAPH2: "An investment cannot be made directly in an index. Government bonds and Treasury bills are guaranteed by the full faith and credit of their respective issuing governments as to the timely payment of principal and interest, while stocks are not guaranteed and have been more volatile than the other asset classes shown. International stocks involve special risks such as fluctuations in currency, foreign taxation, economic and political risks, liquidity risks, and differences in accounting and financial standards. Past performance is not an indicator of future performance."
    },
    
    SOURCES:{
        
        TITLE: "Sources:",

        PARAGRAPH1: "US Cash: CRSP 90-Day T-Bill Returns—Center for Research in Security Prices (CRSP). SA Equity pre-1961: Firer (industrial shares only), SA Equity post 1961: JSE All Share Total Return Index, SA Bonds: South Africa SARB Government Bond Return Index, Global Equity: Developed World Return Index, Global Bonds: World Government Bond GDP-weighted Return Index, inflation, exchange rates—Global Financial Data, Inc."
    },
    
    DOFU :{
        DOFU_CODE: ""
    }
    
});
