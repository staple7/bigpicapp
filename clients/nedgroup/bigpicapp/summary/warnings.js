 
define({

    mac:{
        chrome:{
            message:    "Chrome does not fully support PDF viewing. Please go to your \"Downloads\" folder and open the PDF using Safari, Firefox, or Preview.",
            supported:  true,
            action:     "download"
        },
        safari:{
            message:   "",
            supported: true,
            action:    "download"
        },
        firefox:{
            message:   "",
            supported: true,
            action:    "download"
        },
        opera:{
            message:   "",
            supported: true,
            action:    "download"
        }
    },
    ios:{
        chrome:{
            message:    "Chrome does not fully support PDF viewing. Please access the app using Safari.",
            supported:  false,
            action:     ""
        },
        safari:{
            message:   "",
            supported: true,
            action:    "download"
        },
        firefox:{
            message:    "Firefox does not fully support PDF viewing. Please access the app using Safari.",
            supported:  false,
            action:     ""
        },
        opera:{
            message:   "Opera does not fully support PDF viewing. Please access the app using Safari.",
            supported: false,
            action:    "download"
        }
    },
    android:{
        chrome:{
            message:    "",
            supported:  true,
            action:     "download"
        },
        firefox:{
            message:    "",
            supported:  true,
            action:     "download"
        },
        opera:{
            message:   "",
            supported: true,
            action:    "open"
        }
    },
    pc:{
        ie:{
            message:    "IE does not support PDF exports. Please access the app using Firefox or Chrome.",
            supported:  false,
            action:     ""
        },
        edge:{
            message:    "",
            supported:  true,
            action:     "download"
        },
        firefox:{
            message:    "",
            supported:  true,
            action:     "download"
        },
        opera:{
            message:   "",
            supported: true,
            action:    "download"
        },
        chrome:{
            message:    "",
            supported:  true,
            action:     "download"
        },
    }
    
     
//        if (isComputer){
//            if (isChrome){
//                saveFile(file);
//            }else if (isSafari){
//                saveFile(file); 
//            }else if (isFirefox){
//                saveFile(file);
//            }else if (isIE){
//                //nothing works, so far.
//            }else{
//                saveFile(file);
//            }
//        }else{
//            if (isIOs){
//                if (isChrome){
//                    openNewWindowViewer(file);
//                }else if (isSafari){
//                    saveFile(file); //works as openning.
//                }else if (isFirefox){
//                    //nothing works, so far.
//                }else{
//                    saveFile(file);
//                }
//            }else if (isAndroid){
//                if (isChrome){
//                    saveFile(file);
//                }else if (isFirefox){
//                    saveFile(file); //???
//                }else{
//                    saveFile(file);
//                }
//            }else{
//                saveFile(file);
//            }
//            
//        }
//           

});
