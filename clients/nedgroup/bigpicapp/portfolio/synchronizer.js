define (['postal'], function(postal){
    
    
    postal.channel().subscribe("portfolioController.portfolio.changed", function(portfolioConfig){
        persistPortfolio(portfolioConfig);
    });
    
    function persistPortfolio(portfolioConfig){
        
        if (portfolioConfig){
            var data = {
                'email': $.cookie('email'),
                'portfolioConfiguration': portfolioConfig
            };
//            console.log(data);
            $.ajax({
                type:     "POST",
                dataType: "json",
                data:     data,
                url:      "/bigpicture_integrations/bigpicture_database/persist_portfolio.php",
                success: function(){},
                error: function(e){}
            });
        }
    } 
      
    function getSavedPortfolio(){
        postal.channel().publish("portfolio.synchronizer.config.retrieved", undefined);
    }
    
    function publishConfigRetrieved(config){
        postal.channel().publish("portfolio.synchronizer.config.retrieved", config);
    }
    
    return{
        getSavedPortfolio: getSavedPortfolio
    }
    
});
