 
define({
    

    inRetirement: {
        graphTitle:     "Historical Success Rate",
        graphSubtitle:  {
            normal: "Frequency at which portfolio ended above legacy capital, {{regionStartYear}} to present",
            abbreviate: "Frequency at which portfolio ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle:     "Success Rate: The proportion of historical {{investmentHorizon}}-year periods* at the end of which the portfolio's value was equal to or greater than the stated amount of legacy capital, provided the inputs at left.<br><br>* <em>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>."
    },
    savingForOneTime : {        
        graphTitle:     "Historical Success Rate",
        graphSubtitle:  {
            normal: "Frequency at which portfolio met savings goal, {{regionStartYear}} to present",
            abbreviate: "Frequency at which portfolio met goal, {{regionStartYear}} – present"
        },
        xAxisTitle:     "Success Rate: The proportion of historical {{investmentHorizon}}-year periods* at the end of which the portfolio's value was equal to or greater than the stated savings goal, provided the inputs at left.<br><br>* <em>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}.</em>"
    }
});

