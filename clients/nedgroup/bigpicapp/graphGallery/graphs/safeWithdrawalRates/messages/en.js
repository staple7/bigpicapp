 
define({
        
    graphTitle:     "Historical Safe Withdrawal Rates",
    graphSubtitle : {
        normal:     "Maximum sustainable spending, by rolling {{investmentHorizon}}-year period, {{regionStartYear}} to present",
        abbreviate: "Max. sustainable spending, by rolling {{investmentHorizon}}-year. period, {{regionStartYear}} – present"
    },
    xAxisTitle :    "{{investmentHorizon}}-YEAR RETIREMENT PERIOD STARTING IN...",
    y2AxisTitle:    "Your spending"
   
});

