

define (['postal', 'graphGallery/graphs/successRateByHorizon/messages/en'], function(postal, messages){
        
    var context;
    
    function graph(graphData){
        
        removeElements();

        var minYValue;  
        var maxYValue;
        var yAxisValue;
        var xAxisValue;    	
        var dataList = [];              
        var result;
      
        var xAxisRange = calculateXTickValues();
        
        setGraphTitles();
       
        xAxisRange.forEach(function(investmentHorizon){
            postal.channel().publish("variableCalculator.calculateSuccessRateByInvestmentHorizon",
                                     {investmentHorizon:investmentHorizon, 
                                      callback: function(successRate){
                                      
                minYValue = minYValue === undefined ? successRate : 
                            (successRate < minYValue ? successRate : minYValue);
                                          
                maxYValue = maxYValue === undefined ? successRate : 
                            (successRate > maxYValue ? successRate : maxYValue);   
                                          
                dataList.push({x:investmentHorizon, 
                               y:successRate, 
                               lowerThanMin:false,
                               higherThanMax:false});
            }});
        });


        svg = svg.datum(dataList);

        yAxis = d3.svg.axis();
        xAxis = d3.svg.axis();

        x = d3.scale
              .ordinal()
              .domain(dataList.map(function(d){return d.x}))
              .rangeRoundBands([0, graphWidth], .19, .2);

        y = d3.scale.linear();

        var yTickValues = [];
        yTickValues = yTickValues.calculateTicks("years", minYValue, maxYValue, 0);

                
        yAxis.scale(y)            
            .orient("left")
            .tickValues(yTickValues)
            .tickFormat(function(d) {
                return d.format("percentage", 0, d);
            });

        y = y.domain([yTickValues[0], yTickValues[yTickValues.length-1]])
            .range([graphHeight, 0]);

        xAxis.scale(x)
            .orient("bottom")
            .tickFormat(function(d) {
                if (d % 2 ==0){
                    return d.format('number', 0, d);                    
                }
            });

        svg.selectAll(".x.axis").call(xAxis);         
        svg.selectAll(".y.axis").call(yAxis);

        svg.append("g")
            .attr("id","graphBars")
            .selectAll(".rect")
            .data(dataList)
            .enter().append("rect")
            .attr("rx", "2")
            .attr("ry", "2")
            .attr('width', x.rangeBand())                
            .attr('x', function(d, i){
                var yValue = y(d.y);
                var zeroLineFactor = 0;
                var heightFactor = 0.5;
                if (d.y == yTickValues[0]){
                    zeroLineFactor = 1.2;
                }
                var thisObj = d3.select(this);
                thisObj.attr("y", yValue-heightFactor-zeroLineFactor);
                thisObj.attr("height", Math.abs(graphHeight-yValue-zeroLineFactor));
                var match = (xAxisRange[i]==graphData.userInput.getInvestmentHorizon());
                //thisObj.attr("fill", match ? "#0095f5" : "#81D4FA");
                thisObj.attr("fill", match ? "#197254" : "#7fb1a0");
          
                return x(d.x);
            });

        svg.selectAll(".x.axis").selectAll(".tick").each(function(data){
            d3.select(this).select("text").attr("class", "smallerText");
        });


        svg.selectAll(".y.axis").selectAll("text").each(function(){                    
            d3.select(this).attr("class", "normalText");                    
        });

        postal.channel().publish("graphService.graph.done");

    }

    
    function setGraphTitles( ){    
        var graphWidthLimit = 1359;
        var documentWidth = $(document).width();
        var graphSubtitle = localContext.graphSubtitle.normal;
        if  (documentWidth<graphWidthLimit){
            graphSubtitle = localContext.graphSubtitle.abbreviate
        }
        $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                ({regionStartYear:localStorage.getItem("regionStartYear")}));
    }    

    function calculateXTickValues(){
        return [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40];
        //return [2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40];
    } 



    function removeElements(){
        $("#graphBars").remove();
        $("#successRateGraphText").remove();
    }

    function finalize(){
        removeElements();   
        localGraphData = null;
        context = null;
        xAxisTitle.html("");
        removeAxis();
    }

    function branchChanged(){
        finalize();
    }

    function graphChanged(){
        finalize();
    }

    function calculateGraphDimensions(){
        var containerWidth = $(svgArea).width();
        graphWidth = (containerWidth * 0.80);
        var yPos = 50;
        if ($(document).width() <= 1799){
            graphHeight = ($(svgArea).height() * 0.55);   
            
        }else{
            graphHeight = ($(svgArea).height() * 0.60);   
            yPos = 80;            
        }
        svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
        svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
    }

    function removeAxis(){
        svg.selectAll(".x.axis").remove();
        svg.selectAll(".y.axis").remove();
    }

    function addAxis(){
        svg.append("g").attr("class", "x axis");
        svg.append("g").attr("class", "y axis");
    }

    function init(lang, branchCode){
        addAxis();
        calculateGraphDimensions();
        localContext = messages[branchCode];
        $(".graph_title").html(localContext.graphTitle);
        xAxisTitle.html(localContext.xAxisTitle);
        postal.channel().publish("graphGallery.graph.initiated");
    }


    return {
        init:               init,
        graph:              graph,
        graphChanged:       graphChanged,
        branchChanged:      branchChanged
    };


});
