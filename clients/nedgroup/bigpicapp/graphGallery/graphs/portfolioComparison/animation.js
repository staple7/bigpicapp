//portfolioComparisonGraph animation:

define(['postal', 'basic'],function(postal, basic){

    
    function openPortfolioList(){
        basic.popupPosition('.portfolioListWrap', -50);
    }
    
   
    
    function showAndHidePortfolios(){
		var channel     =   postal.channel(),
			speedVal    =   basic.speedVal;
		 $(window).resize(function(){
			basic.popupPosition('.portfolioListWrap', -50);
    });   
        
    //showPortfoliosTip bubble

    function hidePortfoliosTipBubble(){
        $('.showPortfoliosTipBubble, .showPortfoliosTip div').stop(true, true).fadeOut(speedVal);
    }

    function portfoliosTipBubble(){
        if($('.showPortfoliosTipBubble').css('display') === 'block'){
            hidePortfoliosTipBubble();
        }
        else{
            setTimeout(function(){
            $('.showPortfoliosTipBubble, .showPortfoliosTip div').stop(true, true).fadeIn(speedVal);
            }, 100);
        }
    }     
        
    $('body')
        .on('touchstart click', '.showAndHidePortfolios', function(e){
			e.preventDefault();
			$('.showAndHidePortfoliosWrap').load('/app/graphGallery/portfolioComparison/portfolioList.html', function(){
				openPortfolioList();
				var $this = $(this);
				$('.showAndHidePortfoliosWrap').stop().fadeIn(speedVal);
				channel.publish("portfolioGraph.show.portfolio.list");  
			});
           
    });
        
    $('.showAndHidePortfoliosWrap')
            .on({
                'touchstart click': function(e){
                    e.preventDefault();
                var portfolioIds = $('.activated').map(function(){
                    return parseInt(this.id);
                }).get();
                $('.showAndHidePortfoliosWrap').stop().fadeOut(speedVal);
                channel.publish("portfolioGraph.done.button.pressed", portfolioIds);
//					console.log(portfolioIds);
                }
        
            },'.selectPortfolioDone:not(.disableBtn)')
            .on({
                'touchstart click': function(e){
                    e.preventDefault(); 
                    e.stopPropagation();
                    $(this).toggleClass('activated'); 
//                    var checkNumber = $('.activated', '.portfolioOption').length;
//                    if(checkNumber === 1){
//                        $('.portfolioOption_BottomInfo').stop().fadeIn(speedVal);
//                        $('.selectPortfolioDone').addClass('disableBtn');
//                    }
//                    else{
//                        $('.portfolioOption_BottomInfo').stop().fadeOut(speedVal);
//                        $('.selectPortfolioDone').removeClass('disableBtn');
//                    }
                }
                
            },'li div:not(.disableCheckBox)')
              
            .on({
                'touchstart click mouseenter':function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    portfoliosTipBubble();
                },
                 mouseleave:function(){
                     hidePortfoliosTipBubble();
                 }
          },'.showPortfoliosTip')
    
        .on({
            'touchstart click' : function(e){
                e.preventDefault();
                $('.showAndHidePortfoliosWrap').stop().fadeOut(speedVal);
            }
        },'.popup_close_btn')
    
        .on({
        'touchstart click': function(e){
            if(!$(e.target).closest('.portfolioListWrap').length && $('.showAndHidePortfoliosWrap').css('display') === 'block'){
                e.preventDefault();
                e.stopPropagation();
                    $('.showAndHidePortfoliosWrap').stop().fadeOut(speedVal);
            }
        },
        'touchstart click mouseover':function(e){
                e.stopPropagation();    
                if(!$(event.target).closest(".showPortfoliosTip").length && 
                   !$(event.target).closest(".showPortfoliosTipBubble").length && 
                   $('.showPortfoliosTipBubble').css('display') === 'block' ){
                        hidePortfoliosTipBubble();
                }
            }
    	}, this);
        
}

showAndHidePortfolios();
    
});