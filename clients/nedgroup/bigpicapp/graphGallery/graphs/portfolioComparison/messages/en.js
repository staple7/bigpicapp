 
define({
 
    inRetirement: {    		
        graphTitle  : "Historical Success Rates",
        graphSubtitle: {
            normal: "Frequency at which portfolios ended above legacy capital, {{regionStartYear}} – present",
            abbreviate: "Frequency at which portfolios ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle: "",
        y2AxisTitle: ""
    },
    savingForOneTime: {
        graphTitle  : "Historical Success Rates",
        graphSubtitle: {
            normal: "Frequency at which portfolios ended above savings goal, {{regionStartYear}} – present",
            abbreviate: "Frequency at which portfolios ended above savings goal, {{regionStartYear}} – present"
        },
        xAxisTitle: "",
        y2AxisTitle: ""        
    }
    
});
