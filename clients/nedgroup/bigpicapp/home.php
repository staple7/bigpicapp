<?php

    $app_version = "1.0.13";    
    $regionCode = "sa";

    $requirejs = "";
    $jsBigPictureCode = "";
    $jsBigPictureData = "";


    $host = $_SERVER['HTTP_HOST'];

    if ($host == 'bigpicapp.co' || $host == 'www.bigpicapp.co' ||
        $host == 'test.bigpicapp.co' || $host == 'www.test.bigpicapp.co' ||
        $host == 'internal-testing.bigpicapp.co' || $host == 'www.internal-testing.bigpicapp.co'){
            $jsBigPictureCode = "<script src=" . "bigpicture." . $regionCode . ".js?v" . $app_version . "></script>";
            $jsBigPictureData = "<script src=" . "bigpicture." . $regionCode  . ".data.js?v" . $app_version . "></script>";
            $requirejs="<script src='https://www.bigpicapp.co/lib/require.js'></script>";
    }

?>



<!DOCTYPE html>




<html prefix="og: http://ogp.me/ns#">

    
    <script src="vendors/js/jquery-1.11.3.min.js"></script>    
    <script src="vendors/js/jquery.cookie.min.js"></script>
    
    <script>
        
        $.cookie("app_version", "<?php echo $app_version;?>" , { path: '/' });
        $.cookie("plancode", "big_picture_premium" , { path: '/' });
        $.cookie("regioncode", "sa" , { path: '/' });
        $.cookie("email", "email@nedgroup.com" , { path: '/' });
    
    
    </script>
    
    <head>
        <title>Withdrawal Program - The Big Picture</title>
        <meta name="keywords" content="" />
		<meta name="description" content="" />

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link rel="shortcut icon" href="PUT YOUR FAVICON HERE">-->
         
        <!-- Google Web Font Embed -->
         
        <!-- USE THIS IF YOU PREFER GOOGLE REMOTE CDN -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->
        
    	<!-- **OR** THIS TO USE LOCAL FONTS -->
    	<style type='text/css'>
    	@font-face {
			font-family: 'Roboto';
			src: url('fonts/roboto-regular-webfont.woff2') format('woff2'),
				 url('fonts/roboto-regular-webfont.woff') format('woff');
			font-weight: normal;
			font-style: normal;
		}
    	</style>
    	<!-- END LOCAL FONT -->
    	
		<link rel="stylesheet" href="css/style.css?v<?php echo $app_version;?>">

		<script src="vendors/js/handlebars.min.js"></script>
        <script src="vendors/js/d3.v3.min.js"></script>
        <script src="vendors/js/jquery-1.11.3.min.js"></script>
        <script src="vendors/js/jquery.cookie.min.js"></script>
        <script src="vendors/js/jspdf.min.js"></script>
        
        
    </head>
    
<body>




<!-- 
this is the TOP NAVIGATION
	it has a min-width and is maxed-out at 90%
	it's in a 100% container
============================================	
 -->
<div class=header>

	<div class=nav>
	
		<a href='index.html' class='is_active with' style="display:none">Withdrawal Program</a>

<!--		<a href='key.html' class='key'>Key Principles</a>-->

		<div class='logowrap'><img style="float:left" src="images/bigpicture_logo.svg?v<?php echo $app_version; ?>" alt='The Big Picture' class='logotitle'></div>
        
        <div class='logowrap'><img style="float:right" src="images/nedgroup_logo.svg?v<?php echo $app_version; ?>" alt='The Big Picture' class='logotitle2'></div>

		<div class=extranav>

<!--
            <a target="_blank" href='https://youtu.be/DTQfapmtI_E' data-pop=''>Video Tutorial</a>
            
-->


<!--			<a href='' class='morebutton'>More</a>-->
		
		</div>
	
	<!-- 
	MORE MENU -->
<!--
	<div class='more'>
		<div class='inside'>
			<div class="rewards"><a href=''>Referral Rewards</a></div>
			<div class="share"><a href='' class='popper' data-pop='sharewrap'>Share</a></div>
			<div class="sources"><a href=''>Sources</a></div>
		</div>
	</div>
-->
	<!-- end MORE MENU -->
	
	
	
	</div><!-- end .nav -->


</div><!-- end .header -->






<!-- 
============================================	
end TOP NAVIGATION
-->







<!-- 
this is the MAIN CONTENT (everything but the top nav)
	it has a min-width and is maxed-out at 90%
============================================	
 -->
<div class='main'>
	
		<!--
		animation for PAGE load/refresh
		]]]]]]]]]]]]]]]]]]]]]
		-->
		<div class="loaderPageGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>
		<!--
		END animation for PAGE load/refresh
		]]]]]]]]]]]]]]]]]]]]]
		-->

	<!-- 
	here is the SIDEBAR = it has a fixed with
	============================================	
	-->
	
	<div class='sidebar'>
		

	</div><!-- end .sidebar -->
	<!-- 
	============================================
	end SIDEBAR	
	-->
	
	
	

	<!-- these are the WIDGETS
	+++++++++++++++++++++++ -->
	
	<div class='help popper' data-pop='helpwrap'>
		<svg fill="#bbbbbb" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
			<path d="M0 0h24v24H0z" fill="none"/>
			<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/> 
		</svg>
	</div><!-- end .help -->
	
	<!-- widgets TK --> 
	
	<!-- <div class='widget'></div> --><!-- end .widget -->
	
    
    <div class='monthly_data_label'>Results based on rolling periods using monthly-frequency historical data.</div>
    <div class='risk_label'>* Volatility (standard deviation) of annual portfolio returns.</div>

	<!-- END WIDGETS
	+++++++++++++++++++++++ -->
   

	<!-- 
	here are all the PAGES with the charts
	============================================	
	-->	
	
	<div class='pages'>

		<!--
		transition animation
		]]]]]]]]]]]]]]]]]]]]]
		-->
		<div class="loaderGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>
		<!--
		END transition animation
		]]]]]]]]]]]]]]]]]]]]]
		-->

		<!-- 
		PAGESETS are the set of pages that get show in a MODE
		============================================	
		-->
		<div class='pageset retirement_pages is_active graph-area-container'> </div><!-- end .retirement_pages -->

        
        
		<!-- 
		PAGESETS are the set of pages that get show in a MODE
		============================================	
		-->
		<div class='pageset _pages keypage'><!--  "_pages" because there is no mode so only one pageset -->
			
			<!-- ONE SINGLE PAGE: hidden unless is has the "is_active" class
			\\\\\\\\\\\\\\\\\\\\\\\\\\\\
			 -->
			<div class='onepage is_active'>	
				
				<!-- page text is the stuff before the chart -->
				<div class='pagetext pdftext'>
			
					
					<h1>A Collection of Classic Investment Illustrations</h1>
			
					<h2>Download a PDF from the menu below</h2>
			
				</div><!-- end .pagetext -->
			
			
				<!-- this is where the pdfs go -->
				<div class='chartspace pdfs'>
                    
                    <div>                                      
                    <a href='' target='_blank'>
                    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
                        <path d="M0 0h24v24H0z" fill="none"/>
                    </svg>
                    Power of Compounding I</a>
                    </div>  


                    <div> 
                    <a href='' target='_blank'> 
                    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
                        <path d="M0 0h24v24H0z" fill="none"/>
                    </svg>
                    Power of Compounding II</a>
                    </div>
                    
                   
					<div>
					<a href='' target='_blank'>  
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Bulls and Bears</a>
					</div>


					<div>  
					<a href='' target='_blank'>
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Chasing Performance</a>
					</div>

					<div> 
					<a href='' target='_blank'> 
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Time and Risk</a>
					</div>

				
					<div>  
					<a href='' target='_blank'>
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Downturns and Recoveries</a>
					</div>
                    
                    
                    <div> 
					<a href='' target='_blank'> 
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Diversification</a>
					</div>

					<div>
					<a href='' target='_blank'> 
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Rebalancing Act</a>
					</div>

					<div>  
					<a href='' target='_blank'>
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Stocks vs. Portfolio</a>
					</div>
                    

				</div>		
					
			</div><!-- end .onepage -->

		</div><!-- end ._pages -->
		
	</div><!-- end .pages -->

</div><!-- end .main -->
<!-- 
============================================	
end MAIN CONTENT
-->	





<!--
POPUPS 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->
<div id="popupOverlay"> 
    <div class='portfoliowrapper popup onepop portfolio_wrap portfolio_container'></div> 
<!-- </div> --><!-- end .portfoliowrapper.popup -->



<!-- MORE popup -->
<!-- <div id="popupOverlay"> -->
    <div class='ratiowarpper popup onepop extra_info_wrap' id='extra_info'> </div><!-- end .ratiowarpper.popup -->
<!-- </div> -->
<!-- end MORE popup -->



<!-- SETTINGS popup -->
	<div class='settingswrap popup onepop settings_setting_container'> 

			
 
	</div><!-- end .settingswrap.popup -->

<!-- end SETTINGS popup -->



<!-- LOGOUT popup -->
    <div class='logoutwrap popup onepop'>

            <p>If you log out you will not be able to open the Big Picture offline.</p><br><br>

            <div class='center'><span class="btn logout-got-it" style='position: static;'>Got it</span><br><br>
            <span><label><input type='checkbox' class='logout-checkbox'> Don't show again</label></span>
            </div>


    </div><!-- end .ratiowarpper.popup -->
<!-- end LOGOUT popup -->


<!-- SHARE popup -->
    <div class='sharewrap popup onepop'>

            <p>This is the "SHARE" popup This is the "SHARE" popup This is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popup</p>



    </div><!-- end .ratiowarpper.popup -->
<!-- end LOGOUT popup -->



<!-- HELP popup -->
    <div class='helpwrap popup onepop'>

        <h1 style="text-align:center;margin-bottom:15px">Key Definitions</h1>
        <br>
        <div id="help_inRetirement">
            {{#each VARIABLES}}
                <div>
                    <span>{{{this.name}}}</span>
                    <span style='color: #757575;line-height: 1.3em;'>{{this.definition}}</span>
                </div>
            <br>
            {{/each}}

            {{#each FOOTER}}
                <div class='center' style='margin-top: 20px;'>
                    <span style='font-style: italic;color: #757575;'>{{{this}}}</span>
                </div>
            {{/each}}

        </div>  
        
        <div class="sources" style="float:right"><a href='https://www.bigpicapp.co/nedgroup/disclaimer.html' target="_blank">Sources</a></div>

    </div><!-- end .ratiowarpper.popup -->

<!-- end HELP popup -->




</div>
<!--
end POPUPS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->


    <script src='js/jquery-1.11.3.min.js' type='text/javascript'></script>
    
    <?php echo $requirejs;?>
    
    <?php echo $jsBigPictureData;?>

    <?php echo $jsBigPictureCode;?>
    
    <script data-main="main" src="https://www.bigpicapp.co/lib/require.js"></script>


    
</body>
</html>
