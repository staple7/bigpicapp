define({
    
    MODALITY:           "Modality",
    IN_RETIREMENT:      "In Retirement",
    SAVING:             "Saving",
    YEARS_IN_RETIREMET: "Years in Retirement",
    YEARS_TO_RETIREMET: "Intended Savings Period (Years)",
    INITIAL_CAPITAL:    "Initial Capital",
    SPENDING:           "Spending",
    MONTHLY_SPENDING:   "Monthly Spending",
    MONTHLY_CONTRIBUTION:"Monthly Contribution",
    WITHDRAWAL_RATE:    "Withdrawal Rate",
    INFL_ADJ_SPENDING:  "Inflation-adjust spending", 
    INFL_ADJ:           "Inflation-adjust contributions", 
    LEGACY_CAPITAL:     "Legacy Capital",
    IN_TODAY_RANDS:     "in today's Rands",
    INFLATION_ADJUSTED: "inflation-adjusted",
    SAVINGS_GOAL:       "Savings Goal",
    PORTFOLIO:          "Portfolio",
    EDIT:               "Edit",
    MORE:               "More",  
    MORE_INPUT_TITLE:   "Expenses and Rebalancing",
    TER_TITLE:          "TOTAL EXPENSE RATIO:",
    REBALANCING_TITLE:  "REBALANCING:",
    NONE:               "None",        
    ANNUAL:             "Annual",
    EVERY_3_YRS:        "Every 3 yrs.",
    EVERY_5_YRS:        "Every 5 yrs.",
    OTHER:              "Other",
    YEARS_SHORT:        "Yrs",
    YEAR_SHORT:         "Yr",
    DONE:               "Done",
    SUCCESS_RATE:       "The proportion of historical {{investmentHorizon}}-year periods* at the end of which the portfolio's value was equal to or greater than the stated amount of legacy capital, provided the inputs at left.<br><br>* <em>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>.",

    VARIABLES:{
        "Years_in_Retiremen": {
            name:        "Years in Retirement (\"In retirement\" modality):",
            definition:  "The number of years remaining in retirement.",
        },
       "Years_to_Retirement": {
            name:        "Years to Retirement (\"Saving\" modality):",
            definition:  "The number of years remaining until retirement.",
        },
       "Initial_Capital": {
            name:        "Initial Capital:",
            definition:  "The amount of savings, in today's Rands, available for investment.",
        },
       "Monthly_Spending": {
            name:        "Monthly Spending (\"In retirement\" modality):",
            definition:  "Monthly spending in retirement, in today's Rands, before income tax is deducted.",
        },
       "Withdrawal_Rate": {
            name:        "Withdrawal Rate (\"In retirement\" modality):",
            definition:  "Annual spending, in today's Rands, before income tax is deducted, expressed as a proportion of the initial investment.",
        },
       "Monthly_Contribution": {
            name:        "Monthly Contribution (\"Saving\" modality):",
            definition:  "Monthly saving toward retirement, in today's Rands.",
        },
       "Legacy_Capital": {
            name:        "Legacy Capital (\"In retirement\" modality):",
            definition:  "The amount of capital, in today's Rands, to be left in the retirement portfolio. ",
        },
       "Savings_Goal": {
            name:        "Savings Goal (\"Saving\" modality):",
            definition:  "The amount of capital, in today's Rands, to serve as a retirement nest egg.",
        },
       "Inflation_Adjusted": {
            name:        "Spending / Contributions:",
            definition:  "All returns are real (net-of-inflation) returns. Withdrawal / contribution amounts are in constant, real Rands." ,
        },
       "Total_Expense_Ratio ": {
            name:        "Total Expense Ratio:",
            definition:  "The portfolio\'s total expense burden, including fund and advisory fees. Deducted annually. ",
        }
        
    },
    FOOTER:{
        paragraph_1: "<em>Axis values scale automatically as inputs are adjusted.</em>"
    }


});