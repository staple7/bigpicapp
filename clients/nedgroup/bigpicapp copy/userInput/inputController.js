
define (['postal',
         'userInput/mainInputView',          
         'portfolio/portfolioService',
         'graphGallery/graphGalleryController',
         'userInput/messages/en'],

        function(postal,  
                 mainInputView,
                 portfolioService,
                 graphGalleryController,
                 messages){

            var variableDataMap       = {};
            var branchControllerMap   = {};
            var currentVariableToSolveForCodeMap = {}; 
            var currentBranchController;
            var variableData;
            var currentBranchCode = 'inRetirement';
            var outputDataTimeOut;
            var lastAccessTime;
    
            var channel = postal.channel(); 
    
    
            channel.subscribe("branchesController.iButton.selected", function(modalityCode){
                getVariableDefinition(modalityCode);
            }); 
    
            postal.channel().subscribe("mainInputView.portfolio.selected", function(index){
                localStorage.setItem("portfolioIndex", index);
            });
        
            channel.subscribe("inputController.getSelectedPortfolioIndex", function(callback){        
                callback(getSelectedPortfolioIndex());
            });
    
            function getSelectedPortfolioIndex(){
                return JSON.parse(localStorage.getItem("portfolioIndex")) || 0;
            }

        
            channel.subscribe("inputController.getModalityName", function(callback){        
                callback(mainInputView.getModalityName());
            });
    
            channel.subscribe("inputController.getNamedInput", function(callback){
                callback(mainInputView.getNamedInput());
            });
    

            channel.subscribe("inputController.getSuccessRate", function(callback){
                var input = getInput();
                var successRate = portfolioService.calculateSuccessRate(input.getInitialInvestment(),
                                                                        input.getInvestmentHorizon(),
                                                                        input.getSavingsGoal(),
                                                                        input.getContributionAmount());
                callback(Math.floor(parseFloat(successRate)*100) + "%");
            });
    
    
            channel.subscribe("inputController.getSuccessRateDefinition", function(callback){
                var definition = Handlebars.compile(messages.SUCCESS_RATE)({investmentHorizon:getInput().getInvestmentHorizon(), 
                                                                          rollingPeriodCount:portfolioService.getRealEndValueList().length,
                                                                          regionStartYear:localStorage.getItem("regionStartYear")});

                callback(definition);
            });
    

            channel.subscribe("portfolioController.portfolio.changed", function(){
                channel.publish("portfolioController.getSavedPortfolioNameList", function(portfolioList){
                    mainInputView.renderPortfolio(portfolioList);
                });
               
            });
    
            channel.subscribe("portfolioController.portfolio.calculated", function(){
                calculateOutputData();
            });
    
    
            channel.subscribe("inputView.input.changed", function(){
                calculateOutputData();
            }); 
    
            channel.subscribe("inputController.calculate.output", function(){
                calculateOutputData();
            });     
    

            channel.subscribe("variableCalculator.calculateSuccessRateByInvestmentHorizon", 
                function(obj){
                    var input = getInput();
                    var successRate = portfolioService.calculateSuccessRate(input.getInitialInvestment(),
                                                                            obj.investmentHorizon,
                                                                            input.getSavingsGoal(),
                                                                            input.getContributionAmount());
                    obj.callback(successRate);
                }
            );
                   
            function loadModule(data){
                var branchCodeList = ['inRetirement', 'savingForOneTime'];
                channel.publish("portfolioController.getSavedPortfolioNameList", function(portfolioList){
                    postal.channel().subscribe("mainInputView.rendered", function(){
                        
                       
                        requirejs(['js/scripts']);
                     
                        
                        channel.publish("portfolioController.calculatePortfolioByIndex", getSelectedPortfolioIndex());
                        channel.publish("inputController.module.loaded");
                    });
                    mainInputView.render(portfolioList);
                });
            };
    
    
            function calculateOutputData(){
                var input = getInput();
                //console.log(input.getContributionAmount());
                var successRate  = portfolioService.calculateSuccessRate(input.getInitialInvestment(),
                                                                        input.getInvestmentHorizon(),
                                                                        input.getSavingsGoal(),
                                                                        input.getContributionAmount());
                
              
                channel.publish("inputController.output.seted", {
                    userInput: input,
                    successRate:  successRate,
                    endValueList: portfolioService.getRealEndValueList(),
                    regionStartYear: regionStartYear,
                    regionStartDate: regionStartDate,
                    baseInitialInvestmentList: portfolioService.getBaseInitialInvestmentList(input.getInvestmentHorizon()),
                    baseMonthlyContributionList: portfolioService.getBaseMonthlyContributionList(input.getInvestmentHorizon())
                });
                //currentBranchController.processOutput(variableData);
                //registerAccess("registerAccess");
            }

         
            function getInput(){          
                return mainInputView.getInput();
            }

          
            function getVariableData(){                
                return variableData;
            }
    
            function getVariableDefinition(modalityCode){
               
                var variableTranslation = messages.VARIABLES;
                var variableCode ="SHORTFALL_RISK";
                var helpList = [];
                var context = {};
                var input = getInput();
                context.ROLLING_PERIOD_COUNT = portfolioService.getRealEndValueList().length;
                context.INVESTMENT_HORIZON   = input.getInvestmentHorizon();
                context.REGION_START_YEAR    = window.localStorage.getItem("regionStartYear");   
                
                
                var isAdjForInflation = input.isContributionReal();
                var future ="future";
                var today ="today's";
                context.ADJ_FOR_INFLATION = [isAdjForInflation?future:today, isAdjForInflation?today:future];


                var variableList = [];
                for (variableCode in variableTranslation.VARIABLES){
                    var variable = variableTranslation["VARIABLES"][variableCode];
                    var name = variable.name;
                    var definition = Handlebars.compile(variable.definition)(context);
                    variableList.push({name: name, definition: definition});
                }


                var footerList = [];
                for (paragraph in variableTranslation.FOOTER){
                    var paragraph = variableTranslation["FOOTER"][paragraph];
                    paragraph = Handlebars.compile(paragraph)(context);
                    footerList.push(paragraph);
                }
                var context = {};
                context["VARIABLES"] = variableTranslation;
                context["FOOTER"] = messages.FOOTER;

//
//                channel.publish("inputController.getSuccessRate", function(successRate){
//                    context.SHORTFALL_RISK  = (1 - successRate);
//                    context.SUCCESS_RATE = successRate; 
//                    mainInputView.setKeyDefinition(context);                    
//                });
//                
                
                 mainInputView.setKeyDefinition(context);     
                
            }
    

            return {
                loadModule: loadModule,
                getInput: getInput,
                calculateOutputData: calculateOutputData,
                getSelectedPortfolioIndex: getSelectedPortfolioIndex
            };

        }
);
