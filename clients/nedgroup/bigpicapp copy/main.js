
var app_version;

requirejs.config( { 
    baseUrl: '/clients/nedgroup/bigpicapp/',
    paths: {
        lodash :   "vendors/js/lodash",
        postal :   "vendors/js/postal",
        jquery :   "vendors/js/jquery-1.11.3.min",
        cookie :   "vendors/js/jquery.cookie.min"
       
    },
    shim : {

		'cookie' : { 'deps' :['jquery'] }
	}
});  
   
var bigpic_custom_dir = '';
requirejs(['jquery',
           'postal', 
           'cookie',
           //'menu/menuController',
           'portfolio/portfolioController',
           'graphGallery/graphGalleryController',
           'userInput/inputController'],
          
          function($,
                postal, 
                cookie,
                portfolioController,
                graphGalleryController,
                inputController
                ){
    
                    app_version = $.cookie("app_version");

//                    $('window').bind('beforeunload',function(){ return 'are you sure you want to leave?'; });
//
//    
                    window.localStorage.setItem("lang", "en");

                    var regionCode = $.cookie("regioncode");
                    if (!regionCode) regionCode = "sa";
    
                    bigpic_custom_dir = 'generic/'+regionCode.toLowerCase();
    
                    var custom_group = $.cookie("custom_group");
//custom_group= 'northstr';
                    if (custom_group){
                        bigpic_custom_dir = 'custom/'+custom_group+'/'+regionCode.toLowerCase();
                    } 

                    

                    var initData = {languageCode:"en", regionCode: regionCode};

                    var portfolioModule = postal.channel().subscribe("portfolio.module.loaded", function(){
                        inputController.loadModule(initData);
                        portfolioModule.unsubscribe();
                    });
 
                    var branchesView = postal.channel().subscribe("inputController.module.loaded",function(branchCode){ 
                        graphGalleryController.loadModule("inRetirement");//TEMP;
                        branchesView.unsubscribe();
                    });    


                    portfolioController.loadModule(initData.regionCode);


                    if ($.cookie("plancode") == 'big_picture_basic'){
                        window.open('/logout.php', '_self', false);
                        return;
                    }

                    if (document.cookie.indexOf("remainingTime") >= 0){
                        //*1000 is because php returns seconds and jquery ($.now()) returns milliseconds.
                        var remainingTime = (($.cookie('remainingTime')*1000)-$.now());
                        console.log(remainingTime);
                        if (remainingTime<=0) remainingTime = 500;
                        setTimeout(function logout(){
                            $.cookie("remainingTime", null, { path: '/' });
                            $.cookie("trialExpired", true, { path: '/' });
                            window.open('/logout.php', '_self', false);
                        }, remainingTime);
                    }

                    $(window).resize(function(){
                        postal.channel().publish("app.screen.resized");
                    });


                    requirejs(['summary/summaryController']);

                    requirejs(['keyPrinciples/keyPrinciplesController']);

    
            }
);
