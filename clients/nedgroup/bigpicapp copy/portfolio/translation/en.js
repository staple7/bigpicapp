 
define({
    TITLE:                  "Edit and Save Portfolios",
    SELECT_PORTFOLIO:       "SELECT A PORTFOLIO",
    ASSET_ALLOCATION:       "ASSET ALLOCATION",
   	REGION_DENONYM_SHORT:   "U.S.",
    SAVE_AS:                "Save As",
	EQUITIES: 		  	    "Equities",
    GOLD:                   "Gold",
    SAVE:                   "Save selected portfolio",
	FIXED_INCOME:   	    "Fixed Income",   
    PORTFOLIO_SAVED:        "Portfolio successfully saved.",
    PORFTOLIO_NAME_CHANGED: "Portfolio name successfully changed",
    ALLOCATION_MUST_100:    "allocation must total 100%",
    MESSAGE_AT_BOTTOM:      "Monthly total return data from 1926 onward."
    
});
