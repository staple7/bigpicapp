var regionStartDate;//TEMP;
var regionStartYear;//TEMP;

define (['postal'], function(postal){
                 
        var realEndValueList = [];
        var baseStartingCapitalEndValueMap = {};
        var baseContributionEndValueMap = {};
        var oneDollarReturnList = [];
        var portfolioReturnsMap = {};
       // var realDataListMap = {};
        var nominalDataListMap = {}; 
        var inflationList = [];

        function loadData(portfolioConfig, regionCode){
            require(['portfolio/settings/' + regionCode + '/regionSettings',
                     'portfolio/rawData/'+regionCode+'/'+regionCode+'_i'],
                function(regionSettings, inflation){   
                //console.log(portfolioConfig);
                    inflationList = inflation.I;
                
                    regionStartDate = new Date(regionSettings.START_YEAR,0,1);
                    regionStartYear = regionSettings.START_YEAR;
                
                    localStorage.setItem("regionStartYear", regionSettings.START_YEAR);
                    localStorage.setItem("dataLastUpdatedOn", 
                        getDataLastUpdatedOn(inflation.I.length, regionSettings.START_YEAR));
                    var objectLength = Object.keys(portfolioConfig.ALLOCATION_MAP).length;
                    var allocationCodeList = [];
                    $.each(portfolioConfig.ALLOCATION_MAP, function(index, assetObject){
                        allocationCodeList.push(assetObject.CODE);
                    });
                
                    $.each(allocationCodeList, function(index, assetCode){
                        require(['portfolio/rawData/'+regionCode + '/' +assetCode],function(rawData){
                             //   realDataListMap[assetCode]    = rawData["REAL"]; 
                                nominalDataListMap[assetCode] = rawData["NOMINAL"]; 
                                if (allocationCodeList.length == Object.keys(nominalDataListMap).length){
                                    postal.channel().publish("portfolioService.data.loaded");
                                }
                            }
                        );
                    });   
                }
            );
        }


        function calculateSuccessRate(initialInvestment,
                                      investmentHorizon,
                                       savingsGoal,
                                       monthlyContribution){

            // var start = new Date().getTime();

            var endValue; 
            var savingsGoalCount = 0;
            var endValueIndex = 0;
            realEndValueList = [];

            var baseInitialInvestmentList   = baseStartingCapitalEndValueMap[investmentHorizon];
            var baseMonthlyContributionList = baseContributionEndValueMap[investmentHorizon];


            var dataLength =  baseInitialInvestmentList.length;

            for (endValueIndex = 0; endValueIndex < dataLength; endValueIndex ++){
                endValue = (baseInitialInvestmentList[endValueIndex] * initialInvestment) + 
                           (baseMonthlyContributionList[endValueIndex] * monthlyContribution);
                realEndValueList.push(endValue);   
                if (endValue >= savingsGoal){
                    savingsGoalCount ++;
                }
            }
//            $.each(realEndValueList, function(i, value){
//                console.log(value);
//            });
//            console.log("////////////----------///////////////");
//                   
            
            //console.log("S.R:", (savingsGoalCount>0)?(savingsGoalCount / dataLength):0); 
            return (savingsGoalCount>0)?(savingsGoalCount / dataLength):0;
        }
            
    
        function calculatePortfolioRisk(){
            return stdev(oneDollarReturnList).deviation;
        }
    
        var stdev = function(arr) {
            var n = arr.length;
            var sum = 0;

            arr.map(function(data) {
                sum+=data;
            });

            var mean = sum / n;

            var variance = 0.0;
            var v1 = 0.0;
            var v2 = 0.0;

            if (n != 1) {
                for (var i = 0; i<n; i++) {
                    v1 = v1 + (arr[i] - mean) * (arr[i] - mean);
                    v2 = v2 + (arr[i] - mean);
                }

                v2 = v2 * v2 / n;
                variance = (v1 - v2) / (n-1);
                if (variance < 0) { variance = 0; }
                stddev = Math.sqrt(variance);
            }

            return {
                mean: Math.round(mean*100)/100,
                variance: variance,
                deviation: Math.round(stddev*100)/100
            };
        };
    
        function calculatePortfolio(mer,
                                    isContributionReal,
                                    rebalancingFactor, 
                                    portfolioAllocation){
            
            //console.log("mer", mer, "isContributionReal", isContributionReal, "reb", rebalancingFactor, portfolioAllocation);
            
            oneDollarReturnList = [];
            
            var start = new Date().getTime();

          //  var isPortfolioReal = false;

            var yearLimit = 40;//TEMP  
          
            mer = (1 - mer);

            baseStartingCapitalEndValueMap = {};
            baseContributionEndValueMap = {};                
 
            var dataListMap = nominalDataListMap;
         
            var assetAllocation;
            var startingCapital = 0;
            var contribution = 0;
            var maxHorizonLength = 0;
            var rollingPeriodIndex = 0;
            var maxRollingPeriodLength = 0;
            var startingCapitalEndValue = 0;
            var assetIndex = 0;
            var inflationIndex = 0;
            var startingCapitalValueList = [];
            var contributionValueList = [];
            var contributionEndValue = 0;
            var endValueInYear = 0;
            var dataHistoryLength = dataListMap[Object.keys(dataListMap)[0]].length;
            var inflation = 1;
            var endValueList = [];
            var inflationValue;
 
            var lastStartingCapitalEndValue;
            var lastContributionEndValue;
            var dataPeriodFactor = 12;
            var rebalancingCount = 0;
            var rebalancingDone = false;
 
            for (; yearLimit > 0; yearLimit--){
                 
                maxHorizonLength = (yearLimit * dataPeriodFactor);
                
                for (;(rollingPeriodIndex + maxHorizonLength) 
                     <= dataHistoryLength; rollingPeriodIndex++){
 
                    maxRollingPeriodLength  = (rollingPeriodIndex + maxHorizonLength);
                    assetIndex = rollingPeriodIndex;
                    inflationIndex = rollingPeriodIndex;
                    inflationValue = 1;
                    rebalancingDone = false;
                    rebalancingCount = 0;
                     
                    for (; assetIndex < maxRollingPeriodLength;){

                        for (var key in portfolioAllocation){
                             
                            assetAllocation = portfolioAllocation[key];
                            returnList = dataListMap[key];
                            if (assetIndex == rollingPeriodIndex){
                               startingCapital = assetAllocation;
                               contribution = 0;
                            }else if (rebalancingFactor>0 && rebalancingCount==rebalancingFactor){
                                startingCapital = lastStartingCapitalEndValue * assetAllocation; 
                                contribution = lastContributionEndValue * assetAllocation;
                                rebalancingDone = true;
                            }else{
                                startingCapital = startingCapitalValueList[key];
                                contribution = contributionValueList[key];
                            }
  
                            inflation = inflationValue;
 
                            for (var index = assetIndex; index < (assetIndex + dataPeriodFactor); index++){
                                
                                if (isContributionReal){ 
                                    inflation *= (inflationList[inflationIndex]);
                                }
                                 
                                returnValue = returnList[index];
                                 
                                startingCapital = (startingCapital * (returnValue));
                                 
                                contribution = (contribution * (returnValue))
                                                +
                                                (inflation)
                                                * 
                                                assetAllocation;
 
                                inflationIndex++;
                            }
 
                            inflationIndex -= dataPeriodFactor;
 
                            startingCapital *= mer;
                            contribution *= mer;
 
                            startingCapitalValueList[key] = startingCapital;
                            contributionValueList[key]    = contribution;
 
                            startingCapitalEndValue += startingCapital;
                            contributionEndValue += contribution;
 
                        }
                         
                        if (rebalancingDone){                           
                            rebalancingDone = false;
                            rebalancingCount = 0;
                        }
                         
                        rebalancingCount ++;
                         
                        inflationValue = inflation;
 
                        endValueInYear++;
 
                        endValueList = baseStartingCapitalEndValueMap[endValueInYear];
                        endValueList === undefined ? 
                                        baseStartingCapitalEndValueMap[endValueInYear] =
                                        [startingCapitalEndValue] :
                                        endValueList.push(startingCapitalEndValue);
    
                        if (endValueInYear == 1){
                            oneDollarReturnList.push((startingCapitalEndValue-1)/1);
                        }
 
                        endValueList = baseContributionEndValueMap[endValueInYear];
                        endValueList === undefined ? 
                                        baseContributionEndValueMap[endValueInYear] = 
                                        [contributionEndValue] : 
                                        endValueList.push(contributionEndValue);
 
                        
                        lastStartingCapitalEndValue = startingCapitalEndValue;
                        lastContributionEndValue = contributionEndValue;
 
                        startingCapitalEndValue = 0;
                        contributionEndValue    = 0;
                        assetIndex += dataPeriodFactor;
                        inflationIndex += dataPeriodFactor;
 
                    }
                   
                    endValueInYear=0;
                }
 
            }
   
           
            postal.channel().publish("portfolioService.portfolio.done");
        }        



        function getRealEndValueList(){
            return realEndValueList;
        }

        function getBaseInitialInvestmentList(investmentHorizon){
            
            //console.log(baseStartingCapitalEndValueMap[investmentHorizon]);
            return baseStartingCapitalEndValueMap[investmentHorizon];
        } 

        function getBaseMonthlyContributionList(investmentHorizon){
            // console.log(investmentHorizon, baseContributionEndValueMap[investmentHorizon]);
            return baseContributionEndValueMap[investmentHorizon];
        }    

        function getBaseDataMap(){
            return {startingCapitalMap: baseStartingCapitalEndValueMap,
                   contributionMap: baseContributionEndValueMap};
        }
     
        function getDataLastUpdatedOn(length, regionStartYear){         
            var startDate = new Date("Jan 01 "+regionStartYear);            
            return new Date(startDate.setMonth(startDate.getMonth()+(length-1)));
        }

        return {
            loadData:                       loadData,
            calculatePortfolio:             calculatePortfolio,
            getRealEndValueList:            getRealEndValueList,              
            getBaseInitialInvestmentList:   getBaseInitialInvestmentList,
            getBaseMonthlyContributionList: getBaseMonthlyContributionList,
            calculateSuccessRate:           calculateSuccessRate,
            getBaseDataMap:                 getBaseDataMap,
            calculatePortfolioRisk:         calculatePortfolioRisk
        };

    }
);      