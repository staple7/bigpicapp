
define (['postal', 'portfolio/portfolioController'], function(postal, portfolioController){

    var portfolioTotal = 100;
    var assetCurrentValue = 0; 
    var timer;
    var currentPortfolioIndex;
    
    function render(context, index){
        portfolioTotal = 100;
        currentPortfolioIndex = index;
        var container = $('.portfolio_container');
        
              //  <link href="portfolio/portfolio.css" rel="stylesheet" type="text/css">
            

        container.load('portfolio/portfolioTemplate.html?v'+app_version, function(template){
           // container.append($('<link rel="stylesheet" type="text/css"/>').attr('href', 'portfolio/portfolio.css'));
            container.html("<link href='portfolio/portfolio.css?v"+app_version+"' rel='stylesheet' type='text/css'>" +Handlebars.compile(template)(context));
            $('.note-date').html(localStorage.getItem("regionStartYear"));
            $('.message').animate({'opacity': '0'}, 0);
            setAssetArrayColor();
            bindEvents();
           // $('.portfolio-list input[type=text].'+index).focus();
        }); 
    } 
    

    
    function portfolioSelected(indexSelected){
        if (indexSelected == currentPortfolioIndex) return;
//        var savedName = portfolioController.getPortfolioNameByIndex(currentPortfolioIndex);
//        setPortfolioNameByIndex(currentPortfolioIndex, savedName);
        currentPortfolioIndex = indexSelected;
        portfolioController.portfolioSelected(currentPortfolioIndex);
        enableOrDisableButtons(true);
    }

    function portfolioLeft(){
        if (portfolioTotal == 100){
            var map = {};
            var radio = $('.portfolio-list input[type=radio].'+currentPortfolioIndex);
            var index = radio.attr('class');
            var name = radio.next().val();
            $('.portfolio-allocation-box').find('input[type=text]').each(function(index, element){
                map[element.id] = {CODE:$(this).attr("data-id"), VALUE: parseInt($(this).val())};
            });
            portfolioController.saveButtonPressed({index: index, name: name, map: map});
        }else{
            portfolioSelected(currentPortfolioIndex);            
        }
    }
    
    
    function allocationBoxLeft(){
        if (portfolioTotal == 100){
            var map = {};
            var radio = $('.portfolio-list input[type=radio].'+currentPortfolioIndex);
            var index = radio.attr('class');
            var name = radio.next().val();
            $('.portfolio-allocation-box').find('input[type=text]').each(function(index, element){
                map[element.id] = {CODE:$(this).attr("data-id"), VALUE: parseInt($(this).val())};
            });
            portfolioController.saveButtonPressed({index: index, name: name, map: map});
        }
    }
    
    function bindEvents(){
        $('.portfolio-list input[type="text"]').on({
//            'click touchstart': function(e){                
//                e.stopPropagation();
//                e.preventDefault();
//                portfolioSelected($(this).attr('class'));
//            },
            'focus': function(e){                
                e.stopPropagation();
                e.preventDefault();
              
                portfolioSelected($(this).attr('class'));
            },
            'blur': function(e){                
                e.stopPropagation();
                e.preventDefault();
                portfolioLeft();
            }
        });

        $('.portfolio-list input[type="radio"]').on({
            'click touchstart': function(e){                
                e.stopPropagation();
                e.preventDefault();
                portfolioSelected($(this).attr('class'));
            }
        });
        
        $('.portfolio-input-text').on({
            'focus': function(e){
                assetCurrentValue = parseInt($(this).val());
                $(this).val(assetCurrentValue == 0 ? '' : assetCurrentValue); 
            },
            'input': function(e){
                var value = parseInt($(e.target).val());
                enableOrDisableButtons((showPortfolioTotal(value) === 100)? true : false);
            },
            'keypress': function(e){
                var $this = $(this);
                var newValue = ($this.val() + String.fromCharCode(e.keyCode));
                if (stopInput(e, $this, newValue, 100)){
                    e.preventDefault();
                }
                if(e.keyCode == 13){
                    $this.blur();
                }
            },
            'blur': function(e){
                var $this = $(this);
                var value = $this.val();
                setAssetColor($this);
                value = value == ''|| (value.indexOf('%')>-1) ? 0 : value;
                $this.val(formatPercentage(parseInt(value)/100)); 
                allocationBoxLeft();
           }
        });
        
        $('.learnMoreGFD').on({'click' : function(e){
                window.open('http://www.globalfinancialdata.com', '_blank');
            }
        });

        $('.learnMoreCRSP').on({'click' : function(e){
                window.open('http://www.crsp.com', '_blank');
            }
        });
        
       
        bindSaveBtnEvent();

    }
    
    function unbindEvents(){
        $('.portfolio-list input[type="text"]').unbind('click touchstart');
        $('.portfolio-save-button').unbind('click touchstart');
        $('.portfolio-input-text').unbind('focus input keypress blur');
        $('.learnMoreGFD').unbind('click');
        $('.learnMoreCRSP').unbind('click');
    }
    
    function setAssetArrayColor(){
        $('.portfolio-allocation-box').find('input[type=text]').each(function(){
            setAssetColor($(this));
        }); 
    }
    
    function setAssetColor(element){
        var value = getNumber(element.val());
        if(value==0 || value == ''){
            element.css('color', '#999').prev().css('color', '#999');
        }else{
            element.css('color', '#000').prev().css('color', '#000');
        }        
    }
    
    function bindSaveBtnEvent(){
        $('.portfolio-save-button').on({
            'click touchstart': function(e){
                var map = {};
                var radio = $('.portfolio-list input[name=portoption]:checked');
                var index = radio.attr('class');
                var name = radio.next().val();
                $('.portfolio-allocation-box').find('input[type=text]').each(function(index, element){
                    map[element.id] = {CODE:$(this).attr("data-id"), VALUE: parseInt($(this).val())};
                });
                showSavedPortfolioMessage();
                portfolioController.saveButtonPressed({index: index, name: name, map: map});
            }
        });
    }
    
    function checkPortfolioByIndex(index){        
        
        currentPortfolioIndex = index;
        $('.portfolio-list .'+parseInt(index)).prop('checked', true);
    }
    
    function showSavedPortfolioMessage(){
        var element = $('.message');
        element.removeAttr('style').stop().animate({'opacity':'1'},0);
        clearTimeout(timer);
        timer = setTimeout(function(){
            element.animate({'opacity': '0'}, 600);
        }, 1500);
    }
        
    function enableOrDisableButtons(enable){
        var totalElement = $('.portfolio-total-number');
        var button = $('.portfolio-save-button');
        if (enable == false){
            totalElement.addClass("warning");
            button.addClass("disable-btn").removeAttr('style');
            button.unbind('click touchstart');
        }else{
            $('.portfolio-save-button').unbind('click touchstart');//temp
            bindSaveBtnEvent();
            totalElement.removeClass("warning");
            button.removeClass("disable-btn");  
        }
    }

    function showPortfolioTotal(newValue){
        if (!newValue || newValue=="") newValue = 0;
        portfolioTotal = (portfolioTotal - assetCurrentValue) + parseInt(newValue);
        assetCurrentValue = newValue;
        $(".portfolio-total-number").html(formatPercentage(portfolioTotal/100));
        return portfolioTotal;
    }
    
    function resetTotalPortfolio(){
        portfolioTotal = 100; 
        var totalElement = $('.portfolio-total-number');
        totalElement.html(formatPercentage(1));
        totalElement.removeClass("warning");   
        $(".portfolio-total-number").html(formatPercentage(portfolioTotal/100));
    }

    function showPortfolioData(allocationMap, index, savedName){
        resetTotalPortfolio(); 
        checkPortfolioByIndex(index);
        setPortfolioNameByIndex(index, savedName);
        setPortfolioAllocation(allocationMap);
    }
    
    function setPortfolioNameByIndex(index, name){
        ($('.portfolio-list').find('input[type=text]').eq(index)).val(name);
    }
    
    function setPortfolioAllocation(allocationMap){
        $('.portfolio-allocation-box').find('input[type=text]').each(function(){
            var $this = $(this);
            var allocation = allocationMap[$this.attr("data-id")];
            if (!allocation) allocation = 0;      
            $this.val(formatPercentage(allocation));
            setAssetColor($this);
        });        
    }
    
    function formatPercentage(value, lang){
        return new Number(value).toLocaleString('en', {style: 'percent'});
    }
    
    function isValidKey(e){
        var keyCode = e.keyCode;
        if (e.shiftKey || (-1==$.inArray(keyCode,[9,13,37,39,48,49,50,51,52,53,54,55,56,57,8]))){
            return false;        
        }
        return true;
    }
    
    function stopInput(e, $this, newValue, maxValue){
        if (!isValidKey(e, false)){
            e.preventDefault();
        }
        if (newValue > maxValue){
             if (isTextSelected($this[0])){
                 newValue = String.fromCharCode(e.keyCode);
             }else{
                 e.preventDefault();
             }
        }
    }
    
    function getNumber(value){
        if (!isNumeric(value)){
            value = value.replace(/[^0-9.]/g, "");
        }
        return value;
    }
    
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    
    function isTextSelected(input) {
        if (typeof input.selectionStart == "number") {
            return input.selectionStart == 0 && input.selectionEnd == input.value.length;
        } else if (typeof document.selection != "undefined") {
            input.focus();
            return document.selection.createRange().text == input.value;
        }
    }
  
    Handlebars.registerHelper('formatPercentage', function($this, num){
        var value = $this.ALLOCATION_MAP[num].VALUE;
        return formatPercentage(value/100);
    });
    
    Handlebars.registerHelper('getPortfolioTotal', function(){
        return formatPercentage(1);
    });
    
   

    return{
        render: render,
        checkPortfolioByIndex: checkPortfolioByIndex,
        setPortfolioAllocation: setPortfolioAllocation,
        showPortfolioData: showPortfolioData
    }
  
});