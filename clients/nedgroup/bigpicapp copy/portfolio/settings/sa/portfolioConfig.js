define({
    
DEFAULT_PORTFOLIO_LIST: 
[
     {
        NAME: "Equity",
        ID: 5,
        ALLOCATION_MAP:{
            1:{CODE:"sa_1",VALUE:0},
            2:{CODE:"sa_2",VALUE:70},
            3:{CODE:"sa_3",VALUE:0},
            4:{CODE:"a_1",VALUE:30},
            5:{CODE:"a_2",VALUE:0},
            6:{CODE:"us_1",VALUE:0}  
        }    
     },
     {
        NAME: "High Equity",
        ID: 4,
        ALLOCATION_MAP:{
            1:{CODE:"sa_1",VALUE:10},
            2:{CODE:"sa_2",VALUE:52},
            3:{CODE:"sa_3",VALUE:10},
            4:{CODE:"a_1",VALUE:17},
            5:{CODE:"a_2",VALUE:8},
            6:{CODE:"us_1",VALUE:3}  
        }    
     },        
     {
        NAME: "Medium Equity",
        ID: 3,
        ALLOCATION_MAP:{
            1:{CODE:"sa_1",VALUE:15},
            2:{CODE:"sa_2",VALUE:40},
            3:{CODE:"sa_3",VALUE:20},
            4:{CODE:"a_1",VALUE:12},
            5:{CODE:"a_2",VALUE:10},
            6:{CODE:"us_1",VALUE:3}  
        }    
    },
    {
        NAME: "Low Equity",
        ID: 2,
        ALLOCATION_MAP:{
            1:{CODE:"sa_1",VALUE:20},
            2:{CODE:"sa_2",VALUE:30},
            3:{CODE:"sa_3",VALUE:27},
            4:{CODE:"a_1",VALUE:8},
            5:{CODE:"a_2",VALUE:12},
            6:{CODE:"us_1",VALUE:3}  
        }    
    },
    {
        NAME: "Income",
        ID: 1,
        ALLOCATION_MAP:{
            1:{CODE:"sa_1",VALUE:50},
            2:{CODE:"sa_2",VALUE:5},
            3:{CODE:"sa_3",VALUE:35},
            4:{CODE:"a_1",VALUE:0},
            5:{CODE:"a_2",VALUE:5},
            6:{CODE:"us_1",VALUE:5}  
           }
    },     
    {
        NAME: "Cash",
        ID: 0,
        ALLOCATION_MAP:{
            1:{CODE:"sa_1",VALUE:0},
            2:{CODE:"sa_2",VALUE:0},
            3:{CODE:"sa_3",VALUE:100},
            4:{CODE:"a_1",VALUE:0},
            5:{CODE:"a_2",VALUE:0},
            6:{CODE:"us_1",VALUE:0}           
            
           }
    }
    
    
]
});   



