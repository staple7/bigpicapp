 
define({

    graphTitle    : "Historical Safe Savings Rate",
    graphSubtitle : {
        normal: "Monthly contribution required to meet savings goal, by rolling {{investmentHorizon}}-year period, {{regionStartYear}} to present",
        abbreviate: "Contribution / mo. req'd to meet goal, by rolling {{investmentHorizon}}-yr. period, {{regionStartYear}} – present"
    },
    xAxisTitle: "{{investmentHorizon}}-YEAR INVESTMENT PERIOD STARTING IN...",
    y2AxisTitle: "Your savings rate",
    y2AxisTitleShort: "Your sav. rate"
   	

});

    