

define(['postal', 'graphGallery/graphs/mountain/messages/en'], function(postal, messages){
    
        var localGraphData;
        var periodicity; 
        var context;
        var y2AxisLabel;   
        var timer;
    
        function graph(graphData){
            
            localGraphData = graphData;

            removeElements();
      
            var savingsGoal         = graphData.userInput.getSavingsGoal();
            var investmentHorizon   = graphData.userInput.getInvestmentHorizon();
            var endValueList        = graphData.endValueList;
            var regionStartDate     = graphData.regionStartDate;
            var result;
            
            setGraphTitles(investmentHorizon);    
            
            result = calculateDataList(endValueList, regionStartDate);

            var dataList = result[0];
                        
            var minXValue = result[1];
            var maxXValue = result[2];
            var minEndValue = result[3];
            var maxEndValue = result[4];
            var positiveNumList = result[5];
            
            svg = svg.datum(dataList);
                     
            yAxis = d3.svg.axis(); 
            xAxis = d3.svg.axis();
            
            x = d3.scale
                .linear()                   
                .domain([minXValue, maxXValue])
                .range([0, graphWidth]); 
 
            var factor = (maxXValue-minXValue)/6;
            var xTickValues = [];       
           
            for(var i=0; i<(7);i++){
                xTickValues.push(i==0?minXValue:minXValue+factor*i);
            }
            
            xAxis.scale(x)
                 .orient("bottom")
                 .tickValues(xTickValues)
                 .tickFormat(function(d) {return Math.round(d);});
              
            var yTickValues = [];

            for(var numberOfTicks=3; factor<10000; numberOfTicks--){
                factor=(maxEndValue-minEndValue)/numberOfTicks;
            }
            
            for(var i=0; i<(numberOfTicks+2);i++){
                yTickValues.push(i==0?minEndValue:minEndValue+factor*i);
            }
            
            y = d3.scale
                .linear()
                .domain([minEndValue, maxEndValue])
                .range([graphHeight, 0]);

            yAxis.scale(y)
                .orient("left")
                .tickValues(yTickValues)            
                .tickFormat(function(d) {
                    return Handlebars.compile("{{formatNumber type format value}}")
                            ({type: 'currency', format: 'rounded', value: d})
                });

            svg.selectAll(".x.axis").call(xAxis);
            svg.selectAll(".y.axis").call(yAxis);
           
            var barWidth = (graphWidth / dataList.length);
        
            var barIndex = 0;
      
            var shorftallRiskCount=0;
            
            svg.append("g")
                .attr("id","graphBars")
                .selectAll(".rect")
                .data(dataList)
                .enter().append("rect")
                .attr("x", function(d){
                    var thisObj = d3.select(this);
                    thisObj.attr("y", y(d.y));    
                    thisObj.attr("height", Math.abs(graphHeight -  y(d.y)));  
                    thisObj.attr("fill", "#7fb1a0");
                    return (barIndex++)*barWidth;
                })
                .attr('width', barWidth);         

            var y2 = d3.scale
                   .linear()
                   .domain([minEndValue, maxEndValue])
                   .range([graphHeight, 0]);

                
            var savingsGoalLessThanMax = (savingsGoal<=maxEndValue);
            
            var graphWidthLimit = 1359;
            
            var y2Axis = d3.svg
                        .axis()
                        .scale(y2)
                        .tickValues([savingsGoalLessThanMax?savingsGoal:''])
                        .tickFormat(function(d, i){
                             return (savingsGoalLessThanMax) ? y2AxisLabel : '';
                        })
                        .orient("right")
            
                svg.append("g") 
                    .attr("class", "y axis")
                    .attr("id", "y2Axis")
                    .attr("transform", "translate(" + graphWidth + " ,0)")   
                    .call(y2Axis)
                    .selectAll(".tick text")
                    .call((new String).wrap, 10)
                    .attr("transform", "translate(15, -35)");  
            
            if (savingsGoalLessThanMax){
                svg.append("line")
                   .attr("id", "line1")
                   .attr("x1", 0)
                   .attr("x2", graphWidth)                   
                   .attr("stroke", "#197254")
                   .attr('stroke-width', '0.5px')
                   .attr("transform", "translate(" + 0 + "," + y(savingsGoal) + ")");
            }
            
            
            svg.selectAll(".x.axis")
              .selectAll(".tick text")
              .each(function (d, i) {
                 d3.select(this).style("text-anchor",i == 0?"start":i==xTickValues.length-1?"end":"middle");
                 d3.select(this).attr("class", "normalText");  
              });  
            
            svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                d3.select(this).attr("class", "smallerText");                      
            });
         
           
//            positiveNumList = positiveNumList.sort(function(a, b) {return a - b;});
//           
//            var median = positiveNumList[Math.round(positiveNumList.length/2)];
//            
//            median = Handlebars.compile("{{formatNumber type format value}}")
//                                        ({type: 'currency', format: 'rounded', value: median});
//            
//            svg.append("text")
//                .attr("id", "medianText")
//                .attr("class", "lightText")  
//                .text("Median: " + median)
//                .attr("y", -13)
//                .attr("x", function (){
//                    return (graphWidth - d3.select(this).node().getComputedTextLength())/2
//                })
//            
            
            clearTimeout(timer);
                timer = setTimeout(function(){
                    calculateBubbleList(dataList);
            }, 500);
                
            function calculateBubbleList(dataList){
                var regionStartDate = new Date(graphData.regionStartDate);
                $("#activeAreas").remove();
                var activeAreaBarIndex = 0;
                var barWidth = (graphWidth / dataList.length);
                svg.append("g")
                    .on("mouseleave",function(){
                        postal.channel().publish("graphService.graph.hide.bubble");
                    })
                    .attr("id","activeAreas")
                    .selectAll(".rect")
                    .data(dataList)
                    .enter()
                    .append("rect")
                    .attr("x", function(d){
                        var thisObj = d3.select(this);
                        thisObj.attr("y", 0);  
                        thisObj.attr("height", graphHeight);  
                        thisObj.attr("width", barWidth)
                        thisObj.attr("fill", "transparent")
                        return (activeAreaBarIndex++)*barWidth;
                    }).on("mouseover",function(d) {
                        var regionStartDate = new Date(graphData.regionStartDate);
                        var value = dataList[d.x];
                        var dataStartDate = new Date(regionStartDate.setMonth(value.x));
                        var endValue = Handlebars.compile("{{formatNumber type format value}}")
                                    ({type: 'currency', format: 'rounded', value: value.y});
                        postal.channel().publish("graphService.graph.show.bubble", {text:endValue + 
                                                                                    " starting" + " " 
                                                                                    + d3.time.format("%m/%y")(dataStartDate),
                                                                                    xPosition: d3.mouse(this)[0]});
                    });      
            }
                
            
            postal.channel().publish("graphService.graph.done");
        }
  

        function calculateDataList(endValueList, regionStartDate){
            var lineDataList = [];
            var endValue;
            var minXValue;
            var maxXValue;
            var minYValue;
            var maxYValue;       
            var monthIndex = 0;
            var positiveNumList = []; 
            var regionStartDate = localGraphData.regionStartDate;
            for(var index=0; index<endValueList.length; index += periodicity){
                endValue = endValueList[index];
                if (endValue<0) endValue = 0;
                maxYValue = (maxYValue === undefined || endValue > maxYValue) ? endValue : maxYValue;
                minYValue = (minYValue === undefined || endValue < minYValue) ? endValue : minYValue;
                lineDataList.push({x:monthIndex, y:endValue});
                positiveNumList.push(endValue);
                monthIndex+=periodicity;
            }          
            minXValue = new Date(regionStartDate).getFullYear();
            maxXValue = new Date(new Date(regionStartDate).setMonth(regionStartDate.getMonth() + monthIndex)).getFullYear();
            return [lineDataList, minXValue, maxXValue, 0, maxYValue, positiveNumList];            
        }
    
        
        function setGraphTitles(investmentHorizon){
            xAxisTitle.html(Handlebars.compile(context.xAxisTitle)({investmentHorizon:investmentHorizon}));       
            
            var graphWidthLimit = 1359;
            var documentWidth = $(document).width();
            var graphSubtitle = context.graphSubtitle.normal;

            if  (documentWidth<graphWidthLimit){
                graphSubtitle = context.graphSubtitle.abbreviate
            }
            
            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                ({investmentHorizon:investmentHorizon,
                                regionStartYear:localStorage.getItem("regionStartYear")}));
            
        }     
    

        function removeElements(){
            $("#graphPath").remove();
            $("#y2Axis").remove();
            $("#graphBars").remove();
            $("#activeAreas").remove();
            $("#line1").remove();
            $("#medianText").remove();
        }
    
        
        function finalize(){
            removeAxis();
            removeElements();
            localGraphData = null;
            xAxisTitle.html("");
            context = null;
            periodicity = null;
            y2AxisLabel = null;
            clearTimeout(timer);
            timer = null;
        }
    
        function branchChanged(){
         
            finalize();
        }
    
        function graphChanged(){
            finalize();
        }
    
        
    
        function getGraphDefinition(){
            return Handlebars.compile(context.graphDefinition)(localGraphData);
        }

        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            graphWidth = (containerWidth * 0.80);
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
    
        function removeAxis(){
            svg.selectAll(".x.axis").remove();
            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            svg.append("g").attr("class", "x axis");
            svg.append("g").attr("class", "y axis");
        }
    
        function init(lang, branchCode){
            addAxis();
            calculateGraphDimensions();
            periodicity = 1; 
          
            context = messages[branchCode]; 
            y2AxisLabel = context.y2AxisTitle;
            $(".graph_title").html(context.graphTitle);
            

            yAxisTitle.html("");
            postal.channel().publish("graphGallery.graph.initiated");
           
        }

        
        function getGraphElements(){
            
        }

    
        return {
            init:               init,
            graph:              graph,
            getGraphDefinition: getGraphDefinition,
            graphChanged:       graphChanged,
            getGraphElements:   getGraphElements,
            branchChanged:      branchChanged
        };
    
});     
                