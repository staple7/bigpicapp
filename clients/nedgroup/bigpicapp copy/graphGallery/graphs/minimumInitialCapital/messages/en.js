 
define({

   
    graphTitle:     "Historical Minimum Nest Egg",
    graphSubtitle : {
        normal: "Initial portfolio values required to sustain spending, by rolling {{investmentHorizon}}-year period, {{regionStartYear}} to present", 
        abbreviate:  "Initial capital req'd to sustain spending, by rolling {{investmentHorizon}}-yr. period, {{regionStartYear}} – present"
    },
    xAxisTitle:     "{{investmentHorizon}}-YEAR RETIREMENT PERIOD STARTING IN...",
    y2AxisTitle:    "Your starting capital",
   	y2AxisTitleShort: "Your start. capital"

});

    