define(['postal', 'graphGallery/graphs/minimumInitialCapital/messages/en'], function(postal, messages){

        var y2AxisLabel;
        var timer;
    
        function graph(graphData){    
           
            removeElements();            

            var savingsGoal           = graphData.userInput.getSavingsGoal();
            var regionStartDate       = graphData.regionStartDate;
            var monthlyContribution   = graphData.userInput.getContributionAmount();
            var userInitialInvestment = graphData.userInput.getInitialInvestment();
            
            setGraphTitles(graphData.userInput.getInvestmentHorizon());
            
         
           
            var result = calculateDataList(monthlyContribution,
                                            savingsGoal,
                                            regionStartDate,
                                            graphData.baseInitialInvestmentList,
                                            graphData.baseMonthlyContributionList,
                                            9999999);
           
 
            var dataList = result[0];
            var minXValue = result[1];
            var maxXValue = result[2];
            var maxYValue = result[3];
            var numberList = result[4];
            
            svg = svg.datum(dataList);
         
            yAxis = d3.svg.axis(); 
           
            xAxis = d3.svg.axis();            
 
            x = d3.scale
                .linear()                   
                .domain([minXValue, maxXValue])
                .range([0, graphWidth]);
             
            var factor = (maxXValue-minXValue)/6;
            var xTickValues = [];
            
            for(var i=0; i<(7);i++){
                xTickValues.push(i==0?minXValue:minXValue+factor*i);
            }
             
            xAxis.scale(x)
                 .orient("bottom")
                 .tickValues(xTickValues)                        
                 .tickFormat(function(d) {return Math.round(d);});
              
             
            y = d3.scale
                  .linear()
                  .range([graphHeight, 0]); 
               
             
            var yTickValues = [];
            yTickValues = yTickValues.calculateTicks('currency', 0, maxYValue, 0.01);
       
            yAxis.scale(y)
                 .orient("left")
                 .tickValues(yTickValues)
                 .tickFormat(function(d){
                         return d.format('currency', 0, d);
                 });
             
            y.domain([0, yTickValues[yTickValues.length-1]]);
 
            svg.selectAll(".x.axis").call(xAxis);
            svg.selectAll(".y.axis").call(yAxis);            
             
            var barIndex = 0;
             
            var barWidth = (graphWidth / dataList.length);
             
            svg.append("g")
                .attr("id","graphBars")
                .selectAll("rect")
                .data(dataList)
                .enter().append("rect")
                .attr("x", function(d){
                        var thisObj = d3.select(this);
                        thisObj.attr("height", graphHeight -  y(d.y));  
                        thisObj.attr("y", y(d.y));  
                        return (barIndex++)*barWidth;
                })
                .attr('width', barWidth)
                .attr("fill", "#7fb1a0");
         
                var y2 = d3.scale
                       .linear()
                       .domain([0, yTickValues[yTickValues.length-1]])
                       .range([graphHeight, 0]);  
            
                var initialInvLessThanMax = (userInitialInvestment<=maxYValue);
            
                var graphWidthLimit = 1359;
            
                var y2Axis = d3.svg
                            .axis()
                            .scale(y2)
                            .tickValues([initialInvLessThanMax?userInitialInvestment:''])
                            .tickFormat(function(d, i){
                               return (initialInvLessThanMax) ? 
                                        (($(document).width()<graphWidthLimit)?context.y2AxisTitleShort:context.y2AxisTitle) 
                                        : '';
                            })
                            .orient("right");

                svg.append("g") 
                    .attr("class", "y axis")
                    .attr("id", "y2Axis")
                    .attr("transform", "translate(" + graphWidth + " ,0)")   
                    .call(y2Axis)
                    .selectAll(".tick text")
                    .call((new String).wrap, 10)
                    .attr("transform", "translate(15, -35)");  
            
          
                if (initialInvLessThanMax){
                    svg.append("line")
                       .attr("id", "line1")
                       .attr("x1", 0)
                       .attr("x2", graphWidth)                   
                       .attr("stroke", "#197254")
                       .attr('stroke-width', '0.5px')
                       .attr("transform", "translate(" + 0 + "," + y(userInitialInvestment) + ")");
                }
            
                svg.selectAll(".x.axis")
                   .selectAll(".tick text")
                   .each(function (d, i) {
                        var textElement = d3.select(this);
                        textElement.style("text-anchor",i == 0?"start":i==xTickValues.length-1?"end":"middle");
                        textElement.attr("class", "normalText");     
                   }); 


                svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                    d3.select(this).attr("class", "smallerText");                    
                });


                if (maxYValue == 0){
                    svg.append("rect")
                        .attr("x", "0")
                        .attr("y", graphHeight-2)
                        .attr("width", graphWidth)
                        .attr("height", 1)
                        .attr("id", "graphPath")
                        .attr("class", "graph_path");     
                }
             
            
            var median = Handlebars.compile("{{formatNumber type format value}}")
                    ({type: 'currency', format: 'rounded', value: (maxYValue/2)});
            
            numberList = numberList.sort(function(a, b) {return a - b;});
           
            var median = numberList[Math.round(numberList.length/2)];
            
            median = Handlebars.compile("{{formatNumber type format value}}")
                                        ({type: 'currency', format: 'rounded', value: median});
            
            
            svg.append("text")
                .attr("id", "medianText")
                .attr("class", "lightText")  
                .text("Median: " + median)
                .attr("y", -13)
                .attr("x", function (){
                    return (graphWidth - d3.select(this).node().getComputedTextLength())/2
                })
            
            
            clearTimeout(timer);
                timer = setTimeout(function(){
                    calculateBubbleList(dataList);
            }, 500);
                
            function calculateBubbleList(dataList){
                var regionStartDate = new Date(graphData.regionStartDate);
                $("#activeAreas").remove();
                var activeAreaBarIndex = 0;
                var barWidth = (graphWidth / dataList.length);
                svg.append("g")
                    .on("mouseleave",function(){
                        postal.channel().publish("graphService.graph.hide.bubble");
                    })
                    .attr("id","activeAreas")
                    .selectAll(".rect")
                    .data(dataList)
                    .enter()
                    .append("rect")
                    .attr("x", function(d){
                        var thisObj = d3.select(this);
                        thisObj.attr("y", 0);  
                        thisObj.attr("height", graphHeight);  
                        thisObj.attr("width", barWidth)
                        thisObj.attr("fill", "transparent")
                        return (activeAreaBarIndex++)*barWidth;
                    }).on("mouseover",function(d) {
                        var regionStartDate = new Date(graphData.regionStartDate);
                        var value = dataList[d.x];
                        var dataStartDate = new Date(regionStartDate.setMonth(value.x));
                        var endValue = Handlebars.compile("{{formatNumber type format value}}")
                                        ({type: 'currency', format: 'rounded', value: value.y});
                        postal.channel().publish("graphService.graph.show.bubble", {text:endValue + 
                                                                                    " starting" + " " 
                                                                                    + d3.time.format("%m/%y")(dataStartDate),
                                                                                    xPosition: d3.mouse(this)[0]});
                    });      
            }
            postal.channel().publish("graphService.graph.done");
             
        }
     

        function calculateDataList(monthlyContribution,
                                  savingsGoal,
                                  regionStartDate,
                                  baseInitialInvestmentList,
                                  baseMonthlyContributionList,
                                  maxOutputValue){
            
            maxOutputValue = 99999999;
            monthlyContribution = monthlyContribution*1;
         
            var endValue;
            var lineDataList  = [];         
            var numberList = [];
            var incremental = 1;
            var divideFactor  = false;            
            var goUp = true;
            var initialFactor = 500000;
            var factor = initialFactor;      
            var initialInvestment = 0;
            var maxValue;
            var minXValue;
            var maxXValue
            var initialInvestmentValue;
            var monthlyContributionValue;            
            var maxLength = baseInitialInvestmentList.length;
            var monthIndex = 0;
            
            for(var index=0; index<maxLength; index += incremental){
                initialInvestmentValue = baseInitialInvestmentList[index];
                monthlyContributionValue = baseMonthlyContributionList[index];
                 
                while(initialInvestment < maxOutputValue && initialInvestment>=0){
                    
                    endValue = (initialInvestmentValue * initialInvestment) + 
                               (monthlyContributionValue * monthlyContribution);
                    
                    if (endValue >= savingsGoal){
                        if (initialInvestment == 0) break;
                        divideFactor = true;
                        goUp = false;
                    }else{
                        goUp = true;
                    }
                    factor = divideFactor ? factor /= 2 : factor;
                    if (factor >= 1){
                        initialInvestment = goUp ? (initialInvestment += factor): 
                                                   (initialInvestment -= factor);
                    }else{
                        break;
                    }
                }

                maxValue = (maxValue === undefined || initialInvestment > maxValue) 
                        ? initialInvestment : maxValue;
                
                factor = initialFactor;
                goUp = true;
                divideFactor = false;                    
                lineDataList.push({x:monthIndex, y:initialInvestment});
                numberList.push(initialInvestment);
                initialInvestment = 0;
                monthIndex += incremental;
            }
 
            minXValue = new Date(regionStartDate).getFullYear();
            maxXValue = new Date(new Date(regionStartDate).setMonth(regionStartDate.getMonth() + monthIndex)).getFullYear();
//console.log([lineDataList,  minXValue, maxXValue, maxValue]);
            return [lineDataList,  minXValue, maxXValue, maxValue, numberList];
        }    
    
     
        function setGraphTitles(invHorizon){  
            xAxisTitle.html(Handlebars.compile(context.xAxisTitle)({investmentHorizon:invHorizon}));  
            var graphWidthLimit = 1359;
            var documentWidth = $(document).width();
            var graphSubtitle = context.graphSubtitle.normal;

            if  (documentWidth<graphWidthLimit){
                graphSubtitle = context.graphSubtitle.abbreviate
            }
            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                ({investmentHorizon:invHorizon,
                                regionStartYear:localStorage.getItem("regionStartYear")}));
                        
        }
    
     
        function removeElements(){                 
            $("#graphBars").remove();
            $("#graphPath").remove();
            $("#line1").remove();
            $("#y2Axis").remove();
            $("#medianText").remove();
        }    

        function finalize(){
            removeAxis();
            xAxisTitle.html("");
            removeElements();
            y2AxisLabel = null;
            clearTimeout(timer);
            timer = null;
        }
     
        function branchChanged(){         
            finalize();
        }
     
        function graphChanged(){
            finalize();
        }
     
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            graphWidth = (containerWidth * 0.80);
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }  
    
        function removeAxis(){
            svg.selectAll(".x.axis").remove();
            svg.selectAll(".y.axis").remove();
        }

        function addAxis(){
            svg.append("g").attr("class", "x axis");
            svg.append("g").attr("class", "y axis");
        }

        function init(lang, branchCode){
            addAxis();
            calculateGraphDimensions();
            context = messages;
            y2AxisLabel = context.y2AxisTitle;
            $(".graph_title").html(context.graphTitle);
            postal.channel().publish("graphGallery.graph.initiated");
             
        }    

        return {
            init:               init,
            graph:              graph,
            graphChanged:       graphChanged,
            branchChanged:      branchChanged           
        };
 
    }
);