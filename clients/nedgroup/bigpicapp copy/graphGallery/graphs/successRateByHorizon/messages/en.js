 
define({

    inRetirement:{
        graphTitle    : "Historical Success Rate",
        graphSubtitle:  {
            normal:     "Frequency at which portfolio ended above legacy capital, by retirement length, {{regionStartYear}} to present",
            abbreviate: "Frequency at which portfolio ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle    : "YEARS INTO RETIREMENT"   
    },
    savingForOneTime: {
        graphTitle    : "Historical Success Rate",
        graphSubtitle:  {
            normal: "Frequency at which portfolio met savings goal, by investment horizon, {{regionStartYear}} to present",
            abbreviate: "Frequency at which portfolio met goal, {{regionStartYear}} – present"
        },
        xAxisTitle    : "YEARS SPENT SAVING"   
    }
    


});

    