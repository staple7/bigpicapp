 
define({

    graphTitle  : "Historical Portfolio Longevity",
    graphSubtitle: {
        normal: "Number of years portfolio lasted, by retirement start date, {{regionStartYear}} to present",
        abbreviate: "No. of yrs. portfolio lasted, by retirement start date, {{regionStartYear}} – present"
    },
    xAxisTitle: "RETIREMENT STARTING IN...",
    y2AxisTitle: "Your retirement length"

    
});

    