
    var graphWidth;
    var graphHeight;
    var svg, x, y, yAxis;
    var xAxis;
    var yAxis = d3.svg.axis(); 

//    var xAxis = d3.svg.axis();

//    var xAxisTitle;
    var yAxisTitle;
//    var y2AxisTitle;

    var graphSvg;
    var currentGraphIndex = 0;
    var maxGraphIndex;
    var xPos=0;

    var svgArea = ".svgArea";

    define (['postal'], function(postal){

        function start(){
            appendSvg();
            yAxisTitle = $(".yAxisTitle");
            graphSvg  = $(".graphSvg");
            postal.channel().publish("graph-gallery.template.rendered");
      
        }
        
    
        function graphChanged(index){
          
            showLoadingGif();
            postal.channel().publish("graph-gallery.graph.changed", index);
        }
      
        d3.selection.prototype.moveToFront = function() {
          return this.each(function(){
            this.parentNode.appendChild(this);
          });
        };
        
        function appendSvg(){
//            svg.append('svg').attr('id','graphSvg');
     
            svg = d3.select('#lifeline_svg')
                    .insert("svg")
//                    .attr("width", "100%")
//                    .attr("height", "120%")
                    .attr("id", "graphSvg")
                    .attr("xmlns", "http://www.w3.org/2000/svg")
                    .append("g");
    
            calculateGraphDimensions();
            
        }
     
 
        function calculateGraphDimensions(){
        
//            var yPos = 50;
//            var w = d3.select(".x.axis").node().getBBox();
//            graphWidth = w.width;
//            xPos = w.x;
            
            graphWidth +=2;
           
            graphHeight = ($(svgArea).height());   
            graphHeight=65;
            if ($(document).width() <= 1799){
//                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
//                graphHeight = ($(svgArea).height() * 0.60);   
//                yPos = 80;            
            }
//            yPos = -30; 
           
            svg.attr("transform", "translate(108,170)");
        }
        
       
        
      
        return {
            start: start,
            appendSvg: appendSvg
        };

    });