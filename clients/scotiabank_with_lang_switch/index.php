<!DOCTYPE html>

<?php
    $app_version = "1.0.24";
?>

<html>
    <head> 
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" /> 
        <link rel="stylesheet" href="css/bootstrap.min.css?v<?php echo $app_version;?>"/>
        <link rel="stylesheet" href="css/general.css?v<?php echo $app_version;?>"/>
        
        <link rel="stylesheet" href="css/graph.css?v<?php echo $app_version;?>" />
        <script src="js/vendor/d3.v3.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery-2.0.3.min.js"></script>
        <script src="js/vendor/jquery.cookie.js"></script>        
        <script type="text/javascript" src="js/vendor/bootstrap.min.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/jquery.hammer.min.js?v<?php echo $app_version;?>"></script>    
        <script type="text/javascript" src="js/vendor/jquery.mmenu_4_0_3.min.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/globalize.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/cultures/globalize.culture.en.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/cultures/globalize.culture.fr.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/data.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/data_update.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/icons.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/d3tip.js?v<?php echo $app_version;?>"></script>

        <script>
                // Handles back key to exit the app every time
        function backKeyHandler(e){
            e.preventDefault();
            navigator.app.exitApp();
        }

        // Capitalize
        if (!String.prototype.capitalize) {
            String.prototype.capitalize = function(lower) {
                return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
            };
        }

        </script>

        
        <title>The Big Picture</title>
        <style>
            
            .onepop {
                width: 80%;
                height: 46%;
                top: 5%;
                margin: auto;
                background-color: #fff;
                position: relative;
                border-radius: 4px;
            }

            #popupOverlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba( 0,0,0, 0.7 );
                text-align: center;
                z-index: 500;
            }

           
            
        </style>
        
        <script>
            $.cookie("app_version", "<?php echo $app_version;?>" , { path: '/' });
        </script>
    </head>
    
    <body>
        
        <div id='popupOverlay' class="small-screen-warning" style="display:none">
            <div class="onepop">
                <div style="padding:3%;text-align:center">
                    <br>
                    <span data-localize="small_screen_warning"></span>
                </div>  
            </div>
        </div>
        
      
         <div class="loaderPageGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>
        

        <script>
            setTimeout(function(){
                $(".loaderPageGraphic").css("display", "none");
                $("body").append("<script src=js/graph.js?v<?php echo $app_version;?>/>");
            },3000);
        </script>
               

        
<div id="parent">

    <div class="header text-center show">

        <div class="row" style="background:#E0E0E0;border-top: 1px solid #BDBDBD;border-bottom: 1px solid #BDBDBD;">
            <div class="col-6">

                <span class="pull-left" style="margin-top:2px;margin-left:17px;width='80px'">
                    <img src="https://www.bigpicapp.co/clients/scotiabank/graph-gallery/images/en/big_picture_logo.svg">                            </span>
                
            </div>
            
            
             <div class="col-6 pull-right" style="margin-right:5px">
          
<!--
                <div id="lang" class="btn-group" style="margin-right:35px;color:#0000EE">
                    <button type="button" class="btn btn-default hidden" value='en' data-localize="en_translation_code" style="margin-right:5px"></button>
                    <button type="button" class="btn btn-default" value='fr' data-localize="fr_translation_code" style="margin-right:5px"></button>
                    <span>|</span>
                </div>
-->
                 
                <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#bnw_modal" data-localize="best_n_worst_years"><div class="icon icon_bnw"></div></button>

                <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_4" data-localize="cagrs"></button>

                
                <button type="button" id='bnw' class="btn btn-icont" data-toggle="modal" data-target="#chart_1" data-localize="time_and_risk"></button>


<!--                <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_2" data-localize="bulls_and_bears"></button>-->

                <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_3" data-localize="downturns_and_recoveries"></button>

                <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_6" data-localize="gics"></button>

                <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_5" data-localize="key_rates"></button>
                
                         
<!--                <button type="button" style="margin-top:4px;margin-right:18px" id="asset_classes" class="btn btn-icon pull-right" data-toggle="modal" data-target="#asset_menu" style=""><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 20 20"><path fill="none" d="M0 0h20v20H0V0z"/><path d="M15.95 10.78c.03-.25.05-.51.05-.78s-.02-.53-.06-.78l1.69-1.32c.15-.12.19-.34.1-.51l-1.6-2.77c-.1-.18-.31-.24-.49-.18l-1.99.8c-.42-.32-.86-.58-1.35-.78L12 2.34c-.03-.2-.2-.34-.4-.34H8.4c-.2 0-.36.14-.39.34l-.3 2.12c-.49.2-.94.47-1.35.78l-1.99-.8c-.18-.07-.39 0-.49.18l-1.6 2.77c-.1.18-.06.39.1.51l1.69 1.32c-.04.25-.07.52-.07.78s.02.53.06.78L2.37 12.1c-.15.12-.19.34-.1.51l1.6 2.77c.1.18.31.24.49.18l1.99-.8c.42.32.86.58 1.35.78l.3 2.12c.04.2.2.34.4.34h3.2c.2 0 .37-.14.39-.34l.3-2.12c.49-.2.94-.47 1.35-.78l1.99.8c.18.07.39 0 .49-.18l1.6-2.77c.1-.18.06-.39-.1-.51l-1.67-1.32zM10 13c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3z"/></svg></button>-->
<!--
                
                <button type="button" style="position:absolute;margin-top:28%;left:280px" id="asset_classes" class="btn btn-icon pull-right" data-toggle="modal" data-target="#asset_menu" style="">Customize</button>
-->
                
                
            </div>
        </div>

    </div>


    <div id="main">


    </div>

    <button type="button" id='info' class="btn btn-icon" data-toggle="modal" data-target="#disclaimer_modal"><div class="icon icon_info icon-30 fill-mgray"></div></button>

    <div class="modal fade" id="bnw_modal" tabindex="-1" role="dialog" aria-labelledby="bnw_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title blue" data-localize="best_n_worst_years"></label>
                </div>
                <div class="modal-body">
                    <div class="div-top"></div>
                    <div class="div-bar"></div>
                    <div class="div-bottom"></div>
                    <div class="div-temp">
                        <div class="bar-cont us_stocks" data-localize-modal="us_stocks_short"></div>
                        <div class="bar-cont cdn_stocks" data-localize-modal="cdn_stocks_short"></div>
                        <div class="bar-cont intl_stocks" data-localize-modal="intl_stocks_short"></div>
                        <div class="bar-cont cdn_bonds" data-localize-modal="cdn_bonds_short"></div>
                        <div class="bar-cont cdn_tbills" data-localize-modal="cdn_tbills_short"></div>
                        <div class="bar-cont aggressive_portfolio" data-localize-modal="aggressive_portfolio"></div>
                        <div class="bar-cont moderate_portfolio" data-localize-modal="moderate_portfolio"></div>
                        <div class="bar-cont conservative_portfolio" data-localize-modal="conservative_portfolio"></div>
                    </div>
                    <div class='footnote'>
                        <p data-localize="bnw_footnote1"></p>
                        <p data-localize="bnw_footnote2"></p>
                    </div>
                    <div class='bestnote'>
                        <p data-localize="best"></p>
                    </div>
                    <div class='worstnote'>
                        <p data-localize="worst"></p>
                    </div>
                    <div class='example'>
                        <div class="ex-top">
                            <p class='tag' data-localize="best"></p>
                            <div class="ex-bar-bar ex-1yr">
                                <p class="text-muted" data-localize="1yr"></p>
                            </div>
                            <div class="ex-bar-bar ex-5yr">
                                <p class="text-muted" data-localize="5yr"></p>
                            </div>
                        </div>
                        <div class="ex-bar" data-localize="asset_class"></div>
                        <div class="ex-bottom">
                            <p class='tag' data-localize="worst"></p>
                            <div class="ex-bar-bar ex-1yr">
                                <p class="text-muted" data-localize="1yr"></p>
                            </div>
                            <div class="ex-bar-bar ex-5yr">
                                <p class="text-muted" data-localize="5yr"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
                <div class="text-center">
                    <p style="font-size:12px" data-localize="disclosure"></p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


</div>


   

    <div class="modal fade" id="asset_menu" tabindex="-1" role="dialog" aria-labelledby="asset_menu" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title blue" data-localize="assets_to_be_shown"></label>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-xs-4">
                            <label class="title" style="margin-bottom:15px" data-localize="equities"></label>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="cdn_stocks"></button>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_stocks"></button>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="intl_stocks"></button>
                        </div>

                        <div class="col-xs-4">
                            <label class="title" style="margin-bottom:15px" data-localize="fixed_income"></label>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="cdn_bonds"></button>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="cdn_tbills"></button>
                        </div>

                        <div class="col-xs-4">
                            <label class="title" style="margin-bottom:15px" data-localize="model_portfolios"></label>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="aggressive_portfolio"></button>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="moderate_portfolio"></button>
                            <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="conservative_portfolio"></button>
                        </div>
                    </div>

                  

                    <div class="assets-modal-footer text-center" style="margin-top:40px">
                        <span data-localize="max_assets_selected" style="color:#d50000"></span>
                    </div>
                    
                    <div class="row text-center" style="font-size:16px;margin-top:20px">
                        <span class="title" data-localize="select_month_and_date" style="color:#78909C"></span>
                    </div>
                    
                    <div id="birth_date" class="row" style="margin-bottom:10px">
                        <div class="col-xs-6">
                            <select id="month" class="form-control"></select>
                        </div>
                        <div class="col-xs-6">
                            <select id='year' class="form-control"></select>
                        </div>
                    </div>
                    
                    <div class="row text-center" style="margin-top:20px">
                        <label class="title" data-localize="historical_monthly_data" style="color:#78909C"></label>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="chart_1" tabindex="-1" role="dialog" aria-labelledby="chart_1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:90%" src="" alt="">
                </div>
            </div>
        </div>
    </div>

 

    <div class="modal fade" id="chart_3" tabindex="-1" role="dialog" aria-labelledby="chart_3" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:99%" src="" alt="">
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="chart_4" tabindex="-1" role="dialog" aria-labelledby="chart_4" aria-hidden="true">
        <div class="modal-dialog" style="width:930px!important;padding:0!important">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:99%" src="" alt="">
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="chart_5" tabindex="-1" role="dialog" aria-labelledby="chart_5" aria-hidden="true">
        <div class="modal-dialog" style="width:930px!important;padding:0!important">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:99%" src="" alt="">
                </div>
            </div>
        </div>
    </div>

   
    <div class="modal fade" id="chart_6" tabindex="-1" role="dialog" aria-labelledby="chart_6" aria-hidden="true">
        <div class="modal-dialog" style="width:950px!important;padding:0!important">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body">
                    <img style="width:99%" src="" alt="">
                </div>
            </div>
        </div>
    </div>

         <div class="modal fade" id="disclaimer_modal" tabindex="-1" role="dialog" aria-labelledby="disclaimer_modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                    <label class="title" data-localize-modal=""></label>
                </div>
                <div class="modal-body" style="padding:20px;text-align:left!important">
                    <p>
                        <label class="" data-localize="disclaimer_t1"></label>
                    </p>
                    <p>
                        <label class="" data-localize="disclaimer_t2"></label>
                    </p>
                    <p>
                        <label class="" data-localize="disclaimer_t3"></label>
                    </p>
                    <p>
                        <label class="" data-localize="disclaimer_t4"></label>
                    </p>
                </div>

            </div>
        </div>
    </div>


        
    </body>
</html>

<script>
    
$(function() {
//	check_network();

	// SESSION DATA PERSISTENCE
	var bd_year = parseInt(window.localStorage.getItem('bd_year'));
	var bd_month = parseInt(window.localStorage.getItem('bd_month'));
	var f_name = window.localStorage.getItem("f_name");
	var temp_lang = window.localStorage.getItem("temp_lang");

	if (f_name !== undefined) { $('#about_you #f_name input#first_name').val(f_name) };

	// FILL YEAR FIELD
	// Every year from 1935 to 15 years ago
	var year_offset = 15;
	var latest_date = JSON.parse(window.localStorage.getItem('data'));
	latest_date = latest_date[latest_date.length-1].date;
	var latest_year = parseInt(latest_date.substr(0,4));
	var latest_month = parseInt(latest_date.substr(5,2));
	
	var temp_str = '';
	for (var i = 1935; i <= latest_year - year_offset; i++) {
		if (bd_year !== undefined && i == bd_year) {
			temp_str += "<option value='"+i+"' selected='selected'>"+i+"</option>";
		} else {
			temp_str += "<option value='"+i+"'>"+i+"</option>";
		}
	}
	$('#birth_date #year').html( temp_str );

	var temp_str = '';
	for (var i = 1; i <= 12; i++) {
		if (bd_month !== undefined && i == bd_month) {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
		} else {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
		}
	}
	$('#birth_date #month').html( temp_str );
	$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });

	$('#birth_date #year').change(function() {
		var curr_sel_month = $('#birth_date #month').val();
		if (parseInt($(this).val()) == latest_year - year_offset && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= latest_month; i++) {
				if ((curr_sel_month > latest_month && i == latest_month) || (curr_sel_month <= latest_month && i == curr_sel_month)) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		} else if ($('#event_menu .content select#month option').length < 12 && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= 12; i++) {
				if (i == curr_sel_month) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		}
		$('#birth_date #month').html( temp_str );
		$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt( $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });
	});
	
	// Hide keyboard fix
	var last_selected = null; 
	$('select').focus(function(){
		if (last_selected.toLowerCase() == 'input') {
			$('input').focus();
			if (last_selected.toLowerCase() !== 'select') {
				last_selected = this.tagName;
				$(this).focus();
			}
		}
	});
	$('input').focus(function () { last_selected = this.tagName; });

	//Lang
	$('#lang button').each(function() {
		if (temp_lang !== undefined && temp_lang !== null) {
			if (temp_lang == $(this).val()) {
				$(this).toggleClass('btn-default btn-primary');
			}
		} else if (window.localStorage.getItem("lang") == $(this).val()) {
			$(this).toggleClass('btn-default btn-primary');
		}
	});

		window.localStorage.setItem("f_name", $('#about_you #f_name input#first_name').val());
		window.localStorage.setItem("bd_month", $('#birth_date select#month').val());
		window.localStorage.setItem("bd_year", $('#birth_date select#year').val());
		window.localStorage.setItem("temp_lang", $('#lang button.btn-primary').val());



});

 </script>

    
    <!-- D3 -->

    <!-- PHONEGAP -->
    <!--<script data-main="main" src="js/vendor/require.js"></script>-->
    <!-- II SCRIPTS -->
<!--    <script type="text/javascript" src="js/graph.js?v<?php echo $app_version;?>"></script>-->
   
 <script type="text/javascript">
            function get_localized(lang) {
                window.localStorage.setItem("lang", "en");
                Globalize.culture(lang);

                // Localization
                $("[data-localize]").each(function() { $(this).html(Globalize.localize($(this).attr('data-localize'),Globalize.culture())); });
            }

        
      
                window.localStorage.clear();
                
                // INITIALIZE PREFERENCES
                if (navigator.globalization !== undefined && navigator.globalization !== null) {
                    navigator.globalization.getPreferredLanguage(
                        // Checks for french. Else, uses English
                        function (language) { // Found a prefered language
                            if (language.value.toLowerCase().substr(0,2) == 'fr') {
                                get_localized('fr');
                            } else {
                                get_localized('en');
                            }
                        },
                        function () { get_localized('en'); }
                    );
                } else { get_localized('en'); }
//               
//                  window.localStorage.setItem("default_assets",["cdn_stocks","aggressive_portfolio","moderate_portfolio","us_stocks","intl_stocks","cdn_bonds","cdn_tbills"]);
//                
     
     
                  window.localStorage.setItem("default_assets",["cdn_stocks","us_stocks","intl_stocks","cdn_bonds","cdn_tbills"]);
                
                window.localStorage.setItem("default_h_events", "true");
                verify_asset_data(true);
                //console.log(window.localStorage.getItem("data"));

                // Handle back button
                document.addEventListener("backbutton", backKeyHandler, false);

          
        </script>

 <script type="text/javascript">
        
        function setImageUrl(lang_code){
        //        var lang_code = window.localStorage.getItem("lang");

                var baseUrl = "graph-gallery/images/";


                $("#chart_1").children().find("img").attr("src", baseUrl + lang_code + "/time_and_risk.jpg?v<?php echo $app_version;?>");
                $("#chart_3").children().find("img").attr("src", baseUrl + lang_code + "/downturns_and_recoveries.jpg?v<?php echo $app_version;?>");
                $("#chart_4").children().find("img").attr("src", baseUrl + lang_code + "/cagrs.jpg?v<?php echo $app_version;?>");
                $("#chart_5").children().find("img").attr("src", baseUrl + lang_code + "/key_rates.jpg?v<?php echo $app_version;?>");
                $("#chart_6").children().find("img").attr("src", baseUrl + lang_code + "/gics.jpg?v<?php echo $app_version;?>");
        }
     
        setImageUrl("en");
     
        var isMobile = false;
        ///Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)?isMobile=true:isMobile=false;
        if ($(document).width() < 1000){
            isMobile=true;
        }
        if (isMobile){
            $("#popupOverlay").css("display", "block");
        }


    </script>
