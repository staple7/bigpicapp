/**
 * DATA UPDATE
 */

function verify_asset_data(force) {
	var d_update = core_data.d_update;
	var data_freq = core_data.data_freq;
	var data = core_data.asset_data;

	if (force == true ||
		window.localStorage.getItem("data") == null ||
		window.localStorage.getItem("data_source") == null ||
		window.localStorage.getItem("data_freq") == null ||
		window.localStorage.getItem("data_hash") == null ||
		window.localStorage.getItem("bnw_data") == null ||
		window.localStorage.getItem("d_update") == null ||
		window.localStorage.getItem("data_freq") != data_freq || 
		window.localStorage.getItem("data_hash") != hashCode(JSON.stringify(data)) ||
		window.localStorage.getItem("d_update") != d_update) {

		window.localStorage.setItem("data_freq", data_freq);
		window.localStorage.setItem("data_hash", hashCode(JSON.stringify(data)));
		if (window.localStorage.getItem("data_source") != "online") { window.localStorage.setItem("data_source", "offline"); }
		if (window.localStorage.getItem("data_checked") == null) { window.localStorage.setItem("data_checked", 0); }
		console.log("There's a data update!");


		window.localStorage.setItem( "data", JSON.stringify( data ) );
		window.localStorage.setItem( "bnw_data", JSON.stringify( make_bnw(data, data_freq) ) );
		window.localStorage.setItem( "d_update", d_update);
	}
}

function make_bnw (data, data_freq) {
   
	var bnw = {};
	var periods = ['alltime', 15, 10, 5];
	var rolling_periods = [1,5];
	if (data_freq == 'q') { data_freq = 4; } else { data_freq = 12; }

	// Latest December date
	for (var i = 1; i < data.length; i++) {
		if (parseInt(data[data.length-i]['date'].substr(5,2)) == 12) { // Looks for December
			var latest_date = data.length-i;
			break;
		}
	}

	var extent = null;
	for (p in periods) { // Period of time in which the rolling periods will be evaluated
		if (periods[p] == 'alltime') {
			var period = latest_date / data_freq;
		} else {
			var period = periods[p];
		}
		var new_period = {};

		for (asset in data[0]) { // Iterates through all assets, except inflation
			if (asset != 'inflation' && asset != 'date') {
				new_period[asset] = {};

				for (r in rolling_periods) { // 1-yr rolling periods, 5-yr rolling periods
					var r_period = rolling_periods[r];
                    
					new_period[asset][r_period+'yr'] = {};
					var highest = null;
					var lowest = null;

					for (var i = latest_date; i >= latest_date - (period * data_freq) + (r_period*data_freq); i -= data_freq) { // Goes from recent to the past
						var prev_value = data[i-(r_period*data_freq)][asset];
						var curr_value = data[i][asset];
						var value = Math.pow( (curr_value/prev_value), (1/r_period) ) - 1;

						if (highest === null || highest < value) { highest = value; }
						if (lowest === null || lowest > value) { lowest = value; }
					}

					new_period[asset][r_period+'yr']['best'] = highest;
					new_period[asset][r_period+'yr']['worst'] = lowest;

					if (extent === null) {
						if (highest > Math.abs(lowest)) {
							extent = highest;
						} else {
							extent = lowest;
						}
					} else {
						if (extent < highest) { extent = highest; }
						if (extent < Math.abs(lowest)) { extent = Math.abs(lowest); }
					}
				}
			}
		}

		// Assign periods to main object
		if (periods[p] == 'alltime') {
			bnw[periods[p]] = new_period;
		} else {
			bnw[periods[p]+'yr'] = new_period;
		}
	}
	bnw['extent'] = extent;
	return(bnw);
}

function hashCode(str){
    var hash = 0, i, char;
    if (str.length == 0) return hash;
    for (i = 0, l = str.length; i < l; i++) {
        char  = str.charCodeAt(i);
        hash  = ((hash<<5)-hash)+char;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}