<?php
   
    $from_name = 'Big Picture Referral Rewards';


    $html = 
        "
 <html lang='en'>
<meta charset='UTF-8'>
<body><table cellpadding='8' cellspacing='0' style='padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff' border='0'>
  <tbody>
    <tr>
      <td valign='top'><table cellpadding='0' cellspacing='0' style='border-radius:4px;border:1px #dceaf5 solid;' border='0' align='center'>
          <tbody>
            <tr>
              <td colspan='3' height='6'></td>
            </tr>
            <tr style='line-height:0px'>
              <td width='100%' style='font-size:0px' align='center' height='1'><img  style='max-height:73px; margin-top:22px' width='192' height='35' alt='logo' src='http://$host/images/bigpicture_logo_mobile.png'></td>
            </tr>
            <tr>
              <td><table cellpadding='0' cellspacing='0' style='line-height:25px' border='0' align='center'>
                  <tbody>
                    <tr>
                      <td colspan='3' height='30'></td>
                    </tr>
                    <tr>
                      <td width='36'></td>
                      <td width='454' align='left' style='color:#444444;border-collapse:collapse;font-size:11pt;max-width:454px;' valign='top'>Dear $subscriber_name,<br>
                        <br>
                        Thank you for referring your friends to the Big Picture app.<br>
                        <br>
                        We are pleased to inform you that, following your invitation, $firstname &nbsp; $lastname has signed up for a Premium subscription! We have extended your current subscription by a full year. That's like $360 in cold, hard cash—right back in your pocket. 
                        <br>
                        
                        <br>
                        You can consult your subscription period in <a href='http://$host/login.php' target='_blank'>Settings</a>.  
                        <br>
                        <br>
                        <strong>You will receive another full year extension for each additional friend who signs up.</strong>
                        <br>
                        <br>
                        <center>
                          <a href='http://$host/referral-rewards.php' target='_blank' style='border-radius:3px;font-size:16px;color:white;border:1px #FF9100 solid;text-decoration:none;padding:14px 7px 14px 7px;width:210px;max-width:210px;margin:6px auto;display:block;background-color:#FF9100;text-align:center'>Refer More Friends</a>
                        </center>
                        <br>
                        <br>
                        All the best,<br>
                        - The Big Picture Team</td>
                      <td width='36'></td>
                    </tr>
                    <tr>
                      <td colspan='3' height='36'></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        <table cellpadding='0' cellspacing='0' align='center' border='0'>
          <tbody>
            <tr>
              <td height='10'></td>
            </tr>
            <tr>
              <td style='padding:0;border-collapse:collapse'><table cellpadding='0' cellspacing='0' align='center' border='0'>
                  <tbody>
                    <tr style='color:#1CA6EB;font-size:11px;'>
                      <td width='324' align='left'></td>
                      <td width='208' style='color:#a4a4a4;' align='right'>© 2016 Investments Illustrated, Inc.</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table></body>
</thml>
";
        
       

?>

