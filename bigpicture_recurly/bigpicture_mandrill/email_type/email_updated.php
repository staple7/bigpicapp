<?php
   
    $from_name = 'Big Picture: An Eye-Opening App For Retirement Planning';

    $subject_email  = "Your email has been changed";

    $html = 
        "
<html lang='en'>
<meta charset='UTF-8'>        
<body><table cellpadding='8' cellspacing='0' style='padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff' border='0'>
  <tbody>
    <tr>
      <td valign='top'><table cellpadding='0' cellspacing='0' style='border-radius:4px;border:1px #dceaf5 solid;' border='0' align='center'>
          <tbody>
            <tr>
              <td colspan='3' height='6'></td>
            </tr>
            <tr style='line-height:0px'>
              <td width='100%' style='font-size:0px' align='center' height='1'><img  style='max-height:73px; margin-top:22px' width='192' height='35' alt='logo' src='http://$host/images/bigpicture_logo_mobile.png'></td>
            </tr>
            <tr>
              <td><table cellpadding='0' cellspacing='0' style='line-height:25px' border='0' align='center'>
                  <tbody>
                    <tr>
                      <td colspan='3' height='30'></td>
                    </tr>
                    <tr>
                      <td width='36'></td>
                      <td width='454' align='left' style='color:#444444;border-collapse:collapse;font-size:11pt;max-width:454px;' valign='top'>Dear $subscriber_name,<br>
                        <br>
                        The email address associated with your Big Picture account was recently changed. If you did not perform this action, please <a href='http://$host/contact-us.html' target='_blank'>contact us</a>.<br>
                        <br>
                        
                        Sincerely, <br>
                        - The Big Picture Team</td>
                      <td width='36'></td>
                    </tr>
                    <tr>
                      <td colspan='3' height='36'></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        <table cellpadding='0' cellspacing='0' align='center' border='0'>
          <tbody>
            <tr>
              <td height='10'></td>
            </tr>
            <tr>
              <td style='padding:0;border-collapse:collapse'><table cellpadding='0' cellspacing='0' align='center' border='0'>
                  <tbody>
                    <tr style='color:#1CA6EB;font-size:11px;'>
                      <td width='324' align='left'></td>
                      <td width='208' style='color:#a4a4a4;' align='right'>© 2016 Investments Illustrated, Inc.</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table></body>
</html>
";
        
       

?>

