<?php

    try {
       
        
        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/session/open_mandrill_session.php';
          
        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/email_type/' . $email_type . '.php'; 

        $message = array(
            'subject'   =>  $subject_email,
            'from_name' =>  $from_name,
            'from_email'=>  $from_email,
            'html'      =>  $html,
            'to'        =>  array(array('email' => $to_email, 'name' => ''))
        );

        $async = true;
        $ip_pool = 'Main Pool';
      //  $send_at = 'example send_at';
        $result = $mandrill->messages->send($message, $async, $ip_pool);
      //  print_r($result);
      
    } catch(Mandrill_Error $e) {
        // Mandrill errors are thrown as exceptions
        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
        // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
       // throw $e;
    }

?>