<?php

    $subscriptions = Recurly_SubscriptionList::getForAccount($recurlyAccountCode);
    foreach ($subscriptions as $subscription) {}

    $plan_code = $subscription->plan->plan_code;

    if ($plan_code == "big_picture_premium"){
        foreach ($subscription->current_period_ends_at as $expireTime) {break;}
        $expireTime = strtotime($expireTime);          
    }

    $account = Recurly_Account::get($recurlyAccountCode);                         
    $subscriber_name = $account->first_name . " " . $account->last_name;  

    $new_expiration_date = date('Y-m-d',strtotime(date("Y-m-d", $expireTime) . " + 365 day"));

    $subscription->postpone(date('c', strtotime($new_expiration_date)), false);
    //$subscription->postpone(date('c', strtotime('2016-12-31Z')), False);
      //  mysqli_query ($conn, "INSERT INTO $db_schema.Log (text) VALUES ('$new_expiration_date')");
    $to_email = $email;            
    $email_type = "free_extension_earned";  
    $subject_email  = $firstname . " " . $lastname . " has signed up for the Big Picture: An Eye-Opening App For Retirement Planning";

    require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php';

?>




