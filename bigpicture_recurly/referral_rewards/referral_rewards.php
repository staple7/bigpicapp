<?php
    //ignore_user_abort(true);
    $errorcode = array('errorOther'=>false,
                       'errorMessage'=>'');

    require '../bigpicture_config.php';

    try {
      
        
        $host = $_SERVER['HTTP_HOST'];
        
        $referrer_email    = $_POST['subscriber_email'];
        $referred_email_list = $_POST['referred_email_list'];
        
        require '../db_connection.php';

        $result = mysqli_query ($conn, "SELECT USERID, RECURLYACCOUNTCODE FROM $db_schema.User WHERE EMAIL = '$referrer_email'");
      
        if ($result->num_rows > 0) {
            $row = mysqli_fetch_array($result);
            $referrer_email_id = $row["USERID"];          
            $account = Recurly_Account::get($row["RECURLYACCOUNTCODE"]);                         
            $subscriber_name = $account->first_name . " " . $account->last_name;            
        }else{
            require '../db_close_connection.php';
            $errorcode['errorOther']   = true;
            $errorcode['errorMessage'] = "Non existing subscriber";  
            echo json_encode($errorcode);
            die;
        }
        
        require '../db_close_connection.php';
        
        foreach ($referred_email_list as &$referred_email){
            if ($referred_email ==  $referrer_email) continue;
            if (!is_resource($conn)) {
               require '../db_connection.php';
            }
            require 'insertReferral.php';
        //    require 'getReferredEmailState.php';
          //  if ($referredEmail_result == 'OK'){
                $to_email = $referred_email;
                $subject_email = $subscriber_name . " invited you to check out the Big Picture: An Eye-Opening App For Retirement Planning";
                $email_type = "referral_rewards";
                require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php'; 
                    
        //    }
        }
     
    }catch (Exception $e){
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e->message();
    }

    if (is_resource($conn)) {
        require '../db_close_connection.php';
    }

    $errorcode['resultMail'] = $resultMail;

    echo json_encode($errorcode);      

?>

