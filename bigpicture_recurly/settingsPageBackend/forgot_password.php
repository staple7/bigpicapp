<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        $to_email = $_POST['email'];

        
        require '../db_connection.php';
        
        $result = mysqli_query ($conn, "SELECT USERID 
                                       FROM $db_schema.User 
                                       WHERE EMAIL = '$to_email'
                                       AND LENGTH(RECURLYACCOUNTCODE) > 0");
        if ($result->num_rows > 0){
            $row = mysqli_fetch_array($result);        
            $email_id = $row["USERID"];
        }else{
            $result = mysqli_query ($conn, "SELECT USERID 
                                       FROM $db_schema.TeamUser 
                                       WHERE EMAIL = '$to_email'");
            
            if ($result->num_rows > 0){
                $row = mysqli_fetch_array($result);        
                $email_id = $row["USERID"];
            }else{
                require '../db_close_connection.php';
                $errorcode['errorEmail'] = true;
                echo json_encode($errorcode);
                die;
            }
        }
        
        $email_type = "forgot_password";        
        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php';   

        
    } catch (Exception $e) {     
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }
    try{
        require '../db_close_connection.php';        
    } catch (Exception $e) {}
    

   echo json_encode($errorcode);      

?>

