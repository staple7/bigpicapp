<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');


    $uuid = "";
    try {
        $email = $_POST['email'];
        
        
        require 'get_subscription_uuid.php';
        if (!$uuid){
            $errorcode['errorEmail'] = true;
            echo json_encode($errorcode);
            die;
        }             
        $subscription = Recurly_Subscription::get($uuid);
        $subscription->cancel();

    } catch (Exception $e) {
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e;        
    }
//    session_start();
//
//// Unset all of the session variables.
//$_SESSION = array();
//
//// If it's desired to kill the session, also delete the session cookie.
//// Note: This will destroy the session, and not just the session data!
//if (ini_get("session.use_cookies")) {
//    $params = session_get_cookie_params();
//    setcookie(session_name(), '', time() - 42000,
//        $params["path"], $params["domain"],
//        $params["secure"], $params["httponly"]
//    );
//}
//
//// Finally, destroy the session.
//session_destroy();

    echo json_encode($errorcode);
    
?>