<?php
    require '../bigpicture_config.php';

    $subscription_data = array();
    $subscription_data['errorEmail'] = false;
    $subscription_data['errorOther'] = false;   
    $subscription_data['errorMessage'] = ''; 
    
    try {
        $email = $_POST['email'];    
            

        require 'get_account_code.php';
        
        if (strlen($recurlyAccountCode)==0){
          //  try{
                require '../db_connection.php';
                $result = mysqli_query ($conn, "SELECT 
                                               user.recurlyAccountCode 
                                               FROM $db_schema.User as user, 
                                                    $db_schema.TeamUser as teamUser, 
                                                    $db_schema.Team as team
                                               WHERE teamUser.EMAIL = '$email'
                                               AND teamUser.TEAMID = team.TEAMID
                                               AND team.USERID = user.USERID");

                if ($result->num_rows > 0){
                    $row = mysqli_fetch_array($result);
                    $recurlyAccountCode = $row["recurlyAccountCode"];     
                }
                require '../db_close_connection.php';
          //  }catch (Exception $e){}
        }
        
        if (!$recurlyAccountCode){
            $subscription_data['errorEmail'] = true;
            echo json_encode($subscription_data);
            die;
        }        

        $account = Recurly_Account::get($recurlyAccountCode);            
         
    
        $subscription_data['subscriber_name'] = $account->first_name . " " . $account->last_name;
                    
        $subscriptions = Recurly_SubscriptionList::getForAccount($recurlyAccountCode);
        foreach ($subscriptions as $subscription) {
            
        }   
        
      // $currencyCode = $subscription->currency;
        $regioncode = $_COOKIE["regioncode"];
        
        $subscription_data['plan_name'] = $subscription->plan->name;
        $subscription_data['plan_code'] = $subscription->plan->plan_code;

        $fc = mb_strtoupper(mb_substr($subscription->state, 0, 1));
        
        $subscription_data['state']     = $fc.mb_substr($subscription->state, 1);
        foreach ($subscription->current_period_started_at as $start_date) {
            break;
        }
        $start_date = mb_substr($start_date, 0, 10);
        $year = mb_substr($start_date, 0, 4);
        $month = mb_substr($start_date, 5, 2);
        $day = mb_substr($start_date, 8, 2);
        if (mb_strtoupper($regioncode) == "CA"){
            $start_date = $day . "-" . $month . "-" . $year;
        }else{
            $start_date = $month . "-" . $day  . "-" . $year;
        }        
        $subscription_data['start_date']= $start_date;        
 
        foreach ($subscription->current_period_ends_at as $expiration_date) {
            break;
        }
        $expiration_date = mb_substr($expiration_date, 0, 10);
        $year = mb_substr($expiration_date, 0, 4);
        $month = mb_substr($expiration_date, 5, 2);
        $day = mb_substr($expiration_date, 8, 2);
        if (mb_strtoupper($regioncode) == "CA"){
            $expiration_date = $day . "-" . $month . "-" . $year;
        }else{
            $expiration_date = $month . "-" . $day  . "-" . $year;
        }
        $subscription_data['expiration_date'] = $expiration_date;
        
        if (mb_strtoupper($regioncode) == "CA"){
            $subscription_data['cost'] = "USD" . " " . ($subscription->unit_amount_in_cents) / 100;
        }else{
            $subscription_data['cost'] = "$" . ($subscription->unit_amount_in_cents) / 100;
        }
        
        
        $invoicesArray = array();
        
        try{
            $billing_info = Recurly_BillingInfo::get($recurlyAccountCode);
            $subscription_data['billing_first_name'] = $billing_info->first_name;
            $subscription_data['billing_last_name'] = $billing_info->last_name;
            $subscription_data['billing_credit_card_last'] = $billing_info->last_four;
            $subscription_data['billing_credit_card_type'] = $billing_info->card_type;

            $subscription_data['billing_country'] = $billing_info->country;
            $subscription_data['billing_zip'] = $billing_info->zip;
            $subscription_data['billing_exp_month'] = $billing_info->month;
            $subscription_data['billing_exp_year'] =  $billing_info->year;
            
            $invoiceObj = array();
            $invoices = Recurly_InvoiceList::getForAccount($recurlyAccountCode);
            foreach ($invoices as $invoice) {
                $invoiceObj = array();
                $invoiceObj["invoice_number"] = $invoice->invoice_number;
                $invoice_created_at = json_decode(json_encode($invoice->created_at))->{'date'};
                $invoice_created_at = mb_substr($invoice_created_at, 0, 10);
                $month = mb_substr($invoice_created_at, 5, 2);
                $year = mb_substr($invoice_created_at, 0, 4);                   
                $day = mb_substr($invoice_created_at , 8, 2);
                if (mb_strtoupper($regioncode) == "CA"){
                    $invoice_created_at = $day . "-" . $month . "-" . $year;
                }else{
                    $invoice_created_at = $month . "-" . $day  . "-" . $year;
                }                
                $invoiceObj["billed_on"] = $invoice_created_at;
                $fc = mb_strtoupper(mb_substr($invoice->state, 0, 1));                
                $invoiceObj["status"]    = $fc.mb_substr($invoice->state, 1);
                if (mb_strtoupper($regioncode) == "CA"){
                    $invoiceObj["total"]     = "USD" . " " . ($invoice->total_in_cents) / 100;
                }else{
                    $invoiceObj["total"]     = "$" . ($invoice->total_in_cents) / 100;
                }
                
                
               array_push($invoicesArray, $invoiceObj);
               break;
            }               
        } catch (Exception $billingErr) {}
        
    
      
        $subscription_data["invoice_list"] = $invoicesArray;
                    
    } catch (Exception $e) {
        $subscription_data['errorOther'] = true;
        $subscription_data['errorMessage'] = $e->getMessage();
    }

    echo json_encode($subscription_data);

 
?>

 