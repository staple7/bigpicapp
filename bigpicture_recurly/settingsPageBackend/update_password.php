<?php
    require '../bigpicture_config.php';
    require '../safe_password.php';

    $errorcode = array('errorEmail'=>false,
                       'errorLogin'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        
        $email         = $_POST['email'];
        $old_password  = $_POST['old_password'];        
        $new_password  = $_POST['new_password'];    
        
        require '../db_connection.php';
        
        $result = mysqli_query ($conn, "SELECT EMAIL, PASSWORD, RECURLYACCOUNTCODE
                                       FROM $db_schema.User 
                                       WHERE EMAIL = '$email'");
        
        if ($result->num_rows > 0){
            $row = mysqli_fetch_array($result);
            $old_hash = $row["PASSWORD"];
        }      
        
        if ($result->num_rows == 0 || (strlen($old_hash) > 0 && !bcrypt_check($old_password, $old_hash))) {
                require '../db_close_connection.php';
                $errorcode['errorLogin'] = true;
                echo json_encode($errorcode);
                die;
        } 
                

        $new_password = bcrypt_hash($new_password);

        
        mysqli_query($conn, "UPDATE $db_schema.User 
                             SET PASSWORD = '$new_password'                           
                             WHERE EMAIL = '$email'"); 

       // $row = mysqli_fetch_array($result);
        $account = Recurly_Account::get($row["RECURLYACCOUNTCODE"]);                         
        $subscriber_name = $account->first_name . " " . $account->last_name;     

        $to_email = $email;            
        $email_type = "password_updated";        
        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php';

            


    
        
    } catch (Exception $e) {            
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }

    require '../db_close_connection.php';
    
    echo json_encode($errorcode);      

?>

