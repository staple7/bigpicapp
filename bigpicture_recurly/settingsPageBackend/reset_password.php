<?php
    require '../bigpicture_config.php';
    require '../safe_password.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        
        $email_id       = $_POST['email_id'];
        $new_password   = $_POST['new_password'];    
        
        $new_password = bcrypt_hash($new_password);
        
        require '../db_connection.php';
        
        $result = mysqli_query ($conn, "SELECT USERID 
                                       FROM $db_schema.User 
                                       WHERE USERID = '$email_id'");
        
        if ($result->num_rows > 0){
            mysqli_query($conn, "UPDATE $db_schema.User 
                                SET                          
                                PASSWORD = '$new_password'
                                WHERE USERID = '$email_id'"); 
        }else{        
            $result = mysqli_query ($conn, "SELECT USERID 
                                       FROM $db_schema.TeamUser
                                       WHERE USERID = '$email_id'");
            if ($result->num_rows > 0){
                mysqli_query($conn, "UPDATE $db_schema.TeamUser 
                                    SET                          
                                    PASSWORD = '$new_password'
                                    WHERE USERID = '$email_id'"); 
            }else{            
                require '../db_close_connection.php';
                $errorcode['errorEmail'] = true;
                echo json_encode($errorcode);
                die;
            }
        }        
 
    } catch (Exception $e) {            
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }

    require '../db_close_connection.php';
    

    echo json_encode($errorcode);      

?>

