<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');


    try {
        $email = $_POST['email'];    
    
        //$email = 'cmolina33@test.com';          
        require 'get_subscription_uuid.php';
        if (!$uuid){
            $errorcode['errorEmail'] = true;
            echo json_encode($errorcode);
            die;
        }             
        $subscription = Recurly_Subscription::get($uuid);           
        $subscription->reactivate();
    } catch (Exception $e) {
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e;
    }

    echo json_encode($errorcode);        
    
?>