<?php
    @ob_start();

    require '../bigpicture_config.php';

    $errorcode = array('errorOldEmail'=>false,
                       'errorSameEmails'=>false,
                       'errorExistingNewEmail'=>false,                       
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try { 
        
        $email      = $_POST['old_email'];    
        $new_email  = $_POST['new_email'];    

//        $new_email  = "justin@investmentsillustrated.com";  
//        $email      = "jianduwen@gmail.com";    

        if ($email == $new_email){
            $errorcode['errorSameEmails'] = true;
            echo json_encode($errorcode);
            die;            
        }
             
        require 'get_account_code.php';
        if (!$recurlyAccountCode){
            $errorcode['errorOldEmail'] = true;
            echo json_encode($errorcode);
            die;
        }

        require '../db_connection.php';
        
        $result = mysqli_query ($conn, "SELECT EMAIL 
                               FROM $db_schema.User 
                               WHERE EMAIL = '$new_email'");   
        
        if ($result->num_rows > 0) {
            $errorcode['errorExistingNewEmail'] = true;            
            echo json_encode($errorcode);
            die;
        }


        $account = Recurly_Account::get($recurlyAccountCode);
        $account->email = $new_email;
        $subscriber_name = $account->first_name . " " . $account->last_name; 
        $account->update();        
        
        mysqli_query($conn, "UPDATE $db_schema.User 
                             SET 
                             EMAIL = '$new_email'
                             WHERE EMAIL = '$email'");   
        
        
        $subscriptions = Recurly_SubscriptionList::getForAccount($recurlyAccountCode);

        foreach ($subscriptions as $subscription) {}

        

        $plan_code = $subscription->plan->plan_code;
        

        foreach ($subscription->current_period_ends_at as $expireTime) {break;}

        $expireTime = strtotime($expireTime);

        
        
        setcookie("email", $new_email, $expireTime, "/");  
      //  setcookie("email",  $new_email); 
        ob_flush();
        echo json_encode($errorcode);
        $to_email = $new_email;            
        $email_type = "email_updated";        
        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php';
        
    } catch (Exception $e) {      
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e;    
    }

    require '../db_close_connection.php';
    
   
   // echo json_encode($errorcode);
 
?>

