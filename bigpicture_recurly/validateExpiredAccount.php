<?php

   if ($plan_code == "big_picture_premium" && strtolower($subscription->state) == "expired"){
       
         
            require 'db_close_connection.php';

            $errorcode['expired'] = true;
            $errorcode['plancode'] = $plan_code;
            
            $account = Recurly_Account::get($recurlyAccountCode);    
            $expireTime = time() + (10 * 24 * 60 * 60);

       
            setcookie("firstname", $account->first_name, $expireTime, "/"); 
            setcookie("lastname",  $account->last_name,  $expireTime, "/"); 
            setcookie("email",  $account->email,  $expireTime, "/"); 

       
            echo json_encode($errorcode);
            die;
    
            
    }
?>

