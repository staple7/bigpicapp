<?php

    @ob_start();

   
    require 'bigpicture_config.php';
    require 'safe_password.php';

    $errorcode = array('errorPremium'=>false,'errorBilling'=>false,'errorOther'=>false, 'errorMessage'=>'');
    
    try {
        
        $email  = strtolower($_POST['email']);
        $numberOfUsers  = $_POST['numberOfUsers'];
        $coupon = $_POST['coupon'];
        $token      = $_POST['recurly-token'];
        $password   = $_POST['password'];
        $firstname  = $_POST['firstname'];
        $lastname   = $_POST['lastname'];
        $country    = $_POST['country'];
        $shipping_address1  = $_POST['shipping_address1'];
        $shipping_city  = $_POST['shipping_city'];
        $shipping_state  = $_POST['shipping_state'];
        $shipping_country  = $_POST['shipping_country'];
        $shipping_postalcode  = $_POST['shipping_postalcode'];
        $recurly_currency = "USD";
        
        if ($numberOfUsers > 1){
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_integrations/teams/get_team_price.php';
        }   
        
        $subscription = new Recurly_Subscription();
        $account_code = $email;
        $subscription->account = new Recurly_Account($account_code);     
        $subscription->plan_code = $premium_plan_code;
        if ($teamPrice !== NULL){
            $subscription->unit_amount_in_cents = ($teamPrice * 100) * 12;
        }
        if (strlen($coupon)){
            $subscription->coupon_code = $coupon;
        }
        
        $subscription->account->first_name = $firstname;
        $subscription->account->last_name = $lastname;
        $subscription->account->email = $email;
        $subscription->account->billing_info = new Recurly_BillingInfo();
        $subscription->account->billing_info->token_id = $token;
        $subscription->collection_method = $invoice_collection_for_premium;   
        $subscription->currency = $recurly_currency;            
        $subscription->create();                                   
  

        $password = bcrypt_hash($password);
        
        $referrer_email_id = $_POST['referrer_email_id'];
        
        
        
        require 'db_connection.php';

        $isNewEmail = false;  //New email (with no previous account).
        $isUpgrade  = false;  //Upgrade from basic to premium
        $isExisting  = false; //Premium renovation
        
        $result = mysqli_query ($conn, "SELECT 
                                        EMAIL, RECURLYACCOUNTCODE
                                        FROM $db_schema.User 
                                        WHERE EMAIL = '$email'");
        
        $row = mysqli_fetch_array($result);
        
        $recurlyAccountCode = $row["RECURLYACCOUNTCODE"];
        
        if ($result->num_rows == 0){
            $isNewEmail = true;
        }else if (strlen($recurlyAccountCode) == 0){
            $isUpgrade = true;
        }else{
            $isExisting = true;
        }

        $sessionId = $_COOKIE['PHPSESSID'];
        

        if ($isNewEmail == true){
            mysqli_query ($conn, "INSERT INTO $db_schema.User 
                        (email, password, recurlyAccountCode, regionCode, sessionId)
                        VALUES ('$email', '$password',  '$email', '$country', '$sessionId')");
            
        }
        
        if ($isUpgrade == true || $isExisting == true){
            mysqli_query ($conn, "UPDATE $db_schema.User 
                                 SET
                                 PASSWORD = '$password',
                                 RECURLYACCOUNTCODE = '$email',
                                 REGIONCODE = '$country',
                                 SESSIONID = '$sessionId'
                                 WHERE EMAIL = '$email'");

        }
        
        $result = mysqli_query ($conn, "SELECT 
                                        USERID
                                        FROM $db_schema.User 
                                        WHERE EMAIL = '$email'");
        
        $row = mysqli_fetch_array($result);
        $userId = $row["USERID"];
        if (strlen($shipping_address1) > 0){
            $shipping_country = $country;
            mysqli_query ($conn, "INSERT INTO $db_schema.ShippingAddress 
                        (userId, address , city, state, country, postalCode)
                        VALUES ('$userId', 
                                '$shipping_address1', 
                                '$shipping_city', 
                                '$shipping_state', 
                                '$shipping_country', 
                                '$shipping_postalcode')");  
        }             
        
        //Create a team
        if ($numberOfUsers > 1){
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_integrations/teams/create_team.php';
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_integrations/teams/get_team_price.php';
        }        

        session_start();        
        
        $_SESSION['login'] = "1";
        
        
        
        require '../validateNewEmail.php';
        
        $subs_state = strtolower($subscription->state);
        if ($subs_state == 'active' || $subs_state == 'canceled'){
            $lastappvaldexp = time() + (32 * 24 * 60 * 60);
            setcookie("lastappvald", $subs_state, $lastappvaldexp, "/");
        }
        
        $expireTime = time() + (35 * 24 * 60 * 60);
        
        setcookie("plancode", $premium_plan_code, $expireTime, "/"); 
        setcookie("email",  $email, $expireTime, "/"); 
        setcookie("regioncode", $country, $expireTime, "/");
        setcookie("isTeamMember", "false", $expireTime, "/");

        if (strlen($coupon)){
            setcookie("custom_group", $coupon, $expireTime, "/");
        }
        
        
        if ($isNewEmail == true || $isUpgrade == true){ 
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/referral_rewards/premium_account_created.php';
        }
        

        
    } catch (Exception $e) {
        $errorcode['errorBilling'] = true;
        $errorcode['errorMessage'] = $e->getMessage();
    }



    require 'db_close_connection.php';

    ob_flush();

    echo json_encode($errorcode);           

        
?>


