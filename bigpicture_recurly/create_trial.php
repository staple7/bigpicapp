<?php
    @ob_start();


    require 'bigpicture_config.php';

    $errorcode = array('errorBasic'=>false, 'errorOther'=>false, 'trialExpired'=>false, 'errorMessage'=>'');
    

    try{
        $daysForTrial = 7;
       // $daysForTrial = .001; it's like 1 min.
       // $daysForTrial = 0.002; //it's like 2 min.
        
        $email   = strtolower($_POST['email']);
        $country = $_POST['country'];
        $referrer_email_id = $_POST['referrer_email_id'];
//        
//        $email   = "cmoligo@gmail.com";            
//        $country = "US";
       
        
        require 'db_connection.php';        
        
        $result = mysqli_query($conn, "SELECT RECURLYACCOUNTCODE, CREATEDATE
                                       FROM $db_schema.User 
                                       WHERE EMAIL = '$email'");
        
        $teamUserResult = mysqli_query($conn, "SELECT EMAIL
                                                FROM $db_schema.TeamUser 
                                                WHERE EMAIL = '$email'");
        
        $currentTime = time();
  
        $cookieExpireTime = $currentTime + (50 * 24 * 60 * 60);
        if ($result->num_rows == 0 && $teamUserResult->num_rows == 0) { // new email
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_integrations/platform/getUserPlatform.php';

            mysqli_query($conn, "INSERT INTO $db_schema.User 
                        (email, regionCode, platform)
                         VALUES ('$email', '$country', '$platform')");
           
            setcookie("hardReload", $country, $cookieExpireTime, "/");
            
            if (strlen($referrer_email_id)>0){
                require 'referral_rewards/basic_account_created.php';
            }
             
            $to_email = $email;            
            $email_type = "welcome_to_trial";        
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php';   
            
            $initTime = $currentTime;
            
        }else{ // existing email
            $row = mysqli_fetch_array($result);
            $recurlyAccountCode = $row["RECURLYACCOUNTCODE"];
            $createTime = $row["CREATEDATE"];
            if ($recurlyAccountCode || $teamUserResult->num_rows > 0){
                require 'db_close_connection.php';
                $errorcode['errorBasic'] = true;
                echo json_encode($errorcode);
                die;
            }    
            
            list($date, $time) = explode(' ', $createTime);
            list($year, $month, $day) = explode('-', $date);
            list($hour, $minute, $second) = explode(':', $time);
            $initTime = mktime($hour, $minute, $second, $month, $day, $year); 
        }
        
        
        $trialTime = ($daysForTrial * 24 * 60 * 60);
        
        
        $expireTime = ($initTime + $trialTime);
        
       

        if ($expireTime < $currentTime){
            require 'db_close_connection.php';
            $errorcode['trialExpired'] = true;
            $exp_time = $currentTime - 3600;
            setcookie("plancode", "", $exp_time, "/");
            echo json_encode($errorcode);
            die;  
        }
        
      

            
        session_start();        
        $_SESSION['login'] = "1";
        
        require '../validateNewEmail.php';
        
        
        
        setcookie("plancode", "big_picture_premium", $cookieExpireTime, "/"); 
        setcookie("email", $email, $cookieExpireTime, "/");        
        setcookie("regioncode", $country, $cookieExpireTime, "/");
        setcookie("isTeamMember", "false", $cookieExpireTime, "/");       
        
        ob_flush();
        
        
    }catch (Exception $e) {        
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e->getMessage();   
        echo $e;
    } 
    
    require 'db_close_connection.php';

    echo json_encode($errorcode);    

?>