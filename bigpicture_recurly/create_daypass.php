<?php
    @ob_start();
    require 'bigpicture_config.php';
    require 'safe_password.php';

    $errorcode = array('errorDayPass'=>false,'errorBilling'=>false,'errorOther'=>false, 'errorMessage'=>'');
    
    try {
        
        $token      = $_POST['recurly-token'];
        $password   = $_POST['password'];
        $firstname  = $_POST['firstname'];
        $lastname   = $_POST['lastname'];            
        $email      = $_POST['email'];             
        $country    = $_POST['country'];
                
        $recurly_currency = "USD";
    
        $password = bcrypt_hash($password);
        
        require 'db_connection.php';
        
        $isNewEmail = false;
        $isUpgrade  = false;
        $isExisting = false;

        $result = mysqli_query ($conn, "SELECT 
                                       EMAIL, RECURLYACCOUNTCODE
                                       FROM $db_schema.User 
                                       WHERE EMAIL = '$email'");
        
        $row = mysqli_fetch_array($result);
        $recurlyAccountCode = $row["RECURLYACCOUNTCODE"];  
        
   
        if ($result->num_rows == 0){
            $isNewEmail = true;
        }else if (strlen($recurlyAccountCode) == 0){
            $isUpgrade = true;
        }else{
            $isExisting = true;
        }
        
        if ($isNewEmail == true || $isUpgrade == true){
            $subscription = new Recurly_Subscription();
            $recurlyAccountCode = uniqid();
            $subscription->account = new Recurly_Account($recurlyAccountCode);
            $subscription->plan_code = $daypass_plan_code;
            $subscription->account->first_name = $firstname;
            $subscription->account->last_name = $lastname;
            $subscription->account->email = $email;
            $subscription->account->billing_info = new Recurly_BillingInfo();
            $subscription->account->billing_info->token_id = $token;
            $subscription->collection_method = $invoice_collection_for_premium;
            $subscription->currency = $recurly_currency;
            $subscription->create();
        }else if ($isExisting == true){
            //It's an existing email in Recurly, basically with a day pass account type.
            $account = Recurly_Account::get($recurlyAccountCode);        
            $subscriptions = Recurly_SubscriptionList::getForAccount($recurlyAccountCode);
            foreach ($subscriptions as $subscription) {}                    
            if ($subscription->plan->plan_code == $premium_plan_code &&               
                strtolower($subscription->state) == "active"){
                require 'db_close_connection.php';
                $errorcode['errorDayPass'] = true;
                echo json_encode($errorcode);
                die;
            }
            
            $account->first_name = $firstname;
            $account->last_name = $lastname;
            $account->email = $email; 
            
            $billing_info = new Recurly_BillingInfo();
            $billing_info->account_code = $recurlyAccountCode;
            $billing_info->token_id = $token;
            
            $subscription->plan_code = $daypass_plan_code;
            $subscription->collection_method = $invoice_collection_for_premium;
            $subscription->account->currency = $recurly_currency;
            
            $account->update();
            $billing_info->update();
            $subscription->updateImmediately();
                                        
        }
        $timestamp = date('Y-m-d G:i:s');
        if ($isNewEmail == true){
            mysqli_query ($conn, "INSERT INTO $db_schema.User 
                            (email, password, recurlyAccountCode, regionCode, createDate)
                            VALUES ('$email', '$password', '$recurlyAccountCode', '$country', '$timestamp')");
        }
        
        if ($isUpgrade == true || $isExisting == true){
            mysqli_query ($conn, "UPDATE $db_schema.User 
                                 SET                          
                                 PASSWORD = '$password',
                                 RECURLYACCOUNTCODE = '$recurlyAccountCode',
                                 REGIONCODE = '$country',
                                 CREATEDATE = '$timestamp'
                                 WHERE EMAIL = '$email'");                  
        }
        
        
        
            
        session_start();
        $_SESSION['login'] = "1";                   
        $expireTime = time() + (24 * 60 * 60);
        
        require '../validateNewEmail.php';
        
        setcookie("plancode", $daypass_plan_code, $expireTime, "/"); 
        setcookie("email",  $email, $expireTime, "/"); 
        setcookie("regioncode", $country, $expireTime, "/");
        
                    
    } catch (Exception $e) {
        $errorcode['errorBilling'] = true;
        $errorcode['errorMessage'] = $e->getMessage();
    }

    ob_flush();

    require 'db_close_connection.php';


    echo json_encode($errorcode);          

        
?>