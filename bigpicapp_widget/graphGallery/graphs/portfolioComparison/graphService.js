
define (['portfolio/portfolioController', 
         'userInput/inputController',
         'postal',
         'graphGallery/graphs/portfolioComparison/messages/en'], 
        
    function(portfolioController,
            inputController,
            postal,
            messages
            ){

        var yAxisType = 'percentage';
        var subscribtion;
    
        function subscribe(){
            subscribtion = postal.channel().subscribe("portfolio.closed", function(){
                graph();        
            });
        }
    
//        postal.channel().subscribe("portfolioService.portfolio.std.dev", function(stdev){
//            var std = stdev.deviation;
//            if (std==1){
//                std = "100%";
//            }else{ 
//                std = (stdev.deviation*100).toFixed(1)+"%";
//            }
//            
//            $("#itext_1").text(std);
//        }); 
//    
//    
        
        var minInputValue = 0;

        function graph(){
            
            $('.risk_label').fadeIn();

            removeElements();
          
            setGraphTitles();
       
            var result = calculateDataList();
            
            var dataList  = result[0];
            var minYValue = result[1];            
            var maxYValue = result[2];
            var portfolioNameList = result[3];
            var portfolioRiskList = result[4];
     
            yAxis = d3.svg.axis();
            xAxis = d3.svg.axis();

            x = d3.scale.ordinal()
                .domain(dataList.map(function(d){return d.x}))
                .rangeRoundBands([0, graphWidth], .2, .2);

            y = d3.scale.linear();


            var yTickValues = [];
            //yTickValues = yTickValues.calculateTicks(yAxisType, minYValue, maxYValue, minInputValue);
            yTickValues = [0, 0.25, 0.50, 0.75, 1];
            
            svg = svg.datum(dataList);
            y = y.domain([yTickValues[0], yTickValues[yTickValues.length-1]])
                .range([graphHeight, 0]); 

            yAxis.scale(y)
                 .orient("left")
                 .tickValues(yTickValues)
                 .tickFormat(function(d) {
                    var yValue = d.format(yAxisType, 0, d);
                    if (d==1){
                        yValue = "100%";
                    }
                   
                    return yValue;
                });

            xAxis.scale(x)
                 .orient("bottom")
                 .tickPadding(5)
                 .tickFormat(function(d,i){ return portfolioNameList[i]; });

            svg.selectAll(".x.axis")
                .call(xAxis)
                .selectAll(".tick text")
                .call((new String).wrap, x.rangeBand());
            
            svg.selectAll(".y.axis").call(yAxis);
            svg.append("g")
                .attr("id","graphBars") 
                .selectAll("rect")
                .data(dataList)
                .enter().append("rect")
                .attr("rx", "3") 
                .attr("ry", "3")      
                .attr('width', x.rangeBand())
                .attr('x', function(d, i){         
                        var yValue = y(d.y);
                        var zeroLineFactor = 0;
                        var heightFactor = 0.5;
                        if (d.y == yTickValues[0]){
                            zeroLineFactor = 1.2;
                        }
                        var thisObj = d3.select(this);
                        thisObj.attr("y", yValue-heightFactor-zeroLineFactor);
                        thisObj.attr("height", Math.abs(graphHeight-yValue-zeroLineFactor));
                        //thisObj.attr("class", "graphBar" + (i==0?" applied":""));
                        thisObj.attr("class", (i==inputController.getSelectedPortfolioIndex()?"portfolioBar selected":"portfolioBar graphBar"));
                        return x(d.x);
                });
                
            
            svg.append("g")
                .attr("id","portfolioTextAreas")
                .selectAll("rect")
                .data(dataList)
                .enter()
                .append("text")
                .attr("x", function(d, i) {
                        var yValue = d.y;
                        var yYValue = y(yValue);
                        var heightFactor = 22;
                        var textColor = "rgba(1,87,155,1)";
                        if (yValue==0||yYValue>graphHeight*(0.75)){
                            heightFactor = -6;
                        }else if (i==inputController.getSelectedPortfolioIndex()){
                            textColor = "white";                            
                        }
                        var text = yValue.format(yAxisType, 1, yValue);
                        text = (d.lowerThanMin?"<": d.higherThanMax?">":"") + text;
                        var textId  = "text_"+i;  
                        var textObj = d3.select(this);
                        textObj.text(yValue!==1?text:"100%");
                        textObj.attr("id", textId);
                        textObj.attr("fill", textColor);  
                        textObj.attr("y", yYValue + heightFactor);
                        textObj.attr("style", 'font-size:18px');
                        var textWidth=document.getElementById(textId).getBBox().width;
                        return x(d.x) + ((x.rangeBand()-parseFloat(textWidth))/2);
                });
            
        
            
            
 
                svg.selectAll(".x.axis").selectAll("text").each(function(i){   
                    if (i==inputController.getSelectedPortfolioIndex()){
                        d3.select(this).attr("color", "#000");
                    }else{
                        d3.select(this).attr("class", "normalText");
                    }
                    
                    
                });
            
                svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                    d3.select(this).attr("class", "normalText");                    
                });
            
            postal.channel().publish("graphService.graph.done");

        }  
    

    
        function calculateDataList(selectedPortfolioList){
            var dataList = [];
            var nameList = [];
            var riskList = [];
            var maxValue;
            var minValue;
            var value;
            var result; 
            var successRateList = portfolioController.calculateAllSavedPortfolioSuccessRate();
            $.each(successRateList, function(index, value){
                var successRate = value.successRate;
                var portfolioRisk = value.portfolioRisk;
                var portfolioName = value.portfolioName.split(" ");
                var finalName = "";
                $.each(portfolioName, function(index, name){
                    if (name.length>12){
                        name = name.substr(0,11) + ". ";
                    }
                    finalName += name + " ";
                });
                nameList.push(finalName.trim(" "));                
                finalName = "";
                riskList.push(portfolioRisk);
                minValue = (minValue === undefined || successRate < minValue) ? successRate : minValue;
                maxValue = (maxValue === undefined || successRate > maxValue) ? successRate : maxValue;
                
                dataList.push({x:index, y:successRate});
            }); 
            return [dataList, minValue, maxValue, nameList, riskList];
        }
    
    
        function finalize(){
            removeElements();
            localGraphData = null;
            context = null;
            xAxisTitle.html("");
            removeAxis();
            subscribtion.unsubscribe();
            $(".risk_label").fadeOut();
        }           
    
        function branchChanged(){
            finalize();
        }
    
        function graphChanged(){
            finalize();
        }
    
        function getSelectedPortfolioIdList(){
            return JSON.parse(localStorage.getItem("portfolioGraphIdList"));
        }
 
        function removeSelectedPortfolioIdList(){
            localStorage.removeItem("portfolioGraphIdList");
        }  
    
        function removeElements(){
            $("#graphBars").remove();
            $("#yAxis").remove();
            $("#portfolioTextAreas").remove();
            $("#standardDeviationAreas").remove();
            $("#riskLabel").remove();
            
        } 
     
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            graphWidth = (containerWidth * 0.80);
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
    
        function removeAxis(){
            svg.selectAll(".x.axis").remove();
            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            svg.append("g").attr("class", "x axis");
            svg.append("g").attr("class", "y axis");
        }
     
    
        function setGraphTitles( ){    
            var graphWidthLimit = 1359;
            var documentWidth = $(document).width();
            var graphSubtitle = localContext.graphSubtitle.normal;
            if  (documentWidth<graphWidthLimit){
                graphSubtitle = localContext.graphSubtitle.abbreviate
            }
            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                    ({regionStartYear:localStorage.getItem("regionStartYear")}));
        }
    
        function init(lang, branchCode){
            addAxis();
            subscribe();
            calculateGraphDimensions();
            localContext = messages[branchCode]; 
            xAxisTitle.html(localContext.xAxisTitle);
            y2AxisLabel = localContext.y2AxisTitle;
            $(".graph_title").html(localContext.graphTitle);            
            $('.risk_label').fadeIn();	
            postal.channel().publish("graphGallery.graph.initiated");
        }

        return {
            init:               init,
            graph:              graph,
            graphChanged:       graphChanged,
            branchChanged:      branchChanged
        };

        
});



