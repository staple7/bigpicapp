
   
    var graphWidth;
    var graphHeight;
    var svg, x, y, yAxis, xAxis;
    var yAxis = d3.svg.axis(); 
    var xAxis = d3.svg.axis();

    var xAxisTitle;
    var yAxisTitle;
    var y2AxisTitle;

    var graphSvg;
    var currentGraphIndex = 0;
    var maxGraphIndex;
 
    var svgArea = ".svgArea";
    //var mtnBubble = $("#mtnBubble");
    var bubble;
    var bubbleText;
    var bubbleBar;
    var bubbleRect;

    define (['postal'], function(postal){
        
        postal.channel().subscribe("graphService.graph.done", function(){
             hideBubble();

        });
        
        
        postal.channel().subscribe("graphService.graph.show.bubble", function(data){
            bubble.moveToFront();
            bubble.attr("transform", "translate("+(data.xPosition-38)+")");
            bubbleRect.attr("fill", "rgba(234,234,234,1)");
            bubbleBar.attr("transform", "translate("+data.xPosition+",0)");
            bubbleBar.attr("height", graphHeight);
            bubbleBar.attr("fill", "gray");
            bubbleText.text(data.text);
            bubbleText.call((new String).wrap, 24);

        });
        
        postal.channel().subscribe("graphService.graph.hide.bubble", function(){
            hideBubble();
        });
        
        function hideBubble(){
            bubbleRect.attr("fill", "transparent");
            bubbleBar.attr("fill", "transparent");
            bubbleText.text("");
        }
        
        Number.prototype.format = function formatNumber(type, decimalPlaces, value){
            return Handlebars.compile("{{formatNumber type decimalPlaces value}}")
                            ({type: type, decimalPlaces: decimalPlaces, value: value});
        }
        
        Handlebars.registerHelper('formatThousands', function(value){
            return d3.format(",")(value);
        });
                  
        Handlebars.registerHelper('formatNumber', function(type, decimalPlaces, value){
            if (type == 'monthYear'){
                var date = new Date(value);//TEMP;
                var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", 
                              "Aug", "Sep", "Oct", "Nov", "Dec" ];
                value = months[date.getMonth()]+'. '+date.getFullYear();
            }else if (type == 'years'){
                value = Math.round(value);
            }else if (type == 'currency'){
                if (value<1){
                    value = Number(value);
                    value = "$" + value.toFixed(2); 
                }else{
                    value = d3.format("$,.3s")(value);
                    value = value.replace('G','B').toUpperCase();
                }
            }else if (type == 'percentage'){
                return (value*100).toFixed(decimalPlaces)+"%";
            }
            return value;
        });

        function start(numberOfGraphs){
            $.get('graphGallery/graphGalleryTemplate.html', function(resp){
                $(".graph-area-container").html($.parseHTML("<link href='graphGallery/graphGalleryStyle.css rel='stylesheet' type='text/css'>"+resp));
                

                appendSvg();
                displayGraphSelector(numberOfGraphs);
                setMaxGraph(numberOfGraphs);
                xAxisTitle = $(".xAxisTitle .x-text");
                yAxisTitle = $(".yAxisTitle");
                y2AxisTitle = $(".y2AxisTitle");
                graphSvg  = $(".graphSvg");
                //bindEvents();
                postal.channel().publish("graphGallery.template.rendered");
            });
        }
        
        function bindEvents(){
            $('.graph-arrow').on({
                'click touchstart': function(e){
                    e.preventDefault();
                    var element = $(e.target);
                    var className = $(this).attr("class");
                   // console.log("currentGraphIndex",currentGraphIndex);
                    if (className.indexOf("prev")>-1){
                        currentGraphIndex -= 1;
                        if (currentGraphIndex < 0) currentGraphIndex = 0;
                    }else{
                        currentGraphIndex += 1;
                        if (currentGraphIndex > maxGraphIndex) currentGraphIndex = maxGraphIndex;
                    }
                    //console.log("prev", currentGraphIndex);
                    if (currentGraphIndex < maxGraphIndex && currentGraphIndex >= 0){
                        graphChanged(currentGraphIndex);
                    }
                }
            });

            $('.graph-dash').on({
                'click touchstart': function(e){
                    e.preventDefault();
                    currentGraphIndex = $(this).index()-1;
                    graphChanged(currentGraphIndex);
                }
            });
          
        }

        function graphChanged(index){
            
            showLoadingGif();

            $('.graph-dash.is_active').removeClass('is_active').addClass('disabled');
            $('.graph-dash').eq(index).removeClass('disabled').addClass('is_active');
            //prev graph-arrow
           
            postal.channel().publish("graphGallery.graph.changed", index);
            
             /*
			if you are on the first page, don't show "previous"
			on the last page don't show "next"
			*/
            var mode='';//you are on KEY page
        	if($('.nav > a.is_active').hasClass('with'))//if on withdrawal page
            	mode = $('input[name=mode]:checked').val();
            $('.prev').removeClass('disabled');
            //console.log("----------------- .pageset." + mode + '_pages .prev + .graph-dash.is_active');
//			if($(".pageset." + mode + '_pages .prev + .graph-dash.is_active').length)
//			{
//				$(".pageset." + mode + '_pages .prev').addClass('disabled');
//			}
            if(index==0)
			{
				$('.prev').addClass('disabled');
			}
			$('.next').removeClass('disabled');
			if((index + 1) == maxGraphIndex)
			{
				$('.next').addClass('disabled');
			}	
                   
        }
        
        function setMaxGraph(count){
            maxGraphIndex = count;
        }
        
		function modalityChanged(graphCount){
            displayGraphSelector(graphCount);
            setMaxGraph(graphCount);
            currentGraphIndex = 0;
        }
			
        function displayGraphSelector(count){
         
           var parent = $('.pagenavwrap .graphnav');

                            
            var html = "<div class='prev graph-arrow disabled'><svg height='37' viewBox='0 0 24 24' width='37' xmlns='http://www.w3.org/2000/svg'><path d='M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z'/></svg></div>";
            
            
            var activeDash = "&nbsp&nbsp;<div class='graph-dash is_active'>&mdash;&nbsp;</div>";
            var dash = "<div class='graph-dash'>&mdash;&nbsp;</div>";
            
            for (var i=0; i<count; i++){
              html += (i==0?activeDash:dash);   
            }
            
            html += "<div class='next graph-arrow'><svg  height='37' viewBox='0 0 24 24' width='37' xmlns='http://www.w3.org/2000/svg'><path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z'/></svg></div>";
            
            parent.html(html);
            bindEvents();
        }
        
        function selectDash(graphIndex){
            
        }
        
        d3.selection.prototype.moveToFront = function() {
          return this.each(function(){
            this.parentNode.appendChild(this);
          });
        };
        
        function appendSvg(){
            
            svg = d3.select('.chartspace')
                    .append("svg")
                    .attr("width", "100%")
                    .attr("height", "120%")
                    .attr("id", "graphSvg")
                    .attr("xmlns", "http://www.w3.org/2000/svg")
                    .append("g");
            
            svg.append("g").attr("id", "mtnBubble");
            
            bubble = d3.select("#mtnBubble");
            
            bubble.append("rect")
                    .attr("id", "mtnBubbleRect")
                    .attr("height", "48px")
                    .attr('width', "75px")
                    .attr("y", "-49")
                    .attr("fill", "transparent");
            
            bubble.append("text")
                    .attr("id", "mtnBubbleText")                
                    .attr("dy", "-1.2")
                    .attr("fill", "black")         
                    .attr("style", 'font-size:12px')
                    .attr("text-anchor", "middle")
                    .attr("x", "30")
                    .attr("y", "-30")
                    .attr("transform", "translate(38,-2)");
            
            
            bubbleText = d3.select("#mtnBubbleText");
          
            svg.append("rect")
                .attr("id", "mtnBubbleBar")               
                .attr("y", -1)
                .attr('width', 1)
                .attr("fill", "transparent");
            
            bubbleBar = d3.select("#mtnBubbleBar");
            bubbleRect = d3.select("#mtnBubbleRect");
            
        }
        
        
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            graphWidth = (containerWidth * 0.80);
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
        

        function resize(){
            calculateGraphDimensions();
        }

        function showGraphSvg(show){
            graphSvg.stop().animate({'opacity':show == false? 0 : 1}, 0);          
        }
        
        function showAllGraphElements(show){
            var elements = xAxisTitle.add(yAxisTitle)
                                     .add(graphSvg)
                                     .add($('.graphExtraInfoWrap'));
            
            elements.stop().animate({'opacity':show == false? 0 : 1}, 0);
            elements.css({visibility:show == false ? "hidden": "visible"}); 
        }

        function showLoadingGif(show){
            showAllGraphElements(show===undefined?false:!show);
            var element = $('.chartLoaderWrap');
            $('.loaderGraphic').css('display', (show == false)?'none':'block');
        }

      
        
  



        

        return {
            start:                  start,
            resize:                 resize,        
            showLoadingGif:         showLoadingGif,
            showGraphSvg:           showGraphSvg,
            modalityChanged:        modalityChanged,
            showAllGraphElements:   showAllGraphElements
        };

    });