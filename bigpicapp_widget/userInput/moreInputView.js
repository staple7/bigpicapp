
define (['postal'], function(postal){

    var unchecked = "radio_button_unchecked";
    var checked   = "radio_button_checked";
    var localInput;

    var lastValidValue;
//    postal.channel().subscribe("moreInputView.popup.open", function(){
//        bindEvents();
//    });
//    
//    postal.channel().subscribe("moreInputView.popup.close", function(){
//        unbindEvents();
//    });
//    
    function unbindEvents(){
        $('.total_expense_ratio').unbind('click touchstart');
        $('.other_ter_box').unbind('click touchstart');
        $('.other_ter').unbind('keypress focus blur');
        $('.rebalancing-box').unbind('click touchstart');
    }
    
    
    function checkTer(ter){
        var found = false;
        $('.total_expense_ratio input[type=radio]').each(function() {
            var element = $(this);
           
            if (!element.hasClass('other_ter_radio')){
                 
                if (ter == getNumber(element.val())/100){
                    found = true;
                    element.prop('checked', true);
                }else{
                    element.prop('checked', false);
                }
            }
        }); 
        if (!found && ter>=0){
            $('.other_ter_radio').prop('checked', true);
            if (ter == 0){
                $('.other_ter').val('0.0%');
            }else{
                $('.other_ter').val(formatPercentage(ter));
            }
            
        }
    };

    function checkRebalancing(years){
        $('.rebalancing-box input').each(function() {
            var element = $(this);
            element.prop('checked', (years == element.val()) ? true : false);
        }); 
    };

    function render(input, messages){
        //console.log(input.getTer())
        localInput = input;
        var moreContainer = $('.extra_info_wrap');
        var savedMoreInput = getSavedMoreInput();
        if (savedMoreInput){
            localInput.ter = savedMoreInput.ter;
            localInput.rebalancingYears = savedMoreInput.rebalancingYears;
        }else{
            saveMoreInput(localInput.getTer(), localInput.getRebalancingYears());
        }
        moreContainer.load('userInput/template/moreInputs.html', function(template){
            messages.TER_LIST = [0.005, 0.01, 0.015, 0.02];
            moreContainer.html(Handlebars.compile(template)(messages));  
            checkTer(localInput.ter);
            checkRebalancing(localInput.rebalancingYears);
            bindEvents();
        });            
    }

    function bindEvents(){
        
        
        $('input[name=ratio]').change(function(){
            var value = getNumber($(this).val().trim());
            if (value>0){
                localInput.setTer(value/100);
                publishMoreInputChanged(localInput);
            }else{
                $('.other_ter').focus();
            }
            $('.other_ter').val('');
        });    

        $('.other_ter').on({
            'keypress': function(e){
                var keyCode = e.keyCode;
                var currentValue = $(this).val();
                
                lastValidValue = currentValue;
                if (e.shiftKey || (-1==$.inArray(keyCode,[46,190,110,38,40,8,13,39,37,48,49,50,51,52,53,54,55,56,57])) ||
                    currentValue.indexOf('.') >-1 && (keyCode == 46) ||
                    currentValue.length > 3 
                   ){
                    
                  
                    e.preventDefault();
                    
                }
            },
            'keyup': function(e){ //I need to use keydown to accept periods and arrows from keyboard(up and down).
                var keyCode = e.keyCode;
                var $this = $(this);
                var currentValue = $this.val();
                if (currentValue > 3){
                    $this.val(lastValidValue);
                }else{
                    lastValidValue = currentValue;
                }
                if(keyCode == 13){
                    $this.blur();
                }
 
            },
            focus: function(e){
                $(this).val(getNumber($(this).val()));   
            },
            'blur': function(e){                                 
                var value = $(this).val().trim();
               
                if (value.length==0 || value.length=='' || value == '.'){
                    localInput.setTer(0);
                    $(this).val('0.0%'); 
                    e.preventDefault();
                }else{                            
                    localInput.setTer(value/100);
                    $(this).val(formatPercentage(value)); 
                }
                
                publishMoreInputChanged(localInput);
            }
        }); 



        $('.rebalancing-box input').on({
            'click touchstart': function(e){
                e.stopPropagation();
                var element = $(this);        
                localInput.setRebalancingYears(element.val().trim());
                publishMoreInputChanged(localInput);                
            }
        });

    }
    
    function formatPercentage(value, lang){
        return  (value/100).toLocaleString('en', {style: 'percent', minimumFractionDigits:1});
    }
    
    function publishMoreInputChanged(){
//        console.log("--- --- --- --- ---");
//        console.log("isSaving", localInput.isSaving());
//        console.log("InvestmentHorizon", localInput.getInvestmentHorizon()); 
//        console.log("InitialInvestment", localInput.getInitialInvestment()); 
//        console.log("ContributionPercentage", localInput.isContributionPercentage()); 
//        console.log("ContributionAmount", localInput.getContributionAmount()); 
//        console.log("ContributionReal", localInput.isContributionReal()); 
//        console.log("SavingsGoal", localInput.getSavingsGoal()); 
//        console.log("PortfolioIndex", localInput.getPortfolioIndex());
//        console.log("Ter", localInput.getTer());
//        console.log("Rebalancing", localInput.getRebalancingYears());
        saveMoreInput(localInput.getTer(), localInput.getRebalancingYears());
        postal.channel().publish("inputController.more.inputs.changed", localInput);
    }
    
    function getSavedMoreInput(){
        return JSON.parse(localStorage.getItem("moreInputMap"));
    }
    
    function saveMoreInput(ter, rebalancingYears){
        localStorage.setItem("moreInputMap", 
                             JSON.stringify({ter:ter, rebalancingYears: rebalancingYears}));
    }  

    function isValidKey(e){
        var keyCode = e.keyCode;
        if (keyCode == 190) return true;
        if (e.shiftKey || 
            (-1==$.inArray(keyCode,[9,13,37,39,48,49,50,51,52,53,54,55,56,57,8]))){
            return false;        
        }
        return true;
    }

    function getNumber(value){
        return value.replace(/[^0-9.]/g, "");
    }    


    function getInput(){   
        return input;
    }

    return{
        render: render,
        getInput: getInput
    }

});