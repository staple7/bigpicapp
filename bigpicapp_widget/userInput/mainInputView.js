
define (['postal',          
         'userInput/moreInputView', 
         'userInput/input', 
         'userInput/messages/en'], 
        
        function(postal, moreInputView, defaultInput, messages){

            var portfolioArea;
            var portfolioOpen = false;
            var lastValidValue;
            setDefaultInputs(false);
    
            function checkPortfolio(){
                postal.channel().publish("inputController.getSelectedPortfolioIndex", function(index){
                    input.setPortfolioIndex(index);
                    $('input[name=port_div][value='+index+']').prop('checked', true);
                });
            }
    
            function checkModality(isSaving){
                $('input[value="retirement"]').prop('checked', isSaving?false:true);
                $('input[value="saving"]').prop('checked', isSaving?true:false);
            };
    
            function setInvestmentHorizonLabel(isSaving){ 
                $('.investment-horizon-label').html((isSaving ? messages.YEARS_TO_RETIREMET 
                                                    : messages.YEARS_IN_RETIREMET)  + ":");
            };
    
            function checkInvestmentHorizon(years){
                var element = $('input[name=yrs][value='+years+']');
                element.prop('checked', true);
                if (!element.prop( "checked" )){
                    $('input[name=yrs][value=0]').prop('checked', true);
                    $('.other_years').val(years);
                }
            };
    
          
            function setSavingsGoalLabel(isSaving){
                $('.saving-goal-label').html((isSaving ? messages.SAVINGS_GOAL : messages.LEGACY_CAPITAL) + ":");
            };
    
            
            function setContributionValues(){
                var isSaving = input.isSaving();
               
                $('.inflationadjlabel').html((isSaving ? messages.INFL_ADJ : messages.INFL_ADJ_SPENDING));
                var contributionAmount;
                if (isSaving){ 
                    
                    $('.contribution-label').html(messages.MONTHLY_CONTRIBUTION + ":");
                    $('.contribution_amount').removeClass('smallinput').addClass('mediumlargeinput');
                    $('.amount-in-dollars').hide();
                    $('.mo-label').hide();
                    input.setIsContributionPercentage(false);
                    //var valueInDollars = Math.round(getContributionInDollar(getNumber($('.contribution_amount').val())));
                    $('.contribution_amount').val(formatMoney(input.getContributionAmount())); 
                }else{
                     if (!input.isContributionPercentage()){
                         var valueInDollars = getNumber($('.contribution_amount').val());
                         var valueInPercentage = getContributionInPercentage(valueInDollars);
                         $('.contribution_amount').val(formatPercentage(valueInPercentage));
                         $('.amount-in-dollars').html(formatMoney(valueInDollars));
                    }else{
                        var amount = 4;
                        $('.contribution_amount').val(formatPercentage(getNumber(amount)));
                        var valueInDollars = Math.round(getContributionInDollar(getNumber(amount)));
                        $('.amount-in-dollars').html(formatMoney(valueInDollars));
                        var valueInDollars = Math.round(getContributionInDollar(getNumber(amount)));
                        $('.amount-in-dollars').html(formatMoney(valueInDollars));
                    }
                    $('.contribution-label').html(messages.WITHDRAWAL_RATE + ":");
                    $('.contribution_amount').removeClass('mediumlargeinput').addClass('smallinput');
                    $('.amount-in-dollars').show();
                    $('.mo-label').show();
                    input.setIsContributionPercentage(true);
                }
                
            }
    
            function getContributionInPercentage(valueInDollars){
                var initialInvestment = input.getInitialInvestment();
                if (initialInvestment == 0) return 0;
                return ((valueInDollars * 12) / initialInvestment)*100;
            }
    
            function displayContributionAmount(amount){
                if (input.isSaving()){
                    var valueInDollars = Math.round(getNumber(amount));
                    $('.contribution_amount').val(formatMoney(valueInDollars));
                }else{
                    $('.contribution_amount').val(formatPercentage(getNumber(amount)));
                    var valueInDollars = Math.round(getContributionInDollar(getNumber(amount)));
                    $('.amount-in-dollars').html(formatMoney(valueInDollars));
                }
            }
    
        
    
            function getContributionInDollar(valueInPercentage){
                return (valueInPercentage/100 * input.getInitialInvestment()) / 12;
            }

            function setDefaultInputs(isSaving){
                input = isSaving ? defaultInput['saving'] : defaultInput['retirement'];
            }
    
    
            function checkInflation(){   
                var isContributionReal = input.isContributionReal()
                $('input[name=inflationadjusted]').prop("checked", isContributionReal ? true: false);
            };
   
            function render(portfolioList, isSaving){
                var container = $('.sidebar');
            
                container.load('userInput/template/mainInputs.html', function(template){
                    portfolioArea = $(".portfolio-box-list").clone(false);
                    messages.PORTFOLIO_LIST = portfolioList;
                    container.html(Handlebars.compile(template)(messages)); 
                    checkModality(isSaving);                    
                    checkInvestmentHorizon(input.getInvestmentHorizon());
                    setInvestmentHorizonLabel(isSaving);
                    setContributionValues(); 
                    setSavingsGoalLabel(isSaving);
                    checkInflation();            
                    checkPortfolio();
                    addListeners();
                    bindEvents();
                    postal.channel().publish("mainInputView.rendered");
                });
                moreInputView.render(input, messages);
            }
            
            function bindEvents(){ 
            
                $('input[name=mode]').change(function(){
                	$('.modes').fadeTo(300, 0, function(){
                        var isSaving = !input.isSaving();
                        setDefaultInputs(isSaving);
                        postal.channel().publish("portfolioController.getSavedPortfolioNameList", function(portfolioList){
                            render(portfolioList, isSaving);
                            postal.channel().publish("inputController.modality.changed", isSaving);
                        });
						$('.modes').fadeTo(400, 1);
                    });
                });

                $('input[name=yrs]').change(function(){                    
                    var value = getNumber($(this).val().trim());
                    if (value>0){
                        input.setInvestmentHorizon(value);
                        publishInputChanged(input);
                    }else{
                        $('.other_years').focus();
                    }
                    $('.other_years').val('');
                });    
                
                $('.other_years').on({
                    'keypress': function(e){
                        var $this = $(this);
                        var maxValue = 40;
                        var newValue = ($this.val() + String.fromCharCode(e.keyCode));
                        if (stopInput(e, $this, newValue, maxValue, false)){
                            e.preventDefault();
                        }
                        if(e.keyCode == 13){
                            $this.blur();
                        }
                    },
                    'blur': function(e){                                 
                        var value = $(this).val();
                        if (value.length==0 || value.length==''){
                            checkInvestmentHorizon(input.getInvestmentHorizon());
                            e.preventDefault();
                        }else{                            
                            input.setInvestmentHorizon(value);
                        }
                        publishInputChanged(input);
                    }
                });                
                
                $('input[name=inflationadjusted]').change(function(){       
                    input.setIsContributionReal(!input.isContributionReal());                      
                    postal.channel().publish("inputController.more.inputs.changed", input);
                  
                });
                
                $('.contribution_amount').on({
                    'keypress': function(e){                       
                        var keyCode = e.keyCode;
                        var currentValue = $(this).val();
                        
                        lastValidValue = currentValue;
                        if (e.shiftKey || (-1==$.inArray(keyCode,[46,190,110,38,40,8,13,39,37,48,49,50,51,52,53,54,55,56,57])) ||
                            currentValue.indexOf('.') >-1 && (keyCode == 46) ||
                            currentValue.length > 3 
                           ){
                           
                            e.preventDefault();
                            
                        }
                    },
                    'keyup': function(e){ //I need to use keydown to accept periods and arrows from keyboard(up and down).
                        var keyCode = e.keyCode;
                        var $this = $(this);
                        var currentValue = $this.val();
                        
                        var maxValue = input.isSaving() ? 99999 : 100;
 
                        if (currentValue > maxValue){
                            $this.val(lastValidValue);
                        }else{
                            lastValidValue = currentValue;
                        }
                        
                        if(keyCode == 13){
                            $this.blur();
                        }
                    },
                    'focus': function(e){
                        $(this).val(getNumber($(this).val()));   
                    },
                    'blur': function(e){     
                        var value = $(this).val().trim();
                        if (value.length==0 || value.length=='' || value == '.'){
                            $(this).val('0.0'); 
                        }
                        displayContributionAmount($(this).val());
                        publishInputChanged(input);
                    }
                });     

                $('.initial_capital, .savings_goal').on({
                    'keypress': function(e){ //could use keypress intead for numeric keyboards.
                        
                        var $this = $(this);
                        var className = $this.attr('class');
                        var maxValue = 99999999;
                        
                        // var keyCode = 
                        //console.log(e.keyCode, String.fromCharCode(e.keyCode)); 
                        var newValue = ($this.val() + String.fromCharCode(e.keyCode));
                        if (stopInput(e, $this, newValue, maxValue, false)){                            
                            e.preventDefault();
                        }
                        if(e.keyCode == 13){
                            $this.blur();
                        }
                    },
                    'focus': function(e){      
                        
                        $(this).val(getNumber($(this).val()));                           
                    },
                    'blur': function(e){
                        var $this = $(this);
                        var value = $this.val().trim();
                        if (value.length==''){ value = 0; }
                        $this.val(value);   
                        var className = $this.attr('class');
                        if (className.indexOf('initial_capital')>-1){
                            input.setInitialInvestment(value);
                            displayContributionAmount($('.contribution_amount').val());
                        }else{
                            input.setSavingsGoal(value);                                
                        }
                        $this.val(formatMoney(parseInt(value))); 
                        publishInputChanged(input);
                    }
                });
            
                 bindPortfolioEvent();
               
//                $('.more-inputs').on({
//                    'click touchstart': function(e){
//                        console.log("OK.."); 
//                        showMoreInputsWindow();
//                    }  
//                });

                $('.print').unbind("click touchstart");
                $('.print').on({
                    'click touchstart': function(e){
                        postal.channel().publish("summaryController.start");
                    }
                });
                
                $('.help').on({//TEMP
                    'click touchstart': function(e){
                        var modalityCode = getModalityCode();
                        postal.channel().publish("branchesController.iButton.selected", modalityCode);
                    }
                });
                
                //
//                $('.extra_info_wrap')
//                .on({
//                    'touchstart click':function(e){
//                        e.preventDefault();
//                        console.log("extra...");
//                        $( ".extra_info_wrap" ).fadeOut(300, function() {});
//                    }
//                }, '.window_close_btn');
            }
  
    
            function bindPortfolioEvent(){
                $('input[name=port_div]').on({
                    'click touchstart': function(e){
                        var element = $(this);
                        var index = element.attr("value").trim();
                        input.setPortfolioIndex(index);
                        postal.channel().publish("mainInputView.portfolio.selected", index);
                    }
                });
            }
    
            function isTextSelected(input) {
                if (typeof input.selectionStart == "number") {
                  //  console.log(input.selectionEnd, input.value.length);
                    return input.selectionStart == 0 && input.selectionEnd == input.value.length;
                } else if (typeof document.selection != "undefined") {
                    input.focus();
                    return document.selection.createRange().text == input.value;
                }
            }
//    
//            function showMoreInputsWindow(show){
//                var element = $('.extra_info_wrap');
//                element.css("display", (show == false) ? "none" : "block");               
//            }
    
            function stopInput(e, $this, newValue, maxValue, allowPoint){
                if (allowPoint && e.keyCode == 190 && newValue.indexOf('.')>-1){
                    e.preventDefault();
                }
                if (!isValidKey(e, allowPoint)){
                    e.preventDefault();
                }
                if (newValue > maxValue){
                     if (!isTextSelected($this[0])){
                        // newValue = String.fromCharCode(e.keyCode);
                        e.preventDefault();
                            
                     }
                }
            }
    
            function isValidKey(e, allowPoint){
                var keyCode = e.keyCode;
                if (allowPoint && keyCode == 190) return true;
                if (e.shiftKey || 
                    (-1==$.inArray(keyCode,[9,13,37,39,48,49,50,51,52,53,54,55,56,57,8]))){
                    return false;        
                }
                return true;
            }
    
            function getNumber(value){
                if (!isNumeric(value)){
                    value = value.replace(/[^0-9.]/g, "");
                }
                return value;
            }    
    
            function isNumeric(n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            }
    

    
            function formatPercentage(value, lang){
                return  (value/100).toLocaleString('en', {style: 'percent', minimumFractionDigits:1});
                
            }
    
            function formatMoney(value, lang){
                return new Number(value).toLocaleString('en', {style: 'currency', 
                                                                currency: 'USD',
                                                                minimumFractionDigits:0, 
                                                                maximumFractionDigits: 0});
            }

            function publishInputChanged(input){
                if (input.isSaving()){
                    input.setContributionAmount(getNumber($('.contribution_amount').val()));
                }else{
                    input.setContributionAmount(-Math.abs(getNumber($('.amount-in-dollars').html())));
                }
//                console.log("InitialInvestment", input.getInitialInvestment());
//                console.log("SavingsGoal", input.getSavingsGoal());
//                console.log("InvestmentHorizon", input.getInvestmentHorizon());
//                console.log("Contribution", input.getContribution().getContributionAmount());
                postal.channel().publish("inputView.input.changed", input);
            }

    
            function getModalityName(){
                var element = $('input[value="retirement"]');
                if (!element.prop('checked')){
                    element = $('input[value="saving"]');
                }
                return element.siblings('span').text();       
            }
    
            function getModalityCode(){
                var code;
                var element = $('input[value="retirement"]');
                if (!element.prop('checked')){
                    element = $('input[value="saving"]');
                }
                return element.val();       
            }
    
    
            function formatYears(years){
                return years = years + ' ' + ((years == 1) ? messages.YEAR_SHORT : messages.YEARS_SHORT);
            }
    
            function getNamedInput(){
                var inputList = [];        
                
                inputList.push({name: $('.investment-horizon-label').text().replace(':',''), 
                                value: input.getInvestmentHorizon()});
                
                inputList.push({name: $('.initial-capital-label').text().replace(':',''), 
                                value: formatMoney(input.getInitialInvestment())});
                
//                inputList.push({name: $('.contribution-label').text().replace(':',''), 
//                                value: Math.abs(getNumber($('.contribution_amount').val()))});
                var contrValue = $('.contribution_amount').val();
                
                if (!input.isSaving()){
                   // contrValue = contrValue + '(' + $('.amount-in-dollars').html() + ' /mo)';
                }
                inputList.push({name: $('.contribution-label').text().replace(':',''), value: contrValue});
                                
                inputList.push({name: $('.saving-goal-label').text().replace(':',''), 
                                value: formatMoney(input.getSavingsGoal())});                         
                
             
                inputList.push({name: 'Expense Ratio', 
                                value: formatPercentage(input.getTer()*100)});
                
                inputList.push({name: 'Rebalancing', 
                                value: formatYears(input.getRebalancingYears())});
                
                inputList.push({name: $('.inflationadjlabel').text(), 
                                value: input.isContributionReal()?"Yes":"No"});
                return inputList;
            }
    
            
            function getInput(){
                if (input.isSaving()){
                    input.setContributionAmount(getNumber($('.contribution_amount').val()));
                }else{
                    input.setContributionAmount(-Math.abs(getNumber($('.amount-in-dollars').html())));
                }                
                return input;
                
            }
    
            function setKeyDefinition(context){
                var theTemplate = Handlebars.compile($('.helpwrap').html());
               
                $(".helpwrap").html(theTemplate(context));        
            }
    
            function renderPortfolio(portfolioList){
                var map = {};            
                var theTemplate = Handlebars.compile(portfolioArea.html());
                $(".portfolio-box-list").html(theTemplate({PORTFOLIO_LIST:portfolioList}));
                checkPortfolio();
                bindPortfolioEvent();
            }
    
    
            function addListeners(){
                
                


                  /* popups */
                var close        = $('<i class="popclose"><svg version="1.1"xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" xml:space="preserve"><path id="X_1_1_" fill="#999999" d="M11.9,10.4L9.5,8l2.5-2.5c0.2-0.2,0.2-0.5,0-0.7l-0.7-0.7c-0.2-0.2-0.5-0.2-0.7,0L8.1,6.6 L5.6,4.1c-0.2-0.2-0.5-0.2-0.7,0L4.2,4.8C4,5,4,5.3,4.2,5.5L6.6,8l-2.5,2.5C4,10.6,4,11,4.2,11.2l0.7,0.7c0.2,0.2,0.5,0.2,0.7,0 l2.5-2.5l2.5,2.5c0.2,0.2,0.5,0.2,0.7,0l0.7-0.7C12.1,11,12.1,10.6,11.9,10.4z"/></svg></i>');

                // When a user clicks on a popuplink
                $('.popper:not(.logout-link)').on('click', function(event){

                   
                    event.preventDefault();  
                    
                    var thisPopUp = $('.' + $(this).data('pop'));
                    thisPopUp.css('display', 'block');
                    
                    if(thisPopUp.eq(0).hasClass('portfolio_container')){
                        portfolioOpen = true;
                        $('#popupOverlay').fadeIn('1000');			// Show the popup
                    }else if(thisPopUp.eq(0).hasClass('settingswrap')){                        
                        postal.channel().publish("settings.open");
                    }else{
                        $('#popupOverlay').fadeIn('1000');
                    }
                    $('.popclose').remove();
                    
                    if(!$(this).data('noclose')){
                        $('.popup', $('#popupOverlay')).append(close);			// Add the close button to the popup
                    }

                    $('body').addClass($(this).data('noclose'));
                    $('body').addClass('popbody');
                    

                    


                });


                // close popup
                $('body').on('click', '.popclose, .popperclose', function(){
                    
                    $('#popupOverlay').fadeOut('1000', function(){
                        
                        portfolioOpen = false;
                        publishClosingPortfolioEvent($(this));

                        $('.popup').css('display', 'none');
                        $('body').removeClass('noclose');
                        $('body').removeClass('popbody');
                    });
                });

                //close popup on click outside
                $("html").on('click', 'body:not(.noclose)', function(event) {

                    if ($(event.target).closest('.onepop, [data-pop]').length === 0) {
                      
                        if (portfolioOpen){
                            portfolioOpen = false;
                            
                            publishClosingPortfolioEvent();
                        }
                        
                        $('#popupOverlay').fadeOut('1000', function(){
                            $('.popup').css('display', 'none');
                            $('body').removeClass('noclose');
                            $('body').removeClass('popbody');
                        });
                    }
                });

            }
    
    
            function publishClosingPortfolioEvent($this){
                postal.channel().publish("portfolio.closed");           
            }
    
    
            Handlebars.registerHelper('getPortfolioName', function(name){
                var maxLen = 13;
                if (name.length>maxLen){
                    name = name.substr(0, maxLen) + '...';
                }else if (name.trim().length == 0){
                    name = 'Unnamed';
                }
                return name;
            });
    
    
            Handlebars.registerHelper('getInitialCapital', function(){
                return formatMoney(input.getInitialInvestment());
            });
    
            Handlebars.registerHelper('getSavingsGoal', function(){
                return formatMoney(input.getSavingsGoal());
            });
            
            Handlebars.registerHelper('formatPercentage', function(num){
                return formatPercentage(num);
            });
    
            return{
                render:             render,
                getInput:           getInput,
                getModalityName:    getModalityName,
                getNamedInput:      getNamedInput,
                setKeyDefinition:   setKeyDefinition,
                renderPortfolio:    renderPortfolio
                
            }

     }
);