define({
        
    US_7:   "5-Year Gov. Bonds",
    US_6:   "10-Year Gov. Bonds",
    US_8:   "T-Bills",
    A_2:    "Global Bonds",
    A_3:    "Gold",    
    US_5:   "Total Market",    
    US_1:   "Large Cap",
    US_3:   "Mid Cap",
    US_2:   "Small Cap",
    US_4:   "Micro Cap",    
    A_1:    "Int'l (ex-USA)"   
    
});