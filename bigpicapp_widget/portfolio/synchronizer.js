define (['postal'], function(postal){
    
    
    postal.channel().subscribe("portfolioController.portfolio.changed", function(portfolioConfig){
        persistPortfolio(portfolioConfig);
    });
    
    function persistPortfolio(portfolioConfig){
       
    } 
      
    function getSavedPortfolio(){
        if (!navigator.onLine) { 
            publishConfigRetrieved(undefined);
            return;
        }
        $.ajax({ 
            type:     "POST", 
            dataType: "json",
            data:     {'email':""},
            //data:     {'email': null},
            url:      "/bigpicture_integrations/bigpicture_database/get_portfolio.php",
            timeout: 15000,
            success: function(portfolioConfiguration){
                if (portfolioConfiguration.length==0){
                    portfolioConfiguration = undefined;
                }else{
                    portfolioConfiguration = JSON.parse(portfolioConfiguration);
                }
                publishConfigRetrieved(portfolioConfiguration);
            },
            error: function(e){
                publishConfigRetrieved(undefined);
            }
        });
    }
    
    function publishConfigRetrieved(config){
        postal.channel().publish("portfolio.synchronizer.config.retrieved", config);
    }
    
    return{
        getSavedPortfolio: getSavedPortfolio
    }
    
});
