define({
    
   DEFAULT_PORTFOLIO_LIST: 
    [{
        NAME: "Diversified",
        ID: 0,
        ALLOCATION_MAP:{
            1:{CODE:"CA_2",VALUE:50},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"CA_1",VALUE:50},
            8:{CODE:"CA_3",VALUE:0},
            9:{CODE:"US_7",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}
           }
        },
     {
        NAME: "Income",
        ID: 1,
        ALLOCATION_MAP:{
            1:{CODE:"CA_2",VALUE:50},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"CA_1",VALUE:50},
            8:{CODE:"CA_3",VALUE:0},
            9:{CODE:"US_7",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}            
           }
        },
     {
        NAME: "Mixed Income",
        ID: 2,
        ALLOCATION_MAP:{
            1:{CODE:"CA_2",VALUE:50},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"CA_1",VALUE:50},
            8:{CODE:"CA_3",VALUE:0},
            9:{CODE:"US_7",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}            
        }    
    },
     {
        NAME: "Mixed Growth",
        ID: 3,
        ALLOCATION_MAP:{
            1:{CODE:"CA_2",VALUE:50},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"CA_1",VALUE:50},
            8:{CODE:"CA_3",VALUE:0},
            9:{CODE:"US_7",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}           
        }    
    },
     {
        NAME: "Growth",
        ID: 4,
        ALLOCATION_MAP:{
            1:{CODE:"CA_2",VALUE:50},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"CA_1",VALUE:50},
            8:{CODE:"CA_3",VALUE:0},
            9:{CODE:"US_7",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}            
        }    
    },
     {
        NAME: "Max. Growth",
        ID: 5,
        ALLOCATION_MAP:{
            1:{CODE:"CA_2",VALUE:50},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"CA_1",VALUE:50},
            8:{CODE:"CA_3",VALUE:0},
            9:{CODE:"US_7",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}           
        }    
    }
    
    
    ]
});   

