define (['postal'], function(postal){

    
    var isComingForKeyPrinciples = false;
    

    $('.logout-link').on('click touchstart', function(event) {
        event.preventDefault(); 
        if (!window.localStorage.getItem("logout")){
            $('.' + $(this).data('pop')).css('display', 'block');
            $('.popclose').remove();
            $('body').addClass($(this).data('noclose'));
            $('body').addClass('popbody');
            $('#popupOverlay').fadeIn('1000');	
        }else{
            window.open('/logout.php', '_self', false);
        }
    });

    $('.logout-got-it').on('click touchstart', function(event) {
        event.preventDefault(); 
        if ($('.logout-checkbox').prop('checked')){
            window.localStorage.setItem("logout", true);
        }
       window.open('/logout.php', '_self', false);
    });
    
    
    
    
    /*
    takes us to a certain "page" or chart
    p is the index
    */ 
    function toPage(p, mode)
    {
        
        if(mode!='')
            $('.loaderGraphic').css('display', 'block');
        $(".pageset." + mode + "_pages .onepage.is_active").fadeTo(500, 0, function() {
            $(this).removeClass('is_active');
            $(".pageset." + mode + '_pages .pnav').removeClass('is_active');
            $(".pageset." + mode + '_pages .onepage').eq(p).css('opacity', '0').addClass('is_active').fadeTo(500, 1, function(){
                $('.loaderGraphic').css('display', 'none');
            });
            $(".pageset." + mode + "_pages .pnav").eq(p).addClass('is_active');
            setNextPrev(getMode());
        });
    }


    /*
    if you are on the first page, don't show "previous"
    on the last page don't show "next"
    */
    function setNextPrev(mode)
    {
        $(".pageset." + mode + '_pages .prev').removeClass('disabled');
        if($(".pageset." + mode + '_pages .prev + .graph-dash.is_active').length)
        {
            $(".pageset." + mode + '_pages .prev').addClass('disabled');
        }

        $(".pageset." + mode + '_pages .next').removeClass('disabled');
        if($(".pageset." + mode + '_pages .graph-dash.is_active + .next').length)
        {
            $(".pageset." + mode + '_pages .next').addClass('disabled');
        }
    }



    /*
    switch the mode
    "mode" e.g., in the class .mode_retirement = "retirement" (.pageset.retirement_pages is the pageset for retirement)
    if no mode is selected, then choose the first
    */
    function switchMode(mode)
    {

        if (mode == '') isComingForKeyPrinciples = true;
        
        if(typeof mode === 'undefined')
        {
            $("input[name='mode']").eq(0).prop("checked", true);
        }
        else
        {
            //make the sidebar mode active
            addside='';
            if(mode=='')
            {
                $('.main .print').fadeOut('2000');	
                $('.monthly_data_label').fadeOut('2000');
                $('.risk_label').fadeOut('2000');
                addside=',.sidebar';
            }else{
                
                if (isComingForKeyPrinciples){
                  
                    isComingForKeyPrinciples = false;
                     //showLoadingGif();
                    setTimeout(function(){
                        postal.channel().publish("graphGalleryController.regraph");
                    },1000);
                    
                    
                }
                
                setTimeout(function(){
                    $('.monthly_data_label').fadeIn();	
                }, 500);
                $('.main .print').fadeIn('1000');	
            }
                
            
               
            
            $('.modes, .pageset.is_active'+ addside).fadeTo(300, 0, function(){
                    
                

                if (mode == 'saving') mode='retirement';
                
                
                    
                
                $(".onemode.is_active").removeClass('is_active');

                //make the sidebar mode active
                if(mode=='')
                {
                    $('.sidebar').css('display','none');
                    //center nav when no sidebar
                    $('.pagenav').css('left', '0');
                }
                else
                {
                    $('.sidebar').css({'display':'block', 'opacity':'1'});
                    //center nav with sidebar
                    $('.pagenav').css('left', '5px');
                }
                
                $('.onemode.mode_' + mode).addClass('is_active');

                $('.pageset').removeClass('is_active');

                //make the set of pages active
               
                $('.pageset.' + mode + '_pages').css('opacity', '0').addClass('is_active').fadeTo(300, 1);

                $('.onepage').removeClass('is_active');

                //make the first page active
                $('.pageset.' + mode + '_pages .onepage').eq(0).addClass('is_active').css('opacity','1');

                
                
                
                
                $('.pnav').removeClass('is_active');
                $('.pageset.' + mode + '_pages .pnav').eq(0).addClass('is_active');
                
                
                setNextPrev(getMode());

                $('.modes').fadeTo(300, 1);
                


            })
        }	
    }

    
    
    /* always make sure we are on the right mode */
    function getMode()
    {
        var m;

        if($('.nav > a.is_active').hasClass('with'))//if on withdrawal page
            m = $('input[name=mode]:checked').val();

        else//you are on KEY page
            m='';

        return m;	
    }




    /*
    run when the page is ready
    */
    $(document).ready(function(){

    	/* ON LOAD OF REFRESH show page loading graphic */
    	//$('.loaderPageGraphic').fadeOut("slow");

		$(".onepop .twocols.assets input[value='0%']").each(function(){
			$(this).prev().css('color', '#999');
		});

        /*
        when clicking in a text field, auto-select the previous radio button
        */
        $('body').on('focus click', 'input[type=text]',  function(){
            $(this).prev('input[type=radio]').trigger('click');
        });



        /*
        This is the drop-down for the MORE menu
        */
        $('.morebutton').on('mouseenter', function(e){
            e.preventDefault();
            $('.more').addClass('is_active');		
        })

        $('.morebutton').on('click', function(e){
            e.preventDefault();
            $('.more').toggleClass('is_active');		
        })
        /*
        Closes the MORE menu when you click away
        */
        $("html").on('click', function(event) {
            if ($(event.target).closest('.more, .morebutton').length === 0) {
                $('.more').removeClass('is_active');
            }
        });


        /* navigate between pages */
        $('.nav > a').on('click', function(event){
            event.preventDefault();

            $('.nav > a').removeClass('is_active');
            $(this).addClass('is_active');

            if($(this).hasClass('with'))//clicked "withdrawal"
            {	
                switchMode(getMode());	
                //$('.sidebar').css('display','block');
                $('.help').show();
            }
            else if($(this).hasClass('key'))//clicked "key"
            {
                switchMode('');
                //$('.sidebar').css('display','none');
                $('.help').hide();
            }
        })



        //disable first or last arrow if necessary
        setNextPrev(getMode());

        //choose correct mode in case user's choice is still active from a reload
        switchMode(getMode());
        
       
      
        
        $('.smnote').on('click', function(e){e.preventDefault();})
        
    });
    
 
    
    
});    