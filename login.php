<?php
    session_start();
?>

<!DOCTYPE html>
    
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Big Picture | Login</title>
<meta name="description" content="Login here to access the Big Picture app, and see a new view of retirement planning.">
<meta name="keywords" content="Big Picture login">
<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="css/fonts.css">
<link rel="stylesheet" type="text/css" href="css/form.css">
<link rel="stylesheet" type="text/css" href="custom/custom-bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/client.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
              
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-43634131-2', 'auto');
      ga('send', 'pageview');

</script>
</head>
<body >
<!--nav bar-->
<div class="container-fluid nav-bg navbar-fixed-top borderBottom">
        <div class="row">
            <div class="container">
                <div class="row">
                <nav class="navbar navbar-default no-border" >
      
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header" >
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
           </button>
         <a class="navbar-brand" href="/"></a>
         </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="defaultNavbar1">
          <ul class="nav navbar-nav navbar-right">
            <li class="signUpForFree"><a  href="/sign-up-for-free.html" class="dropdown-toggle btn btn-default btn-warning white"  role="button" aria-expanded="false" id="signUpForFree_link">Free Trial</a>
            <li><a  href="/premium.html" data-toggle="">Pricing</a></li>
            <li><a  href="/login.php" class="dropdown-toggle login"  role="button" aria-expanded="false"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>&nbsp;Log In</a></li>
          </ul>
        </div>
        
      <!-- /.container-fluid --> 
</nav>
                </div>
            </div> 
        </div>
 </div> 
<!--end nav bar-->
<div class="wrap">

<div class="container text-center">
<!--
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning loginForReferralling display_none">Please sign in or register to access this page.</div>
        </div>
    </div>
-->
    <div class="row">
        <div class="col-lg-12">
            <div class="h1Spacing">
            
            <h1 class="positionIndex clearfix">Log in to the Big Picture App</h1>
            <h4>Don't have a subscription? <a class="thin " href="pro.html">Sign up here.</a></h4>
            </div>
        </div>
        <div class="col-sm-10 col-sm-offset-1" >
            <div class="bottom_form" >
            <!--End Free Trial-->
                <form class="log-in input_h" id="log-in" data-role="log-in">
                    <!--internet required-->
                    <div class="alert alert-danger display_none internetRequest" role="alert"></div>
                    <!--end error alert-->
                    
                    <div class="form-group">
                      <input required placeholder="Email" name="email" class="form-control" id="email" data-recurly="email" type="email" maxlength="50" value="" />
                    </div>
                    
                    <div class="form-group">
                      <input required placeholder="Password" name="password" id="pass-word" maxlength="50" class="form-control" type="password" value="" />
                      <input type="hidden" value="" class="" name="openFree">
                    </div>

                    <button id="" class="btn btn-lg btn-block thin btn-warning">Log In</button>
                   
                   
                    <span class="light_grey thin small text-left loginTipsForfirefox"></span>

                    <p></p>
                    <div class="forgotPasswordSpacing">
                        <a href="forgot_password.html" class="thin btn-link">Forgot your password?</a>
                    </div>
                    <div class="throw_error"></div>
                    <div id="success"></div>
                </form>
            </div>
        </div>
    </div>
    

  
     
</div>




 
<!--footer--->
<footer>

</footer>
</div>
<!--end pricing page--->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script data-main="/js/main.js?v2.0.0" src="/lib/require.js"></script>  


</body>
</html>
