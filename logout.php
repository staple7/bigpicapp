<?php

    session_start();
    $exp_time = time() - 3600;
    $current_plan_code = $_COOKIE['plancode'];
    setcookie("plancode", "", $exp_time, "/");
    setcookie("firstname", "", $exp_time, "/");
    setcookie("lastname", "", $exp_time, "/");
    setcookie("newEmail", "", $exp_time, "/");
    setcookie("hardReload", "", $exp_time, "/");
    setcookie("custom_group", "", $exp_time, "/");

    $_SESSION['login'] = "0";
    $_SESSION = array();
    session_destroy();
   


    header("Location: " . "login.php");
?>

