<?php
    require 'bigpicture_recurly/bigpicture_config.php';

    $errorcode = array('errorOther'=>false,
                       'errorMessage'=>'');

    try {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $comment = $_POST['comment'];
        
//        $name = 'Carlos';
//        $email = 'cmoligo@gmail.com';
//        $comment = 'asdasd'; 

        $to_email = "info@bigpicapp.co";           
        $email_type = "contact_form";        
        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php'; 
        
    } catch (Exception $e){
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e->getMessage();   
    }

    echo json_encode($errorcode);      

?>

