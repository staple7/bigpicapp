<?php

    @ob_start();

    require 'bigpicture_recurly/bigpicture_config.php';
    require 'bigpicture_recurly/safe_password.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false, 
                       'errorLogin'=>false,
                       'expired'=>false,
                       'plancode'=>false,
                       'errorMessage'=>'');

    try {

$expireTime = time() + (35 * 24 * 60 * 60);
       
        setcookie("plancode", "big_picture_premium",$expireTime, "/");
        setcookie("email",  "cmoligo@gmail.com", $expireTime,"/");
        setcookie("regioncode", "US",$expireTime, "/");
        
        
    } catch (Exception $e){
        $errorcode['errorMessage'] = $e->getMessage(); 
        $errorcode['errorOther'] = true;
    }


    require 'bigpicture_recurly/db_close_connection.php';       

    echo json_encode($errorcode);   

    ob_flush();



?>



