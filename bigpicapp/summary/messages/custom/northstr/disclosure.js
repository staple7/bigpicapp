define({
    

    DISCLOSURE:{

        TITLE: "Disclosure:",
        
        PARAGRAPH1: "This hypothetical example is for illustrative purposes only. It is not a guarantee of a future success rate. The success rate is a hypothetical value presented to the user to show what the success would have historically been, based on historical returns and the information input by the user. Under no circumstances does the information contained herein represent a recommendation to buy or sell securities. This value should not be relied upon by the user as a guaranteed measure of achieving a given result.",

        PARAGRAPH2: "The charts and figures contained herein are based on historical total return data beginning January 1, 1926, and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended.",
        
        PARAGRAPH3: "An investment cannot be made directly in an index. Government bonds and Treasury bills are guaranteed by the full faith and credit of the United States government as to the timely payment of principal and interest, while stocks are not guaranteed and have been more volatile than the other asset classes shown.",
    
        PARAGRAPH4: "Neither asset allocation nor diversification guarantee against loss. They are methods used to manage risk. Investments will fluctuate and when redeemed may be worth more or less than when originally invested. Past performance does not guarantee future results.",
        
        PARAGRAPH5: "Investments in fixed income securities are subject to the creditworthiness of their issuers and interest rate risk. As such, the net asset value of bond funds will fall as interest rates rise.",
        
        PARAGRAPH6: "Investments in small, mid or micro cap companies involve greater risks not associated with investing in more established companies, such as business risk, stock price fluctuations, increased sensitivity to changing economic conditions, less certain growth prospects and illiquidity.",
        
        PARAGRAPH7: "Investment risks associated with international investing, in addition to other risks, may include currency fluctuations, political, social and economic instability, and differences in accounting standards when investing in foreign markets.",
        
        PARAGRAPH8: "Portfolios that invest in gold bullion are subject to certain risks. The price of gold has fluctuated widely over the past several years. Several factors affect the price of gold, including: global supply and demand; global or regional political, economic or financial events and situations, investors’ expectations with respect to the rate of inflation; currency exchange rates and interest rates. There is no assurance that gold will maintain its long-term value in terms of purchasing power in the future.",
        
        PARAGRAPH9: "Securities and investment advisory services are offered through CRI Securities, LLC and Securian Financial Services, Inc. Member FINRA/SIPC. CRI Securities, LLC is affiliated with Securian Financial Services, Inc. and North Star Resource Group. North Star Resource Group is not affiliated with Securian Financial Services, Inc. but is independently owned and operated.",
        
        PARAGRAPH10: "Securian Financial Services, Inc, CRI Securities, LLC, and North Star Resource Group are not affiliated with Investments Illustrated, Inc. / the Big Picture App. © 2017 Investments Illustrated, Inc."
    },
    
    SOURCES:{
        
        TITLE: "Sources:",

        PARAGRAPH1: "Center for Research in Security Prices (CRSP): U.S. Micro Cap Stocks, U.S. Small Cap Stocks, U.S. Mid Cap Stocks, U.S. Large Cap Stocks, and U.S. Total Market Stocks are represented by total-return indexes consisting of publicly-traded equities organized into relevant percentiles by their market capitalizations. U.S. Treasury Bills and U.S. 5-Year Government Bonds are represented by total-return indexes that track the respective performance of said instruments.",
        
        PARAGRAPH2: "Global Financial Data, Inc.: International Stocks are represented by a total-return index consisting of globally-diversified, publicly-traded equities. The index excludes U.S. equities. U.S. 10-Year Government Bonds are represented by a total-return index that tracks the performance of said instrument. Global Bonds are represented by a total-return index consisting of globally-diversified government debt instruments. Gold is represented by an index that tracks the Gold Bullion Price-New York (USD/oz.).",
    
        PARAGRAPH3: "U.S. Bureau of Labor Statistics and Global Financial Data, Inc.: U.S. Inflation is represented by the Consumer Price Index."
    },
    
    DOFU :{
        DOFU_CODE: "1931215 DOFU 07/2017"
    }
    
});
