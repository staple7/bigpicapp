 
define({

    "PROVIDED":         "Provided",
    "YOUR_PORTFOLIO":   "Your portfolio",
    "ASSET_ALLOCATION": "Asset allocation",
    "OTHER_INPUTS":     "Other inputs",
    "HISTORICAL":       "*Hypothetical success rate based on historical performance."
    
});
