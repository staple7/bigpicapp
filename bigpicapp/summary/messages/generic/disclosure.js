define({
    

    DISCLOSURE:{

        TITLE: "Disclaimer:",
        
        PARAGRAPH1: "The charts and figures contained herein are based on historical total return data beginning January 1, 1926 (U.S.) and January 1, 1935 (Canada), and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended. ",

        PARAGRAPH2: "An investment cannot be made directly in an index. Government bonds and Treasury bills are guaranteed by the full faith and credit of the United States government as to the timely payment of principal and interest, while stocks are not guaranteed and have been more volatile than the other asset classes shown. International Stocks exclude U.S. Stocks. International stocks involve special risks such as fluctuations in currency, foreign taxation, economic and political risks, liquidity risks, and differences in accounting and financial standards. Past performance is not an indicator of future performance."
    },
    
    SOURCES:{
        
        TITLE: "Sources:",

        PARAGRAPH1: "U.S. Micro Cap Stocks, U.S. Small Cap Stocks, U.S. Mid Cap Stocks, U.S. Large Cap Stocks, U.S. Total Market Stocks, U.S. Treasury Bills, U.S. 5-Year Government Bonds: USA 5-Year Government Bond Total Return Index—Center for Research in Security Prices (CRSP). International Stocks: ex-U.S.A. Total Return Index, U.S. 10-Year Government Bonds: USA 10-Year Government Bond Total Return Index, Canadian Government Bonds: Canada 10-Year Total Return Government Bond Index, Canadian Treasury Bills: Canada Total Return Bills Index, Canadian Stocks: Canada S&P/TSX-300 Total Return Index, Global Bonds: GFD Global USD Total Return Government Bond Index, Gold: Gold Bullion Price-New York (US$/oz)—Global Financial Data, Inc. U.S. Inflation: Consumer Price Index—U.S. Bureau of Labor Statistics and Global Financial Data. Canada Inflation: Statistics Canada."
    },
    
    DOFU :{
        DOFU_CODE: ""
    }
    
});
