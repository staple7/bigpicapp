

define(['postal', 
        'summary/summaryView', 
        'summary/warnings'],  
       function(postal, summaryView, warnings){
    

        function publishTemplateDone(){
            postal.channel().publish("summaryView.template.done");
        }
    
        postal.channel().subscribe("summaryController.start", function(){
            
            require(['summary/messages/'+bigpic_custom_dir+'/disclosure'], function (disclosure) {
                require(['summary/messages/'+bigpic_custom_dir+'/en'], function (translation) {
                    start(disclosure, translation);
                });
            }, 
            function () {
                require(['summary/messages/generic/disclosure', 'summary/messages/generic/en'], function (disclosure, translation) {
                    start(disclosure, translation);
                }); 
            });
        }); 
    
        function start(disclosure, translation){
                        
            if (!validatePlatform()){
                alert("Not supported.");
                publishTemplateDone();
                return;
            }
            
            $.get('graphGallery/graphGalleryStyle.css?v'+app_version, function(style){
             
                    var channel = postal.channel();
                    channel.publish("inputController.getModalityName", function(modalityName){
                        channel.publish("inputController.getNamedInput", function(modalityInput){
                            channel.publish("inputController.getSuccessRate", function(successRate){
                                channel.publish("inputController.getSuccessRateDefinition", function(successRateDefinition){
                                    channel.publish("portfolioController.getSelectedNamedAllocation", function(portfolio){

                                        //successRate = Math.floor(parseFloat(successRate)*100) + "%";
                                        var icon = "<svg class='svgColor' viewBox='0 0 40 40'"
                                                            +" xmlns='http://www.w3.org/2000/svg'>"
                                                            + "<defs><style type='text/css'><![CDATA["
                                                            + ".svgColor{ fill: rgba(3, 169, 244, 1)}"
                                                            + "]]></style></defs>"
                                                            + "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='24px' height='24px' viewBox='3.5 3 17 17' style='enable-background:new 0 0 18 18' xml:space='preserve'><polygon points='10,17 5,12 6.4,10.6 10,14.2 17.6,6.6 19,8 '></polygon></svg>"
                                                            +"</svg>";

                                        successRate = {value: successRate, name: "Success Rate", icon: icon};

                                        var context = $.extend({translation:    translation}, 
                                                               {modalityInput:  modalityInput}, 
                                                               {portfolio:      portfolio}, 
                                                               {modalityName:   modalityName},
                                                               {successRate:    successRate},
                                                               {successRateDefinition: successRateDefinition},
                                                               {disclosure:     disclosure},
                                                               {action:         getAction()}); 
                                        
                                        summaryView.start(context);
                                        publishTemplateDone();
                                    });
                                });
                            });
                        });
                    }); 
                    

            });

            
        }
    
        function getAction(){
            var action = "download";
            var warning = getWarningMessage();
            if (warning){
                action = warning.action;
            }
            return action;
        }
    
        function validatePlatform(){
            return getWarningMessage().supported;
        }

        function getWarningMessage(){
            var platform = getPlatform();
            var notFound = {message: "Functionality not supported.", supported: false};
            if (platform){
                var warning;
                var warning = warnings[platform.device];
                if (warning){
                    warning = warning[platform.browser];
                }else{
                    warning = notFound;
                }
            }else{
                warning = notFound;
            }
            return warning;
        }
    
        function getPlatform(){
            var platform;
            var device, browser;
            var navAgent = navigator.userAgent;
                
            //Device
            if (navAgent.indexOf('Mac OS X') > -1){
                device = 'mac';
            }else{
                device = 'pc';
            }
            if (/Android/i.test(navAgent)){
                device = 'android';
            }
            
            var isSafari = false;
            var isChrome = false;
            var isFirefox= false;
            var isOpera= false;            
            var isIE = false;
            var isEdge = false;
            
            
            if (/iPad|iPhone|iPod/.test(navAgent) && !window.MSStream){
                device = 'ios';
                isChrome = navAgent.indexOf('CriOS') > -1;
                if (!isChrome){
                    isFirefox = navAgent.indexOf('FxiOS') > -1;
                }
                if (!isChrome && !isFirefox){
                    isOpera = navAgent.indexOf("OPiOS") > -1;
                }
                if (!isChrome && !isFirefox && !isOpera){
                    isSafari = navAgent.indexOf("Safari") > -1;
                }
            }else{
                isIE = navAgent.indexOf('MSIE') > -1;
                if (!isIE){
                    isEdge = navAgent.indexOf("Edge") > -1;
                }
                if (!isIE && !isEdge){
                    isFirefox = navAgent.indexOf("Firefox") > -1;
                }
                if (!isIE && !isEdge && !isFirefox){
                    isOpera = navAgent.indexOf("OPR") > -1;
                }
                if (!isIE && !isEdge && !isFirefox && !isOpera){
                    isChrome = navAgent.indexOf('Chrome') > -1;
                }
                if (!isIE && !isEdge && !isFirefox && !isOpera && !isChrome){
                     isSafari = navAgent.indexOf("Safari") > -1;
                }

            }
          
            //alert(navAgent);
       
            if (isChrome){
                browser = 'chrome';   
            }else if (isSafari){
                browser = 'safari';
            }else if (isFirefox){
                browser = 'firefox';
            }else if (isOpera){
                browser = 'opera';
            }else if (isIE){
                browser = 'ie';
            }else if (isEdge){
                browser = 'edge';
            } 
          
            if (device && browser){
                platform = {};
                platform['device'] = device;
                platform['browser'] = browser;    
            }
            
           //alert(platform.device+ " " +platform.browser);
            return platform;
        }

        return {
            start: start,
            getWarningMessage: getWarningMessage
        };

    }
);




