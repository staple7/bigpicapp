
define([],  function(){
    
    function start(context){
       
        var doc = new jsPDF('p', 'pt','a4', true);
        var docWidth = 595;        

        var firstLineY = 50;
        var rectangleY = 300; 
        var successRateY = rectangleY + 60;
        var iconY = successRateY + 45;
        var marginInputs = 80;
        var providedY = 70;
        var portfolioY = providedY + marginInputs;
        var portfolioY = providedY + marginInputs;
        var inputsY = providedY + marginInputs;
        var disclosureY = 540;
        var historicalPercentage=.29;
        
        //Outoput var rectangle
        doc.setDrawColor(3, 169, 244);
        doc.setFillColor(255, 255, 255);
        var rectDim = docWidth * .50;
        var outputRectX = (docWidth-rectDim)/2;
        doc.rect(outputRectX, rectangleY, rectDim, rectangleY-125, 'FD');
        
        var image = new Image();
        
//        
//        html2canvas(document.body, {
//          onrendered: function(canvas) {
//            document.body.appendChild(canvas);
//          },
//          width: 300,
//          height: 300
//        });
//        
        var encodedData = window.btoa(context.successRate.icon);
                    
        image.src = "data:image/svg+xml;base64,"+encodedData;
        
        image.onload = function() {
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');
            var dataUrl;
            var factor=8;
            canvas.width = image.width*factor;
            canvas.height = image.height*factor;
            ctx.scale(3,3);
            ctx.drawImage(image, 0, 0, image.width*4, image.height);
            dataUrl = canvas.toDataURL('image/png');
            //doc.setFillColor(3, 169, 244);
            //doc.circle(300, iconY+17, 20, 'F');
            var iconWidth = 25;
          
                
            try{
                doc.addImage(dataUrl, 'PNG', (docWidth-iconWidth)/2, iconY, 45, 200, '', 'FAST');
            }catch(e){    
                successRateY += 20; 
                historicalPercentage = .23;
            }finally{            
                render();
            }
            
        }

    

        function render(){   
            
            
            
            //Branch Name
            doc.setFontSize(24);
            doc.setTextColor(251, 140, 79);
            doc.text(20, firstLineY, context.modalityName);

            //Your Name
            doc.setTextColor(117, 117, 117);
            doc.setFontSize(14);
           
 
            doc.setDrawColor(116, 116, 116);
            var lineMargin = 70;
            var contentText = context.translation.ASSET_ALLOCATION;
            doc.text(131, providedY + 60, contentText);  
            doc.line(25, providedY + lineMargin, 335, providedY + lineMargin);
            
            var contentText = context.translation.OTHER_INPUTS;
            doc.text(430, providedY + 60, contentText);  
            doc.line(378, providedY + lineMargin, 570, providedY + lineMargin);

            //Provided portfolio
            
            doc.setFontSize(11);
            var portfolioX = 25;
            
            $.each(context.portfolio , function(index, object){
                if (index == 6){
                    portfolioX = 200;
                    portfolioY = providedY + marginInputs;
                }
                portfolioY += 15;
                doc.setFontType("normal");
                if (object.allocation!=="0%"){
                    doc.setFontType("bold");
                }
                doc.text(portfolioX, portfolioY, object.name);    
                doc.text(portfolioX + 115, portfolioY, object.allocation);
            });

            
            doc.setFontType("normal");
            
            //Provided inputs.
            doc.setFontSize(12);
            doc.setTextColor(117, 117, 117);
            var portfolioX = 380;
            
            $.each(context.modalityInput , function(index, object){  
                var varName = object.name;
                var varValue = object.value;
                if (varName=='Inflation-adjust spending'){
                    varName = 'Inf.-adj. spending';
                }
                if (varName=='Inflation-adjust contributions'){
                    varName = 'Inf.-adj. contr.';
                }
                inputsY += 15;
                doc.setFontType("bold");
                doc.text(portfolioX, inputsY, varValue.toString());
                doc.setFontType("normal");
                doc.text(portfolioX + 80, inputsY, varName);

            });
            
          
            //Output variable
            var successRate = context.successRate;
            if (successRate){
                
                var varValue = successRate.value;
                var textNoYRS= removeYRS(varValue);

                doc.setTextColor(3, 169, 244);
                doc.setFontSize(20);
                
                doc.text(centerText(doc, textNoYRS, 20), successRateY, textNoYRS);   
                doc.setFontSize(16);

                doc.setFontSize(16);
                doc.text(centerText(doc, successRate.name, 16), successRateY + 27, successRate.name);
                
                
                doc.setFontSize(16);

                doc.text(centerText(doc, successRate.name, 16), successRateY + 27, successRate.name+"*");
                doc.setFontSize(9);
                doc.setTextColor(129, 129, 129);
                
                doc.text(outputRectX+10, successRateY + successRateY*historicalPercentage, context.translation.HISTORICAL);
                
//                doc.setFontSize(11);
//                
//                var splitText = doc.splitTextToSize(context.successRateDefinition, 200);
//                
//                var textY = successRateY + 55;
//                 $.each(splitText, function(key, text){
//                    //doc.text(20, successRateY + 20*key, text);
//                   doc.text(centerText(doc, text, 16), textY + 13*key, text);
//                });
                  //  doc.text(20, disclosureY + 40, splitText);
                
                //doc.text(centerText(doc, context.successRateDefinition, 16), successRateY + 40, context.successRateDefinition)
               
            }

            //Disclosure.
            
                
            doc.setFillColor(240,240,240);
            doc.rect(0, disclosureY - 30, docWidth, 842, 'F');
         
            doc.setTextColor(117, 117, 117);
            var extraSpace;                
            
            var paragraphCounter = 0;
            var dofu_code = context.disclosure.DOFU.DOFU_CODE;
            
            $.each(context.disclosure, function(key, object){
              
                extraSpace = 13;
                doc.setFontSize(11);
                
                $.each(object, function(key, text){
                    if (key=='DOFU_CODE') return true;
                    paragraphCounter++;
                    var splitText = doc.splitTextToSize(text, 550);
                    if (splitText[0]=='Sources:'){
                        doc.text(20, disclosureY+10, splitText);
                        disclosureY += (splitText.length * 13 ) + 24;
                    }else{
                        doc.text(20, disclosureY, splitText);
                        disclosureY += (splitText.length * 13 ) + extraSpace;
                    }
                    doc.setFontSize(9);
                    extraSpace = 3; 
                    if (paragraphCounter == 7 ){
                        doc.text(20, disclosureY+7, dofu_code);
                        doc.text(500, disclosureY+7, "Cont'd next page");
                        disclosureY = 50;
                        doc.addPage();
                        doc.setFillColor(240,240,240);
                        doc.rect(0, 0, docWidth, 842, 'F');
                    }
                });
            });
            doc.setFontSize(9);
            doc.text(20, disclosureY+7, dofu_code);
            getPDF(doc, context.action);
            
            function centerText(doc, text, fontsize){
                var w = doc.getStringUnitWidth(text) * fontsize,
                   calx	= ( docWidth - w ) / 2;
                return calx;
            }

   
            
        }


    }
    
    function openTab(url) {
    // Create link in memory
    var a = window.document.createElement("a");
    a.target = '_blank';
    a.href = url;
 
    // Dispatch fake click
    var e = window.document.createEvent("MouseEvents");
    e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    a.dispatchEvent(e);
};
 

    function getPDF(file, action){
        if (action=='download'){
            file.save('BigPictureSummary.pdf');
        }else{
            file.output('dataurlnewwindow');
        }
    }

    function removeYRS(text){
//        if (text.indexOf("YR")>-1){ 
//            text = text.replace("YRS", "");
//            text = text.replace("YR", "");
//        }
        return text;
    }    
    
    return {
        start: start
    };
    
    
    
});