define (['postal'], 
         function(postal){
         // 	function upload_logo(image_file){        
	        	//    console.log('here in controller');
	        	// 	console.log(image_file);
	        	// }
	        	// postal.channel().subscribe("graphService.graph.done", function(){

	      	 //  	postal.channel().subscribe("userInfo.userlogoController.upload_logo", function(){
        	//      console.log('here2');

        	// });
        	// postal.channel().subscribe("userInfo.userlogoController.upload_logo", function(){
	       	postal.channel().subscribe("userInfo.userlogoController.upload_logo", function(image_info){  
                var validImageTypes = ["image/jpeg", "image/png"]; 
                if(image_info == '' || image_info == undefined) 
		        {  
                    $("#error_img").removeClass('green');
                   $("#error_img").addClass('red');
		           $("#error_img").text('Please upload logo image.');
		           window.setTimeout(function() {
                        $("#error_img").text('');
                    }, 3000);
		           return;
		        }


		        var fileType = image_info["type"];
		        if ($.inArray(fileType, validImageTypes) < 0) { 
                    $("#error_img").removeClass('green');
                    $("#error_img").addClass('red');
                    $("#error_img").text('Please enter valid image ( jpg, png and jpeg are allowed ).');
                    window.setTimeout(function() {
                        $("#error_img").text('');
                    }, 3000);
                    return; 
                }   
                var sizeKB = image_info.size / 1024;

                if(sizeKB > 700) 
		        {  
                    $("#error_img").removeClass('green');
                    $("#error_img").addClass('red');
                    $("#error_img").text('Please upload smaller size image ( max. 700 kb ).');
	              window.setTimeout(function() {
                        $("#error_img").text('');
                    }, 3000);
		           return;
		        } 
				var form_data = new FormData();
                form_data.append('file', image_info);
       			$.ajax({
                    url:  "/bigpicture_integrations/users_uploads/manage_logo.php",
                    type: "POST",
                    contentType: false,
                    processData: false, 
                    data: form_data,
                    success: function (data) {
                        if(data != 0){
                            $("#error_img").removeClass('red');
                            $("#error_img").addClass('green');
                        	$("#error_img").text('Image uploaded successfully.');
                            data = $.trim(data); 
		                    window.setTimeout(function() {
		                        $("#error_img").text('');
                                $('#file_upload').val('');
                                $(".logo-preview-img").css('display' , 'inline-block');
                                $("#logo_changed").attr("src", "images/uploads/"+data);
                                $("#upload_image1").hide();
		                    }, 3000);
		                    
		                    setTimeout(function() {
						   		$("#cancel").trigger('click');
							}, 3000);
		                    return; 
                        }else{
                            $("#error_img").removeClass('green');
                            $("#error_img").addClass('red');
                        	$("#error_img").text('Something wrong hpppen.Please try after sometime.');
		                    window.setTimeout(function() {
		                        $("#error_img").text('');
		                    }, 2000); 
		                    return; 
                        }
                    }
                });  
	        }); 
	       	// function upload_logo(val){
	       	// 	console.log('in a function');
	       	// 	console.log(val);
	       	// 	var form_data = new FormData();
         //        form_data.append('file', val);
       		// 	$.ajax({
         //            url:  "/bigpicture_integrations/users_uploads/manage_logo.php",
         //            type: "POST",
         //            contentType: false,
         //            processData: false,
         //            // dataType: 'json',
         //            // async: false,
         //            data: form_data,
         //            success: function (data) {
                        
         //            }
         //        }); 
	       	// }
	    	return {
                // upload_logo : upload_logo
            };
        });