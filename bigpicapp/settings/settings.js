  
 define (['postal',,
          '../../lib/jquery.validate', 
          '../../lib/recurly', 
          '../../lib/payment.min'], 
         function(postal, payment){
     
            $.get('settings/settings.html?v'+$.cookie("app_version"), function(resp){
                 $(".settingswrap").html($.parseHTML("<link href='settings/style.css?v"+$.cookie("app_version")+"' rel='stylesheet' type='text/css'>" +resp));
            });
     
//            $(".settingswrap").load('/bigpicapp/settings/settings.html?v'+app_version,function(resp){
//                $(".settingswrap").html("<link href='bigpicapp/settings/style.css?v"+app_version+"' rel='stylesheet' type='text/css'>" +resp);
//            });

            if(document.domain == 'www.bigpicapp.co' || document.domain == 'bigpicapp.co'){
                recurly.configure('ewr1-FK1UvBU8VkBr0bHJWt1UZm');
            }else{      
                recurly.configure('ewr1-xaeGjoPWYfleHqVT987m9Z');         
            }
     
            postal.channel().subscribe("settings.open", function(){
                $('.loaderPageGraphic').css('display', 'block');
                getSubscriberDetail();
            });
            

            // //Global setting

            var ajax_response = {
                'successfully_updated_password'     : 'Your password has been successfully updated.',
                'successfully_updated_billing_info' : 'Your billing information has been successfully updated.',
                'successfully_updated_email'        : 'Your email has been successfully updated.',
                'subscription_canceled'             : 'Your subscription has been successfully canceled.',
                'subscription_reactivate'           : 'Your subscription has been successfully reactivated.',
                'errorEmail'                        : 'Sorry, email does not exist.',
                'errorExistingNewEmail'             : 'An account already exists for this email address.',
                'errorSameNewEmail'                 : 'Please enter a new email address.',
                'passwordNotMatch'                  : 'Old password and email don’t match.',
                'errorOther'                        : 'Sorry, there is a system error. Please try again later.'
                },
                internetRequestInfo = 'An internet connection is required.';


            //error warning
            function errorWarning(obj,text){
                $(obj).html(text)
                .stop(true, true)
                .slideDown(500)
                .delay(4000)
                .slideUp(500);

            }

            function emptySpaceLimit(obj){
                //upgrade_password
                $(obj).on('keypress', function(e) {
                    if (e.which == 32)
                        return false;
                });

            }	
                emptySpaceLimit('.change_password_wrap input');

            //system alert
                function systemAlert(obj,text){
                    $(obj).addClass("display_block");
                    $(obj).stop(true, true).siblings().hide()
                          .end()
                          .text(text)
                          .stop(true, true)
                          .fadeIn(1000)
                          .delay(6000)
                          .fadeOut(1000);
                } 

            //login error
             function $errorLogin(obj,text){
                var $email          = $('input',obj).eq(0),
                    $emailNextEle   = $email.siblings('.systemError');
                if(!$emailNextEle.length){ //just show one time
                    $('<label class="systemError"></label>')
                     .insertBefore($email)
                     .text(text);
                }
                 //remove error info when typing again:
                $email.on('click',function(){
                    if($email.length){
                    $email.siblings('.systemError').remove();
                    }
                });
             }

            //validate zip
            jQuery.validator.addMethod("zipcode", function(value, element) {
              return this.optional(element) || /^[0-9a-zA-Z]+$/.test(value);// /^\d{5}(?:-\d{4})?$/
            }, "Please provide a valid zipcode.");



            function switchZipcodelength(){
                var zipEle = $('.settings_zip input[data-recurly="postal_code"]');
                if($.cookie('country') === 'US'){
                    zipEle.attr('maxlength', 5);
                }
                else{
                    zipEle.attr('maxlength', 25);
                }

                $('.settings_billing_country select').on('change',function(){
                    if($(this).val() === 'US'){
                        zipEle.attr('maxlength', 5);
                    }
                    else{
                        zipEle.attr('maxlength', 25);
                    }
                });
            }

            switchZipcodelength();
            // recurly error

            function error (obj,err) {
                var newErrorText=[];
                $.each(err.fields, function (i, field) {
                  newErrorText.push($('[data-recurly=' + field + ']').siblings('label').text());
                });
                systemAlert('.settings_error_alert', 'The following fields appear to be invalid: ' + newErrorText.join(', '));
                //$('.settings_error_alert').text('The following fields appear to be invalid: ' + err.fields.join(', ')).fadeIn(1000).delay(10000).fadeOut(1000);
                $('input[type = "button"]',obj).prop('disabled', false);
                $.each(err.fields, function (i, field) {
                  $('[data-recurly=' + field + ']').addClass('error');
                });
            }


            //reset input

             $.fn.clearFormvalues = function(obj){
                  $('input',obj).not(':button, :reset, :hidden, :submit')
                  .val('')
                  .removeAttr('checked')
                  .removeAttr('selected');
              };

            //validate internet



            var data = {'email':$.cookie('email')};
                                //validate credit card type
                                $('.billing_info_form form').each(function() {
                                    $('#credit_card_numaber').payment('formatCardNumber');
                                    //$('.expdate').payment('formatCardExpiry');
                                    $('.settings_cvv input').payment('formatCardCVC');

                                });  

                     function getSubscriberDetail(){
                                //$('.loading_settings').fadeIn(100);
                                $.ajax({
                                    type:'POST',
                                    //data:{'email':'cmolina33@test.com'},
                                    data:data,
                                    dataType:'json',
                                    timeout: 20000,
                                    url:'/bigpicture_recurly/settingsPageBackend/subscription_detail.php',                        
                                    success: function(subscription_data){


                                        $('.loading_settings').fadeOut(100);                            

                                        var     subscriber_name            = subscription_data.subscriber_name,
                                                plan_name                  = subscription_data.plan_name,
                                                expiration_date            = subscription_data.expiration_date, 
                                                email                      = $.cookie('email'),
                                                billing_first_name         = subscription_data.billing_first_name,
                                                billing_last_name          = subscription_data.billing_last_name,

                                                //billing_credit_card_first  = subscription_data.billing_credit_card_first,
                                                billing_credit_card_last   = subscription_data.billing_credit_card_last,

                                                billing_credit_card_type   = subscription_data.billing_credit_card_type,
                                                billing_exp_year           = subscription_data.billing_exp_year,
                                                billing_exp_month          = subscription_data.billing_exp_month,
                                                billing_country            = subscription_data.billing_country,
                                                billing_zip                = subscription_data.billing_zip,

                                                state                      = subscription_data.state,
                                                start_date                 = subscription_data.start_date,
                                                end_date                   = subscription_data.expiration_date,
                                                cost                       = subscription_data.cost,

                                                invoiceList                 = subscription_data.invoice_list;

                                         var billing_credit_card_middle = {
                                                'val' : (function(){
                                                    if($.payment.cardType($('input[data-recurly="number"]').val()) === 'amex'){
                                                        return 'XXXX XXXXXX X';
                                                    }
                                                    else{
                                                        return 'XXXX XXXX XXXX ';
                                                    }
                                                })()
                                         };       

                                                for(var index=0;index < invoiceList.length;index++){
                                                    var invoiceObj       = invoiceList[index];

                                                    var billed_on        = invoiceObj.billed_on;
                                                    var invoice_number   = invoiceObj.invoice_number;
                                                    var invoice_status   = invoiceObj.status;
                                                    var invoice_total    = invoiceObj.total;
                                                    $('td[data-settings="billing_number"]').text(invoice_number);
                                                    $('td[data-settings="billed_on"]').text(billed_on);
                                                    $('td[data-settings="invoice_status"]').text(invoice_status);
                                                    $('td[data-settings="invoice_total"]').text(invoice_total);
                                                }


                                        var  response_info = {
                                            'confirm_cancel_info'       : 'Canceling your subscription will cause it not to renew. If you cancel your subscription, it will continue until ' + expiration_date + '. Then, the subscription will expire and will not be invoiced again. Please confirm your cancelation.',
                                            'reactivate_info'           : 'Are you sure you want to reactivate this subscription?',
                                            'cancel'                    : 'Cancel',
                                            'reactivate'                : 'Reactivate',
                                            'status_canceled'           : 'Canceled',
                                            'status_Active'             : 'Active',
                                            'submit_cancel'             : 'Cancel subscription',
                                            'submit_reactivate'         : 'Reactivate'

                                        };




                                        //col 1
                                        $('span[data_settings = "subscriber_name"]').text(subscriber_name);
                                        $('p[data-settings = "plan_name"]').text(plan_name);    
                                        $('p[data-settings = "email"]').text(email);

                                        //col 2
                                            //billing info
                                        $('span[data-settings = "subscription_first_name"]').text(billing_first_name);
                                        $('span[data-settings = "subscription_last_name"]').text(billing_last_name);

                                        $('span[data-settings = "billing_credit_card_last"]').text(billing_credit_card_last);
                                        $('span[data-settings = "billing_credit_card_type"]').text('(' + billing_credit_card_type + ')');

                                        $('span[data-settings="billing_exp_month"]').text(billing_exp_month);
                                        $('span[data-settings="billing_exp_year"]').text(billing_exp_year);


                                            //billing form
                                        $('input[data-settings = "subscription_first_name"]').val(billing_first_name);
                                        $('input[data-settings = "subscription_last_name"]').val(billing_last_name);

                                        $('.billing_info_form input[data-recurly="number"]').attr('placeholder', billing_credit_card_middle.val + billing_credit_card_last);

                                        $('.settings_month option').each(function() {
                                            if($(this).val() === billing_exp_month){
                                                $(this).prop('selected',true);
                                            }
                                        });

                                        $('.settings_year option').each(function() {
                                            if($(this).val() === billing_exp_year){
                                                $(this).prop('selected',true);
                                            }
                                        });

                                         $('.settings_billing_country option').each(function() {
                                            if($(this).val() === billing_country){
                                                $(this).prop('selected',true);
                                            }
                                        });

                                        $('.billing_info_form input[data-recurly="postal_code"]').val(billing_zip);

                                        //col 3
                                        $('td[data-settings="state"]').text(state);
                                        $('td[data-settings="start-date"]').text(start_date);
                                        $('td[data-settings="end-date"]').text(end_date);
                                        $('td[data-settings="cost"]').text(cost);

                                        //cancel and reactivate
                                        $('div[data-settings="cancel_container"] p').text(response_info.confirm_cancel_info);
                                        $('div[data-settings="reactivate_container"] p').text(response_info.reactivate_info);

                                        var status  =   $('td[data-settings="state"]'),
                                            data    =   { 'email' : $.cookie('email')
                                                            };
                                            //$this   =   $('.Cancel_Reactivate');
                                            //cancel subscription

                                            if(status.text() !== 'Canceled'){
                                                console.log('active');
                                                $('.submitCancel').removeClass('display_none');
                                                $('.submitReactivate').hide();

                                            }
                                            //reactivate subscription
                                            else{
                                                console.log('canceled');
                                                $('.submitCancel').hide();
                                                $('.submitReactivate').show();

                                                //$this.text(response_info.reactivate);  

                                            }

                                        //console.log($this.text); 

                                                $('body')  
                                                //cancel subscription
                                                .on({
                                                    'click touchstart' :function(e){
                                                        e.preventDefault();
                                                        e.stopPropagation();
                                                        var $btn    =   $(this);
                                                            if(!navigator.onLine){
                                                                errorWarning('.internetRequest', internetRequestInfo);
                                                            } 
                                                            else{
                                                                $btn.prop('disabled', true);
                                                                $.ajax({
                                                                    type:'POST',
                                                                    dataType:"json",
                                                                    data:data,
                                                                    url:"/bigpicture_recurly/settingsPageBackend/cancel_premium.php",
                                                                    success: function(errorcode){
                                                                        console.log('test:' , errorcode);
                                                                        $btn.prop('disabled', false);
                                                                        if(errorcode.errorEmail){
                                                                                systemAlert('.settings_error_alert', ajax_response.errorEmail);

                                                                        }
                                                                        if(errorcode.errorOther || errorcode.errorMessage){
                                                                                systemAlert('.settings_error_alert', ajax_response.errorOther);

                                                                        }
                                                                        else{
                                                                            systemAlert('.settings_success_alert', ajax_response.subscription_canceled);
                                                                            status.text(response_info.status_canceled);
                                                                            $('.submitCancel').hide();
                                                                            $('.submitReactivate').show();
                                                                            $('div[data-settings="cancel_container"]').hide();
                                                                            $('div[data-settings="reactivate_container"]').hide();
                                                                            $('.settings_plan_info').show();
                                                                        }

                                                                    }
                                                                });
                                                            }




                                                    }
                                               },'.subscription_cancel_btn')

                                                .on({
                                                    'click touchstart' :function(e){
                                                        //reactivate
                                                        e.preventDefault();
                                                        e.stopPropagation();
                                                        var $btn    =   $(this);
                                                        if(!navigator.onLine){
                                                            errorWarning('.internetRequest', internetRequestInfo);
                                                        } 

                                                        else{ 
                                                            $btn.prop('disabled', true);
                                                            $.ajax({
                                                                type:'POST',
                                                                dataType:"json",
                                                                data:data,
                                                                url:"/bigpicture_recurly/settingsPageBackend/reactivate_premium.php",
                                                                success: function(errorcode){
                                                                    $btn.prop('disabled', false);
                                                                    if(errorcode.errorEmail){
                                                                            systemAlert('.settings_error_alert', ajax_response.errorEmail);

                                                                    }
                                                                    if(errorcode.errorOther || errorcode.errorMessage){
                                                                            systemAlert('.settings_error_alert', ajax_response.errorOther);

                                                                    }
                                                                    else{
                                                                        systemAlert('.settings_success_alert', ajax_response.subscription_reactivate);
                                                                        status.text(response_info.status_Active);
                                                                        $('.submitCancel').show();
                                                                        $('.submitReactivate').hide();
                                                                        $('div[data-settings="cancel_container"]').hide();
                                                                        $('div[data-settings="reactivate_container"]').hide();
                                                                        $('.settings_plan_info').show();
                                                                    }

                                                                }
                                                            });  
                                                        }   

                                                    }
                                               },'input[data-settings="subscription_reactivate"]');


 $('#popupOverlay').fadeIn('slow');
                $('.loaderPageGraphic').fadeOut("slow");


                                    },
                                    error: function(){
                                        console.log('send data fail', data);
                                        //console.log(event, XMLHttpRequest, ajaxOptions, thrownError)
                                    }

                                 });
             }

            // animation for settings
               $('body')

              .on({
                    'click touchstart' :function(e){
                        e.preventDefault();
                            $('div[data-settings="cancel_container"]').show();
                            $('div[data-settings="reactivate_container"]').hide();
                            $('.settings_plan_info').hide();
                    }
              },'.settingswrap .submitCancel')

              .on({
                    'click touchstart' :function(e){
                        e.preventDefault();
                            $('div[data-settings="cancel_container"]').hide();
                            $('div[data-settings="reactivate_container"]').show();
                            $('.settings_plan_info').hide();
                    }
              },'.settingswrap .submitReactivate')

              .on({
                  'click touchstart':function(e){
                        e.preventDefault();
                        var invoice_number = $('td[data-settings="billing_number"]').text();

                        window.open('/bigpicture_recurly/settingsPageBackend/invoice_pdf.php?invoice_number=' + invoice_number);

                  }

              },'.invoice_pdf')

              .on({
                    'click touchstart':function(event){
                       event.preventDefault();
                       var  $form = $(this).parent().parent(),
                            $btn  = $(this);   
                        $form.validate({
                            errorClass: 'errorBorder',
                            rules:{
                                first_name              : 'required',
                                last_name               : 'required',
                                credit_card_numaber     :   {
                                        required    :   true,
                                        minlength:15
                                        //creditcard  :   true
                                },

                                ccode   :   {
                                        required:true,
                                        minlength:3
                                    },
                                zip     :   {
                                        required:true,
                                        //zipcode:true,
                                        //number: true
                                }

                            },
                            errorPlacement: function() {
                                     return true;
                                },
                            messages: {
                                    first_name          :   "Please enter your firstname.",
                                    last_name           :   "Please enter your lastname",
                                    credit_card_numaber :   "Please enter a valid credit card number.",   
                                    ccode               :   "Please enter a valid security code.",
                                    zip                 :   "Please enter a valid postal code."
                                }


                        });
                         if($form.valid()){
                            if(!navigator.onLine){
                                errorWarning('.internetRequest', internetRequestInfo);
                            } 
                            else{
                                $btn.prop('disabled', true);
                                $('#errors').text('');
                                $('input').removeClass('error');

                                // Disable the submit button

                                var form = $form;

                                // Now we call recurly.token with the form. It goes to Recurly servers
                                // to tokenize the credit card information, then injects the token into the
                                // data-recurly="token" field above
                                recurly.token(form, function (err, token) {
                                  // send any errors to the error function below
                                  if ($form, err) {

                                      error ($form, err);
                                      console.log(err);
                                  }
                                  else {
                                      var data = {
                                            'email'         :   $.cookie('email'), 
                                            'recurly-token' :   $('input[data-recurly="token"]').val()
                                       };

                                         $.ajax({
                                               type: "POST",
                                               dataType: "json",
                                               data:data,
                                               url:'/bigpicture_recurly/settingsPageBackend/update_billing_info.php', //Relative or absolute path to response.php file
                                               success 	: function(errorcode) {
                                                        $btn.prop('disabled', false);
                                                        if (errorcode.errorEmail) { 
                                                             systemAlert('.settings_error_alert', ajax_response.errorEmail);

                                                        } 

                                                        if (errorcode.errorBilling) { 
                                                                console.log(errorcode.errorBilling);
                                                        }
                                                        if (errorcode.errorOther) { 
                                                                systemAlert('.settings_error_alert', ajax_response.errorOther);
                                                                console.log('Error about System'); //Throw relevant error

                                                        }
                                                        if(errorcode.errorMessage){
                                                            console.log('Recurly error:' + errorcode.errorMessage);
                                                         }
                                                        if(errorcode.errorEmail === false && errorcode.errorBilling === false){
                                                            console.log('Everthing is ok');
                                                            var firstname                =   $('input[data-recurly="first_name"]').val(),
                                                                lastname                 =   $('input[data-recurly="last_name"]').val(),
                                                                billing_credit_card      =   $('input[data-recurly="number"]').val(),
                                                                billing_credit_card_last =   billing_credit_card.substr(billing_credit_card.length - 4),
                                                                credit_card_type         =   $.payment.cardType($('input[data-recurly="number"]').val());
                                                                //billing_exp_month        =   $('#billing_info_month input[selected]').val(),
                                                               // billing_exp_year         =   $('#billing_info_year input[selected]').val();

                                                             //if(billing_exp_month.length < 2){
                                                             //    return '0' + billing_exp_month;
                                                             // }  

                                                             $('span[data-settings="subscription_first_name"]').text(firstname);
                                                             $('span[data-settings="subscription_last_name"]').text(lastname);
                                                             $('span[data-settings="billing_credit_card_last"]').text(billing_credit_card_last);
                                                             $('span[data-settings="billing_credit_card_type"]').text(credit_card_type);
                                                            
//                                                            $('span[data-settings="billing_exp_month"]').text(billing_exp_month);
//                                                             $('span[data-settings="billing_exp_year"]').text(billing_exp_year);
//                                                            
                                                             $('.billing_info_form,.settings_bill_info').toggle();
                                                             systemAlert('.settings_success_alert', ajax_response.successfully_updated_billing_info);

                                                         }
                                                    },
                                                    error:function(data){
                                                        $btn.prop('disabled', false);
                                                        console.log('fail info:' + data );
                                                    }
                                         });
                                  }




                                });
                            } 

                         }
                    }

               },'.update_billing_info_btn')


              .on({
                    'touchstart click':function(e){
                       
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).parent('p').siblings('form').find('.settings_input').val('');
                        $('.limiInfo').fadeOut(200);
                        $('.change_password_wrap').stop().fadeOut(200, function(){
                            $('.change_email_wrap input').removeClass('errorBorder');
                            $('.change_email_wrap').stop().fadeToggle(200);
                        });

                    }
              },'.change_email,.cancel_change_email')

              .on({
                    'touchstart click':function(e){
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).parent('p').siblings('form').find('.settings_input').val('');
                        $('.change_email_wrap').stop().fadeOut(200, function(){
                            $('.change_password_wrap input').removeClass('errorBorder');
                            $('.change_password_wrap').stop().fadeToggle(200, function(){
                                $('.limiInfo').fadeToggle(200);
                            });
                        });




                    }
              },'.change_password,.cancel_change_password')

              .on({
                'touchstart click':function(e){
                    e.stopPropagation();
                    e.preventDefault();
                    var $settings_bill_info = $(this).parent().siblings().find('.settings_bill_info'),
                        $billing_info_form  = $(this).parent().siblings().find('.billing_info_form');
                        $settings_bill_info.hide();
                        $billing_info_form.show();

                }
              },'.settings_billing_info_edit')

              .on({
                    'touchstart click':function(e){
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).parent().parent().parent().hide().find('input').removeClass('errorBorder').end().siblings().show();  
                    }
              },'.settings_cancel_edit')
                //submit change email
                .on({
                   'click touchstart':function(event){

                       event.preventDefault(); 
                       var  $btn = $(this),
                            $form = $(this).parent().parent();

                       $form.validate({
                           errorClass: 'errorBorder',
                            rules:{
                                new_email: {
                                        required: true,
                                        email: true
                                    },
                                confirm_email:{
                                    equalTo: "#new_email"
                                    }
                            },

                            errorPlacement: function() {
                                 return true;
                            },
                            messages: {
                                change_password_old: {
                                    required: "Please provide an email"
                                },
                                change_password_new: {
                                    required: "Your email is not match"
                                }


                           } 

                       });

                       if($form.valid()){
                           if(!navigator.onLine){
                                errorWarning('.internetRequest', internetRequestInfo);
                            } 
                            else{
                                var data = {
                                    'old_email':$.cookie('email'),
                                    'new_email'  :   $('input[name="new_email"]',$form).val()
                                };
                                console.log(data);
                                $btn.prop('disabled', true);
                                $.ajax({
                                    type:'POST',
                                    data:data,
                                    dataType:'json',
                                    url:'/bigpicture_recurly/settingsPageBackend/update_email.php',
                                    success: function(errorcode){
                                        console.log(errorcode);
                                         $btn.prop('disabled', false);
                                        if(errorcode.errorEmail ){
                                            console.log('means old email does not exist');
                                        }
                                        else if(errorcode.errorExistingNewEmail){
                                            systemAlert('.settings_error_alert', ajax_response.errorExistingNewEmail);
                                            console.log('new email already exists');

                                        }
                                        else if(errorcode.errorOther){
                                            systemAlert('.settings_error_alert', ajax_response.errorOther);
                                            //$errorLogin('.change_email_wrap', ajax_response.errorOther);
                                            console.log('system error 1');
                                        }
                                        else if(errorcode.errorMessage){
                                            console.log('system error');
                                        }
                                        else{
                                            
                                             systemAlert('.settings_success_alert', ajax_response.successfully_updated_email);
                                            // $.cookie('email', data.new_email, {expires: $.cookie('expireTime'), path: '/'});
                                             //$('.change_password_wrap').fadeIn(300); 
                                             $('p[data-settings="email"]').text($.cookie('email'));
                                             $('.change_password_wrap').fadeOut(300).clearFormvalues();     
                                        }
                                    },
                                    error:function(errocode){
                                         $btn.prop('disabled', false);
                                        console.log(errocode);
                                    }

                                });
                            }
                        }
                    }
                },'.change-email-btn')   

                   //submit change password
              .on({
                       'click touchstart':function(event){
                           console.log('test');
                           event.preventDefault(); 
                           var $form = $(this).parent().parent();
                           $form.validate({
                                errorClass: 'errorBorder', 
                                rules:{
                                    change_password_old: {
                                            required: true,
                                            minlength: 6
                                        },
                                    change_password_new:{
                                        required: true,
                                            minlength: 6
                                        }    
                                },
                                errorPlacement: function() {
                                     return true;
                                },
                                messages: {
                                    change_password_old: {
                                        required: "Please provide a password",
                                        minlength: "Your password must be at least 6 characters long"
                                    },
                                    change_password_new: {
                                        required: "Please provide a password",
                                        minlength: "Your password must be at least 6 characters long"
                                    }


                               } 



                           });

                           if($form.valid()){
                               if(!navigator.onLine){
                                    errorWarning('.internetRequest', internetRequestInfo);
                                } 
                                else{
                                    console.log(123);
                                    var data = {
                                        'email':$.cookie('email'),
                                        'old_password'  :   $('input[name="change_password_old"]',$form).val(),
                                        'new_password'  :   $('input[name="change_password_new"]',$form).val()

                                   };
                                    console.log(data);
                                    $.ajax({
                                        type:'POST',
                                        data:data,
                                        dataType:'json',
                                        url:'/bigpicture_recurly/settingsPageBackend/update_password.php',
                                        success: function(errocode){
                                            console.log(errocode);

                                            if(errocode.errorLogin){
                                                systemAlert('.settings_error_alert', ajax_response.passwordNotMatch);
                                                console.log('old password and email don’t match');
                                            }

                                            else if(errocode.errorOther){
                                                console.log('system error');
                                                $errorLogin('.change_password_wrap', ajax_response.errorOther);
                                            }
                                            else{
                                                console.log('send');
                                                 systemAlert('.settings_success_alert', ajax_response.successfully_updated_password);
                                                $('.limiInfo').fadeOut(200);
                                               // $('.change_password_wrap').fadeIn(300); 
                                                $('.change_password_wrap').fadeOut(300).clearFormvalues();     
                                            }
                                        },
                                        error:function(errocode){
                                            console.log(errocode);
                                        }

                                    });
                                }

                            }


                       }

                   },'.change-password-btn')
                   .on({
                        'click touchstart': function(e){
            //                e.preventDefault(); //open new window on touch screen
                            e.stopPropagation();
                            console.log(e.type);
                            window.open('/forgot_password.html', '_blank');
                        }
                   },'.forgot_password')

              .on({
                    'touchstart click':function(e){
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).parent().parent().parent().fadeOut(200);
                        $('.billing_info_form').fadeOut(200);
                        $('.settings_bill_info').fadeIn(200);
                        $('div[data-settings="cancel_container"]').hide();
                        $('div[data-settings="reactivate_container"]').hide();
                        $('.settings_plan_info').show();
                     }
               },'.settings_wrap_close')

              .on({
                   'touchstart click':function(e){
                      
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).parent().hide();
                        $('div[data-settings="cancel_container"]').hide();
                        $('.buttonholder').show();
                        $('.settings_plan_info').show();
                   }
                },'.notNow');


               $(document).on({

                'touchstart click':function(e){
                    e.stopPropagation();
                    if(!$(e.target).closest('.settings_setting_container').length && !$(e.target).closest('.historicalInfo').length && $('.settings_setting_container').css('display') === 'block'){
                        var speedVal   = (localStorage.getItem('deviceType') === 'MOBILE') ? 0 : 300;
                        $('.settings_setting_wrap').stop().fadeOut(speedVal);
                        $('.billing_info_form').stop().fadeOut(speedVal);
                        $('.settings_bill_info').stop().fadeIn(speedVal); 
                        $('.settings_plan_info').show(speedVal);
                        $('div[data-settings="cancel_container"]').hide(speedVal);
                        $('div[data-settings="reactivate_container"]').hide(speedVal);
                    }
                }

            });
   
     
 });

