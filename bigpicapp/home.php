<?php
    //die('hf');
    ini_set('display_errors', 1);
    error_reporting(0);
    $app_version = "2.2.20";
    $host = $_SERVER['HTTP_HOST'];
    // session_start();
    // echo "<pre>";
    // print_r($_SESSION);
    // echo "----------------------";
    // print_r($_COOKIE);
    $email = $_COOKIE['email'];
    $old_image_val = '';
    // die;
    require '../bigpicture_integrations/bigpicture_database/open_db_connection.php'; 
    require '../bigpicture_integrations/bigpicture_database/get_user_logo_image.php';
    // echo $old_image;
    $old_image_val = $old_image['logo_image'];
    // print_r($old_image);
    
    require '../bigpicture_integrations/bigpicture_database/close_db_connection.php'; 


    if ($host == 'test.bigpicapp.co' || $host == 'www.test.bigpicapp.co'){
        $regionCode = "us";
    }else{
        if (!isset($_COOKIE["plancode"]) || !isset($_COOKIE["email"])){
            header("Location: /");
        }
        $regionCode  = strtolower($_COOKIE["regioncode"]);
    }

    // $requirejs = "";

    // if ($host == 'bigpicapp.co' || $host == 'www.bigpicapp.co' ||
    //     $host == 'test.bigpicapp.co' || $host == 'www.test.bigpicapp.co' ||
    //     $host == 'internal-testing.bigpicapp.co' || $host == 'www.internal-testing.bigpicapp.co'){
    //         $jsBigPictureCode = "<script src=" . "bigpicture." . $regionCode . ".js?v" . $app_version . "></script>";
    //         $jsBigPictureData = "<script src=" . "bigpicture." . $regionCode  . ".data.js?v" . $app_version . "></script>";
    //         // $requirejs="<script src='/lib/require.js'></script>";
    // }
// $requirejs="<script src='/lib/require.js'></script>";
?>
       

<!DOCTYPE html>


<html prefix="og: http://ogp.me/ns#">

    
    <script src="vendors/js/jquery-1.11.3.min.js?<?php echo time();?>"></script>    
    <script src="vendors/js/jquery.cookie.min.js?<?php echo time();?>"></script>
    
    <script>
        
        //$(window).bind("beforeunload",function(event) {
        //    return "You have some unsaved changes";
        //});
         $(window).bind('beforeunload', function() {
             if (!navigator.onLine) { 
                return "By refreshing your browser, you will not be able to access the app until you reconnect to the internet.";
             }
         });
        
    </script>
    
    <?php

        $host = $_SERVER['HTTP_HOST'];

        if ($host == 'localhost:8888' ||
            $host == 'bigpicapp.co' || $host == 'www.bigpicapp.co' ||
            $host == 'internal-testing.bigpicapp.co' || $host == 'www.internal-testing.bigpicapp.co'){
    ?>        

            <script>
                if (document.cookie.indexOf("plancode") < 0){
                    window.location.replace("/");
                }
                if(document.cookie.indexOf("hardReload") >= 0){
                    localStorage.clear();
                    $.cookie("hardReload", null, { path: '/' });
                    window.location.replace("/bigpicapp/home?" + $.cookie("regioncode").toLowerCase());

                }
                // $.cookie("app_version", "<?php echo $app_version;?>" , { path: '/' });
                
            </script>

    <?php } ?>
    

    
    <head>
        <title>Withdrawal Program - The Big Picture</title>
        <meta name="keywords" content="" />
		<meta name="description" content="" />

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link rel="shortcut icon" href="PUT YOUR FAVICON HERE">-->
         
        <!-- Google Web Font Embed -->
         
        <!-- USE THIS IF YOU PREFER GOOGLE REMOTE CDN -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> -->
        
    	<!-- **OR** THIS TO USE LOCAL FONTS -->
    	<style type='text/css'>
    	@font-face {
			font-family: 'Roboto';
			src: url('fonts/roboto-regular-webfont.woff2') format('woff2'),
				 url('fonts/roboto-regular-webfont.woff') format('woff');
			font-weight: normal;
			font-style: normal;
		} 

		/* New Changes added to home page end*/

		 
    	</style>
    	
    	<!-- END LOCAL FONT -->
    	<!-- ============Start code edited by Anuj================== */  -->
		<link rel="stylesheet" href="css/custom_style.css?v<?php echo $app_version;?>">
	 
    		<?php if(!empty($old_image_val)){ ?> 
    			<style>
    			.logo-preview-img{
    				display: inline-block;
    			}

    			</style>
    		<?php }
    	?>
    	<!--  /* ============End code edited by Anuj================== */  -->
		<link rel="stylesheet" href="css/style.css?v<?php echo $app_version;?>">
		 

		<script src="vendors/js/handlebars.min.js"></script>
        <script src="vendors/js/d3.v3.min.js"></script>
        <script src="vendors/js/jquery-1.11.3.min.js"></script>
        <script src="vendors/js/jquery.cookie.min.js"></script>
        <script src="vendors/js/jspdf.min.js"></script>
        <script src="vendors/js/d3tip.js"></script>
        <!--  /* ============Start code edited by Anuj================== */ -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
         <script src="vendors/js/date.format.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
         <!--  /* ============End code edited by Anuj================== */ -->
        
        
    </head>
    
<body>
<!--  /* ============Start code edited by Anuj================== */ -->
<div class="loader-panel" style="display: none;">
	<div class="loader"></div>
</div>
<div class="container">
  
  <!-- Trigger the modal with a button -->
  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
 -->
  <!-- Modal -->
  <div class="modal fade" id="download-pdfs" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Download Withdrawal Program Pdf</h4>
        </div>
        <div class="modal-body">
         <p id="error_mail"></p>
        <div class="input-field rs_img">
   <!--      <p>Results will be based on rolling periods using monthly-frequency historical data.</p> -->
      	  <p>Results will be sent on the below mentioned email address.</p>
          <input type="email" name="forward_email" id="forward_email" class="form-control" value="" placeholder="Email Address" />
         
         <button type="button" class="createPdf btn btn-primary" id="createPdf">Submit</button>
         </div>
        </div>
        
      </div>
      
    </div>
  </div>
  
</div>

<!--
image upload start here -->

<div class="container">
  
  
  <div class="modal fade" id="upload_image" role="dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" id="cancel" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Logo image</h4>
        </div>
        <div class="modal-body">
        <p id="error_img"></p>
        <div class="form-group rs_img">
            <label for="recipient-name" class="col-form-label">Image Upload</label>
            <input type="file" class="form-control" name="file_upload" id="file_upload" />
          </div>
        
          <div class="form-group">
          <button type="button" name="submit_btn"  class="btn btn-primary" id="logo_submit_btn"/>Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>
<!-- image upload end here
 /* ============End code edited by Anuj================== */
-->




<!-- 
this is the TOP NAVIGATION
	it has a min-width and is maxed-out at 90%
	it's in a 100% container
============================================	
 -->

<div class=header>

	<div class=nav>
		 <div class="header-items nav"> 
		<a href='' class='is_active with'>Withdrawal Program</a>
        <a href='' class='growthOf1k'>Growth of 1k</a>
		<a href='' class='key'>Key Principles</a>
		
		<div class='logowrap'><img src="images/bigpicture_logo.svg?v<?php echo $app_version; ?>" alt='The Big Picture' class='logotitle'></div>

		<div class=extranav>

            <a target="_blank" href='https://youtu.be/DTQfapmtI_E' data-pop=''>Video Tutorial</a>
            <!-- <a href='' class=popper data-pop='upgradeplanwrap'>Upgrade</a>
			<a href='' class=popper data-pop='settingswrap'>Settings</a> -->

			<a href='' class='popper logout-link' data-pop='logoutwrap'>Logout</a>

			<span class="logo-preview-img ">
			<img id="logo_changed" src="images/uploads/<?php echo $old_image_val; ?>" alt='Logo-preview' class='logo-preview'>
				<div class="edit_icon" data-toggle="modal" id="" data-target="#upload_image">
				<img src="images/edit-b.png" alt='Logo-edit' class='logo-edit'></div>
			</span>
		<!--			<a href='' class='morebutton'>More</a>-->
			</div>
			</div> 
	 <!-- /* ============End code edited by Anuj================== */ -->
	  		<?php if(empty($old_image_val)){ ?> 
	  			<button type="button" data-toggle="modal" id="upload_image1" data-target="#upload_image"><img src="images/upload_img.png" alt='Download' class='d-pdf' title="Upload Logo"></button> 
	  		<?php } ?>
	 		
	 	 
		

		<button type="button" data-toggle="modal" id="createPdf_link" data-target="#download-pdfs"><img src="images/download.png" alt='Download' class='d-pdf' title="Download Pdf"></button>
		<input type="hidden" name="is_pdf" id="is_pdf" value="0">
		<!-- <button type="button" class="createPdf" id="createPdf">Pdf</button> -->
		 <!-- /* ============End code edited by Anuj================== */ -->
	
	<!-- 
	MORE MENU -->
<!--
	<div class='more'>
		<div class='inside'>
			<div class="rewards"><a href=''>Referral Rewards</a></div>
			<div class="share"><a href='' class='popper' data-pop='sharewrap'>Share</a></div>
			<div class="sources"><a href=''>Sources</a></div>
		</div>
	</div>
-->
	<!-- end MORE MENU -->
	
	
	
	</div><!-- end .nav -->


</div><!-- end .header -->






<!-- 
============================================	
end TOP NAVIGATION
-->







<!-- 
this is the MAIN CONTENT (everything but the top nav)
	it has a min-width and is maxed-out at 90%
============================================	
 -->
<div class='main'>
	
		<!--
		animation for PAGE load/refresh
		]]]]]]]]]]]]]]]]]]]]]
		-->
		<div class="loaderPageGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>
		<!--
		END animation for PAGE load/refresh
		]]]]]]]]]]]]]]]]]]]]]
		-->

	<!-- 
	here is the SIDEBAR = it has a fixed with
	============================================	
	-->
	
	<div class='sidebar'>
		

	</div><!-- end .sidebar -->
	<!-- 
	============================================
	end SIDEBAR	
	-->
	
	
	

	<!-- these are the WIDGETS
	+++++++++++++++++++++++ -->
	
	<div class='help popper' data-pop='helpwrap'>
		<svg fill="#bbbbbb" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
			<path d="M0 0h24v24H0z" fill="none"/>
			<path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"/> 
		</svg>
	</div><!-- end .help -->
	
	<!-- widgets TK --> 
	<div class='print'>
    <svg height=24px id=Layer_1 style="enable-background:new 0 0 24 24"version=1.1 viewBox="0 0 24 24"width=24px x=0px xml:space=preserve xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink y=0px><style>.st0{fill:#bdbdbd}.st1{fill:none}</style><g><path class=st0 d="M19,8H5c-1.7,0-3,1.3-3,3v6h4v4h12v-4h4v-6C22,9.3,20.7,8,19,8z M16,19H8v-5h8V19z M19,12c-0.5,0-1-0.4-1-1
		s0.5-1,1-1s1,0.4,1,1S19.5,12,19,12z M18,3H6v4h12V3z"/></g><path class=st1 d=M0,0h24v24H0V0z /></svg>
    </div><!-- end .print -->
	<!-- <div class='widget'></div> --><!-- end .widget -->
	
    
    <div class='monthly_data_label'>Results based on rolling periods using monthly-frequency historical data.</div>
    <div class='risk_label'>* Volatility (standard deviation) of annual portfolio returns.</div>

	<!-- END WIDGETS
	+++++++++++++++++++++++ -->
   

	<!-- 
	here are all the PAGES with the charts
	============================================	
	-->	
	
	<div class='pages'>

		<!--
		transition animation
		]]]]]]]]]]]]]]]]]]]]]
		-->
		<div class="loaderGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>  
		<!--
		END transition animation
		]]]]]]]]]]]]]]]]]]]]]
		-->

		<!-- 
		PAGESETS are the set of pages that get show in a MODE
		============================================	
		-->
		<div id="new_data">
		<div class='pageset retirement_pages is_active graph-area-container'> </div>
		</div>
		<!-- <div class='pageset graph-area-container1'> </div> -->

		<!-- end .retirement_pages -->

        
        
		<!-- 
		PAGESETS are the set of pages that get show in a MODE
		============================================	
		-->
		<div class='pageset _pages keypage'><!--  "_pages" because there is no mode so only one pageset -->
			
			<!-- ONE SINGLE PAGE: hidden unless is has the "is_active" class
			\\\\\\\\\\\\\\\\\\\\\\\\\\\\
			 -->
			<div class='onepage is_active'>	
				
				<!-- page text is the stuff before the chart -->
				<div class='pagetext pdftext'>
			
					
					<h1>A Collection of Classic Investment Illustrations</h1>
			
					<h2>Download a PDF from the menu below</h2>
			
				</div><!-- end .pagetext -->
			
			
				<!-- this is where the pdfs go -->
				<div class='chartspace pdfs'>
                    
                    
                    <?php if ($regionCode=="us"){ ?>
                    
                        <div>                                      
                        <a href='' target='_blank'>
                        <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
                            <path d="M0 0h24v24H0z" fill="none"/>
                        </svg>
                        Power of Compounding I</a>
                        </div>  


                        <div> 
                        <a href='' target='_blank'> 
                        <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
                            <path d="M0 0h24v24H0z" fill="none"/>
                        </svg>
                        Power of Compounding II</a>
                        </div>
                    
                    <?php } ?>
                    
					<div>
					<a href='' target='_blank'>  
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Bulls and Bears</a>
					</div>


					<div>  
					<a href='' target='_blank'>
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Chasing Performance</a>
					</div>

					<div> 
					<a href='' target='_blank'> 
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Time and Risk</a>
					</div>

				
					<div>  
					<a href='' target='_blank'>
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Downturns and Recoveries</a>
					</div>
                    
                    
                    <div> 
					<a href='' target='_blank'> 
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Diversification</a>
					</div>

					<div>
					<a href='' target='_blank'> 
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Rebalancing Act</a>
					</div>

					<div>  
					<a href='' target='_blank'>
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
						<path d="M0 0h24v24H0z" fill="none"/>
					</svg>
					Stocks vs. Portfolio</a>
					</div>
                        

				</div>		
					
			</div><!-- end .onepage -->

		</div><!-- end ._pages -->
		
        
        
        <div class='pageset life-index-container'>
            
        </div><!-- end .retirement_pages -->
        
	</div><!-- end .pages -->

</div><!-- end .main -->
<!-- 
============================================	
end MAIN CONTENT
-->	





<!--
POPUPS 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->
<div id="popupOverlay"> 
    <div class='portfoliowrapper popup onepop portfolio_wrap portfolio_container'></div> 
    
    <div class='ratiowarpper popup onepop extra_info_wrap' id='extra_info'> </div><!-- end .ratiowarpper.popup -->

	<div class='settingswrap popup onepop settings_setting_container'> 

			
 
	</div>

    <div class='logoutwrap popup onepop'>

            <p>If you log out you will not be able to open the Big Picture offline.</p><br><br>

            <div class='center'><span class="btn logout-got-it" style='position: static;'>Got it</span><br><br>
            <span><label><input type='checkbox' class='logout-checkbox'> Don't show again</label></span>
            </div>


    </div>
    
    <div class='sharewrap popup onepop'>

            <p>This is the "SHARE" popup This is the "SHARE" popup This is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popupThis is the "SHARE" popup This is the "SHARE" popup</p>



    </div><!-- end .ratiowarpper.popup -->
<!-- end LOGOUT popup -->



<!-- HELP popup -->
    <div class='helpwrap popup onepop'>

        <h1 style="text-align:center;margin-bottom:15px">Key Definitions</h1>
        <br>
        <div id="help_inRetirement">
            {{#each VARIABLES}}
                <div>
                    <span>{{{this.name}}}</span>
                    <span style='color: #757575;line-height: 1.3em;'>{{this.definition}}</span>
                </div>
            <br>
            {{/each}}

            {{#each FOOTER}}
                <div class='center' style='margin-top: 20px;'>
                    <span style='font-style: italic;color: #757575;'>{{{this}}}</span>
                </div>
            {{/each}}

        </div>  
        
        <div class="sources" style="float:right"><a href='https://www.bigpicapp.co/terms/Financial-Data.html' target="_blank">Sources</a>               </div>

    </div>
    
    
    <div class='upgradeplanwrap popup onepop plans_manager_container'></div> 


</div>

<!--
end POPUPS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->


    
    <script src="vendors/js/saveSvgAsPng.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		// alert();
    	});
    </script>
    
    
    <?php //echo $requirejs;?>
    
    <?php //echo $jsBigPictureData;?>

    <?php //echo $jsBigPictureCode;?>
    
    <script data-main="main" src="/lib/require.js"></script>

    
</body>
</html>
