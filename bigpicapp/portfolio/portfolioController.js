function getRegionCode() {
    var value = "; " + document.cookie;
    var parts = value.split("; regioncode=");
    if (parts.length == 1) return "US";
    if (parts.length == 2) return parts.pop().split(";").shift();
}//TEMP;

define (['portfolio/portfolioService',  
         'userInput/inputController',
         'postal',
         'portfolio/translation/'+getRegionCode()+'/en',
         'portfolio/translation/en',
         'portfolio/settings/'+getRegionCode()+'/portfolioConfig',
         'portfolio/synchronizer'], 
            
    function(portfolioService, 
             inputController, 
             postal,     
             assetTranslation, 
             portfolioTranslation, 
             portfolioSettings,
             synchronizer){
   
        var planCode = $.cookie("plancode");
//                         if ($.cookie("plancode") == 'big_picture_basic'){
            
        postal.channel().subscribe("mainInputView.portfolio.selected", function(index){
            calculatePortfolioByIndex(index);
            requirejs(["portfolio/portfolioView"], function(portfolioView){
                var portfolio = getSavedPortfolioByIndex(index);
                var allocationMap = getPortfolioAllocation(getSavedPortfolioByIndex(index));
                portfolioView.setPortfolioAllocation(allocationMap, index);
                portfolioView.checkPortfolioByIndex(index);
            });
        });        

        function calculateAllSavedPortfolioSuccessRate(){
        
            var successrateList = [];
            var portfolioList = getSavedPortfolioList();
            var input = inputController.getInput();
           
            $.each(portfolioList, function(index, portfolio){
                portfolioService.calculatePortfolio(input.getTer(),
                                                    input.isContributionReal(),
                                                    input.getRebalancingYears(),
                                                    getPortfolioAllocation(portfolio));
                var successRate = portfolioService.calculateSuccessRate(input.getInitialInvestment(),
                                                                            input.getInvestmentHorizon(),
                                                                            input.getSavingsGoal(),
                                                                            input.getContributionAmount());
                var portfolioRisk = portfolioService.calculatePortfolioRisk();
                       
                successrateList.push({portfolioName: portfolio.NAME, successRate:successRate, portfolioRisk:portfolioRisk});
            });
            var currentPortfolioIndex = inputController.getSelectedPortfolioIndex();
            var portfolio = getSavedPortfolioByIndex(currentPortfolioIndex);
            var input = inputController.getInput();
            var allocationMap = getPortfolioAllocation(portfolio);
            portfolioService.calculatePortfolio(input.getTer(),
                                                input.isContributionReal(),
                                                input.getRebalancingYears(),
                                                allocationMap);
            return successrateList;
        }
          
    
        function portfolioSelected(index){
            var portfolio = getSavedPortfolioByIndex(index);
            var allocationMap = getPortfolioAllocation(portfolio);
            require(["portfolio/portfolioView"], function(portfolioView){
                portfolioView.showPortfolioData(allocationMap, index, portfolio.NAME);
            });
        }
    
        postal.channel().subscribe("portfolio.closed", function(){
            render(portfolioTranslation, assetTranslation, getLocallySavedPortfolioList());
        });   
    
        function render(portfolioTranslation, assetTranslation, portfolioList){
            var currentPortfolioIndex = inputController.getSelectedPortfolioIndex();
            var context = jQuery.extend(true, {}, portfolioList[currentPortfolioIndex]);
            var assetInPlan=false;
            $.each(context.ALLOCATION_MAP, function(index, asset){
                asset.NAME =  assetTranslation[asset.CODE];
                if (planCode=="big_picture_basic"){
                    if (asset.CODE=="US_5" || asset.CODE=="US_6"){
                        assetInPlan = true;
                    }
                }else{ 
                    assetInPlan = true;
                }
                asset.IN_PLAN = assetInPlan;
            });
            context.TRANSLATION = portfolioTranslation;            
            context.PORTFOLIO_LIST = portfolioList;
            requirejs(["portfolio/portfolioView"], function(portfolioView){ 
                portfolioView.render(context, currentPortfolioIndex);
            });                      
        }
    

        postal.channel().subscribe("portfolioController.getSelectedNamedAllocation", function(callback){
            callback(getSelectedNamedAllocation());
        });
    
        function formatPercentage(value, lang){
            return value.toLocaleString('en', {style: 'percent', minimumFractionDigits:0});
        }
    
        function getSelectedNamedAllocation(){     
            var portfolio  = getLocallySavedPortfolioList()[inputController.getSelectedPortfolioIndex()];
            var list = [];
            $.each(portfolio.ALLOCATION_MAP, function(index, asset){
                list.push({name: abbreviate(getAssetNameByCode(asset.CODE)), allocation: formatPercentage(asset.VALUE/100)});
            });
            return list;
        }
 

        function calculatePortfolioByIndex(index){
            var portfolio = getSavedPortfolioByIndex(index);
            var input = inputController.getInput();
            var allocationMap = getPortfolioAllocation(portfolio);
            portfolioService.calculatePortfolio(input.getTer(),
                                                input.isContributionReal(),
                                                input.getRebalancingYears(),
                                                allocationMap);
            publishPortfolioCalculated();
        }
    
    
    
    
        postal.channel().subscribe("portfolioController.calculatePortfolioByIndex", function(index){
            var portfolio = getSavedPortfolioByIndex(index);
            var input = inputController.getInput();
            var allocationMap = getPortfolioAllocation(portfolio);
            portfolioService.calculatePortfolio(input.getTer(),
                                                input.isContributionReal(),
                                                input.getRebalancingYears(),
                                                allocationMap);
           
            
             
        });
    
        function getRollingPeriodTrajectory(rollingPeriodIndex){
            var portfolio = getSavedPortfolioByIndex(inputController.getSelectedPortfolioIndex());
            var input = inputController.getInput();
            var dataList = portfolioService.calculateRollingPeriodTrajectory(input.getTer(),
                                                                            input.isContributionReal(),
                                                                            input.getRebalancingYears(),
                                                                            getPortfolioAllocation(portfolio),
                                                                            input.getInvestmentHorizon(),
                                                                            input.getInitialInvestment(),
                                                                            input.getContributionAmount(),
                                                                            rollingPeriodIndex);
            return dataList;
             
        }
        postal.channel().subscribe("inputController.more.inputs.changed", function(input){
            calculatePortfolioByIndex(inputController.getSelectedPortfolioIndex());
        });
    
        postal.channel().subscribe("portfolioController.getSavedPortfolioNameList", function(callback){
            var list = getLocallySavedPortfolioList();
            var nameList = [];
            $.each(list, function(index, portfolio){
                nameList.push(portfolio.NAME);
            });
            callback(nameList);
        });
    
        postal.channel().subscribe("portfolioService.data.loaded", function(){   
            postal.channel().publish("portfolio.module.loaded");
        });      


        function loadModule(regionCode){
            
            //var defaultPortfolioList = portfolioSettings.DEFAULT_PORTFOLIO_LIST;
            postal.channel().subscribe("portfolio.synchronizer.config.retrieved", function(retrieved){
                if (retrieved){
                    portfolioConfig = retrieved;
                }else {
                     portfolioConfig = getLocallySavedPortfolioList();
                }
                savePortfolioList(portfolioConfig);
                render(portfolioTranslation, assetTranslation, getLocallySavedPortfolioList());
                portfolioService.loadData(portfolioConfig[0], regionCode);
            });
            synchronizer.getSavedPortfolio();
        };

        function saveButtonPressed(input){
            var index = input.index;
            var list = getLocallySavedPortfolioList();
            var portfolio =list[index];
            portfolio.NAME = input.name;
            portfolio.ALLOCATION_MAP = input.map;
            list[index] = portfolio;
            savePortfolioList(list);  
            if (index == inputController.getSelectedPortfolioIndex()){
                calculatePortfolioByIndex(index);
            }
            publishPortfolioChanged();
        }
        
        function getPortfolioNameByIndex(index){
            return getLocallySavedPortfolioList()[index].NAME;
        }

        function getLocallySavedPortfolioList(){
            return JSON.parse(localStorage.getItem("portfolioList")) || portfolioSettings.DEFAULT_PORTFOLIO_LIST;
        }    
       
        function savePortfolioList(list){
            localStorage.setItem("portfolioList", JSON.stringify(list));
        } 
    
        function getPortfolioAllocation(portfolio){
            var allocationMap = getAllocationMap(portfolio);
            var map = {};
            $.each(allocationMap, function(index, assetObject){
                var allocation = assetObject.VALUE;
                if (allocation === 0) return;
                map[assetObject.CODE] = allocation / 100;
            });
            return map;
        }     
   
    
        function getSavedPortfolioList(){
            return getLocallySavedPortfolioList();
        }
    
        function getSavedPortfolioByIndex(index){
            return getLocallySavedPortfolioList()[index];
        }
     
        function getAllocationMap(portfolio){
            return portfolio.ALLOCATION_MAP;
        } 
    
    
        function getAssetNameByCode(assetCode){
            return assetTranslation[assetCode];            
        }
        
    
        function abbreviate(name){//TEMP;
            var fiveGovernment = "5-Year Government";
            var tenGovernment = "10-Year Government";
            var canadian = "Canadian";
            var international = "International";
            var government = "Government";
            
            if (name.match(fiveGovernment)){
               name = name.replace(fiveGovernment, "5-Yr. Gov.");
            }
            if (name.match(tenGovernment)){
               name = name.replace(tenGovernment, "10-Yr. Gov.");
            }
            if (name.match(canadian)){
               name = name.replace(canadian, "Cdn.");
            }
            if (name.match(international)){
               name = name.replace(international, "Int'l.");
            }
            if (name.match(government)){
               name = name.replace(government, "Gov.");
            }
            return name;
        }
    
        postal.channel().subscribe("portfolioController.get.data.map", function(callback){
            callback(portfolioService.getBaseDataMap());
        });
    
        Handlebars.registerHelper('checkPortfolio', function (index, opts){            
            if (index === inputController.getSelectedPortfolioIndex()){
                return opts.fn(this);
            }else{
                return opts.inverse(this);
            }
        });
        
        Handlebars.registerHelper('if_eq', function(a, b, opts){
            if (a == b){
                return opts.fn(this);
            }else{
                return opts.inverse(this);
            }
        }); 
    
        
    
        function publishPortfolioChanged(){
            postal.channel().publish("portfolioController.portfolio.changed",  getLocallySavedPortfolioList());
        }
    
        function publishPortfolioCalculated(){            
            postal.channel().publish("portfolioController.portfolio.calculated");
        }
      
        return{
            loadModule:                         loadModule, 
            saveButtonPressed:                  saveButtonPressed,
            portfolioSelected:                  portfolioSelected,
            getSavedPortfolioList:              getSavedPortfolioList,
            getAssetNameByCode:                 getAssetNameByCode,
            calculatePortfolioByIndex:          calculatePortfolioByIndex,
            getRollingPeriodTrajectory:           getRollingPeriodTrajectory,
            getPortfolioNameByIndex:            getPortfolioNameByIndex,
            calculateAllSavedPortfolioSuccessRate:calculateAllSavedPortfolioSuccessRate
        };
    
    }
);


