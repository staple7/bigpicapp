define({
    
   DEFAULT_PORTFOLIO_LIST: 
    [{
        NAME: "Diversified",
        ID: 0,
        ALLOCATION_MAP:{
            1:{CODE:"US_5",VALUE:45},
            2:{CODE:"US_1",VALUE:0},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:15},            
            7:{CODE:"US_7",VALUE:20},
            8:{CODE:"US_6",VALUE:0},
            9:{CODE:"US_8",VALUE:10},
            10:{CODE:"A_2",VALUE:10},
            11:{CODE:"A_3",VALUE:0}
           }
        },
     {
        NAME: "Income",
        ID: 1,
        ALLOCATION_MAP:{
            1:{CODE:"US_5",VALUE:0},
            2:{CODE:"US_1",VALUE:25},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:0},            
            7:{CODE:"US_7",VALUE:55},
            8:{CODE:"US_6",VALUE:0},
            9:{CODE:"US_8",VALUE:20},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}
           }
        },
     {
        NAME: "Mixed Income",
        ID: 2,
        ALLOCATION_MAP:{
            1:{CODE:"US_5",VALUE:20},
            2:{CODE:"US_1",VALUE:15},
            3:{CODE:"US_3",VALUE:0},
            4:{CODE:"US_2",VALUE:0},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:10},            
            7:{CODE:"US_7",VALUE:20},
            8:{CODE:"US_6",VALUE:15},
            9:{CODE:"US_8",VALUE:10},
            10:{CODE:"A_2",VALUE:10},
            11:{CODE:"A_3",VALUE:0}
        }    
    },
     {
        NAME: "Mixed Growth",
        ID: 3,
        ALLOCATION_MAP:{
            1:{CODE:"US_5",VALUE:25},
            2:{CODE:"US_1",VALUE:10},
            3:{CODE:"US_3",VALUE:5},
            4:{CODE:"US_2",VALUE:5},
            5:{CODE:"US_4",VALUE:0},
            6:{CODE:"A_1",VALUE:10},            
            7:{CODE:"US_7",VALUE:25},
            8:{CODE:"US_6",VALUE:0},
            9:{CODE:"US_8",VALUE:10},
            10:{CODE:"A_2",VALUE:10},
            11:{CODE:"A_3",VALUE:0}
        }    
    },
     {
        NAME: "Growth",
        ID: 4,
        ALLOCATION_MAP:{
            1:{CODE:"US_5",VALUE:0},
            2:{CODE:"US_1",VALUE:35},
            3:{CODE:"US_3",VALUE:10},
            4:{CODE:"US_2",VALUE:10},
            5:{CODE:"US_4",VALUE:5},
            6:{CODE:"A_1",VALUE:15},            
            7:{CODE:"US_7",VALUE:25},
            8:{CODE:"US_6",VALUE:0},
            9:{CODE:"US_8",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}
        }    
    },
     {
        NAME: "Max. Growth",
        ID: 5,
        ALLOCATION_MAP:{
            1:{CODE:"US_5",VALUE:0},
            2:{CODE:"US_1",VALUE:25},
            3:{CODE:"US_3",VALUE:20},
            4:{CODE:"US_2",VALUE:20},
            5:{CODE:"US_4",VALUE:10},
            6:{CODE:"A_1",VALUE:15},            
            7:{CODE:"US_7",VALUE:10},
            8:{CODE:"US_6",VALUE:0},
            9:{CODE:"US_8",VALUE:0},
            10:{CODE:"A_2",VALUE:0},
            11:{CODE:"A_3",VALUE:0}
        }    
    }
    
    
    ]
});   



