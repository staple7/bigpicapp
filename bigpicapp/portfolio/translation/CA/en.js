 
define({
	 
    CA_1:   "Canadian Gov. Bonds",
    CA_3:   "Canadian T-Bills",    
    US_7:   "U.S. Gov. Bonds",
    A_2:    "Global Bonds",
    A_3:    "Gold",
    CA_2:   "Canadian (TSX 300)",
    US_1:   "U.S. Large Cap",
    US_3:   "U.S. Mid Cap",
    US_2:   "U.S. Small Cap",
    US_4:   "U.S. Micro Cap",
    A_1:    "Int'l (ex-USA)"   

});
