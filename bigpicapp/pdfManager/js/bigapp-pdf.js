
var webPage = require('webpage');
var page = webPage.create();
var system = require("system");
/* page.paperSize = {
    width: '8.27in',
    height: '15.69in'
} */
 page.paperSize = {
    format: 'A4',
    orientation: 'portrait',
    // outputEncoding: "utf8",
    // scriptEncoding: "utf8",
    margin: '1cm',
    height: '20in',
    footer: {
    height: '1cm',
    contents: phantom.callback(function (pageNum, numPages) {
      return '<style> .footer { font-size: 12px;font-family: "Roboto", sans-serif; src: url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap") }</style> <footer class="footer"style="text-align: center;padding: 20px 0 0 0;color: #888; margin:0;">Page ' + pageNum + ' of ' + numPages + '</footer>';
    }),
  },
   /* margin: {
        top: '1cm',
        left: '1,6cm',
        right: '1,6cm',
        bottom: '1cm'
    }*/
}; 
page.open(system.args[1], function start(status) {
     window.setTimeout(function () {
            page.render(system.args[2]);
            phantom.exit();
            }, 5000);
        
});