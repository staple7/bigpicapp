define({
    
    MODALITY:           "Modality",
    IN_RETIREMENT:      "In Retirement",
    SAVING:             "Saving",
    YEARS_IN_RETIREMENT: "PLANNED RETIREMENT PERIOD (YRS.)",
    YEARS_TO_RETIREMENT: "PLANNED SAVINGS PERIOD (YRS.)",
    INITIAL_CAPITAL:    "Initial Capital",
    SPENDING:           "Spending",
    MONTHLY_SPENDING:   "Monthly Spending",
    MONTHLY_CONTRIBUTION:"Monthly Contribution",
    WITHDRAWAL_RATE:    "Withdrawal Rate",
    INFL_ADJ_SPENDING:  "Inflation-adjust", 
    INFL_ADJ:           "Inflation-adjust contributions", 
    LEGACY_CAPITAL:     "Legacy Capital",
    SAVINGS_GOAL:       "Savings Goal",
    PORTFOLIO:          "Portfolio",
    EDIT:               "Edit allocations",
    MORE:               "Fees & more",  
    MORE_INPUT_TITLE:   "Expenses and Rebalancing",
    TER_TITLE:          "TOTAL EXPENSE RATIO:",
    REBALANCING_TITLE:  "REBALANCING:",
    NONE:               "None",        
    ANNUAL:             "Annual",
    EVERY_3_YRS:        "Every 3 yrs.",
    EVERY_5_YRS:        "Every 5 yrs.",
    OTHER:              "Other",
    YEARS_SHORT:        "Yrs",
    YEAR_SHORT:         "Yr",
    DONE:               "Done",
    SUCCESS_RATE:       "The proportion of historical {{investmentHorizon}}-year periods* at the end of which the portfolio's value was equal to or greater than the stated amount of legacy capital, provided the inputs at left.<br><br>* <em>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>.",

    VARIABLES:{
        "Years_in_Retiremen": {
            name:        "Planned Retirement Period (Yrs.) (\"In retirement\" modality):",
            definition:  "The number of years remaining in retirement.",
        },
       "Years_to_Retirement": {
            name:        "Investment Horizon (\"Saving\" modality):",
            definition:  "The number of years remaining until retirement.",
        },
       "Initial_Capital": {
            name:        "Initial Capital:",
            definition:  "The amount of savings, in today's dollars, available for investment.",
        },
       "Monthly_Spending": {
            name:        "Monthly Spending (\"In retirement\" modality):",
            definition:  "Monthly spending in retirement, in today's dollars, net of social security, pension, and other sources of income.",
        },
       "Withdrawal_Rate": {
            name:        "Withdrawal Rate (\"In retirement\" modality):",
            definition:  "Annual spending, in today's dollars, net of social security, pension, and other sources of income—expressed as a proportion of the initial investment.",
        },
       "Monthly_Contribution": {
            name:        "Monthly Contribution (\"Saving\" modality):",
            definition:  "Monthly saving toward retirement, in today's dollars.",
        },
       "Legacy_Capital": {
            name:        "Legacy Capital (\"In retirement\" modality):",
            definition:  "The amount of capital, in future dollars, to be left in the retirement portfolio. ",
        },
       "Savings_Goal": {
            name:        "Savings Goal (\"Saving\" modality):",
            definition:  "The amount of capital, in future dollars, to serve as a retirement nest egg.",
        },
       "Inflation_Adjusted": {
            name:        "Inflation-Adjust Spending / Contributions:",
            definition:  "Portfolios are grown at nominal (gross-of-inflation historical returns). Withdrawal / contribution amounts are grown, in each historical month, at the then-prevailing rate of inflation (i.e. constant-dollar spending / earning, in purchasing power terms). To simulate an annuity in retirement, deselect this option.",
        },
       "Total_Expense_Ratio ": {
            name:        "Total Expense Ratio:",
            definition:  "The portfolio\'s total expense burden, including fund and advisory fees. Deducted annually. ",
        }
        
    },
    FOOTER:{
        paragraph_1: "<em>Axis values scale automatically as inputs are adjusted.</em>"
    }


});