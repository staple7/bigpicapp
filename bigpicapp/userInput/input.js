define({
        //Optimize
        retirement:{
            isSav: false,
            investmentHorizon: 30,
            initialInvestment: 1000000,
            isContribPercent: true,
            contributionAmount: 4,
            isContrReal: true,
            savingsGoal: 0,
            portfolioIndex: 0,
            ter: 0.015,        
            rebalancingYears: 1,
            isSaving: function() {
                return this.isSav;
            },
            setIsSaving: function (isSav) {
                this.isSav = isSav;
            },
            getInvestmentHorizon: function(){
                return this.investmentHorizon;
            },
            setInvestmentHorizon: function(investmentHorizon){
                this.investmentHorizon = investmentHorizon;
            },
            getInitialInvestment: function(){
                return this.initialInvestment;
            },
            setInitialInvestment: function(initialInvestment){
                this.initialInvestment = initialInvestment;
            },
            isContributionPercentage: function(){
                return this.isContribPercent;
            },
            setIsContributionPercentage: function(isContribPercent){
                this.isContribPercent = isContribPercent;
            },
            getContributionAmount: function(){
                return this.contributionAmount;
            },
            setContributionAmount: function(contributionAmount){
                this.contributionAmount = contributionAmount;
            },
            isContributionReal: function(){
                return this.isContrReal;
            },
            setIsContributionReal: function(isContrReal){
                this.isContrReal = isContrReal;
            },
            getSavingsGoal: function(){
                return this.savingsGoal;
            },
            setSavingsGoal: function(savingsGoal){
                this.savingsGoal = savingsGoal;
            },
            getPortfolioIndex: function(){
                return this.portfolioIndex;
            },
            setPortfolioIndex: function(portfolioIndex){
                this.portfolioIndex = portfolioIndex;
            },
            getTer: function(){
                return this.ter;
            },
            setTer: function(ter){
                this.ter = ter;
            },
            getRebalancingYears: function(){
                return this.rebalancingYears;
            },
            setRebalancingYears: function(rebalancingYears){
                this.rebalancingYears = rebalancingYears;
            },
            getContributionInDollar: function(withdrawalRate){
                return -Math.abs((withdrawalRate/100 * this.initialInvestment) / 12);
            }
            
        },
        saving:{
            isSav: true,
            investmentHorizon: 20,
            initialInvestment: 650000,
            isContrPercent: true,
            contributionAmount: 835,
            isContrReal: true,
            savingsGoal: 1000000,
            portfolioIndex: 0,
            ter: 0.015,
            rebalancingYears: 1,
            isSaving: function() {
                return this.isSav;
            },
            setIsSaving: function (isSav) {
                this.isSav = isSav;
            },
            getInvestmentHorizon: function(){
                return this.investmentHorizon;
            },
            setInvestmentHorizon: function(investmentHorizon){
                this.investmentHorizon = investmentHorizon;
            },
            getInitialInvestment: function(){
                return this.initialInvestment;
            },
            setInitialInvestment: function(initialInvestment){
                this.initialInvestment = initialInvestment;
            },
            isContributionPercentage: function(){
                return this.isContribPercent;
            },
            setIsContributionPercentage: function(isContribPercent){
                this.isContribPercent = isContribPercent;
            },
            getContributionAmount: function(){
                return this.contributionAmount;
            },
            setContributionAmount: function(contributionAmount){
                this.contributionAmount = contributionAmount;
            },
            isContributionReal: function(){
                return this.isContrReal;
            },
            setIsContributionReal: function(isContrReal){
                this.isContrReal = isContrReal;
            },
            getSavingsGoal: function(){
                return this.savingsGoal;
            },
            setSavingsGoal: function(savingsGoal){
                this.savingsGoal = savingsGoal;
            },
            getPortfolioIndex: function(){
                return this.portfolioIndex;
            },
            setPortfolioIndex: function(portfolioIndex){
                this.portfolioIndex = portfolioIndex;
            },
            getTer: function(){
                return this.ter;
            },
            setTer: function(ter){
                this.ter = ter;
            },
            getRebalancingYears: function(){
                return this.rebalancingYears;
            },
            setRebalancingYears: function(rebalancingYears){
                this.rebalancingYears = rebalancingYears;
            }
            
        }
    
});