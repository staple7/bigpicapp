define(['postal', 'graphGallery/graphs/safeWithdrawalRates/messages/en'], function(postal, messages){
     
        var context;
        var y2AxisLabel;    
        var timer;
        var is_pdf = $("#is_pdf").val();  //  ## ANUJ CODE ##
        function graph(graphData){

            removeElements();            
            
            var savingsGoal         = graphData.userInput.getSavingsGoal();
            var regionStartDate     = graphData.regionStartDate;
            var initialInvestment   = graphData.userInput.getInitialInvestment();
            var userSpending        = graphData.userInput.getContributionAmount();
            var isContributionPercentage = graphData.userInput.isContributionPercentage();
            var investmentHorizon   = graphData.userInput.getInvestmentHorizon();
            
            setXAxisAndGraphTitles(investmentHorizon);
            
            var result = calculateSafeWithdrawalRateList(initialInvestment,
                                                         savingsGoal,
                                                         regionStartDate,
                                                         graphData.baseInitialInvestmentList,
                                                         graphData.baseMonthlyContributionList,
                                                         isContributionPercentage,
                                                         "999999");
 
            var dataList = result[0];
            var minXValue = result[1];
            var maxXValue = result[2];
            var maxYValue = result[3];
            var swrList   = result[4];

            svg = svg.datum(dataList);
         
            yAxis = d3.svg.axis(); 
           
            xAxis = d3.svg.axis();            
 
            x = d3.scale
                .linear()                   
                .domain([minXValue, maxXValue])
                .range([0, graphWidth]);
             
             
            var factor = (maxXValue-minXValue)/6;
            var xTickValues = [];
     
            
            for(var i=0; i<(7);i++){
                xTickValues.push(i==0?minXValue:minXValue+factor*i);
            }
             
            xAxis.scale(x)
                 .orient("bottom")
                 .tickValues(xTickValues)                        
                 .tickFormat(function(d) {return Math.round(d);});
              
             
            y = d3.scale
                  .linear()
                  .range([graphHeight, 0]); 
               
          //  var yTickValues = calculateTickValues(maxYValue);
             
            var yTickValues = [];
            yTickValues = yTickValues.calculateTicks('percentage', 0, maxYValue, 0.01);
 
            
            yAxis.scale(y)
                 .orient("left")
                 .tickValues(yTickValues)
                 .tickFormat(function(d){
               
                         return isContributionPercentage ?  d.format('percentage', 1, d) : 
                                 Handlebars.compile("{{formatNumber type format value}}")
                                 ({type: 'currency', format: 'rounded', value: d});
                                 
                 });
             
            y.domain([0, yTickValues[yTickValues.length-1]]);
 
            svg.selectAll(".x.axis").call(xAxis);
            svg.selectAll(".y.axis").call(yAxis);            
                         
            var barIndex = 0;
             
            var barWidth = (graphWidth / dataList.length);
               
            svg.append("g")
                .attr("id","graphBars")
                .selectAll("rect")
                .data(dataList)
                .enter().append("rect")
                .attr("x", function(d){
                        var thisObj = d3.select(this);
                        var xPos = (barIndex++)*barWidth;
                        var height = graphHeight - y(d.y);
                        thisObj.attr("height", height);  
                        thisObj.attr("y", y(d.y));  
                        return xPos;
                })
                .attr('width', barWidth)
                .attr("fill", "#4FC3F7");
            

            svg.selectAll(".x.axis")
               .selectAll(".tick text")
               .each(function (d, i) {
                    var textElement = d3.select(this);
                    textElement.style("text-anchor",i == 0?"start":i==xTickValues.length-1?"end":"middle");
                    textElement.attr("class", "normalText");     
               }); 
            
            svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                d3.select(this).attr("class", "normalText");                      
            });

            if (maxYValue == 0){
                svg.append("rect")
                    .attr("x", "0")
                    .attr("y", graphHeight-2)
                    .attr("width", graphWidth)
                    .attr("height", 1)
                    .attr("id", "graphPath")
                    .attr("class", "graph_path");     
            }
            
            var lineValue = userSpending;
            if (isContributionPercentage){
                lineValue = ((userSpending * 12) / initialInvestment);
            } 
            
            lineValue = Math.abs(lineValue);

            var contributionLessThanMax = (Math.abs(lineValue)<=maxYValue);
            
            var y2 = d3.scale
                   .linear()
                   .domain([0, yTickValues[yTickValues.length-1]])
                   .range([graphHeight, 0]);            
            
            
            var graphWidthLimit = 1359;
            /* ============Start code edited by Anuj================== */
            var documentWidths = $(document).width();
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                documentWidths = 1625;
            }
            /* ============End code edited by Anuj================== */
 
            var y2Axis = d3.svg
                        .axis()
                        .scale(y2)
                        .tickValues([contributionLessThanMax?lineValue:''])
                        .tickFormat(function(d, i){
                            var label = y2AxisLabel;
                            if (contributionLessThanMax){
                                if ( documentWidths <graphWidthLimit){
                                    label = y2AxisLabel.replace('spending', 'spend');
                                }
                            }else{
                                label = '';
                                return '';
                            }

                            return label;
                        })
                        .orient("right");
            
     
            if (contributionLessThanMax){
                svg.append("line")
                   .attr("id", "line1")
                   .attr("x1", 0)
                   .attr("x2", graphWidth)                   
                   .attr("stroke", "rgba(1,87,155,1)")
                   .attr('stroke-width', '0.5px')
                   .attr("transform", "translate(" + 0 + "," + y(lineValue) + ")");    
            }
            
            svg.append("g") 
                .attr("class", "y axis")
                .attr("id", "y2Axis")
                .attr("transform", "translate(" + graphWidth + " ,0)")   
                .call(y2Axis)
                .selectAll(".tick text")
                .each(function(){          
                    d3.select(this).attr("class", "smallerText");      
                  //  d3.select(this).style("text-anchor","middle"); 
                })
                .call((new String).wrap, 10)
                .attr("transform", "translate(15, -25)");   
           
            swrList = swrList.sort(function(a, b) {return a - b;});
           
            var median = (swrList[Math.round(swrList.length/2)]*100).toFixed(1);
            
            svg.append("text")
                .attr("id", "medianText")
                .attr("class", "lightText")
               // .text("Median: " + (maxYValue*100 / 2).toFixed(1)+"%")
                .text("Median: " + median+"%")
                .attr("y", -13)
                .attr("x", function (){
                    return (graphWidth - d3.select(this).node().getComputedTextLength())/2
                })
                
            clearTimeout(timer);
            timer = setTimeout(function(){
                calculateBubbleList(dataList);
            }, 500);

            
            function calculateBubbleList(dataList){
                $("#activeAreas").remove();
                var activeAreaBarIndex = 0;
                var barWidth = (graphWidth / dataList.length);
                svg.append("g")
                    .on("mouseleave",function(){
                        postal.channel().publish("graphService.graph.hide.bubble");
                    })
                    .attr("id","activeAreas")
                    .selectAll(".rect")
                    .data(dataList)
                    .enter()
                    .append("rect")
                    .attr("x", function(d){
                        var thisObj = d3.select(this);
                        thisObj.attr("y", 0);  
                        thisObj.attr("height", graphHeight);  
                        thisObj.attr("width", barWidth)
                        thisObj.attr("fill", "transparent")
                        return (activeAreaBarIndex++)*barWidth;
                    }).on("mouseover",function(d) {
                        var regionStartDate = new Date(graphData.regionStartDate);
                        var value = dataList[d.x];
                        var dataStartDate = new Date(regionStartDate.setMonth(value.x));
                        var endValue = ((value.y*100).toFixed(1));
                        postal.channel().publish("graphService.graph.show.bubble", {text:endValue+"%" + 
                                                                                    " starting" + " " 
                                                                                    + d3.time.format("%m/%y")(dataStartDate),
                                                                                    xPosition: d3.mouse(this)[0]});
                    });      
            }
            
            postal.channel().publish("graphService.graph.done");
             
            
        }
     
     
        function calculateSafeWithdrawalRateList(initialInvestment,
                                                 savingsGoal,
                                                 regionStartDate,
                                                 baseInitialInvestmentList,
                                                 baseMonthlyContributionList,
                                                 isContributionPercentage,
                                                 maxOutputValue){

            
            var endValue;
            var lineDataList  = [];            
            var incremental = 1;
            var divideFactor  = false;            
            var goUp = true;
            var initialFactor = 50000;
            var factor = initialFactor;        
            var MSIR = 0; //Monthly Spending in Retirement
            var resultValue;
            var maxResultValue;
            var minXValue;
            var maxXValue
            var initialInvestmentValue;
            var monthlyContributionValue;            
            var maxLength = baseInitialInvestmentList.length;
            var ASIR; //Annual Spending in Retirement
            var monthIndex = 0;
            var resultList = [];
            maxOutputValue = maxOutputValue*-1;
            for(var index=0; index<maxLength; index += incremental){
                initialInvestmentValue = baseInitialInvestmentList[index];
                monthlyContributionValue = baseMonthlyContributionList[index];
                 
                while(MSIR > maxOutputValue && MSIR<=0){
                    endValue = (initialInvestmentValue * initialInvestment) + 
                               (monthlyContributionValue * MSIR);
                    if (endValue >= savingsGoal){
                        goUp = true;
                    }else{
                        if (MSIR == 0) break;
                        divideFactor = true;
                        goUp = false;
                    }
                    factor = divideFactor ? factor /= 2 : factor;
                    if (factor >= 0.01){
                        MSIR = goUp ? MSIR -= factor : MSIR += factor;
                    }else{
                        break;
                    }
                }          
                
                MSIR = Math.round((MSIR > 0) ? 0: Math.abs(MSIR));
                
                if (isContributionPercentage){
                    ASIR = MSIR * 12;
                    if (ASIR < initialInvestment){
                        resultValue = (ASIR / initialInvestment);
                    }else{
                        //resultValue = (ASIR - initialInvestment) / initialInvestment;
                        
                        resultValue = 1;
                    }
                    if (isNaN(resultValue)) resultValue =0;
                    
                }else{
                    resultValue = MSIR;
                }
                
                maxResultValue = (maxResultValue === undefined || resultValue > maxResultValue) ? resultValue : maxResultValue;   
                lineDataList.push({x:monthIndex, y:resultValue});
                resultList.push(resultValue);
                
                MSIR = 0;
                factor = initialFactor;
                goUp = true;
                divideFactor = false;   
                
                monthIndex += incremental;
            }
 
            minXValue = new Date(regionStartDate).getFullYear();
            maxXValue = new Date(new Date(regionStartDate).setMonth(regionStartDate.getMonth() + monthIndex)).getFullYear();
           // console.log([lineDataList,  minXValue, maxXValue, maxResultValue, resultList]);
            return [lineDataList,  minXValue, maxXValue, maxResultValue, resultList];
        }    
    

     
        function setXAxisAndGraphTitles(invHorizon){     
            
            var graphWidthLimit = 1359;
            /* ============Start code edited by Anuj================== */
            var documentWidth = $(document).width();
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                documentWidth = 1625;
            }
            /* ============End code edited by Anuj================== */
            var graphSubtitle = localContext.graphSubtitle.normal;

            if  (documentWidth<graphWidthLimit){
                graphSubtitle = localContext.graphSubtitle.abbreviate
            }
        
            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                      ({investmentHorizon:invHorizon,
                                       regionStartYear:localStorage.getItem("regionStartYear")}));

            xAxisTitle.html(Handlebars.compile(localContext.xAxisTitle)({investmentHorizon:invHorizon}));            
             
        }
    


        function removeElements(){                 
            $("#graphBars").remove();
            $("#graphPath").remove();
            $("#y2Axis").remove();
            $("#activeAreas").remove();            
            $("#line1").remove();
            $("#medianText").remove();
            $("#portfolioLabels").remove(); //  ## ANUJ CODE ##
            
        }    

        function finalize(){
            removeAxis();
            xAxisTitle.html("");
            removeElements();
            context = null;    
            y2AxisLabel = null;
            clearTimeout(timer);
            timer = null;
        }
     
        function branchChanged(){
            finalize();
        }
     
        function graphChanged(){
            finalize();
        }
        function formatMoney(value, lang){
                return new Number(value).toLocaleString('en', {style: 'currency', 
                                                                currency: 'USD', 
                                                                maximumFractionDigits: 0});
        }
    
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width(); 
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            /* ============Start code edited by Anuj================== */
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                containerWidth = 980;
                yPos = 50;
                graphHeight = (600 * 0.55);  

            } 
            /* ============End code edited by Anuj================== */
            graphWidth = (containerWidth * 0.80);
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
    
        function removeAxis(){
            svg.selectAll(".x.axis").remove();
            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            svg.append("g").attr("class", "x axis");
            svg.append("g").attr("class", "y axis");
        }
    
        function init(lang, branchCode){
            addAxis();
            calculateGraphDimensions();
            localContext = messages;
            y2AxisLabel = localContext.y2AxisTitle;
            /* ============Start code edited by Anuj================== */
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                var contributionInPercentaje= $(".contribution_amount").val();
                y2AxisLabel = (localContext.y2AxisTitle+ '\n(' +contributionInPercentaje+')');
            }
            /* ============End code edited by Anuj================== */
            $(".graph_title").html(localContext.graphTitle);
            postal.channel().publish("graphGallery.graph.initiated");
              
        }    

        return {
            init:               init,
            graph:              graph,
            graphChanged:       graphChanged,
            branchChanged:      branchChanged           
        };
 
    }
);