define({

    inRetirement: {            
        graphTitle : "Legacy Capital",
        graphSubtitle: {
            normal:     "Terminal portfolio values, by rolling {{investmentHorizon}}-year period, {{regionStartYear}} to present",
            abbreviate: "Terminal portfolio values, by rolling {{investmentHorizon}}-yr. period, {{regionStartYear}} – present"
        },
        xAxisTitle : "{{investmentHorizon}}-YEAR RETIREMENT PERIOD STARTING IN…",
        yAxisTitle : "LEGACY CAPITAL BY PERIOD",
        y2AxisTitle: "Your legacy capital"

    },
    savingForOneTime: {
        graphTitle : "Savings Amassed",
        graphSubtitle: {
            normal:     "Accumulated portfolio values, by rolling {{investmentHorizon}}-year period, {{regionStartYear}} to present",
            abbreviate: "Accumulated portfolio values, by rolling {{investmentHorizon}}-yr. period, {{regionStartYear}} – present"
        },        
        xAxisTitle : "{{investmentHorizon}}-YEAR INVESTMENT PERIOD STARTING IN…",
        yAxisTitle : "SAVINGS AMASSED BY PERIOD",
        y2AxisTitle: "Your savings goal"
    }


});

    