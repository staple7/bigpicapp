 
define({
 
    inRetirement: {    		
        graphTitle  : "Odds of Success Across Portfolios",
        graphSubtitle: {
            normal: "Historical frequency at which portfolios ended above legacy capital, {{regionStartYear}} – present",
            abbreviate: "Historical frequency at which portfolios ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle: "",
        y2AxisTitle: ""
    },
    savingForOneTime: {
        graphTitle  : "Odds of Success Across Portfolios",
        graphSubtitle: {
            normal: "Historical frequency at which portfolios ended above savings goal, {{regionStartYear}} – present",
            abbreviate: "Historical frequency at which portfolios ended above savings goal, {{regionStartYear}} – present"
        },
        xAxisTitle: "",
        y2AxisTitle: ""        
    }
    
});
