 
define({

    inRetirement:{
        graphTitle    : "Odds of Success",
        graphSubtitle:  {
            normal:     "Historical frequency at which portfolio ended above legacy capital, by retirement length, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle    : "YEARS INTO RETIREMENT"   
    },
    savingForOneTime: {
        graphTitle    : "Odds of Success by Savings Period",
        graphSubtitle:  {
            normal: "Historical frequency at which portfolio met savings goal, by investment horizon, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio met goal, {{regionStartYear}} – present"
        },
        xAxisTitle    : "YEARS SPENT SAVING"   
    }
    


});

    