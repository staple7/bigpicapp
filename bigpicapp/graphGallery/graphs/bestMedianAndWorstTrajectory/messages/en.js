 define({

    inRetirement:{
        graphTitle    : "Best & Worst Case Scenarios",
        graphSubtitle:  {
            normal:     "Historical best and worst portfolio trajectories",
            abbreviate: "Historical best, worst, & median portfolio trajectories"
        },
        xAxisTitle    : "PLANNED RETIREMENT PERIOD (YRS.)",
        y2AxisTitle:    "Your starting capital",
   	    y2AxisTitleShort: "Your start. capital"
    },
    savingForOneTime: {
         graphTitle    : "Best & Worst Case Scenarios",
        graphSubtitle:  {
            normal:     "Historical best and worst portfolio trajectories",
            abbreviate: "Historical best, worst, & median portfolio trajectories"
        },
        xAxisTitle    : "PLANNED SAVINGS PERIOD (YRS.)",
        y2AxisTitle:    "Your starting capital",
   	    y2AxisTitleShort: "Your start. capital"
    }

     



});

    