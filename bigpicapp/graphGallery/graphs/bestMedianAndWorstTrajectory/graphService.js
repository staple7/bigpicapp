define (['postal', 'graphGallery/graphs/bestMedianAndWorstTrajectory/messages/en','portfolio/portfolioController'], function(postal, messages,portfolioController){
    
        var localGraphData;
        var context;
        
    
        function graph(graphData){ 
            
            postal.channel().publish("portfolioController.get.data.map", function(data){
                
                localGraphData = graphData;

                removeElements();

                var savingsGoal         = graphData.userInput.getSavingsGoal();
                var monthlyContribution = graphData.userInput.getContributionAmount()
                var investmentHorizon   = graphData.userInput.getInvestmentHorizon();
                var endValueList        = graphData.endValueList;
                var regionStartDate     = graphData.regionStartDate;
                var userInitialInvestment = graphData.userInput.getInitialInvestment();
                var result;

                setGraphTitles(investmentHorizon);

                var result = getBestMedianAndWorstCandidateIndexList(endValueList);

                var bestIndexList = result.bestIndexList;
                var worstIndexList = result.worstIndexList;
                var medianIndex = result.medianIndex;
                var allNegative = result.allNegative;
                var allPositive = result.allPositive;
              
             
                var worstTrajectoryDataList = undefined;
                var worstTrajectoryIndex = undefined; 
                var bestTrajectoryIndex = undefined;
                var bestTrajectoryDataList = undefined;
                var medianTrajectoryIndex = undefined;
                var medianTrajectoryDataList = undefined;
                var bestEndValue = undefined;
                
                //CALCULATING BEST AND WORST INIT.
                if (allNegative==true){
                     
                    //All End Values are negative. In this case, best and worst trajectories are selected based on the date that it drops to zero (indexWhenDropZero variable).
                    
                    $.each(worstIndexList ,function(index, rollingPeriodIndex){
                        
                        var result = portfolioController.getRollingPeriodTrajectory(rollingPeriodIndex);
                        var trajectoryDataList = result.trajectoryDataList;
                        var indexWhenDropZero = result.indexWhenDropZero;
                         
                        if (!bestTrajectoryDataList){
                            worstTrajectoryDataList = trajectoryDataList;
                            worstTrajectoryIndex = rollingPeriodIndex;
                            bestTrajectoryDataList = trajectoryDataList;
                            bestTrajectoryIndex = rollingPeriodIndex;
                        }
                        
                        if (indexWhenDropZero < worstTrajectoryIndex){
                            worstTrajectoryDataList = trajectoryDataList;
                            worstTrajectoryIndex = rollingPeriodIndex; 
                        }
                        
                        if (indexWhenDropZero > bestTrajectoryIndex){
                            bestTrajectoryDataList = trajectoryDataList;
                            bestTrajectoryIndex = rollingPeriodIndex;
                        }
                        
                    }); 
        
                }else{
                    //Here we can have either one worst index in the worstIndexList or many of them, same for bestIndexList. If we have only one value that is because all end values are positive, if we have many of them is because all end values are both kind, negative and positive.
                    $.each(worstIndexList ,function(index, rollingPeriodIndex){
                        var result = portfolioController.getRollingPeriodTrajectory(rollingPeriodIndex);
                        var trajectoryDataList = result.trajectoryDataList;
                        var indexWhenDropZero = result.indexWhenDropZero;
                        if (!worstTrajectoryIndex){
                            worstTrajectoryDataList = trajectoryDataList;
                            worstTrajectoryIndex = rollingPeriodIndex;
                        }else if (indexWhenDropZero < worstTrajectoryIndex){
                            worstTrajectoryDataList = trajectoryDataList;                             
                            worstTrajectoryIndex = rollingPeriodIndex;
                        }
                    });

                    $.each(bestIndexList ,function(index, rollingPeriodIndex){                   
                        var result = portfolioController.getRollingPeriodTrajectory(rollingPeriodIndex);
                        var trajectoryDataList = result.trajectoryDataList;
                        var thisEndValue = trajectoryDataList[trajectoryDataList.length-1];
                        if (!bestTrajectoryIndex){
                            bestTrajectoryDataList = trajectoryDataList;
                            bestTrajectoryIndex = rollingPeriodIndex;
                            bestEndValue = thisEndValue;
                        }else if (thisEndValue > bestEndValue){
                            bestTrajectoryDataList = trajectoryDataList;
                            bestTrajectoryIndex = rollingPeriodIndex;
                            bestEndValue = thisEndValue;
                        }
                    }); 
                    
                    
                }                
                //CALCULATING BEST AND WORST END.
                
                //CALCULATING MEDIA INIT.
                if (allNegative == true){

                    var medianIndex = worstTrajectoryIndex + ((bestTrajectoryIndex - worstTrajectoryIndex)/2);
                   
                    $.each(worstIndexList ,function(index, rollingPeriodIndex){

                        var result = portfolioController.getRollingPeriodTrajectory(rollingPeriodIndex);
                        var trajectoryDataList = result.trajectoryDataList;
                        var indexWhenDropZero = result.indexWhenDropZero;
                        var indexDiff = Math.abs(indexWhenDropZero-medianIndex);

                        if (index == 0){
                            medianTrajectoryDataList = trajectoryDataList;
                            medianTrajectoryIndex = indexDiff;                           
                        }
 
                        //Sets medianTrajectoryIndex based on the smallest difference betweeen indexWhenDropZero minus medianIndex. 
                        if (indexDiff < medianTrajectoryIndex){
                            medianTrajectoryDataList = trajectoryDataList;
                            medianTrajectoryIndex = rollingPeriodIndex; 
                        }

                    });
                     
                    
                }else{
                    
                    if (allPositive == true){

                        medianTrajectoryIndex = medianIndex;
                        var result = portfolioController.getRollingPeriodTrajectory(medianIndex);
                        medianTrajectoryDataList = result.trajectoryDataList;
 
                    }else{ 
                        
                        if (result.percentageOfPositiveEndValues>=50){
                            //Both positive and negative end values with at least 50% of positive end values.
//                            console.log("at least 50% are positive:",medianIndex);
                            medianTrajectoryIndex = medianIndex;
                            var result = portfolioController.getRollingPeriodTrajectory(medianIndex);
                            medianTrajectoryDataList = result.trajectoryDataList;
                        }else{
                            //less than 50% of positive end values. Here, we'll rank duration of all of the trayectories and get the one in the middle.
                            var minTrayectoryDuration = undefined;
                            var maxTrayectoryDuration = undefined;
                   
                            $.each(worstIndexList ,function(index, rollingPeriodIndex){
                                var result = portfolioController.getRollingPeriodTrajectory(rollingPeriodIndex);
                                var trajectoryDataList = result.trajectoryDataList;
                                var indexWhenDropZero = result.indexWhenDropZero;
                                if (index == 0){
                                    minTrayectoryDuration = indexWhenDropZero;
                                    maxTrayectoryDuration = indexWhenDropZero;                         
                                }
                                if (indexWhenDropZero < minTrayectoryDuration){
                                    minTrayectoryDuration = indexWhenDropZero;
                                }else if (indexWhenDropZero > maxTrayectoryDuration){
                                    maxTrayectoryDuration = indexWhenDropZero;
                                }
                            });
                            
                            var medianIndex = minTrayectoryDuration + ((maxTrayectoryDuration - minTrayectoryDuration)/2);
                   
                            $.each(worstIndexList ,function(index, rollingPeriodIndex){

                                var result = portfolioController.getRollingPeriodTrajectory(rollingPeriodIndex);
                                var trajectoryDataList = result.trajectoryDataList;
                                var indexWhenDropZero = result.indexWhenDropZero;
                                var indexDiff = Math.abs(indexWhenDropZero-medianIndex);

                                if (index == 0){
                                    medianTrajectoryDataList = trajectoryDataList;
                                    medianTrajectoryIndex = indexDiff;                           
                                }

                                //Sets medianTrajectoryIndex based on the smallest difference betweeen indexWhenDropZero minus medianIndex. 
                                if (indexDiff < medianTrajectoryIndex){
                                    medianTrajectoryDataList = trajectoryDataList;
                                    medianTrajectoryIndex = rollingPeriodIndex; 
                                }

                            });
                            
                        }
                        
                    }
                  
                } 
                //CALCULATING MEDIA END.
               
                
                
//                console.log(worstTrajectoryDataList);
//                                console.log(medianTrajectoryDataList);

                var resultBest   = calculateDataList(bestTrajectoryDataList, regionStartDate);
                var resultWorst  = calculateDataList(worstTrajectoryDataList, regionStartDate);
                var resultMedian = calculateDataList(medianTrajectoryDataList, regionStartDate);
                
                var dataList = resultBest[0];
                var maxYValue = Math.max(resultBest[1], resultWorst[1], resultMedian[1]);
                var monthList = resultBest[2];

                svg = svg.datum(dataList);

                yAxis = d3.svg.axis(); 
                xAxis = d3.svg.axis();

                x = d3.scale
                    .linear()
                    .domain([0, dataList.length-1])
                    .range([0, graphWidth]);

                var xTickValues = [];

                xAxis.scale(x)
                     .orient("bottom")
                     .tickValues(monthList)
                     .tickFormat(function(d,i) {
                        if (i%12===0){
                            var year=i/12;
                            if (year%2===0){
                                return year;
                            }
                        }
                        d3.select(this).node().parentNode.remove();
                    });


                var yTickValues = [];
                yTickValues = yTickValues.calculateTicks('currency', 0, maxYValue, 0.01);

                y = d3.scale
                    .linear()
                    .domain([0, maxYValue])
                    .range([graphHeight, 0]);

                yAxis.scale(y)
                    .orient("left")
                    .tickValues(yTickValues) 
                    .tickFormat(function(d) {
                        return Handlebars.compile("{{formatNumber type format value}}")
                                ({type: 'currency', format: 'rounded', value: d})
                    });

                svg.selectAll(".x.axis").call(xAxis);
                svg.selectAll(".y.axis").call(yAxis);

                var line = d3.svg.line()
                                 .interpolate("cardinal")
                                 .x(function(d) {return x(d.x);})
                                 .y(function(d) {return y(d.y);});

                svg.append("path")                
                    .attr("fill", "none")
                    .attr("stroke", "#388e3C")
                    .attr("stroke-width", "1.5px")
                    .attr("id","bestLine")
                    .attr("d", line);


                var dataList = resultWorst[0];
                svg = svg.datum(dataList);

                svg.append("path")                
                 .attr("fill", "none")
                    .attr("stroke", "red")
                    .attr("stroke-width", "1.5px")
                    .attr("id","worstLine")
                    .attr("d", line);
                
                
                var dataList = resultMedian[0];
                svg = svg.datum(dataList);

                svg.append("path")                
                 .attr("fill", "none")
                    .attr("stroke", "steelblue")
                    .attr("stroke-width", "1.5px")
                    .attr("id","medianLine")
                    .attr("d", line);
          

                svg.selectAll(".x.axis")
                    .selectAll(".tick text")
                    .each(function (d, i) {
                        d3.select(this).style("text-anchor",i == 0?"start":i==xTickValues.length-1?"end":"middle");
                        d3.select(this).attr("class", "normalText");  
                    });

                svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                    d3.select(this).attr("class", "smallerText");                      
                });


                var y2 = d3.scale
                           .linear()
                           .domain([0, maxYValue])
                           .range([graphHeight, 0]);  


                var initialInvLessThanMax = (userInitialInvestment<=maxYValue);

                var graphWidthLimit = 1359;
                /* ============Start code edited by Anuj================== */
                var documentWidth1 = $(document).width();
                var is_pdf = $("#is_pdf").val();
                if(is_pdf == 1){
                    documentWidth1 = 1625;
                }
                // Use "documentWidth1" variable in return condition below line no 326
                /* ============End code edited by Anuj================== */

                var y2Axis = d3.svg 
                            .axis()
                            .scale(y2)
                            .tickValues([initialInvLessThanMax?userInitialInvestment:''])
                            .tickFormat(function(d, i){
                               return (initialInvLessThanMax) ? 
                                        (( documentWidth1 <graphWidthLimit)?context.y2AxisTitleShort:context.y2AxisTitle) 
                                        : '';
                            })
                            .orient("right");

                svg.append("g") 
                    .attr("class", "y axis") 
                    .attr("id", "y2Axis")
                    .attr("transform", "translate(" + graphWidth + " ,0)")   
                    .call(y2Axis)
                    .selectAll(".tick text")
                    .call((new String).wrap, 10) 
                    .attr("transform", "translate(15, -35)");  
 
                svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                        d3.select(this).attr("class", "smallerText");                    
                    });


                if (initialInvLessThanMax){
                    svg.append("line")
                       .attr("id", "line1")
                       .attr("x1", 0)
                       .attr("x2", graphWidth)                   
                       .attr("stroke", "rgba(1,87,155,1)")
                       .attr('stroke-width', '0.5px')
                       .attr("transform", "translate(" + 0 + "," + y(userInitialInvestment) + ")");
                }

                
                var bestStartDate = calculateDateByDataIndex(bestTrajectoryIndex, regionStartDate);
                var worstStartDate = calculateDateByDataIndex(worstTrajectoryIndex, regionStartDate);
                var medianStartDate = calculateDateByDataIndex(medianTrajectoryIndex, regionStartDate);
                /* ============Start code edited by Anuj================== */
                var bestDates = 172;
                var medianDates = 30;
                var worstDates = 207;

                var is_pdf = $("#is_pdf").val();
                if(is_pdf == 1){
                    var bestDates = 203;
                    var medianDates = 19;
                    var worstDates = 197;
                }

                /* ============End code edited by Anuj================== */
                svg.append("text")
                    .attr("id", "bestDate")
                    .attr("fill", "#388e3C")  
                    .html("Best start date: " + bestStartDate)
                    .attr("y", -13)
                    .attr("x", function (){
                        return ((graphWidth - d3.select(this).node().getComputedTextLength())/2)- bestDates //  ## ANUJ Edit this line ##
                    });
                
                 svg.append("text")
                    .attr("id", "medianDate")
                    .attr("fill", "steelblue")  
                    .html("Median: " + medianStartDate)
                    .attr("y", -13)
                    .attr("x", function (){
                        return ((graphWidth - d3.select(this).node().getComputedTextLength())/2)+ medianDates //  ## ANUJ Edit this line ##
                    });
                
                svg.append("text")
                    .attr("id", "worstDate")
                    .attr("fill", "red")  
                    .html("Worst: " + worstStartDate)
                    .attr("y", -13)
                    .attr("x", function (){
                        return ((graphWidth - d3.select(this).node().getComputedTextLength())/2)+ worstDates //  ## ANUJ Edit this line ##
                    });
                
                
                postal.channel().publish("graphService.graph.done");
            });
        }  
  
        //Selects end values index candidates.
        function getBestMedianAndWorstCandidateIndexList(endValueList){
            var endValueListSize = endValueList.length;
            var list = endValueList.slice();
            
            list.sort(function(a, b){return a-b});
//             console.log(list);
            var lowest = list[0];
            var highest = list[list.length-1];            
            
//            console.log("lowest:",lowest,"median:",median,"highest:",highest);
            var worstIndexList = [];
            var bestIndexList = [];
            var median = list[Math.round(list.length/2)];
            var medianIndex = Math.floor(endValueListSize/2);

            if (lowest >= 0){
                //all end values are positive or zero.
//                console.log("all positive...");
                $.each(endValueList, function( index, value ) {
                    if (value == lowest){
                        worstIndexList.push(index);
                    }else if (value == highest){
                        bestIndexList.push(index); 
                    }
                }); 
                
                
            }else if (highest <= 0){
                //all the end values are negative or zero. In this case, all the end values are candidates to be evaluated because we don't know when each of them will drop to zero. In this scenario it is viable to calculate the median.
//                console.log("all negative...");
                for (var index = 0; index < endValueListSize; index++) {
                    bestIndexList.push(index);
                    worstIndexList.push(index);
                }
                
                
            }else if (lowest <= 0 && highest >=0){ 
                //there are both kind of end values, positive and negative.   
//                console.log("mixed values...");
                var numberOfNegEndValues = 0;
                $.each(endValueList, function( index, value ) {
                    if (value < 0){
                        worstIndexList.push(index);
                        numberOfNegEndValues++;
                    }
                    
                    if (value == highest){
                        bestIndexList.push(index);
                    }
                    
                });
                
//                var positiveEndValueList = list.filter(function(x){ return x > -1 });
////                median = positiveEndValueList[Math.round(positiveEndValueList.length/2)];
//                $.each(endValueList, function( index, value ) {
//                    if (value == median){
//                        medianIndex = index;
//                    }
//                });
                
                
            }          
//           console.log({worstIndexList:worstIndexList, medianIndex:medianIndex, bestIndexList:bestIndexList, allNegative: (highest <= 0)?true:false});
            return {worstIndexList:worstIndexList, 
                    medianIndex:medianIndex, 
                    bestIndexList:bestIndexList, 
                    allNegative:(highest <= 0)?true:false,
                    allPositive:(lowest >= 0)?true:false,
                    percentageOfPositiveEndValues:(100-(numberOfNegEndValues*100/endValueListSize))};
        }

    
        function calculateDateByDataIndex(dataIndex, regionStartDate){
            var regionStartDate = new Date(regionStartDate);
            return dataStartDate = d3.time.format("%m/%y")(new Date(regionStartDate.setMonth(dataIndex)));
        }
    
        function calculateDataList(dataList, regionStartDate){
            var lineDataList = [];
            var monthIndexList = [];
            var endValue;
            var minYValue;
            var maxYValue;       
            var monthIndex = 0;
            var regionStartDate = localGraphData.regionStartDate;
            for(var index=0; index<dataList.length; index += 1){
                endValue = dataList[index];
                if (endValue<0) endValue = 0;
                maxYValue = (maxYValue === undefined || endValue > maxYValue) ? endValue : maxYValue;
                lineDataList.push({x:monthIndex, y:endValue});
                monthIndexList.push(monthIndex);
                monthIndex+=1;
            }          
            return [lineDataList, maxYValue, monthIndexList];            
        }
        
        function setGraphTitles(investmentHorizon){
            xAxisTitle.html(Handlebars.compile(context.xAxisTitle)({investmentHorizon:investmentHorizon}));       
            
            var graphWidthLimit = 1359;
            var documentWidth = $(document).width();
            /* ============Start code edited by Anuj================== */
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                var documentWidth = 1625;
            }
            /* ============End code edited by Anuj================== */
            var graphSubtitle = context.graphSubtitle.normal;

            if  (documentWidth<graphWidthLimit){
                graphSubtitle = context.graphSubtitle.abbreviate
            }
            
            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                ({investmentHorizon:investmentHorizon,
                                regionStartYear:localStorage.getItem("regionStartYear")}));
            
        }     
    

        function removeElements(){
            $("#graphPath").remove();
            $("#bestDate").remove();
            $("#worstDate").remove();
            $("#medianDate").remove();
            $("#bestLine").remove();
            $("#worstLine").remove();
            $("#medianLine").remove();
            $("#line1").remove();
            $("#y2Axis").remove();
            $("#portfolioLabels").remove(); //  ## ANUJ CODE ##
            
        }
    
        
        function finalize(){
            removeAxis();
            removeElements();
            localGraphData = null;
            xAxisTitle.html("");
            context = null;
            
        }
    
        function branchChanged(){
            finalize();
        }
    
        function graphChanged(){
            finalize();
        }
    
    
        function getGraphDefinition(){
            return Handlebars.compile(context.graphDefinition)(localGraphData);
        }

        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();

            var is_pdf = $("#is_pdf").val(); //  ## ANUJ CODE ##
            
            var yPos = 50;
            if ($(document).width() <= 1799){ 
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            /* ============Start code edited by Anuj================== */

            if(is_pdf == 1){
                containerWidth = 980;
                graphHeight = (600 * 0.55); 
                yPos = 50;
            }
            /* ============End code edited by Anuj================== */

            graphWidth = (containerWidth * 0.80); // Use this line after condtion in every graph

            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
    
        function removeAxis(){
            svg.selectAll(".x.axis").remove();
            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            svg.append("g").attr("class", "x axis");
            svg.append("g").attr("class", "y axis");
        }
    
        function init(lang, branchCode){ 
            addAxis();
            calculateGraphDimensions();
            context = messages[branchCode]; 
            $(".graph_title").html(context.graphTitle);
            postal.channel().publish("graphGallery.graph.initiated");
           
        }

        
        function getGraphElements(){
            
        }

    
        return {
            init:               init,
            graph:              graph,
            getGraphDefinition: getGraphDefinition,
            graphChanged:       graphChanged,
            getGraphElements:   getGraphElements,
            branchChanged:      branchChanged
        };
    
});     
                