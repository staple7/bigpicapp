define (['postal', 'graphGallery/graphs/successRateByWithdrawalRate/messages/en'], function(postal, messages){
         
    var context;
     
    function graph(graphData){
         
        removeElements();
 
        var minYValue;  
        var maxYValue;
        var yAxisValue;
        var xAxisValue;     
        var dataList = [];              
        var result;
       
        var xAxisRange = calculateXTickValues();
         
        setGraphTitles();
        
        xAxisRange.forEach(function(withdrawalRate){
            postal.channel().publish("variableCalculator.calculateSuccessRateByWithdrawalRate",
                                     {withdrawalRate: withdrawalRate, 
                                      callback: function(successRate){
                                       
                minYValue = minYValue === undefined ? successRate : 
                            (successRate < minYValue ? successRate : minYValue);
                                           
                maxYValue = maxYValue === undefined ? successRate : 
                            (successRate > maxYValue ? successRate : maxYValue);   
                                           
                dataList.push({x:withdrawalRate, 
                               y:successRate, 
                               lowerThanMin:false,
                               higherThanMax:false});
            }});
        });
 
 
        svg = svg.datum(dataList);
 
        yAxis = d3.svg.axis();
        xAxis = d3.svg.axis();
  
       x = d3.scale
              .ordinal()
              .domain(dataList.map(function(d){return d.x}))
              .rangeRoundBands([0, graphWidth], .19, .2);
         
         
 
        y = d3.scale.linear();
 
        var yTickValues = [];
        yTickValues = yTickValues.calculateTicks("years", 0, maxYValue, 0);
 
//                
        yAxis.scale(y)            
            .orient("left")
            .tickValues(yTickValues)
            .tickFormat(function(d) {
                return d.format("percentage", 0, d);
            });
 
     
         
        y = y.domain([yTickValues[0], yTickValues[yTickValues.length-1]])
            .range([graphHeight, 0]);
 
        xAxis.scale(x)
            .orient("bottom")
            .tickFormat(function(d) {
                if (d % 1 ==0){
                    return d.format('number', 0, d)+"%";                    
                }
                d3.select(this).node().parentNode.remove();
            });
 
                
        svg.selectAll(".x.axis").call(xAxis);         
        svg.selectAll(".y.axis").call(yAxis);
 
 
       var contributionInPercentaje= $(".contribution_amount").val().replace("%","");//temp
         
        svg.append("g")
            .attr("id","graphBars")
            .selectAll(".rect")
            .data(dataList)
            .enter().append("rect")
            .attr("rx", "2")
            .attr("ry", "2")
            .attr('width', x.rangeBand())                
            .attr('x', function(d, i){
                var yValue = y(d.y);
                var zeroLineFactor = 0;
                var heightFactor = 0.5;
                if (d.y == yTickValues[0]){
                    zeroLineFactor = 1.2;
                }
                var thisObj = d3.select(this);
                thisObj.attr("y", yValue-heightFactor-zeroLineFactor);
                thisObj.attr("height", Math.abs(graphHeight-yValue-zeroLineFactor));
//            console.log(xAxisRange[i]);
                var match = (xAxisRange[i]==contributionInPercentaje);
                thisObj.attr("fill", match ? "#0277BD" : "#4FC3F7");
          
                return x(d.x);
            });
//              var barIndex = 0;
//              
//            var barWidth = (graphWidth / dataList.length);
//    
//            svg.append("g")
//                .attr("id","graphBars")
//                .selectAll("rect")
//                .data(dataList)
//                .enter().append("rect")
//                .attr("x", function(d,i){
//                        var thisObj = d3.select(this);
//                        thisObj.attr("height", graphHeight -  y(d.y));  
//                        thisObj.attr("y", y(d.y));  
//                        var match = (xAxisRange[i]==contributionInPercentaje);
//                        thisObj.attr("fill", match ? "#0277BD" : "#4FC3F7");
//                        return (barIndex++)*barWidth;
//                })
//                .attr('width', barWidth)
//                .attr("fill", "#4FC3F7");
// 
        svg.selectAll(".x.axis").selectAll(".tick").each(function(data){
            d3.select(this).select("text").attr("class", "normalText");
        });
 
 
        svg.selectAll(".y.axis").selectAll("text").each(function(){                    
            d3.select(this).attr("class", "normalText");                    
        });
 
        postal.channel().publish("graphService.graph.done");
 
    }
 
     
    function setGraphTitles( ){    
        var graphWidthLimit = 1359;
        /* ============Start code edited by Anuj================== */
        var is_pdf = $("#is_pdf").val();  
        var documentWidth = $(document).width(); 
        if(is_pdf == 1){
            var documentWidth = 1625;
        }
        /* ============End code edited by Anuj================== */ 
        
        var graphSubtitle = localContext.graphSubtitle.normal;
        if  (documentWidth<graphWidthLimit){
            graphSubtitle = localContext.graphSubtitle.abbreviate
        }
        $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                ({regionStartYear:localStorage.getItem("regionStartYear")}));
    }    
 
    function calculateXTickValues(){
        return [1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4,4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,5,5.1,5.2,5.3,5.4,5.5,5.6,5.7,5.8,5.9,6,6.1,6.2,6.3,6.4,6.5,6.6,6.7,6.8,6.9,7,7.1,7.2,7.3,7.4,7.5,7.6,7.7,7.8,7.9,8,8.1,8.2,8.3,8.4,8.5,8.6,8.7,8.8,8.9,9,9.1,9.2,9.3,9.4,9.5,9.6,9.7,9.8,9.9,10];
        //return [2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40];
    } 
 
    
 
    function removeElements(){
        $("#graphBars").remove();
        $("#successRateGraphText").remove();
        $("#portfolioLabels").remove(); //  ## ANUJ CODE ##
    }
 
    function finalize(){
        removeElements();   
        localGraphData = null;
        context = null;
        xAxisTitle.html("");
        removeAxis();
    }
 
    function branchChanged(){
        finalize();
    }
 
    function graphChanged(){
        finalize();
    }
 
    function calculateGraphDimensions(){ 
        var containerWidth = $(svgArea).width(); 
        var yPos = 50;
        if ($(document).width() <= 1799){
            graphHeight = ($(svgArea).height() * 0.55);   
             
        }else{
            graphHeight = ($(svgArea).height() * 0.60);   
            yPos = 80;            
        }

        /* ============Start code edited by Anuj================== */
        var is_pdf = $("#is_pdf").val();  
        if(is_pdf == 1){
            yPos = 50;
            graphHeight = (600 * 0.55); 
            containerWidth = 980;
        } 
        /* ============End code edited by Anuj================== */
        graphWidth = (containerWidth * 0.80);
        svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
        svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
    }
 
    function removeAxis(){
        svg.selectAll(".x.axis").remove();
        svg.selectAll(".y.axis").remove();
    }
 
    function addAxis(){
        svg.append("g").attr("class", "x axis");
        svg.append("g").attr("class", "y axis");
    }
 
    function init(lang, branchCode){
        addAxis();
        calculateGraphDimensions();
        localContext = messages[branchCode];
        $(".graph_title").html(localContext.graphTitle);
        xAxisTitle.html(localContext.xAxisTitle);
        postal.channel().publish("graphGallery.graph.initiated");
    }
 
 
    return {
        init:               init,
        graph:              graph,
        graphChanged:       graphChanged,
        branchChanged:      branchChanged
    };
 
 
});