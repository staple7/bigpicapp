 
define({

    inRetirement:{
         graphTitle    : "Odds of Success by Spending Level",
        graphSubtitle:  {
            normal:     "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle    : "WITHDRAWAL RATE"   
    }


});

    