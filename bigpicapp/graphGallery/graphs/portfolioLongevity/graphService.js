define(['postal', 'graphGallery/graphs/portfolioLongevity/messages/en'], function(postal, messages){

        var context;
        var y2AxisLabel;           
        var timer;
        var is_pdf = $("#is_pdf").val(); //  ## ANUJ CODE ##
     
        function graph(graphData){    
            postal.channel().publish("portfolioController.get.data.map", function(data){
                removeElements();            
                setSubtitle();

                var savingsGoal         = 0;
                var regionStartDate     = graphData.regionStartDate;
                var monthlyContribution = graphData.userInput.getContributionAmount();
                var initialInvestment   = graphData.userInput.getInitialInvestment();
                var userInvestmentHorizon   = graphData.userInput.getInvestmentHorizon();

                var result = calculateDataList(monthlyContribution,
                                               initialInvestment,
                                               savingsGoal,
                                               regionStartDate,
                                               data.startingCapitalMap,
                                               data.contributionMap);

                var dataList  = result[0];
                var minXValue = result[1];
                var maxXValue = result[2];
                var maxYValue = result[3];
                var yearList  = result[4];
                
                svg = svg.datum(dataList);

                yAxis = d3.svg.axis(); 

                xAxis = d3.svg.axis();            

                x = d3.scale
                    .linear()                   
                    .domain([minXValue, maxXValue])
                    .range([0, graphWidth]);

                var factor = (maxXValue-minXValue)/6;
                var xTickValues = [];

                for(var i=0; i<(7);i++){
                    xTickValues.push(i==0?minXValue:minXValue+factor*i);
                }

                xAxis.scale(x)
                     .orient("bottom")
                     .tickValues(xTickValues)                        
                     .tickFormat(function(d) {return Math.round(d);});


                y = d3.scale
                      .linear()
                      .range([graphHeight, 0]); 


                var yTickValues = [0,13,27,40];

                yAxis.scale(y)
                     .orient("left")
                     .tickValues(yTickValues)
                     .tickFormat(function(d, i){
                        var formatted = d.format('years', 0, d);
                        return (i+1) == yTickValues.length ? ('>' + formatted): formatted;
                     });

                y.domain([0, yTickValues[yTickValues.length-1]]);

                svg.selectAll(".x.axis").call(xAxis);
                svg.selectAll(".y.axis").call(yAxis);            
 
                var barIndex = 0;

                var barWidth = (graphWidth / dataList.length);

                svg.append("g")
                    .attr("id","graphBars")
                    .selectAll("rect")
                    .data(dataList)
                    .enter().append("rect")
                    .attr("x", function(d){
                            var thisObj = d3.select(this);
                            thisObj.attr("height", graphHeight -  y(d.y));  
                            thisObj.attr("y", y(d.y));  
                            return (barIndex++)*barWidth;
                    })
                    .attr('width', barWidth)
                    //.attr("fill", "rgba(41,182,246,1)");
                .attr("fill", "#4FC3F7");
                
                

                svg.selectAll(".x.axis")
                   .selectAll(".tick text")
                   .each(function (d, i) {
                        var textElement = d3.select(this);
                        textElement.style("text-anchor",i == 0?"start":i==xTickValues.length-1?"end":"middle");
                        textElement.attr("class", "normalText");     
                   }); 


                svg.selectAll(".y.axis").selectAll("text").each(function(){                    
                    d3.select(this).attr("class", "normalText");                    
                });

                var y2 = d3.scale
                       .linear()
                       .domain([0, yTickValues[yTickValues.length-1]])
                       .range([graphHeight, 0]);  
                
                var graphWidthLimit = 1359;
                /* ============Start code edited by Anuj================== */
                var documentWidth = $(document).width();
                var is_pdf = $("#is_pdf").val();
                if(is_pdf == 1){
                    documentWidth = 1625;
                }
                
                /* ============End code edited by Anuj================== */
                var y2Axis = d3.svg
                            .axis()
                            .scale(y2)
                            .tickValues([userInvestmentHorizon])
                            .tickFormat(function(d, i){
                                var label = y2AxisLabel;
                                if (documentWidth < graphWidthLimit){
                                    label = y2AxisLabel.replace('retirement', 'ret.');
                                }
                                return label;
                            })
                            .orient("right");

                svg.append("g") 
                    .attr("class", "y axis")
                    .attr("id", "y2Axis")
                    .attr("transform", "translate(" + graphWidth + " ,0)")   
                    .call(y2Axis)
                    .selectAll(".tick text")
                    .each(function(){              
                        d3.select(this).attr("class", "smallerText"); 
                    })
                    .call((new String).wrap, 10)
                    .attr("transform", "translate(15, -35)");
                
                if (documentWidth >=graphWidthLimit){
                    svg.append("text")
                        .attr("id", "yearText")
                        .attr("x", -85)
                        .attr("y", graphHeight / 2)
                        .attr("class", "smallerText")
                        .attr("dy", ".35em")
                        .text("YEARS");
                }
                

                if (maxYValue == 0){
                    svg.append("rect")
                        .attr("x", "0")
                        .attr("y", graphHeight-2)
                        .attr("width", graphWidth)
                        .attr("height", 1)
                        .attr("id", "graphPath")
                        .attr("class", "graph_path");     
                }

                
                svg.append("line")
                   .attr("id", "line1")
                   .attr("x1", 0)
                   .attr("x2", graphWidth)                   
                   .attr("stroke", "rgba(1,87,155,1)")
                   .attr('stroke-width', '0.5px')
                   .attr("transform", "translate(" + 0 + "," + y(userInvestmentHorizon) + ")");                

                
                clearTimeout(timer);
                timer = setTimeout(function(){
                    calculateBubbleList(dataList);
                }, 500);
                
                function calculateBubbleList(dataList){
                    var regionStartDate = new Date(graphData.regionStartDate);
                    $("#activeAreas").remove();
                    var activeAreaBarIndex = 0;
                    var barWidth = (graphWidth / dataList.length);
                    svg.append("g")
                        .on("mouseleave",function(){
                            postal.channel().publish("graphService.graph.hide.bubble");
                        })
                        .attr("id","activeAreas")
                        .selectAll(".rect")
                        .data(dataList)
                        .enter()
                        .append("rect")
                        .attr("x", function(d){
                            var thisObj = d3.select(this);
                            thisObj.attr("y", 0);  
                            thisObj.attr("height", graphHeight);  
                            thisObj.attr("width", barWidth)
                            thisObj.attr("fill", "transparent")
                            return (activeAreaBarIndex++)*barWidth;
                        }).on("mouseover",function(d, i) {
                            var regionStartDate = new Date(graphData.regionStartDate);
                            var value = dataList[d.x];
                            var dataStartDate = new Date(regionStartDate.setMonth(value.x));
                            var years = Math.round(value.y);
                            years = years>=40 ? ('>' + years) : years;
                            postal.channel().publish("graphService.graph.show.bubble", {text:years+"-yrs." + 
                                                                                        " starting" + " " 
                                                                                        + d3.time.format("%m/%y")(dataStartDate),
                                                                                        xPosition: d3.mouse(this)[0]});
                        });      
                }
                
                yearList = yearList.sort(function(a, b) {return a - b;});
         
                var median = (yearList[Math.round(yearList.length/2)]).toFixed(0);
            
                svg.append("text")
                    .attr("id", "medianText")
                    .attr("class", "lightText")
                    .text("Median: " + median + " years")
                    .attr("y", -13)
                    .attr("x", function (){
                        return (graphWidth - d3.select(this).node().getComputedTextLength())/2
                    })

                
                postal.channel().publish("graphService.graph.done");
            });
             
        }
     
     
        function calculateDataList(monthlyContribution,
                                  initialInvestment,
                                  savingsGoal,
                                  regionStartDate,
                                  startingCapitalMap,
                                  contributionMap){
 
            var endValue;
            var lineDataList  = [];            
            var incremental = 1;
           
            var maxValue;
            var minXValue;
            var maxXValue
            var startingCapital;
            var contribution;
            var maxYearLength = Object.keys(startingCapitalMap).length;
            var maxDataLength = startingCapitalMap[40].length;
            var monthIndex = 0;
            var yearList = [];
            for(var dataIndex=0; dataIndex < maxDataLength; dataIndex ++){
                
                for(var yearIndex=1; yearIndex <= maxYearLength; yearIndex++){
                    startingCapital = startingCapitalMap[yearIndex][dataIndex];
                    contribution = contributionMap[yearIndex][dataIndex];  
                    endValue = (startingCapital * initialInvestment) + 
                               (contribution * monthlyContribution);
                    if (endValue < savingsGoal || yearIndex == maxYearLength){                    
                        lineDataList.push({x:dataIndex, y:yearIndex});
                        yearList.push(yearIndex);
                        break;
                    }
                }
            }
            minXValue = new Date(regionStartDate).getFullYear();
            maxXValue = new Date(new Date(regionStartDate).setMonth(regionStartDate.getMonth() + dataIndex)).getFullYear();
            
            return [lineDataList,  minXValue, maxXValue, 40, yearList];
        }    
    
        function setSubtitle(){     
            var graphWidthLimit = 1359; 
            /* ============Start code edited by Anuj================== */
            var documentWidth = $(document).width(); 
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                documentWidth = 1625;
            }
            /* ============End code edited by Anuj================== */
            var graphSubtitle = context.graphSubtitle.normal;
            if  (documentWidth<graphWidthLimit){
                graphSubtitle = context.graphSubtitle.abbreviate
            }
            $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                     ({regionStartYear:localStorage.getItem("regionStartYear")}));
        }
     
        function removeElements(){                 
            $("#graphBars").remove();
            $("#graphPath").remove();
            $("#y2Axis").remove();
            $("#activeAreas").remove();            
            $("#line1").remove();
            $("#yearText").remove();
            $("#medianText").remove();
            $("#portfolioLabels").remove(); //  ## ANUJ CODE ##
            
        }    

        function finalize(){
            removeAxis();
            xAxisTitle.html("");
            removeElements();
            context = null;     
            y2AxisLabel = null;
            clearTimeout(timer);
            timer = null;
        }
     
        function branchChanged(){        
            finalize();
        }
     
        function graphChanged(){
            finalize();
        }
     
    
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            /* ============Start code edited by Anuj================== */
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                containerWidth = 980;
                yPos = 50;
                graphHeight = (600 * 0.55);  

            } 
            /* ============End code edited by Anuj================== */
            graphWidth = (containerWidth * 0.80);
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
    
        function removeAxis(){
            svg.selectAll(".x.axis").remove();
            svg.selectAll(".y.axis").remove();
        }
    
        function addAxis(){
            svg.append("g").attr("class", "x axis");
            svg.append("g").attr("class", "y axis");
        }
    
        function init(lang){
            addAxis();
            calculateGraphDimensions();
            context = messages;
            xAxisTitle.html(context.xAxisTitle);
            y2AxisLabel = context.y2AxisTitle;
            /* ============Start code edited by Anuj================== */
            var is_pdf = $("#is_pdf").val();
            if(is_pdf == 1){
                var years= $("input[name=yrs]:checked").val();//temp
                y2AxisLabel = (context.y2AxisTitle+ '\n(' +years+'-yrs.)'); 
            } 
            /* ============End code edited by Anuj================== */
            $(".graph_title").html(context.graphTitle);            
            postal.channel().publish("graphGallery.graph.initiated");
        }    

        return {
            init:               init,
            graph:              graph,
            graphChanged:       graphChanged,
            branchChanged:      branchChanged,
            calculatePortfolioLongevity:  calculateDataList
        };
 
    }
);