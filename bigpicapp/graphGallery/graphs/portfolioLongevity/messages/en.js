 
define({

    graphTitle  : "Portfolio Longevity",
    graphSubtitle: {
        normal: "Number of years portfolio lasted, by retirement start date",
        abbreviate: "No. of yrs. portfolio lasted, by retirement start date"
    },
    xAxisTitle: "RETIREMENT STARTING IN...",
    y2AxisTitle: "Your retirement length"

    
});

    