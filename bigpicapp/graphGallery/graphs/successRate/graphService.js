define (['postal', 'graphGallery/graphs/successRate/messages/en','graphGallery/graphs/successRate/messages/en_Pdf'], function(postal, messages,pdf_messages){ 
    //  ## ANUJ CODE (In Define(Above Line) we added one new message file (en_Pdf) in same directory for pdf message layout )##
    var xAxisElement = $('.x-text').clone();
    var localContext; 
    var is_pdf = $("#is_pdf").val(); //  ## ANUJ CODE ##
    function graph(graphData){ 
        removeElements(); 
        var documentWidth = $(document).width(); 

        /* ============Start code edited by Anuj================== */

        var is_pdf = $("#is_pdf").val(); 
        if(is_pdf == 1){
            documentWidth = 1625;
        } 

        /* ============End code edited by Anuj================== */
        var percent = graphData.successRate*100; 
    
        var createGradient=function(svg,id,color1,color2){
            //alert(id);
            var defs = svg.append("svg:defs");
            var red_gradient = defs.append("svg:linearGradient")
                    .attr("id", id)
                    .attr("x1", "0%")
                    .attr("y1", "0%")
                    .attr("x2", "50%")
                    .attr("y2", "100%")
                    .attr("spreadMethod", "pad");

            red_gradient.append("svg:stop")
                    .attr("offset", "50%")
                    .attr("stop-color", color1)
                    .attr("stop-opacity", 1);

            red_gradient.append("svg:stop")
                    .attr("offset", "100%")
                    .attr("stop-color", color2)
                    .attr("stop-opacity", 1);
        };

      //  var percent = 65;

        var ratio=percent/100;


        var pie=d3.layout.pie()
                .value(function(d){return d})
                .sort(null);
        
        var w=300,h=300;
        var outerRadius=(w/2)-10;
        var innerRadius=118;
        var textFontSize = '70px';
        var textDy = 27;
        var lineHeight = '1.7em';
       
        // We put the code in If Else condition for pdf view. In if condition prior code is used and in Else condtion we added new code
        var is_pdf = $("#is_pdf").val();
        if(is_pdf != 1){ //  ## ANUJ CODE ##

            // Old Code
            svg = svg.attr({          
                class:'',
                viewBox: "",        
                transform: 'translate('+($(svgArea).width()  -  (svg.attr('width'))) / 2 + ',230)'
            });
             if (documentWidth <= 1799){
                w=280;
                h=280;
                outerRadius=(w/3);
                innerRadius=110;
                textFontSize = '54px';
                textDy = 20;
                lineHeight = '1.4em';
                svg.attr('transform', 'translate('+($(svgArea).width()  -  (svg.attr('width'))) / 2 + ',153)');
             
            }
        }else{

            // New Code //  ## ANUJ CODE  Start##
            w=280;
            h=280;
            outerRadius=(w/3);
            innerRadius=110;
            textFontSize = '54px';
            textDy = 20;
            lineHeight = '1.4em';
            svg.attr('transform', 'translate('+(980) / 2 + ',153)');
            //  ## ANUJ CODE  End ##
        }
        
        /*console.log(graphData.userInput);
        console.log(graphData.endValueList.length);*/

        setGraphTitles(graphData.userInput.getInvestmentHorizon(), graphData.endValueList.length, documentWidth, lineHeight);

        // var color = ['black','blue','red'];
        var color = ['#efefef','#81c784','#eeeeee'];
        
        createGradient(svg,'gradient',color[1],color[1]);

        var arc=d3.svg.arc()
                .innerRadius(innerRadius)
                .outerRadius(outerRadius)
                .startAngle(0)
                .endAngle(2*Math.PI);

        var arcLine=d3.svg.arc()
                .innerRadius(innerRadius)
                .outerRadius(outerRadius)
                .startAngle(0);

        var pathBackground=svg.append('path')
                .attr({
                    id: "path1",
                    d:arc
                })
                .style({
                    fill:color[2]
                });

        var pathChart=svg.append('path')
                .datum({endAngle:0})
                .attr({
                    id: "path2",
                    d:arcLine
                })
                .style({
                    fill:'url(#gradient)'
                });

        var middleCount=svg.append('text')
                .datum(0)
                .text(function(d){
                    return d+'%';
                })
            .attr({
                id: "text",
                class:'middleText',
                'text-anchor':'middle',
                dy:textDy
            })
            .style({
                fill:'#424242',
                'font-size': textFontSize
            });
 
        var oldValue=0;
        var arcTween=function(transition, newValue,oldValue) {

            /*console.log(newValue);
            console.log(oldValue);*/

            transition.attrTween("d", function (d) {
                var interpolate = d3.interpolate(d.endAngle, ((2*Math.PI))*(newValue/100));
                var interpolateCount = d3.interpolate(oldValue, newValue);
                return function (t) {
                    d.endAngle = interpolate(t);
                    var value =Math.floor(interpolateCount(t));
                    var dx;
                    var pDx=30;
                    middleCount.text(value);
                    //alert(value);
                    if (value<100){
                        dx=-12;
                    }else{
                        dx=-16;
                        pDx=46;
                    }
                    middleCount.attr('dx',dx+'px');
                    
                    
                    svg.append('text')
                        .datum(0)
                        .text(function(d){
                            return '%';
                        })
                        .attr({
                            id: "percentageSymbol",
                            dy:textDy, dx:(dx+pDx)
                        })
                        .style({
                            fill:'#424242','font-size': '41px'
                        });
                    
                    return arcLine(d);
                };
            });
        };

        pathChart.transition()
                 .duration(0)
                 .ease('cubic')
                 .call(arcTween,percent,0);
        //console.log(arcTween);
        
        postal.channel().publish("graphService.graph.done");

    } 
   
    function getSelectedPortfolioIdList(){
        return JSON.parse(localStorage.getItem("portfolioGraphIdList"));
    }

    function removeSelectedPortfolioIdList(){
        localStorage.removeItem("portfolioGraphIdList");
    }  

  

    function removeElements(){
        $("#path1").remove();
        $("#path2").remove();
        $("#circle").remove();
        $("#text").remove();        
        $("#graphText1").remove();       
        $("#percentageSymbol").remove();       
        $("#portfolioLabels").remove();      //  ## ANUJ CODE ##
        
    }

    function finalize(){
        removeElements();   
        localGraphData = null;
        context = null;
        localContext = null;
        xAxisTitle.html("");
    } 

    function branchChanged(){
        finalize();
    }

    function graphChanged(){
        finalize();
    } 

    function setGraphTitles(invHorizon, rollingPeriodCount, documentWidth, lineHeight){    

        var theTemplate = Handlebars.compile($('.helpwrap').html());

        //console.log(invHorizon);

        xAxisTitle.css({'line-height': lineHeight});
        xAxisTitle.css({'font-size': '17px'});

        xAxisTitle.html(Handlebars.compile(localContext.xAxisTitle)
                        ({investmentHorizon:invHorizon, 
                          rollingPeriodCount:rollingPeriodCount,
                          regionStartYear:localStorage.getItem("regionStartYear")})); 
        
        var graphWidthLimit = 1359;
        
        var graphSubtitle = localContext.graphSubtitle.normal;
        
        if  (documentWidth<graphWidthLimit){
            graphSubtitle = localContext.graphSubtitle.abbreviate
        }
        
        $(".graphSubTitle").html(Handlebars.compile(graphSubtitle)
                                ({regionStartYear:localStorage.getItem("regionStartYear")}));


    }
                        
    function init(lang, branchCode){
        /* ============Start code edited by Anuj================== */
        var is_pdf_en = $("#is_pdf").val();
        if(is_pdf_en == 1){
            localContext = pdf_messages[branchCode];
        }else{ 
            localContext = messages[branchCode];
        }
        /* ============End code edited by Anuj================== */
        //console.log(localContext);
        $(".graph_title").html(localContext.graphTitle);
        postal.channel().publish("graphGallery.graph.initiated");
 
    }

    return {
        init:               init,
        graph:              graph,
        graphChanged:       graphChanged,
        branchChanged:      branchChanged
    };

        
});



