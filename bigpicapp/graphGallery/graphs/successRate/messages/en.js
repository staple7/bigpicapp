 
define({
    

    inRetirement: {
        graphTitle:     "Odds of Success",
        graphSubtitle:  {
            normal: "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle:     "<span style='color:black;font-size: 19px;'>Odds of Success: </span><div style='margin-top:8px'>The proportion of historical {{investmentHorizon}}-year periods* at the end of which the portfolio's value was equal to or greater than the stated amount of legacy capital, provided the inputs at left.</div><div style='margin-top:13px'>* <em>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>.</div>"
    },
    savingForOneTime : {        
        graphTitle:     "Odds of Success",
        graphSubtitle:  {
            normal: "Historical frequency at which portfolio met savings goal, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio met goal, {{regionStartYear}} – present"
        },
        xAxisTitle:     "<span style='color:black;font-size: 19px;'>Odds of Success: </span><div style='margin-top:8px'>The proportion of historical {{investmentHorizon}}-year periods* at the end of which the portfolio's value was equal to or greater than the stated savings goal, provided the inputs at left.</div><div style='margin-top:13px'>* <em>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>.</div>"
    }
});

