 
define({ 
    
    inRetirement: {
        graphTitle:     "Odds of Success",
        graphSubtitle:  {
            normal: "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio ended above legacy capital, {{regionStartYear}} – present"
        },
        xAxisTitle:     "<span style='color:black;font-size: 19px;'>Odds of Success: </span><div style='margin-top:8px'>The proportion of historical {{investmentHorizon}}-year periods* at the end of which</br> the portfolio's value was equal to or greater than the stated</br> amount of legacy capital, provided the inputs.</div><div style='margin-top:13px;color:#777;'>* <em style='color:#777;'>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>.</div>"
    },
    savingForOneTime : {        
        graphTitle:     "Odds of Success",
        graphSubtitle:  {
            normal: "Historical frequency at which portfolio met savings goal, {{regionStartYear}} to present",
            abbreviate: "Historical frequency at which portfolio met goal, {{regionStartYear}} – present"
        },
        xAxisTitle:     "<span style='color:black;font-size: 19px;'>Odds of Success: </span><div style='margin-top:8px'>The proportion of historical {{investmentHorizon}}-year periods* at the end of which</br> the portfolio's value was equal to or greater than the stated</br> savings goal, provided the inputs.</div><div style='margin-top:13px;color:#777;'>* <em  style='color:#777;'>There have been {{formatThousands rollingPeriodCount}} rolling {{investmentHorizon}}-year periods since {{regionStartYear}}</em>.</div>"
    }
});

