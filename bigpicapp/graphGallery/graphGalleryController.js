define (['graphGallery/graphGalleryView', 
         'postal'], 

    function(graphGalleryView, postal){

            
    
        var channel = postal.channel();    
        var graphData = {};
        var currentGraphCode;
        var currentBranchCode;
        var graphTimer; 
        var currentGraphService;
    
        var graphServiceCodeMap = {//TEMP; here
            keyPrinciples: ["growthOf1k", ""],
            inRetirement: ["successRate", 
                            "successRateByWithdrawalRate",
                           "bestMedianAndWorstTrajectory",
                           "safeWithdrawalRates", 
                           "portfolioLongevity", 
                           "mountain",
                           "minimumInitialCapital",
                           "portfolioComparison"],  
          
            savingForOneTime: ["successRate",                              
                               "successRateByHorizon",
                               "bestMedianAndWorstTrajectory",
                               "mountain",
                               "minimumSavingRate",
                               "portfolioComparison"]
          
        }
        
        
        
    
        String.prototype.wrap = function wrap(text, width) {
          text.each(function() {
            var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.1, // ems
                y = text.attr("y"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");

            while (word = words.pop()) {
              line.push(word);
              tspan.text(line.join(" "));
              if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                  
//                if (word.length>10){
//                    word = word.substr(0,10)+"...";              
//                }

                tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);

              }
            }
          });
        }        
     
        Array.prototype.calculateTicks = function calculateTicks(type, minValue, maxValue, minInputValue)       {
            var list = [];       



            var isPercentage = (type == 'percentage');
            if (minValue == maxValue){
                if (minValue === 0){
                    if (isPercentage){
                        list = [0, 0.01, 0.02];
                    }else{
                        list = [0, 500, 1000];
                    }
                }else{
                    var midValue = (maxValue / 2);
                    if (isPercentage){
                        if (midValue < 0.01){
                            midValue = null;
                        }
                    }else{
                        if (midValue < 1){
                            midValue = null;
                        }
                    }
                    if (midValue){
                        list = [0, midValue, maxValue];
                    }else{
                        list = [0, maxValue];
                    }
                }
            }else{
                var minDiff = 0.001;
                var factor = 0;
                var room = minValue * 0.05;
                var newMinValue = minValue - room;
                if (newMinValue>=minInputValue) minValue = newMinValue;
                var numberOfTicks = 3;
                for(; factor<minDiff; numberOfTicks--){
                    factor=(maxValue-minValue)/numberOfTicks;
                } 
                numberOfTicks += 2;
                for(var i=0; i<(numberOfTicks);i++){
                    list.push(i==0?minValue:minValue+factor*i);
                }
            }
        
            return list;
        }

        channel.subscribe("inputController.modality.changed", function(isSaving){
            branchCode = (isSaving ? "savingForOneTime" : "inRetirement");
            if (branchCode == 'keyPrinciples') return;
            currentBranchCode = branchCode;
            currentGraphService.branchChanged();
            // console.log(currentBranchCode);
            // console.log(graphServiceCodeMap[currentBranchCode]);
            graphGalleryView.modalityChanged(graphServiceCodeMap[currentBranchCode].length);
            initGraph(0);
            
        });
        

     
        channel.subscribe("graphGallery.graph.changed", function(graphId){ 
            currentGraphService.graphChanged();
            // console.log("currentBranchCode = "+currentBranchCode);
            // console.log("grPHID="+graphId);
            initGraph(graphId)
            // get_html
            
        });
        
    
        channel.subscribe("graphGallery.graph.initiated", function(){
            
            //currentGraphService.graph(graphData);
            channel.publish("inputController.calculate.output");
        });  
     
        channel.subscribe("graphService.graph.done", function(){
            showLoadingGif(false);
            setTimeout(function(){
                $('.loaderPageGraphic').fadeOut("slow");
            },2000);

        });

        channel.subscribe("inputController.output.set", function(data){
            showLoadingGif();
            clearTimeout(graphTimer);
            graphTimer = setTimeout(function(){
                graphData = $.extend(graphData, data);
                graphData.rollingPeriodCount = data.endValueList.length;
                currentGraphService.graph(graphData);
            }, 300);
        });
    
    
        function initGraph(graphId, time){
            // showLoadingGif();
            // console.log(localStorage);
            clearTimeout(graphTimer);
            graphTimer = setTimeout(function(){
                currentGraphCode = graphServiceCodeMap[currentBranchCode][graphId];
                // console.log('graphGallery/graphs/' + currentGraphCode + '/graphService');
                // console.log("currentGraphCode", currentGraphCode);
                require(['graphGallery/graphs/' + currentGraphCode + '/graphService'], 
                        function(graphService){
                            currentGraphService = graphService;   
                            channel.publish("graphGalleryController.current.graph", graphId);
                            currentGraphService.init(localStorage.getItem("lang"), currentBranchCode, graphData);
                });
            }, 300);
        }

        /* ============Start code edited by Anuj================== */
        channel.subscribe("graphGallery.graph.changed1", function(graphId){ 
            currentGraphService.graphChanged(); 
            initGraph1(graphId) 
        });

        function initGraph1(graphId, time){
            // showLoadingGif();
            // console.log(localStorage);
            clearTimeout(graphTimer);
            graphTimer = setTimeout(function(){
                currentGraphCode = graphServiceCodeMap[currentBranchCode][graphId];
                // console.log('graphGallery/graphs/' + currentGraphCode + '/graphService');
                // console.log("currentGraphCode", currentGraphCode);
                require(['graphGallery/graphs/' + currentGraphCode + '/graphService'], 
                        function(graphService){
                            currentGraphService = graphService;   
                            // channel.publish("graphGalleryController.current.graph", graphId);
                            currentGraphService.init(localStorage.getItem("lang"), currentBranchCode, graphData);
                });
            }, 300);
        }    

        /* ============End code edited by Anuj================== */
        
        channel.subscribe("branchesView.slider.slipped", function(){
            showLoadingGif();
        });    

    
        function loadModule(branchCode){ 
            
            currentBranchCode = branchCode;
            // console.log("loadModule= "+branchCode);
            // console.log("loadModule length= "+graphServiceCodeMap[currentBranchCode].length);
            // console.log(graphServiceCodeMap);
            graphGalleryView.start(graphServiceCodeMap[currentBranchCode].length);
        };
 
        channel.subscribe("graphGalleryController.getGraphNameTitle", function(params){
            var code = graphServiceCodeMap[currentBranchCode][params.graphIndex];               
            require(['graphGallery/graphs/' + code + '/messages/en'], 
                    function(messages){
                        var msg = messages[currentBranchCode];
                        if (msg){
                            msg = msg.graphTitle;
                        }else{
                            msg = messages.graphTitle
                        }
                        params.callback(msg);
            });
            
        });
    
    
        channel.subscribe("graphGalleryController.regraph", function(){
            //showLoadingGif();

                graphGalleryView.resize();
                currentGraphService.graph(graphData);
         
            
        });
    
        channel.subscribe("app.screen.resized", function(){
           
            graphGalleryView.resize();
            currentGraphService.graph(graphData);
        });

        function isSWRCurrentGraph(){        
            return (currentGraphCode=='safeWithdrawalRates');
        }
     
        var rendered = channel.subscribe("graphGallery.template.rendered", function(){
            initGraph(0);            
            rendered.unsubscribe();
            rendered = null;
        }); 

        var rendered1 = channel.subscribe("graphGallery.template.rendered1", function(){
                      
            console.log('rendered1');
            console.log(rendered1);
            rendered1.unsubscribe();
            initGraph1(0);  
            rendered1 = null;
        });       


        function showLoadingGif(show){
            return graphGalleryView.showLoadingGif(show);
        }
    
        function showGraph(show){
            graphGalleryView.showAllGraphElements(show);
        }
    
        return {
            loadModule:          loadModule, 
            isSWRCurrentGraph:   isSWRCurrentGraph,
            showLoadingGif:      showLoadingGif,
            showGraph:           showGraph
            
        };

    }
);
