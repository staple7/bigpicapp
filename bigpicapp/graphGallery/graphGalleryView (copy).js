
    var graphWidth;
    var graphHeight;
    var svg, x, y, yAxis, xAxis;
    var yAxis = d3.svg.axis(); 
    var xAxis = d3.svg.axis();

    var xAxisTitle;
    var yAxisTitle;
    var y2AxisTitle;

    var graphSvg;
    var currentGraphIndex = 0;
    var maxGraphIndex;
 
    var svgArea = ".svgArea";
    //var mtnBubble = $("#mtnBubble");
    var bubble;
    var bubbleText;
    var bubbleBar;
    var bubbleRect;
    var toolTip;
       
 
  
    define (['postal'], function(postal){
       
        
  
 
        postal.channel().subscribe("graphService.graph.done", function(){
             hideBubble();

        }); 
        
        
        postal.channel().subscribe("graphService.graph.show.bubble", function(data){
//            toolTip.show;
            bubble.moveToFront();
            bubble.attr("transform", "translate("+(data.xPosition-38)+")");
            bubbleRect.attr("fill", "rgba(234,234,234,1)");
            bubbleBar.attr("transform", "translate("+data.xPosition+",0)");
            bubbleBar.attr("height", graphHeight);
            bubbleBar.attr("fill", "gray");
            bubbleText.text(data.text);
            bubbleText.call((new String).wrap, 5);

        });
        
        postal.channel().subscribe("graphService.graph.hide.bubble", function(){
            hideBubble();
        });
        
      
        postal.channel().subscribe("graphService.graph.show.graphSelectorBubble", function(data){
            var words = $.trim(data.text).split(" ");
            if (words.length==2 || words.length==3){
                data.text = data.text.replace(' ', '<br>');
            }
            toolTip.html(data.text);
            toolTip.style("opacity","1");
            var toolTipWidth = parseFloat(toolTip.style("width"));
            toolTip.style("transform","translate("+(data.xPosition-(toolTipWidth*.44))+"px,-170px)");
            

        });
        
        postal.channel().subscribe("graphService.graph.hide.graphSelectorBubble", function(duration){
//            $('.d3-tip').fadeOut(345);
//            toolTip.style("opacity","0");
//            toolTip.style("transition-delay: 3s");
          
            toolTip.transition()
                .duration(!duration?0:duration)
                .style("opacity", "0");
            
        });
        
        function hideBubble(){
            bubbleRect.attr("fill", "transparent");
            bubbleBar.attr("fill", "transparent");
            bubbleText.text("");
        }
        
        Number.prototype.format = function formatNumber(type, decimalPlaces, value){
            return Handlebars.compile("{{formatNumber type decimalPlaces value}}")
                            ({type: type, decimalPlaces: decimalPlaces, value: value});
        }
        
        Handlebars.registerHelper('formatThousands', function(value){
            return d3.format(",")(value);
        });
                  
        Handlebars.registerHelper('formatNumber', function(type, decimalPlaces, value){
            if (type == 'monthYear'){
                var date = new Date(value);//TEMP;
                var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", 
                              "Aug", "Sep", "Oct", "Nov", "Dec" ];
                value = months[date.getMonth()]+'. '+date.getFullYear();
            }else if (type == 'years'){
                value = Math.round(value);
            }else if (type == 'currency'){
                if (value<1){
                    value = Number(value);
                    value = "$" + value.toFixed(2); 
                }else{
                    value = d3.format("$,.3s")(value);
                    value = value.replace('G','B').toUpperCase();
                }
            }else if (type == 'percentage'){
                return (value*100).toFixed(decimalPlaces)+"%";
            }
            return value;
        });

        function start(numberOfGraphs){
            console.log("numberof graph="+numberOfGraphs);
            $.get('graphGallery/graphGalleryTemplate.html?v'+app_version, function(resp){
                $(".graph-area-container").append().html($.parseHTML("<link href='graphGallery/graphGalleryStyle.css?v"+app_version+"' rel='stylesheet' type='text/css'>"+resp));
                

                appendSvg();
                displayGraphSelector(numberOfGraphs);
                setMaxGraph(numberOfGraphs);
                xAxisTitle = $(".xAxisTitle .x-text");
                yAxisTitle = $(".yAxisTitle");
                y2AxisTitle = $(".y2AxisTitle");
                graphSvg  = $(".graphSvg");
                //bindEvents();
                postal.channel().publish("graphGallery.template.rendered");
            });
        }

        function start_pdf(numberOfGraphs){
            console.log("numberof graph="+numberOfGraphs);
            var old_html = $(".graph-area-container1").html();
            console.log("old html ="+ old_html);
            $.get('graphGallery/graphGalleryTemplate.html?v'+app_version, function(resp){
                $(".graph-area-container").append($.parseHTML("<link href='graphGallery/graphGalleryStyle.css?v"+app_version+"' rel='stylesheet' type='text/css'>"+resp));
                 
                
                // graph-area-container

                appendSvg();
                displayGraphSelector(numberOfGraphs);
                setMaxGraph(numberOfGraphs);
                xAxisTitle = $(".xAxisTitle .x-text");
                yAxisTitle = $(".yAxisTitle");
                y2AxisTitle = $(".y2AxisTitle");
                graphSvg  = $(".graphSvg");
                //bindEvents();
                postal.channel().publish("graphGallery.template.rendered");
            });

            // $.get('graphGallery/graphGalleryTemplate.html?v'+app_version, function(resp){
            //     $(".graph-area-container1").append($.parseHTML("<link href='graphGallery/graphGalleryStyle.css?v"+app_version+"' rel='stylesheet' type='text/css'>"+resp));
                 
                
            //     // graph-area-container

            //     appendSvg();
            //     displayGraphSelector(numberOfGraphs);
            //     setMaxGraph(numberOfGraphs);
            //     xAxisTitle = $(".xAxisTitle .x-text");
            //     yAxisTitle = $(".yAxisTitle");
            //     y2AxisTitle = $(".y2AxisTitle");
            //     graphSvg  = $(".graphSvg");
            //     //bindEvents();
            //     postal.channel().publish("graphGallery.template.rendered");
            // });
        }
        
        function bindEvents(){
            $('.graph-arrow').on({
                'click touchstart': function(e){ 
                    e.preventDefault();
                    var element = $(e.target);
                    var className = $(this).attr("class");
                    // alert();
                    console.log("currentGraphIndex",currentGraphIndex);
                    if (className.indexOf("prev")>-1){
                        currentGraphIndex -= 1;
                        if (currentGraphIndex < 0) currentGraphIndex = 0;
                    }else{
                        currentGraphIndex += 1;
                        if (currentGraphIndex > maxGraphIndex) currentGraphIndex = maxGraphIndex;
                    }
                    //console.log("prev", currentGraphIndex);
                    if (currentGraphIndex < maxGraphIndex && currentGraphIndex >= 0){
                        graphChanged(currentGraphIndex);
                    }
                }
            });

            $('.graph-dash').on({
                'click touchstart': function(e){ 
                    e.preventDefault();
                    currentGraphIndex = $(this).index()-1;
                    graphChanged(currentGraphIndex);
                },
                'mouseover': function(e){     
                    var graphIndex;
                    var thisSelector = $(this)[0];
                    $('.graph-dash').each(function(index, element){
                        if (thisSelector === element){
                            graphIndex = index;
                        }
                    });
                    
                    var offset = $(this).offset();
                    postal.channel().publish("graphGalleryController.getGraphNameTitle",{graphIndex:graphIndex,
                                                                                         callback:function(title){
                        postal.channel().publish("graphService.graph.show.graphSelectorBubble",{text:title, 
                                                                                                xPosition:offset.left});
                    }});
                    
                },
                'mouseout': function(e){
                    postal.channel().publish("graphService.graph.hide.graphSelectorBubble");
                }
            });
        }
        function all_graph(){
           console.log('all graph');
           var selectors = '';
            var checked = $('input[name=mode]:checked').val();
            // var port_div = $('input[name=port_div].span:checked').val();
            var port_div = $('input[name=port_div]:checked').next('span').text();
            var ratio = $('input[name=ratio]:checked').val();
            var yrs = $('input[name=yrs]:checked').val();
            var rebal = $('input[name=rebal]:checked').next('span').text();
            var initcap = $("input[name=initcap]").val();
            var legacycap = $("input[name=legacycap]").val();
            var rate = $("#rate").val();

             selectors += '<h2>Key Assumptions</br>';
             selectors += '<h2>Asset Allocation';
            
            // var i = $('.tr-active').attr('data-index');
            var total_markit = $("[data-id='US_5']").val();
            var gov_bond5 = $("[data-id='US_7']").val();

            var large_cap = $("[data-id='US_1']").val();
            var gov_bond10 = $("[data-id='US_6']").val();

            var mid_cap = $("[data-id='US_3']").val();
            var t_bills = $("[data-id='US_8']").val();

            var small_caps = $("[data-id='US_2']").val();
            var global_bond = $("[data-id='A_2']").val();

            var micro_cap = $("[data-id='US_4']").val();
            var gold = $("[data-id='A_3']").val();

            var int_usa = $("[data-id='A_1']").val();


             selectors += '<p>Total Market</p> = '+total_markit;
             selectors += '<p>Large Cap</p> = '+total_markit;
            
            // var gov_bond = $("[data-id='US_7']").val();
            // $("input:first").attr("value", "aaa");
            // console.log("total_markit= "+total_markit);
            // console.log("gov_bond5= "+gov_bond5);
            // console.log("large_cap= "+large_cap);
            // console.log("gov_bond10= "+gov_bond10);
            // console.log("mid_cap= "+mid_cap);
            // console.log("t_bills= "+t_bills);
            // console.log("small_caps= "+small_caps);
            // console.log("global_bond= "+global_bond);
            // console.log("micro_cap= "+micro_cap);
            // console.log("gold= "+gold);
            // console.log("int_usa= "+int_usa);
            if(ratio == 0){
                 var oyr = $("#oyr").val();
                
            }

            if(yrs == 0){
                 var ratio1 = $(".other_ter.smallinput").val();
                
            }

            var header = '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>The Big Picture</title><link rel="stylesheet" type="text/css" href="../style.css"><style> svg{ height:450px!important; }</style></head><body><div class="main_wrap" id="page-one"><div class="withdraw_panel"><h1 class="orange">Portfolio and Withdrawal Discussion</h1><div class="description_block"><ul><li>Client Name</li><li>The Advisor Name</li><li>25th November, 2018</li></ul><div class="logo"><img src="../logo.png" alt="Logo"></div></div></div><div class="white_global_panel"><h3>Key Assumptions</h3><div class="table_panel"><h4>Asset Allocation</h4><div class="inner_table"><div class="table-left-panel"><ul><li class="t_head"><span class="left_text">Total Market </span> <span class="right_text">'+total_markit+'%</span></li><li><span class="left_text">Large Cap </span> <span class="right_text">'+ large_cap +'%</span></li><li><span class="left_text">Mid Cap  </span> <span class="right_text">'+ mid_cap +'%</span></li><li><span class="left_text">Small Cap</span> <span class="right_text">'+ small_caps +'%</span></li><li><span class="left_text">Micro Cap</span> <span class="right_text">'+ micro_cap +'%</span></li><li class="t_head"><span class="left_text">Int\'l (ex-USA) </span> <span class="right_text">'+ int_usa +'%</span></li></ul></div><div class="table-right-panel"><ul><li class="t_head"><span class="left_text">5-Year Gov. Bonds </span> <span class="right_text">'+ gov_bond5 +'%</span></li><li class="t_head"><span class="left_text">10-Year Gov. Bonds  </span> <span class="right_text">'+ gov_bond10 +'%</span></li><li class="t_head"><span class="left_text">T-Bills</span> <span class="right_text">'+ t_bills +'%</span></li><li class="t_head"><span class="left_text">Global Bonds </span> <span class="right_text">'+ global_bond +'%</span></li><li class="t_head"><span class="left_text">Gold </span> <span class="right_text">'+ gold +'%</span></li></ul></div></div></div><div class="table_panel"><h4>Others</h4><div class="inner_table"><div class="table-full-panel"><ul><li class="t_head"><span class="left_text">"PLANNED RETIREMENT PERIOD (YRS.)"</span><span class="right_text">'+ yrs +'</span> </li><li class="t_head"><span class="left_text">"Initial Capital</span><span class="right_text">'+ initcap +'</span> </li><li class="t_head"> <span class="left_text">"Withdrawal Rate"</span><span class="right_text">'+rate +'%</span></li><li class="t_head"> <span class="left_text">"Legacy Capital"</span><span class="right_text">'+legacycap+'</span></li><li class="t_head"><span class="left_text">"Expense Ratio"</span><span class="right_text">'+ ratio +'%</span> </li><li class="t_head"> <span class="left_text">"Rebalancing"</span><span class="right_text">'+ rebal +' Yr</span></li><li class="t_head"><span class="left_text">"Inflation-adjust"</span><span class="right_text">Yes</span> </li></ul></div></div></div></div>';
            //smallinput other_ter
            // console.log("total_markit= "+total_markit);
            // console.log("ratio= "+ratio);
            // console.log("ratio1= "+ratio1);
            // console.log("initcap= "+initcap);
            // console.log("rate= "+rate);
            // console.log("legacycap= "+legacycap);
            // var port_div = $('input[name=port_div]:checked').closest('span').text();
            // console.log("port_div= "+port_div);
            // console.log("ratio= "+ratio);
            // console.log("rebal= "+rebal);
            // return;

            // var checked = $('input[name=mode]:checked').val();
            // var checked = $('input[name=mode]:checked').val();
            // var checked = $('input[name=mode]:checked').val();
            // var checked = $('input[name=mode]:checked').val();
            if(checked == "retirement"){
                 var count = 8;
            }else{
                 var count = 6;
            }
            console.log(checked); 
            $("#get_html2").html('');
            var i= 0;
            var all_html = '';
            all_html += header;
            var footer = '<div class="main_wrap" id="page-ten"><div class="withdraw_panel"><div class="description_block"><ul><li><strong>Prepared For:</strong>Client Name</li><li><strong>Prepared By:</strong> Advisor Name</li></ul></div></div><div class="white_global_panel"><div class="desclaimer-block"><h5><strong>Disclaimer:</strong></h5><p>The charts and figures contained herein are based on historical total return data beginning January 1, 1926 (U.S.) and January 1, 1935(Canada), and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended.</p><p>The charts and figures contained herein are based on historical total return data beginning January 1, 1926 (U.S.) and January 1, 1935(Canada), and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended.</p><h5><strong>Sources:</strong></h5><p>U.S. Micro Cap Stocks, U.S. Small Cap Stocks, U.S. Mid Cap Stocks, U.S. Large Cap Stocks, U.S. Total Market Stocks, U.S. Treasury Bills, U.S. 5-Year Government Bonds: USA 5-Year Government Bond Total Return Index—Center for Research in Security Prices (CRSP). International Stocks: ex-U.S.A. Total Return Index, U.S. 10-Year Government Bonds: USA 10-Year Government Bond Total Return Index, Canadian Government Bonds: Canada 10-Year Total Return Government Bond Index, Canadian Treasury Bills: Canada Total Return Bills Index, Canadian Stocks: Canada S&P/TSX-300 Total Return Index, Global Bonds: GFD Global USD Total Return Government Bond Index, Gold: Gold Bullion Price-New York (US$/oz)—Global Financial Data, Inc. U.S. Inflation: Consumer Price Index—U.S. Bureau of Labor Statistics and Global Financial Data. Canada Inflation: Statistics Canada.</p></div></div><footer>Page 10 to 10</footer></div></body></html>';
            
            var interval_data = setInterval(function () { 
                if(count == i){
                    clearInterval(interval_data);
                    all_html += footer;
                    // if((count+1) == i){
                        //$("#get_html2").html(all_html);
                        var request = $.ajax({
                            url: "http://bigapp.local/bigpicapp/ajax.php?",
                            type: "POST",
                            data: {id : all_html},
                            success: function(name){ 
                                console.log(name);
                            },
                          // dataType: "html"
                        });
                    // }
                    
                    return;
                }
                    graphChanged(i); 
                    // if(i != 0){

                        setTimeout(function () {
                        var html = '<div class="page_break">';
                        html += $(".svgArea").html();
                        // var html2 = $('.svgArea')[0].outerHTML
                    
                        var div_id  = "<div id='count_"+i+"'></div>";
                        var svg2 = $("#graphSvg").html();
                        // html.text(text.replace('graphSvg', 'graphSvg'+i));
                        // html.text(html.text().trim().replace('graphSvg', 'graphSvg'+i));
                        // var svg2 = $("#graphSvg").html();
                        // var svg = document.getElementById('graphSvg'); // or whatever you call it
                        // var serializer = new XMLSerializer();
                        // var str = serializer.serializeToString(svg); 
                        html += '</div>';
                        all_html += html; 
                         
                     // $(div_id).appendTo("#get_html");
                     // $(html).appendTo('#count_'+i);
                     // $('<div id="count_'+i+'"></div>').appendTo("#get_html");
                     // console.log('#count_'+i);
                     // $('#count_'+i).append(html);
                    html = "";
                    // var html_add = $("#get_html").html();
                    }, 750);
                    // var div_id  = '<div id="count_'+i+'"></div>';
                   // console.log(svg2);
                    
                    // console.log(html2);
                     // console.log("======new============");
                    
                    // return;
                // }
                

            // console.log(html_add);
            console.log("==================");
                    // }
                    
               
                i += 1;
            },890);


        }

        function graphChanged(index){
            
            
            showLoadingGif();

            $('.graph-dash.is_active').removeClass('is_active').addClass('disabled');
            $('.graph-dash').eq(index).removeClass('disabled').addClass('is_active');
            //prev graph-arrow
           
            postal.channel().publish("graphGallery.graph.changed", index);
            
             /*
			if you are on the first page, don't show "previous"
			on the last page don't show "next"
			*/
            var mode='';//you are on KEY page
        	if($('.nav > a.is_active').hasClass('with'))//if on withdrawal page
            	mode = $('input[name=mode]:checked').val();
            $('.prev').removeClass('disabled');
            //console.log("----------------- .pageset." + mode + '_pages .prev + .graph-dash.is_active');
//			if($(".pageset." + mode + '_pages .prev + .graph-dash.is_active').length)
//			{
//				$(".pageset." + mode + '_pages .prev').addClass('disabled');
//			}
            if(index==0)
			{
				$('.prev').addClass('disabled');
			}
			$('.next').removeClass('disabled');
			if((index + 1) == maxGraphIndex)
			{
				$('.next').addClass('disabled');
			}	
                   
        
            setTimeout(function(){
                // console.log('here');
                postal.channel().publish("graphService.graph.hide.graphSelectorBubble",1000);
            },1000);
        }
        
        
        function setMaxGraph(count){
            maxGraphIndex = count;
        }
        
		function modalityChanged(graphCount){
            console.log(graphCount);
            displayGraphSelector(graphCount);
            setMaxGraph(graphCount);
            currentGraphIndex = 0;
        }
			
        function displayGraphSelector(count){
         
           var parent = $('.pagenavwrap .graphnav');

                            
            var html = "<div class='prev graph-arrow disabled'><svg height='37' viewBox='0 0 24 24' width='37' xmlns='http://www.w3.org/2000/svg'><path d='M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z'/></svg></div>";
            
            
            var activeDash = "&nbsp&nbsp;<div class='graph-dash is_active'>&mdash;&nbsp;</div>";
            var dash = "<div class='graph-dash'>&mdash;&nbsp;</div>";
            
            for (var i=0; i<count; i++){
              html += (i==0?activeDash:dash);   
            }
            
            html += "<div class='next graph-arrow'><svg  height='37' viewBox='0 0 24 24' width='37' xmlns='http://www.w3.org/2000/svg'><path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z'/></svg></div>";

            parent.html(html);
            bindEvents();
        }
        
        function selectDash(graphIndex){
            
        }
        
        d3.selection.prototype.moveToFront = function() {
          return this.each(function(){
            this.parentNode.appendChild(this);
          });
        };
        
        function appendSvg(){
            console.log('appendSvg');
            svg = d3.select('.chartspace')
                    .append("svg")
                    .attr("width", "100%")
                    .attr("height", "120%")
                    .attr("id", "graphSvg")
                    .attr("xmlns", "http://www.w3.org/2000/svg")
                    .append("g");
            
            svg.append("g").attr("id", "mtnBubble");                       
            
            
            bubble = d3.select("#mtnBubble");
            
            bubble.append("rect")
                    .attr("id", "mtnBubbleRect")
                    .attr("height", "48px")
                    .attr('width', "75px")
                    .attr("y", "-49")
                    .attr("fill", "transparent");
            
            bubble.append("text")
                    .attr("id", "mtnBubbleText")                
                    .attr("dy", "-1.2")
                    .attr("fill", "black")         
                    .attr("style", 'font-size:12px')
                    .attr("text-anchor", "middle")
                    .attr("x", "30")
                    .attr("y", "-30")
                    .attr("transform", "translate(38,-2)");
            
            
            bubbleText = d3.select("#mtnBubbleText");
 
            svg.append("rect")
                .attr("id", "mtnBubbleBar")               
                .attr("y", -1)
                .attr('width', 1)
                .attr("fill", "transparent");
            
            bubbleBar = d3.select("#mtnBubbleBar");
            bubbleRect = d3.select("#mtnBubbleRect");
       
          
            svg.call(d3.tip()
                          .attr("class", "d3-tip")
                          .offset([-8, 0]));
            
            toolTip = d3.select('.d3-tip');
            
        }
        
        
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            graphWidth = (containerWidth * 0.80);
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
        

        function resize(){
            calculateGraphDimensions();
        }

        function showGraphSvg(show){
            graphSvg.stop().animate({'opacity':show == false? 0 : 1}, 0);          
        }
        
        function showAllGraphElements(show){
            var elements = xAxisTitle.add(yAxisTitle)
                                     .add(graphSvg)
                                     .add($('.graphExtraInfoWrap'));
            
            elements.stop().animate({'opacity':show == false? 0 : 1}, 0);
            elements.css({visibility:show == false ? "hidden": "visible"}); 
        }

        function showLoadingGif(show){
            showAllGraphElements(show===undefined?false:!show);
            var element = $('.chartLoaderWrap');
            $('.loaderGraphic').css('display', (show == false)?'none':'block');
        }

      
        
  



        

        return {
            start:                  start,
            start_pdf:              start_pdf,
            all_graph:              all_graph,
            resize:                 resize,        
            showLoadingGif:         showLoadingGif,
            showGraphSvg:           showGraphSvg,
            modalityChanged:        modalityChanged,
            showAllGraphElements:   showAllGraphElements
        };

    });