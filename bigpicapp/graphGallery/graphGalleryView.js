
    var graphWidth;
    var graphHeight;
    var svg, x, y, yAxis, xAxis;
    var yAxis = d3.svg.axis(); 
    var xAxis = d3.svg.axis();

    var xAxisTitle;
    var yAxisTitle;
    var y2AxisTitle;

    var graphSvg;
    var currentGraphIndex = 0;
    var maxGraphIndex;
 
    var svgArea = ".svgArea";
    //var mtnBubble = $("#mtnBubble");
    var bubble;
    var bubbleText;
    var bubbleBar;
    var bubbleRect;
    var toolTip;
       
 
  
    define (['postal'], function(postal){
       
        
  
 
        postal.channel().subscribe("graphService.graph.done", function(){
             hideBubble();

        }); 
        
        
        postal.channel().subscribe("graphService.graph.show.bubble", function(data){
//            toolTip.show;
            bubble.moveToFront();
            bubble.attr("transform", "translate("+(data.xPosition-38)+")");
            bubbleRect.attr("fill", "rgba(234,234,234,1)");
            bubbleBar.attr("transform", "translate("+data.xPosition+",0)");
            bubbleBar.attr("height", graphHeight);
            bubbleBar.attr("fill", "gray");
            bubbleText.text(data.text);
            bubbleText.call((new String).wrap, 5);

        });
        
        postal.channel().subscribe("graphService.graph.hide.bubble", function(){
            hideBubble();
        });
        
      
        postal.channel().subscribe("graphService.graph.show.graphSelectorBubble", function(data){
            var words = $.trim(data.text).split(" ");
            if (words.length==2 || words.length==3){
                data.text = data.text.replace(' ', '<br>');
            }
            toolTip.html(data.text);
            toolTip.style("opacity","1");
            var toolTipWidth = parseFloat(toolTip.style("width"));
            toolTip.style("transform","translate("+(data.xPosition-(toolTipWidth*.44))+"px,-170px)");
            

        });
        
        postal.channel().subscribe("graphService.graph.hide.graphSelectorBubble", function(duration){
//            $('.d3-tip').fadeOut(345);
//            toolTip.style("opacity","0");
//            toolTip.style("transition-delay: 3s");
          
            toolTip.transition()
                .duration(!duration?0:duration)
                .style("opacity", "0");
            
        });
        
        function hideBubble(){
            bubbleRect.attr("fill", "transparent");
            bubbleBar.attr("fill", "transparent");
            bubbleText.text("");
        }
        
        Number.prototype.format = function formatNumber(type, decimalPlaces, value){
            return Handlebars.compile("{{formatNumber type decimalPlaces value}}")
                            ({type: type, decimalPlaces: decimalPlaces, value: value});
        }
        
        Handlebars.registerHelper('formatThousands', function(value){
            return d3.format(",")(value);
        });
                  
        Handlebars.registerHelper('formatNumber', function(type, decimalPlaces, value){
            if (type == 'monthYear'){
                var date = new Date(value);//TEMP;
                var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", 
                              "Aug", "Sep", "Oct", "Nov", "Dec" ];
                value = months[date.getMonth()]+'. '+date.getFullYear();
            }else if (type == 'years'){
                value = Math.round(value);
            }else if (type == 'currency'){
                if (value<1){
                    value = Number(value);
                    value = "$" + value.toFixed(2); 
                }else{
                    value = d3.format("$,.3s")(value);
                    value = value.replace('G','B').toUpperCase();
                }
            }else if (type == 'percentage'){
                return (value*100).toFixed(decimalPlaces)+"%";
            }
            return value;
        });

        function start(numberOfGraphs){
            // console.log("numberof graph="+numberOfGraphs);
            $.get('graphGallery/graphGalleryTemplate.html?v'+app_version, function(resp){
                $(".graph-area-container").append().html($.parseHTML("<link href='graphGallery/graphGalleryStyle.css?v"+app_version+"' rel='stylesheet' type='text/css'>"+resp));
                

                appendSvg();
                displayGraphSelector(numberOfGraphs);
                setMaxGraph(numberOfGraphs);
                xAxisTitle = $(".xAxisTitle .x-text");
                yAxisTitle = $(".yAxisTitle");
                y2AxisTitle = $(".y2AxisTitle");
                graphSvg  = $(".graphSvg");
                //bindEvents();
                postal.channel().publish("graphGallery.template.rendered");
            });
        }
        
        function bindEvents(){
            $('.graph-arrow').on({
                'click touchstart': function(e){ 
                    e.preventDefault();
                    var element = $(e.target);
                    var className = $(this).attr("class");
                    // alert();
                    // console.log("currentGraphIndex",currentGraphIndex);
                    if (className.indexOf("prev")>-1){
                        currentGraphIndex -= 1;
                        if (currentGraphIndex < 0) currentGraphIndex = 0;
                    }else{
                        currentGraphIndex += 1;
                        if (currentGraphIndex > maxGraphIndex) currentGraphIndex = maxGraphIndex;
                    }
                    console.log("prev", currentGraphIndex);
                    if (currentGraphIndex < maxGraphIndex && currentGraphIndex >= 0){
                        graphChanged(currentGraphIndex);
                    }
                }
            });

            $('.graph-dash').on({
                'click touchstart': function(e){ 
                    e.preventDefault();
                    currentGraphIndex = $(this).index()-1;
                    graphChanged(currentGraphIndex);
                },
                'mouseover': function(e){     
                    var graphIndex;
                    var thisSelector = $(this)[0];
                    $('.graph-dash').each(function(index, element){
                        if (thisSelector === element){
                            graphIndex = index;
                        }
                    });
                    
                    var offset = $(this).offset();
                    postal.channel().publish("graphGalleryController.getGraphNameTitle",{graphIndex:graphIndex,
                                                                                         callback:function(title){
                        postal.channel().publish("graphService.graph.show.graphSelectorBubble",{text:title, 
                                                                                                xPosition:offset.left});
                    }});
                    
                },
                'mouseout': function(e){
                    postal.channel().publish("graphService.graph.hide.graphSelectorBubble");
                }
            });
        } 

        /* ============Start code edited by Anuj================== */

        function all_graph(email_add){ 

            /************ Get All Input Field's Value Here ********************/ 

            var checked = $('input[name=mode]:checked').val();
            // var port_div = $('input[name=port_div].span:checked').val();
            // var port_div = $('input[name=port_div]:checked').next('span').text();
            var port_div = $('input[name=portoption]:checked').next('input').val();
            // console.log(port_div);
            // console.log(port_div1);
            // return;
            var ratio = $('input[name=ratio]:checked').val();
            var yrs = $('input[name=yrs]:checked').val();
            var rebal = $('input[name=rebal]:checked').next('span').text();
            var initcap = $("input[name=initcap]").val();
            var legacycap = $("input[name=legacycap]").val();
            var rate = $("#rate").val();

            var amount_mo = $(".amount-in-dollars").text();
            var mo_label = $(".mo-label").text();
            amount_mo = $.trim(amount_mo);
            mo_label = $.trim(mo_label);
            var mo_amount = amount_mo +' '+ mo_label;
            if(ratio == 0){
                var ratio = $(".other_ter").val();
            }else{
                ratio = ratio+'%';
            }
              
            var total_markit = $("[data-id='US_5']").val();
            var gov_bond5 = $("[data-id='US_7']").val();

            var large_cap = $("[data-id='US_1']").val();
            var gov_bond10 = $("[data-id='US_6']").val();

            var mid_cap = $("[data-id='US_3']").val();
            var t_bills = $("[data-id='US_8']").val();

            var small_caps = $("[data-id='US_2']").val();
            var global_bond = $("[data-id='A_2']").val();

            var micro_cap = $("[data-id='US_4']").val();
            var gold = $("[data-id='A_3']").val();

            var int_usa = $("[data-id='A_1']").val(); 
            
            var inflationadjusted = $('input[name=inflationadjusted]:checked').val();
            if(inflationadjusted){
                inflation = "Yes";
            }else{
                inflation = "No";
            }
            // return;
            if(yrs == 0){
                 var yrs = $("#oyr").val(); 
            }


            /************ Remove % from inpt values ********************/ 

            var port_div1 = port_div.replace(/[^0-9\.]/g, '');
            var ratio1 = ratio.replace(/[^0-9\.]/g, '');
            var rebal1 = rebal.replace(/[^0-9\.]/g, '');
            var initcap1 = initcap.replace(/[^0-9\.]/g, '');
            var legacycap1 = legacycap.replace(/[^0-9\.]/g, '');
            var rate1 = rate.replace(/[^0-9\.]/g, '');
            var total_markit1 = total_markit.replace(/[^0-9\.]/g, '');
            var gov_bond51 = gov_bond5.replace(/[^0-9\.]/g, '');
            var large_cap1 = large_cap.replace(/[^0-9\.]/g, '');
            var gov_bond101 = gov_bond10.replace(/[^0-9\.]/g, '');
            var mid_cap1 = mid_cap.replace(/[^0-9\.]/g, '');
            var t_bills1 = t_bills.replace(/[^0-9\.]/g, '');
            var small_caps1 = small_caps.replace(/[^0-9\.]/g, '');
            var global_bond1 = global_bond.replace(/[^0-9\.]/g, '');
            var micro_cap1 = micro_cap.replace(/[^0-9\.]/g, '');
            var gold1 = gold.replace(/[^0-9\.]/g, '');
            var int_usa1 = int_usa.replace(/[^0-9\.]/g, '');
            port_div = $.trim(port_div);

            var myDate = new Date();
            myDate  = myDate.format('F d, Y');

            /************ PDF Page Html Start Here ********************/
            if(checked == "retirement"){
                var header = '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>The Big Picture</title><link rel="stylesheet" type="text/css" href="../css/style.css"><style>html { zoom: 0.52; }.graphBar { fill:#abe3fd;}.selected{ fill:#03A9F4;}</style><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head><body><div class="main_wrap" id="page-one"><div class="page_layout"><div class="withdraw_panel"><h1 class="orange large-heading">Portfolio and Withdrawal Discussion</h1><div class="description_block"><ul><li><strong>Prepared for:</strong> Client Name</li><li><strong>Prepared by:</strong> The Advisor Name</li><li><strong>Date:</strong> '+ myDate +'</li></ul><div class="logo"><img src="../image/logo.png" alt="Logo"></div></div></div><div class="white_global_panel margin_top"><h3>Key Assumptions</h3><div class="table_panel"><h4>'+ port_div +' Portfolio Asset Allocation</h4><div class="inner_table"><div class="table-left-panel"><ul><li ';
                    if(total_markit1 > 0){
                        header += 'class="t_head"';
                    }
                     
                    header += '><span class="left_text">Total Market Stocks</span> <span class="right_text">'+total_markit+'</span></li><li ';
                    if(large_cap1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Large Cap Stocks</span> <span class="right_text">'+ large_cap +'</span></li><li ';
                    if(mid_cap1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Mid Cap Stocks </span> <span class="right_text">'+ mid_cap +'</span></li><li ';
                    if(small_caps1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Small Cap Stocks</span> <span class="right_text">'+ small_caps +'</span></li><li ';
                    if(micro_cap1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Micro Cap Stocks</span> <span class="right_text">'+ micro_cap +'</span></li><li ';
                    if(int_usa1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Int\'l (ex-USA) Stocks</span> <span class="right_text">'+ int_usa +'</span></li></ul></div><div class="table-right-panel"><ul><li ';
                    if(gov_bond51 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">5-Year Gov. Bonds </span> <span class="right_text">'+ gov_bond5 +'</span></li><li ';
                    if(gov_bond101 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">10-Year Gov. Bonds  </span> <span class="right_text">'+ gov_bond10 +'</span></li><li ';
                    if(t_bills1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">T-Bills</span> <span class="right_text">'+ t_bills +'</span></li><li ';
                    if(global_bond1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Global Bonds </span> <span class="right_text">'+ global_bond +'</span></li><li ';
                    if(gold1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Gold </span> <span class="right_text">'+ gold +'</span></li></ul></div></div></div><div class="table_panel other_table_panel"><h4>Other Inputs</h4><div class="inner_table"><div class="table-full-panel"><ul><li class="t_head"><span class="left_text">Planned Retirement Period (Yrs.)</span><span class="right_text">'+ yrs +'</span> </li><li class="t_head"><span class="left_text">Initial Capital</span><span class="right_text">'+ initcap +'</span> </li><li class="t_head"> <span class="left_text">Withdrawal Rate</span><span class="right_text">'+rate +' ('+ mo_amount +')</span></li><li class="t_head"> <span class="left_text">Required Legacy Capital</span><span class="right_text">'+legacycap+'</span></li><li class="t_head"><span class="left_text">Expense Ratio</span><span class="right_text">'+ parseFloat(ratio1).toFixed(1) +'%</span> </li><li class="t_head"> <span class="left_text">Rebalancing</span><span class="right_text">'+ rebal +'</span></li><li class="t_head"><span class="left_text">Inflation-adjust spending</span><span class="right_text">'+ inflation +'</span> </li></ul></div></div></div><div class="text-panell"><h3>Notes</h3><div class="line"></div><div class="line"></div><div class="line"></div><div class="line"></div></div></div><div class="page_break"></div></div>';
            
                    var numItems = $('.graph-dash').length
                    if(numItems <= 0){
                        var count = 8;
                    }else{
                        var count = numItems;
                    }
                    console.log("numrows_retirenment= "+ count);
                
            }else{
                 //var count = 6;
                var numItems = $('.graph-dash').length
                if(numItems <= 0){
                    var count = 8;
                }else{
                    var count = numItems;
                }
                console.log("numrows_saving= "+ count);
                 var header = '<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>The Big Picture</title><link rel="stylesheet" type="text/css" href="../css/style.css"><style>html { zoom: 0.52; }.graphBar { fill:#abe3fd;}.selected{ fill:#03A9F4;}</style><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head><body><div class="main_wrap" id="page-one"><div class="page_layout"><div class="withdraw_panel"><h1 class="orange large-heading">Portfolio and Savings Discussion</h1><div class="description_block"><ul><li><strong>Prepared for:</strong> Client Name</li><li><strong>Prepared by:</strong> The Advisor Name</li><li><strong>Date:</strong> '+ myDate +'</li></ul><div class="logo"><img src="../image/logo.png" alt="Logo"></div></div></div><div class="white_global_panel margin_top"><h3>Key Assumptions</h3><div class="table_panel"><h4>'+ port_div +' Portfolio Asset Allocation</h4><div class="inner_table"><div class="table-left-panel"><ul><li ';
                    if(total_markit1 > 0){
                        header += 'class="t_head"';
                    }
                     
                    header += '><span class="left_text">Total Market Stocks</span> <span class="right_text">'+total_markit+'</span></li><li ';
                    if(large_cap1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Large Cap Stocks</span> <span class="right_text">'+ large_cap +'</span></li><li ';
                    if(mid_cap1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Mid Cap Stocks </span> <span class="right_text">'+ mid_cap +'</span></li><li ';
                    if(small_caps1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Small Cap Stocks</span> <span class="right_text">'+ small_caps +'</span></li><li ';
                    if(micro_cap1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Micro Cap Stocks</span> <span class="right_text">'+ micro_cap +'</span></li><li ';
                    if(int_usa1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Int\'l (ex-USA) Stocks</span> <span class="right_text">'+ int_usa +'</span></li></ul></div><div class="table-right-panel"><ul><li ';
                    if(gov_bond51 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">5-Year Gov. Bonds </span> <span class="right_text">'+ gov_bond5 +'</span></li><li ';
                    if(gov_bond101 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">10-Year Gov. Bonds  </span> <span class="right_text">'+ gov_bond10 +'</span></li><li ';
                    if(t_bills1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">T-Bills</span> <span class="right_text">'+ t_bills +'</span></li><li ';
                    if(global_bond1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Global Bonds </span> <span class="right_text">'+ global_bond +'</span></li><li ';
                    if(gold1 > 0){
                        header += 'class="t_head"';
                    }
                    header += '><span class="left_text">Gold </span> <span class="right_text">'+ gold +'</span></li></ul></div></div></div><div class="table_panel other_table_panel"><h4>Other Inputs</h4><div class="inner_table"><div class="table-full-panel"><ul><li class="t_head"><span class="left_text">Planned Savings Period (Yrs.)</span><span class="right_text">'+ yrs +'</span> </li><li class="t_head"><span class="left_text">Initial Capital</span><span class="right_text">'+ initcap +'</span> </li><li class="t_head"> <span class="left_text">Monthly Contribution:</span><span class="right_text">'+rate +'</span></li><li class="t_head"> <span class="left_text">Savings Goal</span><span class="right_text">'+legacycap+'</span></li><li class="t_head"><span class="left_text">Expense Ratio</span><span class="right_text">'+  parseFloat(ratio1).toFixed(1) +'%</span> </li><li class="t_head"> <span class="left_text">Rebalancing</span><span class="right_text">'+ rebal +'</span></li><li class="t_head"><span class="left_text">Inflation-adjust spending</span><span class="right_text">'+ inflation +'</span> </li></ul></div></div></div><div class="text-panell"><h3>Notes</h3><div class="line"></div><div class="line"></div><div class="line"></div><div class="line"></div></div></div><div class="page_break"></div></div>';
            }
            $("#get_html2").html('');
            var i = 0;
            var all_html = '';
            all_html += header;
            var footer = '<div class="page_break"></div><div id="page-ten"><div class="white_global_panel"><div class="desclaimer-block"><h5><strong>Disclaimer:</strong></h5><p>The charts and figures contained herein are based on historical total return data beginning January 1, 1926 (U.S.) and January 1, 1935 (Canada), and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended.</p><p>The charts and figures contained herein are based on historical total return data beginning January 1, 1926 (U.S.) and January 1, 1935 (Canada), and ending at the most-recent month end for which data are available. They are for illustrative purposes only; they do not constitute investment advice and must not be relied on as such. They assume the reinvestment of all investment income, and no transaction costs or taxes. Portfolios are neither real, nor recommended.</p><h5><strong>Sources:</strong></h5><p>U.S. Micro Cap Stocks, U.S. Small Cap Stocks, U.S. Mid Cap Stocks, U.S. Large Cap Stocks, U.S. Total Market Stocks, U.S. Treasury Bills, U.S. 5-Year Government Bonds: USA 5-Year Government Bond Total Return Index—Center for Research in Security Prices (CRSP). International Stocks: ex-U.S.A. Total Return Index, U.S. 10-Year Government Bonds: USA 10-Year Government Bond Total Return Index, Canadian Government Bonds: Canada 10-Year Total Return Government Bond Index, Canadian Treasury Bills: Canada Total Return Bills Index, Canadian Stocks: Canada S&P/TSX-300 Total Return Index, Global Bonds: GFD Global USD Total Return Government Bond Index, Gold: Gold Bullion Price-New York (US$/oz)—Global Financial Data, Inc. U.S. Inflation: Consumer Price Index—U.S. Bureau of Labor Statistics and Global Financial Data. Canada Inflation: Statistics Canada.</p></div></div></div></body></html>';
            
            /************ Loop start here to get all svg graph data for pdf ********************/
            var interval_data = setInterval(function () {  
                if(count == i){
                    /************ Break when all graphs data get in html view ********************/
                    clearInterval(interval_data);
                    all_html += footer; 
                    all_html1 = all_html.replace(/M-6,0H0V295.90000000000003H-6/gi, "M-1,0H0V295.90000000000003H-1");
                    all_html1 = all_html1.replace(/M0,6V0H750.4000000000001V6/gi, "M0,1V0H750.4000000000001V1");
                    all_html1 = all_html1.replace(/M6,0H0V295.90000000000003H6/gi, "M1,0H0V295.90000000000003H1");

                     /************ Ajax Request to Generate Html Pages ********************/
                     var path_url = window.location.origin;
                        var request = $.ajax({
                            url: path_url+ "/bigpicapp/pdfManager/php/ajax.php?",
                            type: "POST",
                            async: false,
                            data: {html_data : all_html1,pdf_type:checked},
                            success: function(name){ 

                                /************ Ajax Request to Generate Pdf file and send to provided email ********************/
                                name = $.trim(name);
                                var urls =  path_url+ "/bigpicapp/pdfManager/htmlFiles/"+name+".html";
                                // window.open(urls, '_blank');
                                $.ajax({
                                    url:  path_url+ "/bigpicapp/pdfManager/php/createpdf.php?",
                                    type: "POST",
                                    // async: false,
                                    data: {file_name : name,email_add : email_add},
                                    success: function (data) {
                                        // console.log(email_add);
                                        $("#is_pdf").val('0');
                                        $("#forward_email").val('');
                                        $("#close").trigger("click");
                                        var urls =  path_url+ "/bigpicapp/pdfManager/pdfFiles/"+name+".pdf";
                                        
                                        
                                        alert('File has been sent to email.');
                                        $(".loader-panel").hide(); 
                                        var selected_graph = $('.graph-dash').index($('.graph-dash.is_active')[0]);
                                        if(selected_graph >= 0){ 
                                            graphChanged(selected_graph);
                                        }else{
                                            graphChanged(0);
                                        }
                                        /************ After success reload page to refresh html ********************/ 
                                        
                                        // window.location.reload(true);
                                    }
                                }); 
                            }, 
                        }); 

                        // start(6);
                        
                        return;
                }

                graphChanged1(i); 

                /************ Here Getting the graphs data according to pdf view start********************/
                setTimeout(function () {
                    var html = '';
                    var html_val = ''; 
                    if(i == '0' || i == '2' || i == '4' || i == '6' || i == '8'){ 
                        html += '<div class="page_layout">';
                    }
                    var sum = 0;
                    sum = 1+i; 
                    if(i == '0'){
                        html += '<div id="first_graph" class="white_global_panel first_graph align_check">';
                    }else{
                        if(i == '1' || i == '3' || i == '5' || i == '7' || i == '9'){ 
                            html += '<div class="white_global_panel align_check">';
                        }else{
                            html += '<div class="white_global_panel align_check">';
                        }
                    }
                    html_val2 = $(".svgArea").html(); 
                    html_val = $("<div>" + html_val2 + "</div>");
                    html_val.find('.xAxisTitle').addClass('text_div'+i);
                    html_val.find('.x-text').addClass('graph_text'+i);
                    html_val.find('.x-text > span').addClass('heading_text'+i);
                    
                    if(checked == "retirement"){
                        html_val.find('#graphSvg').attr("class", "graph_area_retire"+i);
                        html_val.find('#graphSvg').attr("id", "graphSvg_retire"+i);
                        if(i == '1'){
                           html_val.find(".pagetext").append('<div id="add_withdrawal"><span class="blue-bar"></span><p id="custom_heading">'+ mo_amount +'</p></div>');
                        }
                    }else{
                        html_val.find('#graphSvg').attr("class", "graph_area_saving"+i);
                        html_val.find('#graphSvg').attr("id", "graphSvg_saving"+i);
                    } 
                    html += $(html_val).html();
                    var div_id  = "<div id='count_"+i+"'></div>"; 
                    html += '</div>';
                    if(i == '1' || i == '3' || i == '5' || i == '7' || i == '9'|| sum == count){
                        html += '<div class="page_break chart-block text-center"><div class="rightchart-text"><h5>Results based on rolling periods using monthly-frequency historical data.</h5></div></div>';
                    }
                    if(i == '1' || i == '3' || i == '5' || i == '7' || i == '9'){
                        html += '</div>';
                    }
                    all_html += html; 
                    html = "";
                    i += 1;
                }, 700); 
                // clearInterval(interval_data);
            },930);

          /************ Here Getting the graphs data according to pdf view end********************/
        }

        function graphChanged1(index){ 
            postal.channel().publish("graphGallery.graph.changed1", index);
        }

        /* ============End code edited by Anuj================== */
        
        function graphChanged(index){
            
            
            showLoadingGif();

            $('.graph-dash.is_active').removeClass('is_active').addClass('disabled');
            $('.graph-dash').eq(index).removeClass('disabled').addClass('is_active');
            //prev graph-arrow
           
            postal.channel().publish("graphGallery.graph.changed", index);
            
             /*
			if you are on the first page, don't show "previous"
			on the last page don't show "next"
			*/
            var mode='';//you are on KEY page
        	if($('.nav > a.is_active').hasClass('with'))//if on withdrawal page
            	mode = $('input[name=mode]:checked').val();
            $('.prev').removeClass('disabled');
            //console.log("----------------- .pageset." + mode + '_pages .prev + .graph-dash.is_active');
//			if($(".pageset." + mode + '_pages .prev + .graph-dash.is_active').length)
//			{
//				$(".pageset." + mode + '_pages .prev').addClass('disabled');
//			}
            if(index==0)
			{
				$('.prev').addClass('disabled');
			}
			$('.next').removeClass('disabled');
			if((index + 1) == maxGraphIndex)
			{
				$('.next').addClass('disabled');
			}	
                   
        
            setTimeout(function(){
                // console.log('here');
                postal.channel().publish("graphService.graph.hide.graphSelectorBubble",1000);
            },1000);
        }
        
        
        function setMaxGraph(count){
            maxGraphIndex = count;
        }
        
		function modalityChanged(graphCount){
            console.log(graphCount);
            displayGraphSelector(graphCount);
            setMaxGraph(graphCount);
            currentGraphIndex = 0;
        }
			
        function displayGraphSelector(count){
         
           var parent = $('.pagenavwrap .graphnav');

                            
            var html = "<div class='prev graph-arrow disabled'><svg height='37' viewBox='0 0 24 24' width='37' xmlns='http://www.w3.org/2000/svg'><path d='M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z'/></svg></div>";
            
            
            var activeDash = "&nbsp&nbsp;<div class='graph-dash is_active'>&mdash;&nbsp;</div>";
            var dash = "<div class='graph-dash'>&mdash;&nbsp;</div>";
            
            for (var i=0; i<count; i++){
              html += (i==0?activeDash:dash);   
            }
            
            html += "<div class='next graph-arrow'><svg  height='37' viewBox='0 0 24 24' width='37' xmlns='http://www.w3.org/2000/svg'><path d='M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z'/></svg></div>";

            parent.html(html);
            bindEvents();
        }
        
        function selectDash(graphIndex){
            
        }
        
        d3.selection.prototype.moveToFront = function() {
          return this.each(function(){
            this.parentNode.appendChild(this);
          });
        };
        
        function appendSvg(){
            // console.log('appendSvg');
            svg = d3.select('.chartspace')
                    .append("svg")
                    .attr("width", "100%")
                    .attr("height", "120%")
                    .attr("id", "graphSvg")
                    .attr("xmlns", "http://www.w3.org/2000/svg")
                    .append("g");
            
            svg.append("g").attr("id", "mtnBubble");                       
            
            
            bubble = d3.select("#mtnBubble");
            
            bubble.append("rect")
                    .attr("id", "mtnBubbleRect")
                    .attr("height", "48px")
                    .attr('width', "75px")
                    .attr("y", "-49")
                    .attr("fill", "transparent");
            
            bubble.append("text")
                    .attr("id", "mtnBubbleText")                
                    .attr("dy", "-1.2")
                    .attr("fill", "black")         
                    .attr("style", 'font-size:12px')
                    .attr("text-anchor", "middle")
                    .attr("x", "30")
                    .attr("y", "-30")
                    .attr("transform", "translate(38,-2)");
            
            
            bubbleText = d3.select("#mtnBubbleText");
 
            svg.append("rect")
                .attr("id", "mtnBubbleBar")               
                .attr("y", -1)
                .attr('width', 1)
                .attr("fill", "transparent");
            
            bubbleBar = d3.select("#mtnBubbleBar");
            bubbleRect = d3.select("#mtnBubbleRect");
       
          
            svg.call(d3.tip()
                          .attr("class", "d3-tip")
                          .offset([-8, 0]));
            
            toolTip = d3.select('.d3-tip');
            
        }
        
        
        function calculateGraphDimensions(){
            var containerWidth = $(svgArea).width();
            graphWidth = (containerWidth * 0.80);
            var yPos = 50;
            if ($(document).width() <= 1799){
                graphHeight = ($(svgArea).height() * 0.55);   

            }else{
                graphHeight = ($(svgArea).height() * 0.60);   
                yPos = 80;            
            }
            svg.selectAll(".x.axis").attr("transform", "translate(0," + graphHeight + ")");     
            svg.attr("transform", "translate(" + ((containerWidth -  graphWidth) / 2) + "," + yPos + ")");
        }
        

        function resize(){
            calculateGraphDimensions();
        }

        function showGraphSvg(show){
            graphSvg.stop().animate({'opacity':show == false? 0 : 1}, 0);          
        }
        
        function showAllGraphElements(show){
            var elements = xAxisTitle.add(yAxisTitle)
                                     .add(graphSvg)
                                     .add($('.graphExtraInfoWrap'));
            
            elements.stop().animate({'opacity':show == false? 0 : 1}, 0);
            elements.css({visibility:show == false ? "hidden": "visible"}); 
        }

        function showLoadingGif(show){
            showAllGraphElements(show===undefined?false:!show);
            var element = $('.chartLoaderWrap');
            $('.loaderGraphic').css('display', (show == false)?'none':'block');
        }
 
        

        return {
            start:                  start,
            all_graph:              all_graph,
            resize:                 resize,        
            showLoadingGif:         showLoadingGif,
            showGraphSvg:           showGraphSvg,
            modalityChanged:        modalityChanged,
            showAllGraphElements:   showAllGraphElements
        };

    });