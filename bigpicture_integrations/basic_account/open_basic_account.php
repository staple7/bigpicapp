<?php
    @ob_start();

    $errorcode = array('errorBasic'=>false, 
                       'errorOther'=>false, 
                       'errorMessage'=>'');
    
    try{
      
        $email              = $_POST['email'];            
        $regionCode         = $_POST['country'];
        $openSessionAnyway  = $_POST['openFree'];
        
//        $email   = 'day3@day3.com';            
//        $country = 'US';
//        $openSessionAnyway = true;
    
        require '../bigpicture_database/select_account_code_by_email.php';
        
        if ($recurlyAccountCode){
            
            if ($openSessionAnyway !== 'true'){            
                require '../bigpicture_recurly/get_account_expire_time_by_account_code.php';
                require '../bigpicture_recurly/validateExpiredAccount.php';
                mysql_close($conn);
                $errorcode['errorBasic'] = true;
                echo json_encode($errorcode);
                die;  
            }
            
        }else{
            
            require '../bigpicture_database/insert_user.php'; 
            
            require '../bigpicture_mailchimp/add_subscriber.php'
            
        }
        
        require '../validateNewEmail.php';
                
        $session_time = (40 * 365 * 24 * 60 * 60);
        
        require '../bigpicture_session/start_session.php';
           
        ob_flush();
        
    }catch (Exception $e) {        
        require '../bigpicture_database/close_db_connection.php';
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e->getMessage();   
        echo $e;
    } 
    
    mysql_close($conn);
    echo json_encode($errorcode);    

?>