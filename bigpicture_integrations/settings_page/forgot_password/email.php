<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        $to_email = $_POST['email'];
        //$to_email  = "carlos@investmentsillustrated.com";
        //$to_email  = "cmoligo@gmail.com";
        //$to_email  = "ryan@investmentsillustrated.com";
        //$to_email  = "justin@investmentsillustrated.com";
        //$email_id = "1";
        
        require '../db_connection.php';
        
        $result = mysql_query("SELECT USERID 
                               FROM $db_schema.User 
                               WHERE EMAIL = '$to_email'", $conn);
        
        if (mysql_num_rows($result) == 0){
            mysql_close($conn);
            $errorcode['errorEmail'] = true;
            echo json_encode($errorcode);
            die;
        }
        
        $row = mysql_fetch_array($result);        
        $email_id = $row["USERID"];
        
        
        $from_email ="info@bigpicapp.co";
        
        $subject = "Big Picture password reset.";
          
        $headers = "From: " . strip_tags($from_email) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
        $body = 
        "<table cellpadding='8' cellspacing='0' style='padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff' border='0'>
  <tbody>
    <tr>
      <td valign='top'><table cellpadding='0' cellspacing='0' style='border-radius:4px;border:1px #dceaf5 solid' border='0' align='center'>
          <tbody>
            <tr>
              <td colspan='3' height='6'></td>
            </tr>
            <tr style='line-height:0px'>
              <td width='100%' style='font-size:0px' align='center' height='1'><img  style='max-height:73px; margin-top:22px' width='34' height='42' alt='' src='https://www.bigpicapp.co/images/bigpicture_logo_mobile.png'></td>
            </tr>
            <tr>
              <td><table cellpadding='0' cellspacing='0' style='line-height:25px' border='0' align='center'>
                  <tbody>
                    <tr>
                      <td colspan='3' height='30'></td>
                    </tr>
                    <tr>
                      <td width='36'></td>
                      <td width='454' align='left' style='color:#444444;border-collapse:collapse;font-size:11pt;font-family:Myriad Pro,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px' valign='top'>Hi there,<br>
                        <br>
                        Someone recently requested a password change for your Torch account. If this was you, you can set a new password <a href='/' target='_blank'>here</a>:<br>
                        <br>
                        <center>
                          <a href='https://www.bigpicapp.co/reset_password.php?id=$email_id' style='border-radius:3px;font-size:15px;color:white;border:1px #1CA6EB solid;text-decoration:none;padding:14px 7px 14px 7px;width:210px;max-width:210px;font-family:proxima_nova,'Open Sans','lucida grande','Segoe UI',arial,verdana,'lucida sans unicode',tahoma,sans-serif;margin:6px auto;display:block;background-color:#33AFED;text-align:center'>Reset password</a>
                          <!--email id-->
                        </center>
                        <br>
                        If you don't want to change your password or didn't request this, just ignore and delete this message.<br>
                        <br>
                        To keep your account secure, please don't forward this email to anyone. See our Help Center for <a href='#' target='_blank'>more security tips</a>.<br>
                        <br>
                        Thanks!<br>
                        - The Torch Team</td>
                      <td width='36'></td>
                    </tr>
                    <tr>
                      <td colspan='3' height='36'></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        <table cellpadding='0' cellspacing='0' align='center' border='0'>
          <tbody>
            <tr>
              <td height='10'></td>
            </tr>
            <tr>
              <td style='padding:0;border-collapse:collapse'><table cellpadding='0' cellspacing='0' align='center' border='0'>
                  <tbody>
                    <tr style='color:#1CA6EB;font-size:11px;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif''>
                      <td width='400' align='left'></td>
                      <td width='128' align='right'>© 2015 Torch</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>";
        
        mail($to_email, $subject, $body, $headers);

        
    } catch (Exception $e) {     
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }
    try{
        if ($conn){
            mysql_close($conn);
        }           
    } catch (Exception $e) {}
    

   echo json_encode($errorcode);      

?>

