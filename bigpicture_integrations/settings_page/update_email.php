<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorOldEmail'=>false,
                       'errorSameEmails'=>false,
                       'errorExistingNewEmail'=>false,                       
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        
        $email      = $_POST['old_email'];    
        $new_email  = $_POST['new_email'];    
        
        if ($email == $new_email){
            $errorcode['errorSameEmails'] = true;
            echo json_encode($errorcode);
            die;            
        }
             
        require '../bigpicture_database/select_account_code_by_email.php';

        if (strlen($recurlyAccountCode) == 0){
            $errorcode['errorOldEmail'] = true;
            echo json_encode($errorcode);
            die;
        }

        require '../bigpicture_database/select_user_by_email.php';
        
        if (strlen($recurlyAccountCode) > 0){
            $errorcode['errorExistingNewEmail'] = true;            
            echo json_encode($errorcode);
            die;
        }

        $account = Recurly_Account::get($recurlyAccountCode);
        $account->email = $new_email;
        $account->update();        
        
        require '../bigpicture_database/update_email.php';

              
         setcookie('email', $new_email, $_COOKIE["expireTime"], "/");          
       

        
    } catch (Exception $e) {      
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e;    
    }
    try{
        mysql_close($conn);
    }catch (Exception $e) {}
    

    echo json_encode($errorcode);

?>

