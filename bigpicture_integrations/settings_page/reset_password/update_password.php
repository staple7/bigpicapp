<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorLogin'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        
        $email         = $_POST['email'];
        $password      = $_POST['old_password'];        
        $new_password  = $_POST['new_password'];    
        
        require '../db_connection.php';
        
        require '../bigpicture_database/select_user_by_email.php';
        
        if (strlen($recurlyAccountCode) == 0){
            mysql_close($conn);
            $errorcode['errorEmail'] = true;
            echo json_encode($errorcode);
            die;
        }
        
        require '../bigpicture_database/select_user_by_email_and_password.php';
        
        if (strlen($recurlyAccountCode) > 0){
            $password = $new_password;
            require '../bigpicture_database/update_password.php';                    
        }else{
            mysql_close($conn);
            $errorcode['errorLogin'] = true;
            echo json_encode($errorcode);
            die;            
        }
        
    } catch (Exception $e) {            
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }
    try{
        mysql_close($conn);
    }catch (Exception $e) {}
    

    echo json_encode($errorcode);      

?>

