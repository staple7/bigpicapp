<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        
        $email_id       = $_POST['email_id'];
        $new_password   = $_POST['new_password'];    
        
        require '../db_connection.php';
        
        $result = mysql_query("SELECT EMAIL 
                               FROM $db_schema.User 
                               WHERE USERID = '$email_id'", $conn);
        
        if (mysql_num_rows($result) == 0){
            mysql_close($conn);
            $errorcode['errorEmail'] = true;
            echo json_encode($errorcode);
            die;
        }        
       
        mysql_query("UPDATE $db_schema.User 
                     SET                          
                     PASSWORD = '$new_password'
                     WHERE USERID = '$email_id'", $conn);       
        
        $from_email ="info@bigpicapp.co";
        
        $subject = "Your Big Picture password was reset";
          
        $headers = "From: " . strip_tags($from_email) . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
        $body = "<table cellpadding='8' cellspacing='0' style='padding:0;width:100%!important;background:#ffffff;margin:0;background-color:#ffffff' border='0'>
  <tbody>
    <tr>
      <td valign='top'><table cellpadding='0' cellspacing='0' style='border-radius:4px;border:1px #dceaf5 solid' border='0' align='center'>
          <tbody>
            <tr>
              <td colspan='3' height='6'></td>
            </tr>
            <tr style='line-height:0px'>
              <td width='100%' style='font-size:0px' align='center' height='1'><img  style='max-height:73px; margin-top:22px' width='34' height='42' alt='' src='https://www.bigpicapp.co/images/bigpicture_logo_mobile.png'></td>
            </tr>
            <tr>
              <td><table cellpadding='0' cellspacing='0' style='line-height:25px' border='0' align='center'>
                  <tbody>
                    <tr>
                      <td colspan='3' height='30'></td>
                    </tr>
                    <tr>
                      <td width='36'></td>
                      <td width='454' align='left' style='color:#444444;border-collapse:collapse;font-size:11pt;font-family:Myriad Pro,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif';max-width:454px' valign='top'>Hi there,<br>
                        <br>
                        Your Big Picture account's password was recently changed using a reset link that we sent to your email address.
                        <br>
                        <p>For more information, check out <a href='/'>this Help Center article.</a></p>
                        
                        <p>If you didn't make this change, please <a href='http://www.bigpicapp.co/contact-us.html' target='_blank'>let us know.</a></p>
                        
                        <br>
                        Thanks!<br>
                        - The Big Picture Team</td>
                      <td width='36'></td>
                    </tr>
                    <tr>
                      <td colspan='3' height='36'></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table>
        <table cellpadding='0' cellspacing='0' align='center' border='0'>
          <tbody>
            <tr>
              <td height='10'></td>
            </tr>
            <tr>
              <td style='padding:0;border-collapse:collapse'><table cellpadding='0' cellspacing='0' align='center' border='0'>
                  <tbody>
                    <tr style='color:#1CA6EB;font-size:11px;font-family:proxima_nova,'Open Sans','Lucida Grande','Segoe UI',Arial,Verdana,'Lucida Sans Unicode',Tahoma,'Sans Serif''>
                      <td width='400' align='left'></td>
                      <td width='128' align='right'>© 2016 Big Picture</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>";
        
        mail($to_email, $subject, $body, $headers);
        
    } catch (Exception $e) {            
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }
    try{
        mysql_close($conn);
    }catch (Exception $e) {}
    

    echo json_encode($errorcode);      

?>

