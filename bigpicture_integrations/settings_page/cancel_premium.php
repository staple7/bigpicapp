<?php
   

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

    try {
        $email = $_POST['email'];
        require '../bigpicture_database/select_account_code_by_email.php';        
        require '../bigpicture_recurly/get_subscription_by_account_code.php';
        $subscription->cancel();
    } catch (Exception $e) {
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e;        
    }

    echo json_encode($errorcode);
    
?>