<?php

    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorBilling'=>false,
                       'errorMessage'=>'');

    try{
        $email = $_POST['email'];    
        $token = $_POST['recurly-token'];

        require '../bigpicture_database/select_account_code_by_email.php';

        if (strlen($recurlyAccountCode) == 0){                    
            $errorcode['errorEmail'] = true;  
            echo json_encode($errorcode);
            die;
        }
        $billing_info = new Recurly_BillingInfo();
        $billing_info->account_code = $recurlyAccountCode;
        $billing_info->token_id = $token;
        $billing_info->update();
    } catch (Exception $e) {
        $errorcode['errorBilling']   = true;
        $errorcode['errorMessage'] = $e;
    }

    echo json_encode($errorcode);      

?>

