<?php
 ob_start();
    require '../bigpicture_config.php';
   
    $errorcode = array('errorOther'=>false, 'errorMessage'=>'');
    try{
       $invoice_number = $_GET['invoice_number'];       
    //    $invoice_number = "1144";
       header("Content-type: application/pdf");
       $pdf = Recurly_Invoice::getInvoicePdf($invoice_number);
       echo $pdf;
    } catch (Exception $e) {
       $errorcode['errorOther'] = true;
       $errorcode['errorMessage'] = $e;    
       echo json_encode($errorcode);
    }    
        
    ob_end_flush(); 


?>

