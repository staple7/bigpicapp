<?php

    require 'open_mailchimp_connection.php';

    $email = $_POST['email'];    

    $double_optin  = false;
	// By default this sends a confirmation email - you will not see new members
	// until the link contained in it is clicked!
	$retval = $api->listSubscribe($listId, $email, $merge_vars, 'html', $double_optin);
	 
	if ($api->errorCode){
	    echo "Unable to load listSubscribe()!\n";
	    echo "\tCode=".$api->errorCode."\n";
	    echo "\tMsg=".$api->errorMessage."\n";
	} else {
	    echo "Subscribed!!";
	}
    
?>