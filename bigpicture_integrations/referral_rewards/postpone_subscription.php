<?php
    require '../bigpicture_config.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false,
                       'errorMessage'=>'');

 
    try {
        
        
        require '../settingsPageBackend/get_subscription_uuid.php';
        
        if (!$uuid){
            $errorcode['errorEmail'] = true;
            echo json_encode($errorcode);
            die;
        }             
        $subscription = Recurly_Subscription::get($uuid);
        foreach ($subscription->current_period_ends_at as $expiration_date) {
            break;
        }
        
        $new_expiration_date = strtotime($expiration_date) + (365 * 24 * 60 * 60);
     
        $subscription->postpone(date('c', strtotime($new_expiration_date)), false);
        //$subscription->postpone(date('c', strtotime('2016-12-31Z')), False);
        
    } catch (Exception $e) {            
        $errorcode['errorOther']   = true;
        $errorcode['errorMessage'] = $e;     
    }finally{
        try{
            mysql_close($conn);
        }catch (Exception $e) {}
    }

    echo json_encode($errorcode);      

?>

