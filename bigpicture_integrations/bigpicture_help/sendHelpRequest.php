<?php

    $errorcode = array('errorOther'=>false, 'errorMessage'=>'');

    try{

        require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/session/open_mandrill_session.php';

        $to_email  = "info@bigpicapp.co"; 
       // $to_email  = "cmoligo@gmail.com";

        $fromName           = $_POST['name'];
        $phoneNumber        = $_POST['phoneNumber'];
        $regionCode         = $_POST['regionCode'];
        $fromEmail          = $_POST['email'];
        $feedback           = $_POST['message'];
        $portfolio          = $_POST['portfolio'];
        $appliedPortfolioId = $_POST['appliedPortfolioId'];
        $addInputs          = $_POST['addInputs'];
        $branchData         = $_POST['branchData'];
        $planCode           = $_POST['planCode'];
        $deviceType         = $_POST['deviceType'];
        $isIE               = $_POST['isIE'];
        $branchCode         = $_POST['branchCode'];
        $graphCode          = $_POST['graphCode'];
        $dataLastUpdate     = $_POST['dataLastUpdate'];
        $variableToSolveFor = $_POST['variableToSolveFor'];
        $navigator          = $_POST['navigator'];
        $manifestVersion    = $_POST['manifestVersion'];
        
        $html = 
            "
            
                Message:                $feedback <br><br>
                Phone number:           $phoneNumber <br><br>
                Email:                  $fromEmail <br><br>                       
                Plan Code:              $planCode <br><br> 
                Country:                $regionCode <br><br>
                Device:                 $deviceType <br><br>             
                IE:                     $isIE <br><br>                            
                Portfolio List:         $portfolio <br><br>
                Applied Portfolio Id:   $appliedPortfolioId <br><br>
                Add Inputs:             $addInputs <br><br>
                Branch Code:            $branchCode <br><br>                
                Variable to solve for:  $variableToSolveFor <br><br>
                Branch Inputs:          $branchData <br><br>
                Graph Code:             $graphCode <br><br>                
                Data Update:            $dataLastUpdate <br><br>
                Navigator info:         $navigator <br><br>
                Manifest version:       $manifestVersion <br><br>
                
            
            ";


        $message = array(
                'subject'   =>  "Help Requested....",
                'from_name' =>  $fromName,
                'from_email'=>  $from_email,
                'html'      =>  $html,
                'to'        =>  array(array('email' => $to_email, 'name' => ''))
            );

        $async = true;
        $ip_pool = 'Main Pool';
        $result = $mandrill->messages->send($message, $async, $ip_pool);

          
    }catch (Exception $e){
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e->getMessage();        
    }

    echo json_encode($errorcode);  

?>
