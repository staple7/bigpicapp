 <?php

    require '../bigpicture_database/db_config.php'; 

    try{ 
        require '../bigpicture_database/open_db_connection.php'; 
        $email = $_COOKIE['email'];
        require '../bigpicture_database/get_user_logo_image.php'; 
        $old_image_val = $old_image['logo_image'];  
 
        $filename = $_FILES['file']['name']; 
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $new_name = time().'_'.rand().'.'. $ext; 
        $location = $_SERVER['DOCUMENT_ROOT'] . "/bigpicapp/images/uploads/".$new_name;
        $uploadOk = 1; 
        $valid_extensions = array("jpg","jpeg","png");
        /* Check file extension */
        if( !in_array(strtolower($ext),$valid_extensions) ) {
            $uploadOk = 0;
        }

        if($uploadOk == 0){
            echo "0";
        }else{ 
            if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
                require '../bigpicture_database/update_user_logo.php';
                if($update_val == 1){
                    if($old_image_val){ 
                        $path = $_SERVER['DOCUMENT_ROOT'] . "/bigpicapp/images/uploads/".$old_image_val;
                        if(file_exists($path)){ 
                            unlink($path);
                        }
                    }
                    echo $new_name;
                }else{
                    echo "0";
                } 
            }else{
                echo "0";
            }
        }  
        require '../bigpicture_database/close_db_connection.php'; 
    }catch (Exception $e){ 
        echo "0"; 
    }
 
?>
