<?php

    session_start();
    $exp_time = time() - 3600;
    setcookie("plancode", "", $exp_time, "/"); 
    setcookie("regioncode", "", $exp_time, "/");  
    $_SESSION['login'] = "0";
    $_SESSION = array();
    session_destroy();
    header("Location: " . "/login.php");

?>