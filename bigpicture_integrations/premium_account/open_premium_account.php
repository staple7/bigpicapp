<?php
    @ob_start();

    $errorcode = array('errorPremium'=>false,'errorBilling'=>false,'errorOther'=>false, 'errorMessage'=>'');
    
    try {
        
        $token      = $_POST['recurly-token'];
        $password   = $_POST['password'];
        $firstname  = $_POST['firstname'];
        $lastname   = $_POST['lastname'];            
        $email      = $_POST['email'];             
        $regionCode = $_POST['country'];
        
       
        require '../bigpicture_recurly/get_recurly_currency_code_by_region_code.php';

        require '../bigpicture_database/select_account_code_by_email.php';
      

        //It's an existing email with a premium account.
        if ($recurlyAccountCode){
            
            $account = Recurly_Account::get($recurlyAccountCode);        
                      
            require 'get_subscription_by_account_code';
            
            $account->first_name = $firstname;
            $account->last_name = $lastname;
            $account->email = $email; 
            
            $billing_info = new Recurly_BillingInfo();
            $billing_info->account_code = $recurlyAccountCode;
            $billing_info->token_id = $token;
            
            $subscription->plan_code = $premium_plan_code;
            $subscription->collection_method = $invoice_collection_for_premium;
            $subscription->account->currency = $recurly_currency;
            
            $account->update();
            $billing_info->update();
            $subscription->updateImmediately();

            require '../bigpicture_database/update_user.php';
            
        }else{
            $subscription = new Recurly_Subscription();
            $account_code = uniqid();
            $subscription->account = new Recurly_Account($account_code);     
            $subscription->plan_code = $premium_plan_code;
            $subscription->account->first_name = $firstname;
            $subscription->account->last_name = $lastname;
            $subscription->account->email = $email;
            $subscription->account->billing_info = new Recurly_BillingInfo();
            $subscription->account->billing_info->token_id = $token;
            $subscription->collection_method = $invoice_collection_for_premium;   
            $subscription->currency = $recurly_currency;            
            $subscription->create();    
            
            require '../bigpicture_database/insert_user.php';
  
        }
            
//        if ($recurlyAccountCode){
//            require '../bigpicture_database/update_user.php';
//        }else{
//            require '../bigpicture_database/insert_user.php';     
//        }
    
        require '../validateNewEmail.php';        
        $session_time = (1 * 365 * 24 * 60 * 60);
        require '../bigpicture_session/start_session.php';
                                    
    } catch (Exception $e) {        
        $errorcode['errorBilling'] = true;
        $errorcode['errorMessage'] = $e->getMessage();
    }

    ob_flush();
    if ($conn){
        mysql_close($conn);
    }


    echo json_encode($errorcode);          

        
?>