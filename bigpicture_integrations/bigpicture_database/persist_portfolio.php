<?php

    $errorcode = array('errorOther'=>false, 'errorMessage'=>'');

    require '../../bigpicture_recurly/bigpicture_config.php';

    try{
        require '../../bigpicture_recurly/db_connection.php';
        
        $email = $_POST['email'];
        
        $portfolioConfiguration = json_encode($_POST['portfolioConfiguration']);
        $portfolioConfiguration = str_replace("'", "\'", $portfolioConfiguration );
            
        
        $result = mysqli_query($conn, "SELECT USERID FROM $db_schema.User WHERE EMAIL = '$email'");
        
        if ($result -> num_rows > 0){                
            $row = mysqli_fetch_array($result);
            $userId = $row["USERID"];
        }else{
            echo json_encode($errorcode);
            die;
        }
            
        $result = mysqli_query ($conn, "SELECT PORTFOLIOALLOCATIONID 
                                        FROM $db_schema.PortfolioAllocation 
                                        WHERE USERID = '$userId'");
      
        if ($result -> num_rows > 0) {
            mysqli_query ($conn, "UPDATE $db_schema.PortfolioAllocation 
                                 SET portfolioConfiguration = '$portfolioConfiguration'
                                 WHERE USERID = '$userId'");
        }else{
            mysqli_query($conn, "INSERT INTO $db_schema.PortfolioAllocation 
                                        (userId, portfolioConfiguration) 
                                        VALUES ('$userId', '$portfolioConfiguration')"); 
        }
       
    }catch (Exception $e){
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e->getMessage();        
    }

    require '../../bigpicture_recurly/db_close_connection.php';

    echo json_encode($errorcode);  

?>
