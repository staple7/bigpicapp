<?php

    @ob_start();

    require 'bigpicture_recurly/bigpicture_config.php';
    require 'bigpicture_recurly/safe_password.php';

    $errorcode = array('errorEmail'=>false,
                       'errorOther'=>false, 
                       'errorLogin'=>false,
                       'expired'=>false,
                       'plancode'=>false,
                       'errorMessage'=>'');

    try {

		$email          = $_POST['email'];
        $password       = $_POST['password'];
		$isSocialLogin  = $_POST['isSocialLogin'];

        require 'bigpicture_recurly/db_connection.php';

        if ($isSocialLogin == true){
             $result = mysqli_query ($conn, "SELECT EMAIL, REGIONCODE, RECURLYACCOUNTCODE, CREATEDATE  
                                           FROM $db_schema.GroupMember 
                                           WHERE EMAIL = '$email'
                                           AND LENGTH(RECURLYACCOUNTCODE) > 0");     
            
             if ($result->num_rows > 0){
                $row = mysqli_fetch_array($result);
             }
            
        }else{
            $result = mysqli_query($conn, "SELECT PASSWORD
                                           FROM $db_schema.GroupMember
                                           WHERE EMAIL = '$email'
                                           AND LENGTH(PASSWORD) > 0");
            
            if ($result->num_rows > 0){
                $row = mysqli_fetch_array($result);
                $hash = $row["PASSWORD"];
            }
        }
         
        
        if ($result->num_rows == 0 || (strlen($hash) > 0) && !bcrypt_check($password, $hash)) {
            require 'bigpicture_recurly/db_close_connection.php';
            $errorcode['errorLogin'] = true;
            echo json_encode($errorcode);            
            die;
        }

        
        $result = mysqli_query($conn, "SELECT REGIONCODE, RECURLYACCOUNTCODE 
                                       FROM $db_schema.User
                                       WHERE EMAIL = '$email'
                                       AND LENGTH(PASSWORD) > 0");

        if ($result->num_rows > 0){
            $row = mysqli_fetch_array($result);
        }
        
        $country = $row["REGIONCODE"];
        $recurlyAccountCode = $row["RECURLYACCOUNTCODE"];

        $subscriptions = Recurly_SubscriptionList::getForAccount($recurlyAccountCode);
        foreach ($subscriptions as $subscription) {}

        $plan_code = $subscription->plan->plan_code;

        if ($plan_code == "big_picture_premium"){
            foreach ($subscription->current_period_ends_at as $expireTime) {break;}
            $expireTime = strtotime($expireTime);
        }
        
        require 'validateNewEmail.php';
        
        require 'bigpicture_recurly/validateExpiredAccount.php';
        
        session_start();
        
        $_SESSION['login'] = "1";
        
        setcookie("plancode", $plan_code, $expireTime, "/");
        setcookie("email",  $email,  $expireTime, "/");
        setcookie("regioncode", $country, $expireTime, "/");
        

        
    } catch (Exception $e){

        $errorcode['errorMessage'] = $e->getMessage(); 

        $errorcode['errorOther'] = true;

    }




    require 'bigpicture_recurly/db_close_connection.php';       

    echo json_encode($errorcode);   

    ob_flush();



?>



