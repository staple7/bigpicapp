<?php

    $errorcode = array('errorOther'=>false, 'errorMessage'=>'');

    require '../../bigpicture_recurly/bigpicture_config.php';

    try{
        require '../../bigpicture_recurly/db_connection.php';
        
        $emailList = $_POST['emailList'];
        $message   = $_POST['message'];        
        $personName   = $_POST['subject'];
        $senderEmail = $_POST['senderEmail'];
        //$senderEmail ='cmoligo@gmail.com';
        
        $result = mysqli_query ($conn, "SELECT USERID, RECURLYACCOUNTCODE 
                                        FROM $db_schema.User 
                                        WHERE EMAIL = '$senderEmail' 
                                        AND LENGTH(RECURLYACCOUNTCODE) > 0");
      
        if ($result->num_rows > 0) {
            $row = mysqli_fetch_array($result);
            $account = Recurly_Account::get($row["RECURLYACCOUNTCODE"]);                         
            $subscriber_name = $account->first_name . " " . $account->last_name;            
        }
       
        $email_type = "share_bigpicture_by_email";
        $subject_email = 'Big Picture: An Eye-Opening App For Retirement Planning';
        if (strlen($subscriber_name)>0){
            $from_name = $subscriber_name . ' via the Big Picture app';
        }else{
            $from_name = $personName . " has shared the Big Picture app with you";
        }
        
        $html =$message;

        foreach ($emailList as &$email){
            $to_email = $email;
            require $_SERVER['DOCUMENT_ROOT'].'/bigpicture_recurly/bigpicture_mandrill/send_email.php';
        }             
        
        require '../../bigpicture_recurly/db_close_connection.php';
    }catch (Exception $e){
        require '../../bigpicture_recurly/db_close_connection.php';
        $errorcode['errorOther'] = true;
        $errorcode['errorMessage'] = $e->getMessage();        
    }

    echo json_encode($errorcode);  

?>
