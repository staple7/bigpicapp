<?php
    @ob_start();

    if (!isset($_COOKIE["emailid"])){
        $host = $_SERVER['HTTP_HOST'];
        setcookie("emailid", htmlspecialchars($_GET["id"]), time() + (40 * 365 * 24 * 60 * 60), "/");
        header("Location: http://$host/reset_password.php");             
        die;
    }

    $email_id = $_COOKIE["emailid"];
    setcookie("emailid", "", time()-3600, "/");
    ob_flush();
?>

<!DOCTYPE html>
<html lang="en"><head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Reset password</title>

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css" href="css/fonts.css">
<link rel="stylesheet" type="text/css" href="custom/custom-bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/client.css">
<link rel="stylesheet" type="text/css" href="css/form.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
   
  
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-43634131-2', 'auto');
      ga('send', 'pageview');

</script>
</head>
<body >
<!--nav bar-->
<div class="container-fluid nav-bg navbar-fixed-top borderBottom">
        <div class="row">
            <div class="container">
                <div class="row">
                <nav class="navbar navbar-default no-border" >
      
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header" >
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
           </button>
         <a class="navbar-brand" href="/"></a>
         </div>
      <!-- /.container-fluid --> 
</nav>
                </div>
            </div> 
        </div>
 </div> 
<!--end nav bar-->
<div class="wrap">


<div class="container">
    <div class="row">
    
        <div class="col-lg-12 text-center">
        <div class="h1Spacing">
        <h2 class=""> Reset your password</h2>
        <span class="thin small gray-lighter">Minimum 6 characters</span>
		</div>
        </div>
          
    </div>

   
    
    
    <div class="row"> 
        <div class="col-sm-10 col-sm-offset-1">
            <div class="text-center bg_gray_light alert display_none submit_succes submit_contactForm_succes reset_password_succes">
					<h4 class="center-block marginTop22">Your password has been reset.</h4>
				<p><span class="thin">You can know use your new password to <a href="login.php">login</a>.</span>
					</p>
					<br>
			</div>
            <form class="input_h formRestyle reset-password clearfix">
            <!--error alert-->
             <div class="alert alert-danger text-center reset_password_error display_none" role="alert">&nbsp;</div>
             <!--successful alert-->
             <div class="text-center alert alert-info reset_password_succes display_none">
            </div>
            <input type="hidden" id="email_id" value="<?php echo $email_id; ?>">
            <label class="error" id="errors"></label>
            <div class="form-group">
            <input placeholder="Enter new password" class="form-control" maxlength="50" title="Your password needs to be at least 6 characters" pattern=".{6,}" name="new_password" type="password" id="reset_password" required>
            </div>
            <div class="form-group">
            
            <input placeholder="Confirm password" type="password" class="form-control" maxlength="50" title="Your password needs to be at least 6 characters" pattern=".{6,}" value="" name="confirm_password" id="confirm_password" required>
            </div>
            <div class="form-group">
            <button type="submit" class="col-sm-4 btn btn-lg btn-block btn-warning text-center">Submit</button>
            </div>
            </form>
        </div>
        
        
    </div>


    
</div>

<!--footer--->
<footer>

</footer>
<!--end pricing page--->
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script data-main="/js/main.js?v2.0.0" src="/lib/require.js"></script>  
</body>
</html>
