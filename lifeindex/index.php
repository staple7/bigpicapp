<!DOCTYPE html>

<?php
    $app_version = "1.0.4";
?>

<html>
    <head> 
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" /> 
        <link rel="stylesheet" href="css/bootstrap.min.css?v<?php echo $app_version;?>"/>
        <link rel="stylesheet" href="css/general.css?v<?php echo $app_version;?>"/>
        
        <link rel="stylesheet" href="css/graph.css?v<?php echo $app_version;?>" />
        <script src="js/vendor/d3.v3.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery-2.0.3.min.js"></script>
        <script src="js/vendor/jquery.cookie.js"></script>        
        <script type="text/javascript" src="js/vendor/bootstrap.min.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/jquery.hammer.min.js?v<?php echo $app_version;?>"></script>    
        <script type="text/javascript" src="js/vendor/jquery.mmenu_4_0_3.min.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/globalize.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/cultures/globalize.culture.en.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/data.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/data_update.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/icons.js?v<?php echo $app_version;?>"></script>
        <script type="text/javascript" src="js/vendor/d3tip.js?v<?php echo $app_version;?>"></script>

        <script>
        
        // Capitalize
        if (!String.prototype.capitalize) {
            String.prototype.capitalize = function(lower) {
                return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
            };
        }

        </script>

        
        <title>The Big Picture</title>
        <style>
            
            .onepop {
                width: 80%;
                height: 46%;
                top: 5%;
                margin: auto;
                background-color: #fff;
                position: relative;
                border-radius: 4px;
            }

            #popupOverlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba( 0,0,0, 0.7 );
                text-align: center;
                z-index: 500;
            }

           
            
        </style>
        
        <script>
            $.cookie("app_version", "<?php echo $app_version;?>" , { path: '/' });
        </script>
    </head>
    
    <body>
        
        <div id='popupOverlay' class="small-screen-warning" style="display:none">
            <div class="onepop">
                <div style="padding:3%;text-align:center">
                    <br>
                    <span data-localize="small_screen_warning"></span>
                </div>  
            </div>
        </div>
        
       
         <div class="loaderPageGraphic">
			<div class="spinner">
			  <div class="bounce1"></div>
			  <div class="bounce2"></div>
			  <div class="bounce3"></div>
			</div>
		</div>
        

        <script>
            setTimeout(function(){
                $(".loaderPageGraphic").css("display", "none");
                $("body").append("<script src=js/graph.js?v<?php echo $app_version;?>/>");
            },3000); 
        </script>
               
 
        <div class="row" style="bottom: 10px;position: absolute;left: 29px;z-index: 10;">
            <div class="row" style="width:50%;background-color: #f0f0f0;height: 47px;margin: auto;"></div>
            <div class="row" style="font-size:14px" data-localize-modal="recession"></div>
        </div>
        
        <div id="parent">

            <div class="header text-center show">

                <div class="row" style="background:#E0E0E0;border-top: 1px solid #BDBDBD;border-bottom: 1px solid #BDBDBD;padding-right:5px;">
                    <div class="col-6">

                        <div class="pull-left" style="margin-top:2px;margin-left:17px;width='80px'">
<!--                            <img class="big-pic-logo" src="graph-gallery/images/bigpicapp-logo.jpg">                           -->
                            <span class="big-pic-logo"></span>
                        </div>

                    </div>


                     <div class="col-6 pull-right" style="margin-right:5px">

      

                        <button type="button" id='bnw' class="btn btn-icon" data-toggle="modal" data-target="#bnw_modal" data-localize="best_n_worst_years"><div class="icon icon_bnw"></div></button>


                        <button type="button" id='bnw' class="btn btn-icont" data-toggle="modal" data-target="#chart_1" data-localize="returns"></button>
                         
                         <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_2" data-localize="time_and_risk"></button>


                        <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_3" data-localize="downturns_and_recoveries"></button>

                        <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_4" data-localize="power_of_compounding"></button>

                        <button type="button" class="btn btn-icon" data-toggle="modal" data-target="#chart_5" data-localize="chasing_returns"></button>

                    </div>
                </div>

            </div>


            <div id="main">


            </div>

            <button type="button" id='info' class="btn btn-icon" data-toggle="modal" data-target="#disclaimer_modal">
                <div class="icon icon_info icon-30 fill-mgray"></div>
            </button>

            <div class="modal fade" id="bnw_modal" tabindex="-1" role="dialog" aria-labelledby="bnw_modal" aria-hidden="true">
                <div class="modal-dialog" style="width:90%!important">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                <div class="icon icon_close icon-44 fill-dblue"></div>
                            </button>
                            <label class="title blue" data-localize="best_n_worst_years"></label>
                        </div>
                        <div class="modal-body">
                            <div class="div-top"></div>
                            <div class="div-bar"></div>
                            <div class="div-bottom"></div>
                            <div class="div-temp">
                                <div class="bar-cont us_stocks" data-localize-modal="us_stocks_short"></div>
                                <div class="bar-cont small_caps" data-localize-modal="small_caps_short"></div>
                                <div class="bar-cont intl_stocks" data-localize-modal="intl_stocks_short"></div>
                                <div class="bar-cont us_bonds" data-localize-modal="us_bonds_short"></div>
                                <div class="bar-cont us_tbills" data-localize-modal="us_tbills_short"></div>
                                <div class="bar-cont growth_portfolio" data-localize-modal="growth_portfolio_short"></div>
                                <div class="bar-cont balanced_portfolio" data-localize-modal="balanced_portfolio_short"></div>
                                <div class="bar-cont income_portfolio" data-localize-modal="income_portfolio_short"></div>
                            </div>
                            <div class='footnote'>
                                <p data-localize="bnw_footnote1"></p>
                                <p data-localize="bnw_footnote2"></p>
                            </div>
                            <div class='bestnote'>
                                <p data-localize="best"></p>
                            </div>
                            <div class='worstnote'>
                                <p data-localize="worst"></p>
                            </div>
                            <div class='example'>
                                <div class="ex-top">
                                    <p class='tag' data-localize="best"></p>
                                    <div class="ex-bar-bar ex-1yr">
                                        <p class="text-muted" data-localize="1yr"></p>
                                    </div>
                                    <div class="ex-bar-bar ex-5yr">
                                        <p class="text-muted" data-localize="5yr"></p>
                                    </div>
                                </div>
                                <div class="ex-bar" data-localize="asset_class" style="padding-top:2px"></div>
                                <div class="ex-bottom">
                                    <p class='tag' data-localize="worst"></p>
                                    <div class="ex-bar-bar ex-1yr">
                                        <p class="text-muted" data-localize="1yr"></p>
                                    </div>
                                    <div class="ex-bar-bar ex-5yr">
                                        <p class="text-muted" data-localize="5yr"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                        <div class="text-center">
                            <p style="font-size:12px" data-localize="disclosure"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="asset_menu" tabindex="-1" role="dialog" aria-labelledby="asset_menu" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title blue" data-localize="assets_to_be_shown"></label>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-xs-4">
                                <label class="title" style="margin-bottom:15px" data-localize="equities"></label>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_stocks"></button>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="small_caps"></button>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="intl_stocks"></button>
                            </div>

                            <div class="col-xs-4">
                                <label class="title" style="margin-bottom:15px" data-localize="fixed_income"></label>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_bonds"></button>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="us_tbills"></button>
                            </div>

                            <div class="col-xs-4">
                                <label class="title" style="margin-bottom:15px" data-localize="model_portfolios"></label>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="growth_portfolio"></button>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="balanced_portfolio"></button>
                                <button type="button" class="btn btn-default btn-lg btn-block btn-asset" value="income_portfolio"></button>
                            </div>
                        </div>



                        <div class="assets-modal-footer text-center" style="margin-top:40px">
                            <span data-localize="max_assets_selected" style="color:#d50000"></span>
                        </div>

                        <div class="row text-center" style="font-size:16px;margin-top:20px">
                            <span class="title" data-localize="select_month_and_date" style="color:#78909C"></span>
                        </div>

                        <div id="birth_date" class="row" style="margin-bottom:10px">
                            <div class="col-xs-6">
                                <select id="month" class="form-control"></select>
                            </div>
                            <div class="col-xs-6">
                                <select id='year' class="form-control"></select>
                            </div>
                        </div>

                        <div class="row text-center" style="margin-top:20px">
                            <label class="title" data-localize="historical_monthly_data" style="color:#78909C"></label>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="chart_1" tabindex="-1" role="dialog" aria-labelledby="chart_1" aria-hidden="true">
            <div class="modal-dialog" style="width:95%!important;padding:0!important">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title" data-localize-modal=""></label>
                    </div>
                    <div class="modal-body">
                        <img style="width:90%" src="" alt="">
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="chart_3" tabindex="-1" role="dialog" aria-labelledby="chart_3" aria-hidden="true">
            <div class="modal-dialog" style="width:80%!important;padding:0!important">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title" data-localize-modal=""></label>
                    </div>
                    <div class="modal-body">
                        <img style="width:99%" src="" alt="">
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="chart_2" tabindex="-1" role="dialog" aria-labelledby="chart_2" aria-hidden="true">
            <div class="modal-dialog" style="width:85%!important;padding:0!important">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title" data-localize-modal=""></label>
                    </div>
                    <div class="modal-body">
                        <img style="width:99%" src="" alt="">
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="chart_5" tabindex="-1" role="dialog" aria-labelledby="chart_5" aria-hidden="true">
            <div class="modal-dialog" style="width:85%!important;padding:0!important">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title" data-localize-modal=""></label>
                    </div>
                    <div class="modal-body">
                        <img style="width:99%" src="" alt="">
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="chart_4" tabindex="-1" role="dialog" aria-labelledby="chart_4" aria-hidden="true">
            <div class="modal-dialog" style="width:85%!important;padding:0!important">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title" data-localize-modal=""></label>
                    </div>
                    <div class="modal-body">
                        <img style="width:99%" src="" alt="">
                    </div>
                </div>
            </div>
        </div>

             <div class="modal fade" id="disclaimer_modal" tabindex="-1" role="dialog" aria-labelledby="disclaimer_modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><div class="icon icon_close icon-44 fill-dblue"></div></button>
                        <label class="title" data-localize-modal=""></label>
                    </div>
                    <div class="modal-body" style="padding:20px;text-align:left!important">
                        <p>
                            <label class="" data-localize="disclaimer_t1"></label>
                        </p>
                        <p>
                            <label class="" data-localize="disclaimer_t2"></label>
                        </p>
                        <p>
                            <label class="" data-localize="disclaimer_t3"></label>
                        </p>
                        <p>
                            <label class="" data-localize="disclaimer_t4"></label>
                        </p>
                    </div>

                </div>
            </div>
        </div>

    </body>
    
</html>

<script>
    

        
    var lang = "en";
    if (lang!=="en" && lang!=="fr") {lang = "en"};
    
    window.localStorage.clear();
    window.localStorage.setItem("lang", "en");

    Globalize.culture(lang);    
    $("[data-localize]").each(function() { $(this).html(Globalize.localize($(this).attr('data-localize'),Globalize.culture())); });

    window.localStorage.setItem("default_assets",["small_caps","us_stocks","intl_stocks","us_bonds","us_tbills","growth_portfolio"]);
    window.localStorage.setItem("default_h_events", "true");
    
    verify_asset_data(true);
    
//    $(".big-pic-logo").attr("src","graph-gallery/images/"+lang+"/big_picture_logo."+((lang=="en")?"svg":"png"));
    if (lang=="en"){
        $(".big-pic-logo").html("<sup>the</sup> Big Picture");
    }else{
        $(".big-pic-logo").html("<sup>le</sup> Portrait Global");
    }
    
        
    function setImageUrl(lang_code){
        
            var baseUrl = "graph-gallery/images/";
        
            var chart1 = $("#chart_1").children().find("img");
            chart1.attr("src", baseUrl + "/returns.jpg?v<?php echo $app_version;?>");        
//            chart1.attr("alt", Globalize.localize("returns_alt",Globalize.culture()));
        
            var chart3 = $("#chart_2").children().find("img");
            chart3.attr("src", baseUrl + "/time_and_risk.jpg?v<?php echo $app_version;?>");

        
            var chart4 = $("#chart_3").children().find("img");
            chart4.attr("src", baseUrl + "/downturns_and_recoveries.jpg?v<?php echo $app_version;?>");

        
            var chart5 = $("#chart_4").children().find("img");
            chart5.attr("src", baseUrl + "/power_of_compounding.jpg?v<?php echo $app_version;?>");
            
        
            var chart6 = $("#chart_5").children().find("img");
            chart6.attr("src", baseUrl + "/chasing_returns.jpg?v<?php echo $app_version;?>");
            
        
    }

    setImageUrl(lang);

    
	// SESSION DATA PERSISTENCE
	var bd_year = parseInt(window.localStorage.getItem('bd_year'));
	var bd_month = parseInt(window.localStorage.getItem('bd_month'));
	var f_name = window.localStorage.getItem("f_name");
	
	if (f_name !== undefined) { $('#about_you #f_name input#first_name').val(f_name) };

	// FILL YEAR FIELD
	// Every year from 1925 to 15 years ago
	var year_offset = 15;
	var latest_date = JSON.parse(window.localStorage.getItem('data'));
	latest_date = latest_date[latest_date.length-1].date;
	var latest_year = parseInt(latest_date.substr(0,4));
	var latest_month = parseInt(latest_date.substr(5,2));
	
	var temp_str = '';
	for (var i = 1926; i <= latest_year - year_offset; i++) {
		if (bd_year !== undefined && i == bd_year) {
			temp_str += "<option value='"+i+"' selected='selected'>"+i+"</option>";
		} else {
			temp_str += "<option value='"+i+"'>"+i+"</option>";
		}
	}
	$('#birth_date #year').html( temp_str );

	var temp_str = '';
	for (var i = 1; i <= 12; i++) {
		if (bd_month !== undefined && i == bd_month) {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
		} else {
			temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
		}
	}
	$('#birth_date #month').html( temp_str );
	$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt(           $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });

	$('#birth_date #year').change(function() {
		var curr_sel_month = $('#birth_date #month').val();
		if (parseInt($(this).val()) == latest_year - year_offset && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= latest_month; i++) {
				if ((curr_sel_month > latest_month && i == latest_month) || (curr_sel_month <= latest_month && i == curr_sel_month)) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		} else if ($('#event_menu .content select#month option').length < 12 && latest_month != 12) {
			var temp_str = '';
			for (var i = 1; i <= 12; i++) {
				if (i == curr_sel_month) {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"' selected='selected'></option>";
				} else {
					temp_str += "<option value='"+i+"' data-localize-month='"+i+"'></option>";
				}
			}
		}
		$('#birth_date #month').html( temp_str );
		$("[data-localize-month]").each(function() { $(this).html( Globalize.culture().calendars.standard.months.names[( parseInt(           $(this).attr('data-localize-month'))-1 )].capitalize(true) ); });
	});
	
	// Hide keyboard fix
	var last_selected = null; 
	$('select').focus(function(){
		if (last_selected.toLowerCase() == 'input') {
			$('input').focus();
			if (last_selected.toLowerCase() !== 'select') {
				last_selected = this.tagName;
				$(this).focus();
			}
		}
	});
	$('input').focus(function () { last_selected = this.tagName; });

	
    window.localStorage.setItem("f_name", $('#about_you #f_name input#first_name').val());
    window.localStorage.setItem("bd_month", $('#birth_date select#month').val());
    window.localStorage.setItem("bd_year", $('#birth_date select#year').val());
    window.localStorage.setItem("temp_lang", lang);
    
    var isMobile = false;
    if ($(document).width() < 900){
        isMobile=true;
    }
    if (isMobile){
        $("#popupOverlay").css("display", "block");
    }
    
    
  

</script>
