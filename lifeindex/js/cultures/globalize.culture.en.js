/*
 * Globalize Culture default (en)
 *
 * http://github.com/jquery/globalize
 *
 * Copyright Software Freedom Conservancy, Inc.
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * This file was generated by the Globalize Culture Generator
 * Translation: bugs found in this file need to be fixed in the generator
 */

(function( window, undefined ) {

var Globalize;

if ( typeof require !== "undefined" &&
	typeof exports !== "undefined" &&
	typeof module !== "undefined" ) {
	// Assume CommonJS
	Globalize = require( "globalize" );
} else {
	// Global variable
	Globalize = window.Globalize;
}
 
Globalize.addCultureInfo("default", {
	messages: {
		"lifeline" : "Big Picture",
        "en_translation_code":"ENG",
        "fr_translation_code":"FRE",
		"get_started": "Get started",
		"cdn_stocks" : "Canadian Stocks",
        "us_bonds_short" : "Bonds",
        "us_bonds" : "Bonds",
        "us_total_equity_mkt" : "U.S. Total Equity Market",
        "us_tbills" : "T-Bills",
        "us_tbills_short": "T-Bills",
		"cdn_stocks_short" : "Cdn. Stocks",
		"us_stocks" : "Large-Cap Stocks",
        "small_caps" : "Small-Cap Stocks",
        "small_caps_short" : "Small-Cap Stocks",
        "us_stocks_short" : "Large-Cap Stocks",
		"us_large_cap_short" : "U.S. Stocks",
        "us_mid_cap" : "U.S. Mid Cap Stocks",
        "us_small_cap" : "U.S. Small Cap Stocks",
        "us_micro_cap" : "U.S. Micro Cap Stocks",
		"intl_stocks" : "International Stocks",
		"intl_stocks_short" : "International Stocks",
        "gold" : "Gold",
        "emerging_mkt_stock" : "Emerging Market Stocks",
        "mg_portfolio":"Maximun Growth Portfolio",
        "growth_portfolio":"Growth Portfolio",
        "bg_portfolio":"Balanced Growth Portfolio",
        "balanced_portfolio":"Balanced Portfolio",
        "bi_portfolio":"Balanced Income Portfolio",
        "income_portfolio":"Income Portfolio",
        "growth_portfolio":"Growth Portfolio",
        "growth_portfolio_short":"Growth Port.",
        "balanced_portfolio":"Balanced Portfolio",
        "balanced_portfolio_short":"Balanced Port.",
        "income_portfolio":"Income Portfolio",  
        "income_portfolio_short":"Income Port.",  
        "customize":"Customize",
        
//        "max_assets_selected":"Max. number of assets. Please deselect one or more before making a new selection.",
        "max_assets_selected":"Up to six asset classes and model portfolios can be selected at a time.",
		"b_portfolio" : "Balanced Portfolio",
        
        "power_of_compounding":"Power of Compounding",
        "chasing_returns":"Chasing Returns",
        "time_and_risk": "Impact of Time",
        "bulls_and_bears": "Bulls & Bears",
        "matrix": "Matrix",
        "diversification": "Diversification",
        "downturns": "Downturns",
        "rebalancing": "Rebalancing",
        "stocks_vs_port": "Stocks vs. Port.",
        "gics": "GICs",    
        "returns": "Returns",
        "key_rates": "Key Rates",        
        "cad_usd_inflation_chart":"<span style='color:#78909C'>Prime Rate</span>&nbsp;/&nbsp;<span style='color:#455A64'>Inflation</span>",
        "exchange_rate_chart":"<span style='color:#26A69A'>USD per CAD</span>",
        "small_screen_warning":"This app is intended for use on larger screens.",
        "inflation_graph_name":"Inflation",
        "downturns_and_recoveries":"Downturns & Recoveries",
        "key_events_graph_name":"Key Events",
        "housing_oild_gold_graph_name":"Housing, Oil, & Gold",
        "risk_and_return_graph_name":"Risk & Return",
        "gic_returns_graph_name":"GIC Returns",        
		"b_portfolio_short" : "Bal. Portfolio",       
		"bonds" : "Government Bonds",
		"bonds_short" : "Bonds",		
		"inflation" : "Inflation",
		"preferences" : "Preferences",
		"asset_classes" : "Asset classes",
		"historial_events" : "Historical events",
		"show" : "Show",
		"hide" : "Hide",
		"language" : "Language",
		"data_update" : "Data Update",
		"check_now" : "Check now",
		"about_you" : "About You",
        "select_start_date": "Select start date",
		"name" : "Name",
		"birth_date" : "Birth date",
		"see_lifeline" : "Next",
		"start_life_in_markets" : "See the Big Picture",
		"personal_events" : "Personal Events",
		"personal_lifeline" : "%s's Big Picture",
		"assets_to_be_shown" : "Select Asset Classes & Model Portfolios",
        "equities" : "Equities",
        "bonds_and_gold" : "Bonds & Gold",
        "fixed_income" : "Fixed Income",
        "model_portfolios" : "Model Portfolios",
        "historical_monthly_data" : "Historical monthly return data, from 1925, supplied by <a target='_blank' href='http://crsp.com/'>CRSP</a> and <a target='_blank' href='https://www.globalfinancialdata.com/'>GFD, Inc.</a>",
        "select_month_and_date" : "Select start date (month and year)",
        
		"add_event" : "Add an event",
		"type_of_event" : "Type of event",
		"event_home" : "Purchased a home",
        "event_marriage" : "Marriage",
        "event_divorce" : "Divorce",
        "event_child" : "Had a child",
        "event_school" : "Started school",
        "event_graduation" : "Graduation",
        "event_purchase" : "Major purchase",
        "event_business" : "Opened a business",
        "event_carrer" : "First/new career",
        "event_canada" : "Arrival in Canada",
        "event_death" : "Death of a spouse",
        "date" : "Date",
        "add" : "Add",
        "events_added" : "Events added",
        "no_events" : "No events have been added to The Big Picture.",
        "best_n_worst_years" : "Best/Worst Years",
        "gics_alt":"",
        
        "cagrs_alt" : "Summary table of historical risk and return by asset class",
        "time_and_risk_alt": "Historical returns by rolling period and asset class",
        "downturns_and_recoveries_alt":"Historical stock market recoveries after major downturns",
        "gics_alt": "Historical GIC returns relative to other asset classes",
        "key_rates_alt":"Historical gold, oil, and housing prices, and key economic rates",
        
        
        "alltime" : "all-time",
        "last_years" : "last %s years",
        "best" : "Best",
        "worst" : "Worst",
        "asset_class" : "Asset Class",
        "1yr" : "1-yr",
        "5yr" : "5-yr",
        "bnw_footnote1" : "1-yr and 5-yr rolling period returns.",
        "bnw_footnote2" : "All returns are compound annual returns.",
        "share" : "Share The Big Picture",
        "share_prompt" : "Please choose a sharing method.",
        "via_device" : "Save to gallery",
        "via_other_app" : "Share menu",
        "image_good" : "The Big Picture has been saved to the images gallery.",
        "image_bad" : "Sorry, there was a problem saving your The Big Picture.",
        "legend" : "Legend",
        "recession": "Recession",
        "no_network" : "Sorry, no network connection is available.",
        "no_connection" : "There was a connection error. It's not possible to update data at this time.",
        "no_update" : "There is no new data available.",
        "yes_update" : "New data has been retrieved and updated.",
        "last_check" : "Last check",
        "ww2" : "World War II", 
		"vietnam_war" : "Vietnam War",
        "oil_crisis":"Oil crisis", 
        "financial_crisis":"Fin. Crisis",
        "brexit":"Brexit",
		"cold_war" : "Cold War ends",
		"nine_eleven" : "9/11",
		"korean_war" : "Korean War",
		"iraq" : "Iraq War",
        "disclosure" : "© 2019 Investments Illustrated, Inc. All Rights Reserved. See disclosures.",
		"disclaimer" : "Disclaimer",        
		"disclaimer_t1" : "<strong>Recessions are marked in grey. International Stocks exclude U.S. Stocks. The Growth Portfolio is composed as follows: 55% U.S. Large Cap Stocks, 25% Bonds, 15% International Stocks, 5% U.S. Small Cap Stocks. The Balanced Portfolio is composed as follows: 55% U.S. Large Cap Stocks, 35% Bonds, 10% T-Bills. The Income Portfolio is composed as follows: 55% Bonds, 25% U.S. Large Cap Stocks, 20% T-Bills.</strong>",
        
		"disclaimer_t2" : "This chart shows the inferred growth of one thousand dollars invested on the selected start date. This chart is for illustrative purposes only; it does not constitute investment advice and must not be relied on as such. It assumes the reinvestment of all income and no transaction costs or taxes. The portfolios shown are neither real, nor recommended. They were rebalanced each January. Risk is measured by the standard deviation (volatility) of annual returns. All returns are compound annual returns. An investment cannot be made directly in an index. Government bonds and Treasury bills are guaranteed by the full faith and credit of the United States government as to the timely payment of principal and interest, while stocks are not guaranteed and have been more volatile than the other asset classes shown. International stocks involve special risks such as fluctuations in currency, foreign taxation, economic and political risks, liquidity risks, and differences in accounting and financial standards.",
        
		"disclaimer_t3" : "Sources: U.S. Stocks: S&P 500® Total Return index, U.S. Small Cap Stocks: NYSE/NYSEMkt,NASDAQ Small Cap Index, Treasury Bills: CRSP 90-Day T-Bill Returns—Center for Research in Security Prices (CRSP). International Stocks: ex-U.S.A. Total Return Index, Bonds: USA 10-year Government Bond Total Return Index, exchange rates—Global Financial Data, Inc. Inflation: Consumer Price Index—U.S. Bureau of Labor Statistics.",
        
        "disclaimer_t4" : "The reproduction of part or all of this publication without prior written consent from Investments Illustrated, Inc. is prohibited. The Big Picture, and the Investments Illustrated name and logo, are registered trademarks. Past performance is not an indicator of future performance. Diversification neither assures a profit nor eliminates the risk of experiencing investment losses. © Investments Illustrated, Inc. and Advisor Perspectives, Inc. All Rights Reserved."
	}
});

}( this ));
